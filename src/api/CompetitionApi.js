/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Customer', 'model/CompetitionCheckout', 'model/CompetitionCheckoutData', 'model/Competition', 'model/CompetitionRedeemData', 'model/CompetitionPurchase', 'model/CompetitionAnswerData', 'model/Venue'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/Customer'), require('../model/CompetitionCheckout'), require('../model/CompetitionCheckoutData'), require('../model/Competition'), require('../model/CompetitionRedeemData'), require('../model/CompetitionPurchase'), require('../model/CompetitionAnswerData'), require('../model/Venue'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.CompetitionApi = factory(root.harpoonApi.ApiClient, root.harpoonApi.Customer, root.harpoonApi.CompetitionCheckout, root.harpoonApi.CompetitionCheckoutData, root.harpoonApi.Competition, root.harpoonApi.CompetitionRedeemData, root.harpoonApi.CompetitionPurchase, root.harpoonApi.CompetitionAnswerData, root.harpoonApi.Venue);
  }
}(this, function(ApiClient, Customer, CompetitionCheckout, CompetitionCheckoutData, Competition, CompetitionRedeemData, CompetitionPurchase, CompetitionAnswerData, Venue) {
  'use strict';

  /**
   * Competition service.
   * @module api/CompetitionApi
   * @version 1.2.30
   */

  /**
   * Constructs a new CompetitionApi. 
   * @alias module:api/CompetitionApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields and include
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/Customer>}
     */
    this.competitionAttendees = function(id, opts) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionAttendees";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = [Customer];

      return this.apiClient.callApi(
        '/Competitions/{id}/attendees', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {module:model/CompetitionCheckoutData} opts.data 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/CompetitionCheckout}
     */
    this.competitionCheckout = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionCheckout";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = CompetitionCheckout;

      return this.apiClient.callApi(
        '/Competitions/{id}/checkout', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find all instances of the model matched by filter from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields, where, include, order, offset, and limit
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/Competition>}
     */
    this.competitionFind = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = [Competition];

      return this.apiClient.callApi(
        '/Competitions', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find a model instance by {{id}} from the data source.
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields and include
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Competition}
     */
    this.competitionFindById = function(id, opts) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionFindById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Competition;

      return this.apiClient.callApi(
        '/Competitions/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find first instance of the model matched by filter from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields, where, include, order, offset, and limit
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Competition}
     */
    this.competitionFindOne = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Competition;

      return this.apiClient.callApi(
        '/Competitions/findOne', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {module:model/CompetitionRedeemData} opts.data 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/CompetitionPurchase}
     */
    this.competitionRedeem = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionRedeem";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = CompetitionPurchase;

      return this.apiClient.callApi(
        '/Competitions/{id}/redeem', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Replace attributes for a model instance and persist it into the data source.
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {module:model/Competition} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Competition}
     */
    this.competitionReplaceById = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionReplaceById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Competition;

      return this.apiClient.callApi(
        '/Competitions/{id}/replace', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Replace an existing model instance or insert a new one into the data source.
     * @param {Object} opts Optional parameters
     * @param {module:model/Competition} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Competition}
     */
    this.competitionReplaceOrCreate = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Competition;

      return this.apiClient.callApi(
        '/Competitions/replaceOrCreate', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Update an existing model instance or insert a new one into the data source based on the where criteria.
     * @param {Object} opts Optional parameters
     * @param {String} opts.where Criteria to match model instances
     * @param {module:model/Competition} opts.data An object of model property name/value pairs
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Competition}
     */
    this.competitionUpsertWithWhere = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
        'where': opts['where']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Competition;

      return this.apiClient.callApi(
        '/Competitions/upsertWithWhere', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * @param {String} id Model id
     * @param {module:model/CompetitionAnswerData} answer Competition Answer
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Object}
     */
    this.competitionValidateAnswer = function(id, answer) {
      var postBody = answer;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionValidateAnswer";
      }

      // verify the required parameter 'answer' is set
      if (answer == undefined || answer == null) {
        throw "Missing the required parameter 'answer' when calling competitionValidateAnswer";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Object;

      return this.apiClient.callApi(
        '/Competitions/{id}/validateAnswer', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields and include
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/Venue>}
     */
    this.competitionVenues = function(id, opts) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling competitionVenues";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = [Venue];

      return this.apiClient.callApi(
        '/Competitions/{id}/venues', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }
  };

  return exports;
}));

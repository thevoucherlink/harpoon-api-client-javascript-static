/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/InlineResponse2001', 'model/AwEventbookingTicket', 'model/InlineResponse2003', 'model/InlineResponse2002'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/InlineResponse2001'), require('../model/AwEventbookingTicket'), require('../model/InlineResponse2003'), require('../model/InlineResponse2002'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.AwEventbookingTicketApi = factory(root.harpoonApi.ApiClient, root.harpoonApi.InlineResponse2001, root.harpoonApi.AwEventbookingTicket, root.harpoonApi.InlineResponse2003, root.harpoonApi.InlineResponse2002);
  }
}(this, function(ApiClient, InlineResponse2001, AwEventbookingTicket, InlineResponse2003, InlineResponse2002) {
  'use strict';

  /**
   * AwEventbookingTicket service.
   * @module api/AwEventbookingTicketApi
   * @version 1.2.30
   */

  /**
   * Constructs a new AwEventbookingTicketApi. 
   * @alias module:api/AwEventbookingTicketApi
   * @class
   * @param {module:ApiClient} apiClient Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Count instances of the model matched by where from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.where Criteria to match model instances
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/InlineResponse2001}
     */
    this.awEventbookingTicketCount = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'where': opts['where']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = InlineResponse2001;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/count', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Create a new instance of the model and persist it into the data source.
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketCreate = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Create a change stream.
     * @param {Object} opts Optional parameters
     * @param {String} opts.options 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link File}
     */
    this.awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'options': opts['options']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = File;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/change-stream', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Create a change stream.
     * @param {Object} opts Optional parameters
     * @param {String} opts.options 
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link File}
     */
    this.awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
        'options': opts['options']
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = File;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/change-stream', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Delete a model instance by {{id}} from the data source.
     * @param {String} id Model id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Object}
     */
    this.awEventbookingTicketDeleteById = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketDeleteById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = Object;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}', 'DELETE',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Check whether a model instance exists in the data source.
     * @param {String} id Model id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/InlineResponse2003}
     */
    this.awEventbookingTicketExistsGetAwEventbookingTicketsidExists = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketExistsGetAwEventbookingTicketsidExists";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = InlineResponse2003;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}/exists', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Check whether a model instance exists in the data source.
     * @param {String} id Model id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/InlineResponse2003}
     */
    this.awEventbookingTicketExistsHeadAwEventbookingTicketsid = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketExistsHeadAwEventbookingTicketsid";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = InlineResponse2003;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}', 'HEAD',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find all instances of the model matched by filter from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields, where, include, order, offset, and limit
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link Array.<module:model/AwEventbookingTicket>}
     */
    this.awEventbookingTicketFind = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = [AwEventbookingTicket];

      return this.apiClient.callApi(
        '/AwEventbookingTickets', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find a model instance by {{id}} from the data source.
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields and include
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketFindById = function(id, opts) {
      opts = opts || {};
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketFindById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Find first instance of the model matched by filter from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.filter Filter defining fields, where, include, order, offset, and limit
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketFindOne = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'filter': opts['filter']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/findOne', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Patch attributes for a model instance and persist it into the data source.
     * @param {String} id AwEventbookingTicket id
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data An object of model property name/value pairs
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}', 'PATCH',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Patch attributes for a model instance and persist it into the data source.
     * @param {String} id AwEventbookingTicket id
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data An object of model property name/value pairs
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Replace attributes for a model instance and persist it into the data source.
     * @param {String} id Model id
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketReplaceById = function(id, opts) {
      opts = opts || {};
      var postBody = opts['data'];

      // verify the required parameter 'id' is set
      if (id == undefined || id == null) {
        throw "Missing the required parameter 'id' when calling awEventbookingTicketReplaceById";
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/{id}/replace', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Replace an existing model instance or insert a new one into the data source.
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketReplaceOrCreate = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/replaceOrCreate', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Update instances of the model matched by {{where}} from the data source.
     * @param {Object} opts Optional parameters
     * @param {String} opts.where Criteria to match model instances
     * @param {module:model/AwEventbookingTicket} opts.data An object of model property name/value pairs
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/InlineResponse2002}
     */
    this.awEventbookingTicketUpdateAll = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
        'where': opts['where']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = InlineResponse2002;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/update', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Patch an existing model instance or insert a new one into the data source.
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketUpsertPatchAwEventbookingTickets = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets', 'PATCH',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Patch an existing model instance or insert a new one into the data source.
     * @param {Object} opts Optional parameters
     * @param {module:model/AwEventbookingTicket} opts.data Model instance data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketUpsertPutAwEventbookingTickets = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }


    /**
     * Update an existing model instance or insert a new one into the data source based on the where criteria.
     * @param {Object} opts Optional parameters
     * @param {String} opts.where Criteria to match model instances
     * @param {module:model/AwEventbookingTicket} opts.data An object of model property name/value pairs
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/AwEventbookingTicket}
     */
    this.awEventbookingTicketUpsertWithWhere = function(opts) {
      opts = opts || {};
      var postBody = opts['data'];


      var pathParams = {
      };
      var queryParams = {
        'where': opts['where']
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['access_token'];
      var contentTypes = ['application/json', 'application/x-www-form-urlencoded', 'application/xml', 'text/xml'];
      var accepts = ['application/json', 'application/xml', 'text/xml', 'application/javascript', 'text/javascript'];
      var returnType = AwEventbookingTicket;

      return this.apiClient.callApi(
        '/AwEventbookingTickets/upsertWithWhere', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }
  };

  return exports;
}));

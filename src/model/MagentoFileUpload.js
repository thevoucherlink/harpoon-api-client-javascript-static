/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.MagentoFileUpload = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The MagentoFileUpload model module.
   * @module model/MagentoFileUpload
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>MagentoFileUpload</code>.
   * @alias module:model/MagentoFileUpload
   * @class
   * @param file {String} File contents encoded as base64
   */
  var exports = function(file) {
    var _this = this;

    _this['file'] = file;


  };

  /**
   * Constructs a <code>MagentoFileUpload</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/MagentoFileUpload} obj Optional instance to populate.
   * @return {module:model/MagentoFileUpload} The populated <code>MagentoFileUpload</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('file')) {
        obj['file'] = ApiClient.convertToType(data['file'], 'String');
      }
      if (data.hasOwnProperty('contentType')) {
        obj['contentType'] = ApiClient.convertToType(data['contentType'], 'String');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
    }
    return obj;
  }

  /**
   * File contents encoded as base64
   * @member {String} file
   */
  exports.prototype['file'] = undefined;
  /**
   * The file's encode type e.g application/json
   * @member {String} contentType
   */
  exports.prototype['contentType'] = undefined;
  /**
   * the file name including the extension
   * @member {String} name
   */
  exports.prototype['name'] = undefined;



  return exports;
}));



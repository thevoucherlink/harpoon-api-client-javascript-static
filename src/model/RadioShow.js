/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/Contact', 'model/RadioShowTime'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./Contact'), require('./RadioShowTime'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.RadioShow = factory(root.harpoonApi.ApiClient, root.harpoonApi.Contact, root.harpoonApi.RadioShowTime);
  }
}(this, function(ApiClient, Contact, RadioShowTime) {
  'use strict';




  /**
   * The RadioShow model module.
   * @module model/RadioShow
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>RadioShow</code>.
   * @alias module:model/RadioShow
   * @class
   * @param name {String} 
   */
  var exports = function(name) {
    var _this = this;

    _this['name'] = name;

















  };

  /**
   * Constructs a <code>RadioShow</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RadioShow} obj Optional instance to populate.
   * @return {module:model/RadioShow} The populated <code>RadioShow</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('content')) {
        obj['content'] = ApiClient.convertToType(data['content'], 'String');
      }
      if (data.hasOwnProperty('contentType')) {
        obj['contentType'] = ApiClient.convertToType(data['contentType'], 'String');
      }
      if (data.hasOwnProperty('contact')) {
        obj['contact'] = Contact.constructFromObject(data['contact']);
      }
      if (data.hasOwnProperty('starts')) {
        obj['starts'] = ApiClient.convertToType(data['starts'], 'Date');
      }
      if (data.hasOwnProperty('ends')) {
        obj['ends'] = ApiClient.convertToType(data['ends'], 'Date');
      }
      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
      if (data.hasOwnProperty('radioShowTimeCurrent')) {
        obj['radioShowTimeCurrent'] = RadioShowTime.constructFromObject(data['radioShowTimeCurrent']);
      }
      if (data.hasOwnProperty('imgShow')) {
        obj['imgShow'] = ApiClient.convertToType(data['imgShow'], 'String');
      }
      if (data.hasOwnProperty('sponsorTrack')) {
        obj['sponsorTrack'] = ApiClient.convertToType(data['sponsorTrack'], 'String');
      }
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('radioStreamId')) {
        obj['radioStreamId'] = ApiClient.convertToType(data['radioStreamId'], 'Number');
      }
      if (data.hasOwnProperty('playlistItemId')) {
        obj['playlistItemId'] = ApiClient.convertToType(data['playlistItemId'], 'Number');
      }
      if (data.hasOwnProperty('radioPresenters')) {
        obj['radioPresenters'] = ApiClient.convertToType(data['radioPresenters'], [Object]);
      }
      if (data.hasOwnProperty('radioStream')) {
        obj['radioStream'] = ApiClient.convertToType(data['radioStream'], Object);
      }
      if (data.hasOwnProperty('radioShowTimes')) {
        obj['radioShowTimes'] = ApiClient.convertToType(data['radioShowTimes'], [Object]);
      }
      if (data.hasOwnProperty('playlistItem')) {
        obj['playlistItem'] = ApiClient.convertToType(data['playlistItem'], Object);
      }
    }
    return obj;
  }

  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} description
   */
  exports.prototype['description'] = undefined;
  /**
   * @member {String} content
   */
  exports.prototype['content'] = undefined;
  /**
   * @member {module:model/RadioShow.ContentTypeEnum} contentType
   * @default 'image'
   */
  exports.prototype['contentType'] = 'image';
  /**
   * Contacts for this show
   * @member {module:model/Contact} contact
   */
  exports.prototype['contact'] = undefined;
  /**
   * When the Show starts of being public
   * @member {Date} starts
   */
  exports.prototype['starts'] = undefined;
  /**
   * When the Show ceases of being public
   * @member {Date} ends
   */
  exports.prototype['ends'] = undefined;
  /**
   * @member {String} type
   */
  exports.prototype['type'] = undefined;
  /**
   * @member {module:model/RadioShowTime} radioShowTimeCurrent
   */
  exports.prototype['radioShowTimeCurrent'] = undefined;
  /**
   * @member {String} imgShow
   */
  exports.prototype['imgShow'] = undefined;
  /**
   * Url of the sponsor MP3 to be played
   * @member {String} sponsorTrack
   */
  exports.prototype['sponsorTrack'] = undefined;
  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Number} radioStreamId
   */
  exports.prototype['radioStreamId'] = undefined;
  /**
   * @member {Number} playlistItemId
   */
  exports.prototype['playlistItemId'] = undefined;
  /**
   * @member {Array.<Object>} radioPresenters
   */
  exports.prototype['radioPresenters'] = undefined;
  /**
   * @member {Object} radioStream
   */
  exports.prototype['radioStream'] = undefined;
  /**
   * @member {Array.<Object>} radioShowTimes
   */
  exports.prototype['radioShowTimes'] = undefined;
  /**
   * @member {Object} playlistItem
   */
  exports.prototype['playlistItem'] = undefined;


  /**
   * Allowed values for the <code>contentType</code> property.
   * @enum {String}
   * @readonly
   */
  exports.ContentTypeEnum = {
    /**
     * value: "image"
     * @const
     */
    "image": "image",
    /**
     * value: "video"
     * @const
     */
    "video": "video"  };


  return exports;
}));



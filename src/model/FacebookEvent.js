/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.FacebookEvent = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The FacebookEvent model module.
   * @module model/FacebookEvent
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>FacebookEvent</code>.
   * @alias module:model/FacebookEvent
   * @class
   */
  var exports = function() {
    var _this = this;









  };

  /**
   * Constructs a <code>FacebookEvent</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/FacebookEvent} obj Optional instance to populate.
   * @return {module:model/FacebookEvent} The populated <code>FacebookEvent</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('node')) {
        obj['node'] = ApiClient.convertToType(data['node'], 'String');
      }
      if (data.hasOwnProperty('category')) {
        obj['category'] = ApiClient.convertToType(data['category'], 'String');
      }
      if (data.hasOwnProperty('updatedTime')) {
        obj['updatedTime'] = ApiClient.convertToType(data['updatedTime'], 'String');
      }
      if (data.hasOwnProperty('maybeCount')) {
        obj['maybeCount'] = ApiClient.convertToType(data['maybeCount'], 'Number');
      }
      if (data.hasOwnProperty('noreplyCount')) {
        obj['noreplyCount'] = ApiClient.convertToType(data['noreplyCount'], 'Number');
      }
      if (data.hasOwnProperty('attendingCount')) {
        obj['attendingCount'] = ApiClient.convertToType(data['attendingCount'], 'Number');
      }
      if (data.hasOwnProperty('place')) {
        obj['place'] = ApiClient.convertToType(data['place'], Object);
      }
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {String} node
   */
  exports.prototype['node'] = undefined;
  /**
   * @member {String} category
   */
  exports.prototype['category'] = undefined;
  /**
   * @member {String} updatedTime
   */
  exports.prototype['updatedTime'] = undefined;
  /**
   * @member {Number} maybeCount
   * @default 0.0
   */
  exports.prototype['maybeCount'] = 0.0;
  /**
   * @member {Number} noreplyCount
   * @default 0.0
   */
  exports.prototype['noreplyCount'] = 0.0;
  /**
   * @member {Number} attendingCount
   * @default 0.0
   */
  exports.prototype['attendingCount'] = 0.0;
  /**
   * @member {Object} place
   */
  exports.prototype['place'] = undefined;
  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;



  return exports;
}));



/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.AwCollpurDeal = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The AwCollpurDeal model module.
   * @module model/AwCollpurDeal
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>AwCollpurDeal</code>.
   * @alias module:model/AwCollpurDeal
   * @class
   * @param id {Number} 
   * @param productId {Number} 
   * @param productName {String} 
   * @param storeIds {String} 
   * @param isActive {Number} 
   * @param isSuccess {Number} 
   * @param closeState {Number} 
   * @param qtyToReachDeal {Number} 
   * @param purchasesLeft {Number} 
   * @param maximumAllowedPurchases {Number} 
   * @param price {String} 
   * @param autoClose {Number} 
   * @param name {String} 
   * @param description {String} 
   * @param dealImage {String} 
   * @param enableCoupons {Number} 
   * @param couponPrefix {String} 
   */
  var exports = function(id, productId, productName, storeIds, isActive, isSuccess, closeState, qtyToReachDeal, purchasesLeft, maximumAllowedPurchases, price, autoClose, name, description, dealImage, enableCoupons, couponPrefix) {
    var _this = this;

    _this['id'] = id;
    _this['productId'] = productId;
    _this['productName'] = productName;
    _this['storeIds'] = storeIds;
    _this['isActive'] = isActive;
    _this['isSuccess'] = isSuccess;
    _this['closeState'] = closeState;
    _this['qtyToReachDeal'] = qtyToReachDeal;
    _this['purchasesLeft'] = purchasesLeft;
    _this['maximumAllowedPurchases'] = maximumAllowedPurchases;


    _this['price'] = price;
    _this['autoClose'] = autoClose;
    _this['name'] = name;
    _this['description'] = description;

    _this['dealImage'] = dealImage;

    _this['enableCoupons'] = enableCoupons;
    _this['couponPrefix'] = couponPrefix;





  };

  /**
   * Constructs a <code>AwCollpurDeal</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/AwCollpurDeal} obj Optional instance to populate.
   * @return {module:model/AwCollpurDeal} The populated <code>AwCollpurDeal</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('productId')) {
        obj['productId'] = ApiClient.convertToType(data['productId'], 'Number');
      }
      if (data.hasOwnProperty('productName')) {
        obj['productName'] = ApiClient.convertToType(data['productName'], 'String');
      }
      if (data.hasOwnProperty('storeIds')) {
        obj['storeIds'] = ApiClient.convertToType(data['storeIds'], 'String');
      }
      if (data.hasOwnProperty('isActive')) {
        obj['isActive'] = ApiClient.convertToType(data['isActive'], 'Number');
      }
      if (data.hasOwnProperty('isSuccess')) {
        obj['isSuccess'] = ApiClient.convertToType(data['isSuccess'], 'Number');
      }
      if (data.hasOwnProperty('closeState')) {
        obj['closeState'] = ApiClient.convertToType(data['closeState'], 'Number');
      }
      if (data.hasOwnProperty('qtyToReachDeal')) {
        obj['qtyToReachDeal'] = ApiClient.convertToType(data['qtyToReachDeal'], 'Number');
      }
      if (data.hasOwnProperty('purchasesLeft')) {
        obj['purchasesLeft'] = ApiClient.convertToType(data['purchasesLeft'], 'Number');
      }
      if (data.hasOwnProperty('maximumAllowedPurchases')) {
        obj['maximumAllowedPurchases'] = ApiClient.convertToType(data['maximumAllowedPurchases'], 'Number');
      }
      if (data.hasOwnProperty('availableFrom')) {
        obj['availableFrom'] = ApiClient.convertToType(data['availableFrom'], 'Date');
      }
      if (data.hasOwnProperty('availableTo')) {
        obj['availableTo'] = ApiClient.convertToType(data['availableTo'], 'Date');
      }
      if (data.hasOwnProperty('price')) {
        obj['price'] = ApiClient.convertToType(data['price'], 'String');
      }
      if (data.hasOwnProperty('autoClose')) {
        obj['autoClose'] = ApiClient.convertToType(data['autoClose'], 'Number');
      }
      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('description')) {
        obj['description'] = ApiClient.convertToType(data['description'], 'String');
      }
      if (data.hasOwnProperty('fullDescription')) {
        obj['fullDescription'] = ApiClient.convertToType(data['fullDescription'], 'String');
      }
      if (data.hasOwnProperty('dealImage')) {
        obj['dealImage'] = ApiClient.convertToType(data['dealImage'], 'String');
      }
      if (data.hasOwnProperty('isFeatured')) {
        obj['isFeatured'] = ApiClient.convertToType(data['isFeatured'], 'Number');
      }
      if (data.hasOwnProperty('enableCoupons')) {
        obj['enableCoupons'] = ApiClient.convertToType(data['enableCoupons'], 'Number');
      }
      if (data.hasOwnProperty('couponPrefix')) {
        obj['couponPrefix'] = ApiClient.convertToType(data['couponPrefix'], 'String');
      }
      if (data.hasOwnProperty('couponExpireAfterDays')) {
        obj['couponExpireAfterDays'] = ApiClient.convertToType(data['couponExpireAfterDays'], 'Number');
      }
      if (data.hasOwnProperty('expiredFlag')) {
        obj['expiredFlag'] = ApiClient.convertToType(data['expiredFlag'], 'Number');
      }
      if (data.hasOwnProperty('sentBeforeFlag')) {
        obj['sentBeforeFlag'] = ApiClient.convertToType(data['sentBeforeFlag'], 'Number');
      }
      if (data.hasOwnProperty('isSuccessedFlag')) {
        obj['isSuccessedFlag'] = ApiClient.convertToType(data['isSuccessedFlag'], 'Number');
      }
      if (data.hasOwnProperty('awCollpurDealPurchases')) {
        obj['awCollpurDealPurchases'] = ApiClient.convertToType(data['awCollpurDealPurchases'], [Object]);
      }
    }
    return obj;
  }

  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Number} productId
   */
  exports.prototype['productId'] = undefined;
  /**
   * @member {String} productName
   */
  exports.prototype['productName'] = undefined;
  /**
   * @member {String} storeIds
   */
  exports.prototype['storeIds'] = undefined;
  /**
   * @member {Number} isActive
   */
  exports.prototype['isActive'] = undefined;
  /**
   * @member {Number} isSuccess
   */
  exports.prototype['isSuccess'] = undefined;
  /**
   * @member {Number} closeState
   */
  exports.prototype['closeState'] = undefined;
  /**
   * @member {Number} qtyToReachDeal
   */
  exports.prototype['qtyToReachDeal'] = undefined;
  /**
   * @member {Number} purchasesLeft
   */
  exports.prototype['purchasesLeft'] = undefined;
  /**
   * @member {Number} maximumAllowedPurchases
   */
  exports.prototype['maximumAllowedPurchases'] = undefined;
  /**
   * @member {Date} availableFrom
   */
  exports.prototype['availableFrom'] = undefined;
  /**
   * @member {Date} availableTo
   */
  exports.prototype['availableTo'] = undefined;
  /**
   * @member {String} price
   */
  exports.prototype['price'] = undefined;
  /**
   * @member {Number} autoClose
   */
  exports.prototype['autoClose'] = undefined;
  /**
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * @member {String} description
   */
  exports.prototype['description'] = undefined;
  /**
   * @member {String} fullDescription
   */
  exports.prototype['fullDescription'] = undefined;
  /**
   * @member {String} dealImage
   */
  exports.prototype['dealImage'] = undefined;
  /**
   * @member {Number} isFeatured
   */
  exports.prototype['isFeatured'] = undefined;
  /**
   * @member {Number} enableCoupons
   */
  exports.prototype['enableCoupons'] = undefined;
  /**
   * @member {String} couponPrefix
   */
  exports.prototype['couponPrefix'] = undefined;
  /**
   * @member {Number} couponExpireAfterDays
   */
  exports.prototype['couponExpireAfterDays'] = undefined;
  /**
   * @member {Number} expiredFlag
   */
  exports.prototype['expiredFlag'] = undefined;
  /**
   * @member {Number} sentBeforeFlag
   */
  exports.prototype['sentBeforeFlag'] = undefined;
  /**
   * @member {Number} isSuccessedFlag
   */
  exports.prototype['isSuccessedFlag'] = undefined;
  /**
   * @member {Array.<Object>} awCollpurDealPurchases
   */
  exports.prototype['awCollpurDealPurchases'] = undefined;



  return exports;
}));



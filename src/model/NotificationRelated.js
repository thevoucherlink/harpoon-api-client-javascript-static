/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.NotificationRelated = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The NotificationRelated model module.
   * @module model/NotificationRelated
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>NotificationRelated</code>.
   * @alias module:model/NotificationRelated
   * @class
   */
  var exports = function() {
    var _this = this;










  };

  /**
   * Constructs a <code>NotificationRelated</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/NotificationRelated} obj Optional instance to populate.
   * @return {module:model/NotificationRelated} The populated <code>NotificationRelated</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('customerId')) {
        obj['customerId'] = ApiClient.convertToType(data['customerId'], 'Number');
      }
      if (data.hasOwnProperty('brandId')) {
        obj['brandId'] = ApiClient.convertToType(data['brandId'], 'Number');
      }
      if (data.hasOwnProperty('venueId')) {
        obj['venueId'] = ApiClient.convertToType(data['venueId'], 'Number');
      }
      if (data.hasOwnProperty('competitionId')) {
        obj['competitionId'] = ApiClient.convertToType(data['competitionId'], 'Number');
      }
      if (data.hasOwnProperty('couponId')) {
        obj['couponId'] = ApiClient.convertToType(data['couponId'], 'Number');
      }
      if (data.hasOwnProperty('eventId')) {
        obj['eventId'] = ApiClient.convertToType(data['eventId'], 'Number');
      }
      if (data.hasOwnProperty('dealPaidId')) {
        obj['dealPaidId'] = ApiClient.convertToType(data['dealPaidId'], 'Number');
      }
      if (data.hasOwnProperty('dealGroupId')) {
        obj['dealGroupId'] = ApiClient.convertToType(data['dealGroupId'], 'Number');
      }
      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
    }
    return obj;
  }

  /**
   * @member {Number} customerId
   * @default 0.0
   */
  exports.prototype['customerId'] = 0.0;
  /**
   * @member {Number} brandId
   * @default 0.0
   */
  exports.prototype['brandId'] = 0.0;
  /**
   * @member {Number} venueId
   * @default 0.0
   */
  exports.prototype['venueId'] = 0.0;
  /**
   * @member {Number} competitionId
   * @default 0.0
   */
  exports.prototype['competitionId'] = 0.0;
  /**
   * @member {Number} couponId
   * @default 0.0
   */
  exports.prototype['couponId'] = 0.0;
  /**
   * @member {Number} eventId
   * @default 0.0
   */
  exports.prototype['eventId'] = 0.0;
  /**
   * @member {Number} dealPaidId
   * @default 0.0
   */
  exports.prototype['dealPaidId'] = 0.0;
  /**
   * @member {Number} dealGroupId
   * @default 0.0
   */
  exports.prototype['dealGroupId'] = 0.0;
  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;



  return exports;
}));



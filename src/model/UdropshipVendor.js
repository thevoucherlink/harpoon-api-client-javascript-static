/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.UdropshipVendor = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The UdropshipVendor model module.
   * @module model/UdropshipVendor
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>UdropshipVendor</code>.
   * @alias module:model/UdropshipVendor
   * @class
   * @param id {Number} 
   * @param vendorName {String} 
   * @param vendorAttn {String} 
   * @param email {String} 
   * @param street {String} 
   * @param city {String} 
   * @param countryId {String} 
   * @param status {Boolean} 
   * @param notifyNewOrder {Number} 
   * @param labelType {String} 
   * @param testMode {Number} 
   * @param handlingFee {String} 
   * @param billingUseShipping {Number} 
   * @param billingEmail {String} 
   * @param billingVendorAttn {String} 
   * @param billingStreet {String} 
   * @param billingCity {String} 
   * @param billingCountryId {String} 
   * @param subdomainLevel {Number} 
   * @param updateStoreBaseUrl {Number} 
   */
  var exports = function(id, vendorName, vendorAttn, email, street, city, countryId, status, notifyNewOrder, labelType, testMode, handlingFee, billingUseShipping, billingEmail, billingVendorAttn, billingStreet, billingCity, billingCountryId, subdomainLevel, updateStoreBaseUrl) {
    var _this = this;

    _this['id'] = id;
    _this['vendorName'] = vendorName;
    _this['vendorAttn'] = vendorAttn;
    _this['email'] = email;
    _this['street'] = street;
    _this['city'] = city;

    _this['countryId'] = countryId;




    _this['status'] = status;




    _this['notifyNewOrder'] = notifyNewOrder;
    _this['labelType'] = labelType;
    _this['testMode'] = testMode;
    _this['handlingFee'] = handlingFee;

















    _this['billingUseShipping'] = billingUseShipping;
    _this['billingEmail'] = billingEmail;


    _this['billingVendorAttn'] = billingVendorAttn;
    _this['billingStreet'] = billingStreet;
    _this['billingCity'] = billingCity;

    _this['billingCountryId'] = billingCountryId;


    _this['subdomainLevel'] = subdomainLevel;
    _this['updateStoreBaseUrl'] = updateStoreBaseUrl;

































  };

  /**
   * Constructs a <code>UdropshipVendor</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/UdropshipVendor} obj Optional instance to populate.
   * @return {module:model/UdropshipVendor} The populated <code>UdropshipVendor</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('vendorName')) {
        obj['vendorName'] = ApiClient.convertToType(data['vendorName'], 'String');
      }
      if (data.hasOwnProperty('vendorAttn')) {
        obj['vendorAttn'] = ApiClient.convertToType(data['vendorAttn'], 'String');
      }
      if (data.hasOwnProperty('email')) {
        obj['email'] = ApiClient.convertToType(data['email'], 'String');
      }
      if (data.hasOwnProperty('street')) {
        obj['street'] = ApiClient.convertToType(data['street'], 'String');
      }
      if (data.hasOwnProperty('city')) {
        obj['city'] = ApiClient.convertToType(data['city'], 'String');
      }
      if (data.hasOwnProperty('zip')) {
        obj['zip'] = ApiClient.convertToType(data['zip'], 'String');
      }
      if (data.hasOwnProperty('countryId')) {
        obj['countryId'] = ApiClient.convertToType(data['countryId'], 'String');
      }
      if (data.hasOwnProperty('regionId')) {
        obj['regionId'] = ApiClient.convertToType(data['regionId'], 'Number');
      }
      if (data.hasOwnProperty('region')) {
        obj['region'] = ApiClient.convertToType(data['region'], 'String');
      }
      if (data.hasOwnProperty('telephone')) {
        obj['telephone'] = ApiClient.convertToType(data['telephone'], 'String');
      }
      if (data.hasOwnProperty('fax')) {
        obj['fax'] = ApiClient.convertToType(data['fax'], 'String');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'Boolean');
      }
      if (data.hasOwnProperty('password')) {
        obj['password'] = ApiClient.convertToType(data['password'], 'String');
      }
      if (data.hasOwnProperty('passwordHash')) {
        obj['passwordHash'] = ApiClient.convertToType(data['passwordHash'], 'String');
      }
      if (data.hasOwnProperty('passwordEnc')) {
        obj['passwordEnc'] = ApiClient.convertToType(data['passwordEnc'], 'String');
      }
      if (data.hasOwnProperty('carrierCode')) {
        obj['carrierCode'] = ApiClient.convertToType(data['carrierCode'], 'String');
      }
      if (data.hasOwnProperty('notifyNewOrder')) {
        obj['notifyNewOrder'] = ApiClient.convertToType(data['notifyNewOrder'], 'Number');
      }
      if (data.hasOwnProperty('labelType')) {
        obj['labelType'] = ApiClient.convertToType(data['labelType'], 'String');
      }
      if (data.hasOwnProperty('testMode')) {
        obj['testMode'] = ApiClient.convertToType(data['testMode'], 'Number');
      }
      if (data.hasOwnProperty('handlingFee')) {
        obj['handlingFee'] = ApiClient.convertToType(data['handlingFee'], 'String');
      }
      if (data.hasOwnProperty('upsShipperNumber')) {
        obj['upsShipperNumber'] = ApiClient.convertToType(data['upsShipperNumber'], 'String');
      }
      if (data.hasOwnProperty('customDataCombined')) {
        obj['customDataCombined'] = ApiClient.convertToType(data['customDataCombined'], 'String');
      }
      if (data.hasOwnProperty('customVarsCombined')) {
        obj['customVarsCombined'] = ApiClient.convertToType(data['customVarsCombined'], 'String');
      }
      if (data.hasOwnProperty('emailTemplate')) {
        obj['emailTemplate'] = ApiClient.convertToType(data['emailTemplate'], 'Number');
      }
      if (data.hasOwnProperty('urlKey')) {
        obj['urlKey'] = ApiClient.convertToType(data['urlKey'], 'String');
      }
      if (data.hasOwnProperty('randomHash')) {
        obj['randomHash'] = ApiClient.convertToType(data['randomHash'], 'String');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('notifyLowstock')) {
        obj['notifyLowstock'] = ApiClient.convertToType(data['notifyLowstock'], 'Number');
      }
      if (data.hasOwnProperty('notifyLowstockQty')) {
        obj['notifyLowstockQty'] = ApiClient.convertToType(data['notifyLowstockQty'], 'String');
      }
      if (data.hasOwnProperty('useHandlingFee')) {
        obj['useHandlingFee'] = ApiClient.convertToType(data['useHandlingFee'], 'Number');
      }
      if (data.hasOwnProperty('useRatesFallback')) {
        obj['useRatesFallback'] = ApiClient.convertToType(data['useRatesFallback'], 'Number');
      }
      if (data.hasOwnProperty('allowShippingExtraCharge')) {
        obj['allowShippingExtraCharge'] = ApiClient.convertToType(data['allowShippingExtraCharge'], 'Number');
      }
      if (data.hasOwnProperty('defaultShippingExtraChargeSuffix')) {
        obj['defaultShippingExtraChargeSuffix'] = ApiClient.convertToType(data['defaultShippingExtraChargeSuffix'], 'String');
      }
      if (data.hasOwnProperty('defaultShippingExtraChargeType')) {
        obj['defaultShippingExtraChargeType'] = ApiClient.convertToType(data['defaultShippingExtraChargeType'], 'String');
      }
      if (data.hasOwnProperty('defaultShippingExtraCharge')) {
        obj['defaultShippingExtraCharge'] = ApiClient.convertToType(data['defaultShippingExtraCharge'], 'String');
      }
      if (data.hasOwnProperty('isExtraChargeShippingDefault')) {
        obj['isExtraChargeShippingDefault'] = ApiClient.convertToType(data['isExtraChargeShippingDefault'], 'Number');
      }
      if (data.hasOwnProperty('defaultShippingId')) {
        obj['defaultShippingId'] = ApiClient.convertToType(data['defaultShippingId'], 'Number');
      }
      if (data.hasOwnProperty('billingUseShipping')) {
        obj['billingUseShipping'] = ApiClient.convertToType(data['billingUseShipping'], 'Number');
      }
      if (data.hasOwnProperty('billingEmail')) {
        obj['billingEmail'] = ApiClient.convertToType(data['billingEmail'], 'String');
      }
      if (data.hasOwnProperty('billingTelephone')) {
        obj['billingTelephone'] = ApiClient.convertToType(data['billingTelephone'], 'String');
      }
      if (data.hasOwnProperty('billingFax')) {
        obj['billingFax'] = ApiClient.convertToType(data['billingFax'], 'String');
      }
      if (data.hasOwnProperty('billingVendorAttn')) {
        obj['billingVendorAttn'] = ApiClient.convertToType(data['billingVendorAttn'], 'String');
      }
      if (data.hasOwnProperty('billingStreet')) {
        obj['billingStreet'] = ApiClient.convertToType(data['billingStreet'], 'String');
      }
      if (data.hasOwnProperty('billingCity')) {
        obj['billingCity'] = ApiClient.convertToType(data['billingCity'], 'String');
      }
      if (data.hasOwnProperty('billingZip')) {
        obj['billingZip'] = ApiClient.convertToType(data['billingZip'], 'String');
      }
      if (data.hasOwnProperty('billingCountryId')) {
        obj['billingCountryId'] = ApiClient.convertToType(data['billingCountryId'], 'String');
      }
      if (data.hasOwnProperty('billingRegionId')) {
        obj['billingRegionId'] = ApiClient.convertToType(data['billingRegionId'], 'Number');
      }
      if (data.hasOwnProperty('billingRegion')) {
        obj['billingRegion'] = ApiClient.convertToType(data['billingRegion'], 'String');
      }
      if (data.hasOwnProperty('subdomainLevel')) {
        obj['subdomainLevel'] = ApiClient.convertToType(data['subdomainLevel'], 'Number');
      }
      if (data.hasOwnProperty('updateStoreBaseUrl')) {
        obj['updateStoreBaseUrl'] = ApiClient.convertToType(data['updateStoreBaseUrl'], 'Number');
      }
      if (data.hasOwnProperty('confirmation')) {
        obj['confirmation'] = ApiClient.convertToType(data['confirmation'], 'String');
      }
      if (data.hasOwnProperty('confirmationSent')) {
        obj['confirmationSent'] = ApiClient.convertToType(data['confirmationSent'], 'Number');
      }
      if (data.hasOwnProperty('rejectReason')) {
        obj['rejectReason'] = ApiClient.convertToType(data['rejectReason'], 'String');
      }
      if (data.hasOwnProperty('backorderByAvailability')) {
        obj['backorderByAvailability'] = ApiClient.convertToType(data['backorderByAvailability'], 'Number');
      }
      if (data.hasOwnProperty('useReservedQty')) {
        obj['useReservedQty'] = ApiClient.convertToType(data['useReservedQty'], 'Number');
      }
      if (data.hasOwnProperty('tiercomRates')) {
        obj['tiercomRates'] = ApiClient.convertToType(data['tiercomRates'], 'String');
      }
      if (data.hasOwnProperty('tiercomFixedRule')) {
        obj['tiercomFixedRule'] = ApiClient.convertToType(data['tiercomFixedRule'], 'String');
      }
      if (data.hasOwnProperty('tiercomFixedRates')) {
        obj['tiercomFixedRates'] = ApiClient.convertToType(data['tiercomFixedRates'], 'String');
      }
      if (data.hasOwnProperty('tiercomFixedCalcType')) {
        obj['tiercomFixedCalcType'] = ApiClient.convertToType(data['tiercomFixedCalcType'], 'String');
      }
      if (data.hasOwnProperty('tiershipRates')) {
        obj['tiershipRates'] = ApiClient.convertToType(data['tiershipRates'], 'String');
      }
      if (data.hasOwnProperty('tiershipSimpleRates')) {
        obj['tiershipSimpleRates'] = ApiClient.convertToType(data['tiershipSimpleRates'], 'String');
      }
      if (data.hasOwnProperty('tiershipUseV2Rates')) {
        obj['tiershipUseV2Rates'] = ApiClient.convertToType(data['tiershipUseV2Rates'], 'Number');
      }
      if (data.hasOwnProperty('vacationMode')) {
        obj['vacationMode'] = ApiClient.convertToType(data['vacationMode'], 'Number');
      }
      if (data.hasOwnProperty('vacationEnd')) {
        obj['vacationEnd'] = ApiClient.convertToType(data['vacationEnd'], 'Date');
      }
      if (data.hasOwnProperty('vacationMessage')) {
        obj['vacationMessage'] = ApiClient.convertToType(data['vacationMessage'], 'String');
      }
      if (data.hasOwnProperty('udmemberLimitProducts')) {
        obj['udmemberLimitProducts'] = ApiClient.convertToType(data['udmemberLimitProducts'], 'Number');
      }
      if (data.hasOwnProperty('udmemberProfileId')) {
        obj['udmemberProfileId'] = ApiClient.convertToType(data['udmemberProfileId'], 'Number');
      }
      if (data.hasOwnProperty('udmemberProfileRefid')) {
        obj['udmemberProfileRefid'] = ApiClient.convertToType(data['udmemberProfileRefid'], 'String');
      }
      if (data.hasOwnProperty('udmemberMembershipCode')) {
        obj['udmemberMembershipCode'] = ApiClient.convertToType(data['udmemberMembershipCode'], 'String');
      }
      if (data.hasOwnProperty('udmemberMembershipTitle')) {
        obj['udmemberMembershipTitle'] = ApiClient.convertToType(data['udmemberMembershipTitle'], 'String');
      }
      if (data.hasOwnProperty('udmemberBillingType')) {
        obj['udmemberBillingType'] = ApiClient.convertToType(data['udmemberBillingType'], 'String');
      }
      if (data.hasOwnProperty('udmemberHistory')) {
        obj['udmemberHistory'] = ApiClient.convertToType(data['udmemberHistory'], 'String');
      }
      if (data.hasOwnProperty('udmemberProfileSyncOff')) {
        obj['udmemberProfileSyncOff'] = ApiClient.convertToType(data['udmemberProfileSyncOff'], 'Number');
      }
      if (data.hasOwnProperty('udmemberAllowMicrosite')) {
        obj['udmemberAllowMicrosite'] = ApiClient.convertToType(data['udmemberAllowMicrosite'], 'Number');
      }
      if (data.hasOwnProperty('udprodTemplateSku')) {
        obj['udprodTemplateSku'] = ApiClient.convertToType(data['udprodTemplateSku'], 'String');
      }
      if (data.hasOwnProperty('vendorTaxClass')) {
        obj['vendorTaxClass'] = ApiClient.convertToType(data['vendorTaxClass'], 'String');
      }
      if (data.hasOwnProperty('catalogProducts')) {
        obj['catalogProducts'] = ApiClient.convertToType(data['catalogProducts'], [Object]);
      }
      if (data.hasOwnProperty('vendorLocations')) {
        obj['vendorLocations'] = ApiClient.convertToType(data['vendorLocations'], [Object]);
      }
      if (data.hasOwnProperty('config')) {
        obj['config'] = ApiClient.convertToType(data['config'], Object);
      }
      if (data.hasOwnProperty('partners')) {
        obj['partners'] = ApiClient.convertToType(data['partners'], [Object]);
      }
      if (data.hasOwnProperty('udropshipVendorProducts')) {
        obj['udropshipVendorProducts'] = ApiClient.convertToType(data['udropshipVendorProducts'], [Object]);
      }
      if (data.hasOwnProperty('harpoonHpublicApplicationpartners')) {
        obj['harpoonHpublicApplicationpartners'] = ApiClient.convertToType(data['harpoonHpublicApplicationpartners'], [Object]);
      }
      if (data.hasOwnProperty('harpoonHpublicv12VendorAppCategories')) {
        obj['harpoonHpublicv12VendorAppCategories'] = ApiClient.convertToType(data['harpoonHpublicv12VendorAppCategories'], [Object]);
      }
    }
    return obj;
  }

  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {String} vendorName
   */
  exports.prototype['vendorName'] = undefined;
  /**
   * @member {String} vendorAttn
   */
  exports.prototype['vendorAttn'] = undefined;
  /**
   * @member {String} email
   */
  exports.prototype['email'] = undefined;
  /**
   * @member {String} street
   */
  exports.prototype['street'] = undefined;
  /**
   * @member {String} city
   */
  exports.prototype['city'] = undefined;
  /**
   * @member {String} zip
   */
  exports.prototype['zip'] = undefined;
  /**
   * @member {String} countryId
   */
  exports.prototype['countryId'] = undefined;
  /**
   * @member {Number} regionId
   */
  exports.prototype['regionId'] = undefined;
  /**
   * @member {String} region
   */
  exports.prototype['region'] = undefined;
  /**
   * @member {String} telephone
   */
  exports.prototype['telephone'] = undefined;
  /**
   * @member {String} fax
   */
  exports.prototype['fax'] = undefined;
  /**
   * @member {Boolean} status
   */
  exports.prototype['status'] = undefined;
  /**
   * @member {String} password
   */
  exports.prototype['password'] = undefined;
  /**
   * @member {String} passwordHash
   */
  exports.prototype['passwordHash'] = undefined;
  /**
   * @member {String} passwordEnc
   */
  exports.prototype['passwordEnc'] = undefined;
  /**
   * @member {String} carrierCode
   */
  exports.prototype['carrierCode'] = undefined;
  /**
   * @member {Number} notifyNewOrder
   */
  exports.prototype['notifyNewOrder'] = undefined;
  /**
   * @member {String} labelType
   */
  exports.prototype['labelType'] = undefined;
  /**
   * @member {Number} testMode
   */
  exports.prototype['testMode'] = undefined;
  /**
   * @member {String} handlingFee
   */
  exports.prototype['handlingFee'] = undefined;
  /**
   * @member {String} upsShipperNumber
   */
  exports.prototype['upsShipperNumber'] = undefined;
  /**
   * @member {String} customDataCombined
   */
  exports.prototype['customDataCombined'] = undefined;
  /**
   * @member {String} customVarsCombined
   */
  exports.prototype['customVarsCombined'] = undefined;
  /**
   * @member {Number} emailTemplate
   */
  exports.prototype['emailTemplate'] = undefined;
  /**
   * @member {String} urlKey
   */
  exports.prototype['urlKey'] = undefined;
  /**
   * @member {String} randomHash
   */
  exports.prototype['randomHash'] = undefined;
  /**
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * @member {Number} notifyLowstock
   */
  exports.prototype['notifyLowstock'] = undefined;
  /**
   * @member {String} notifyLowstockQty
   */
  exports.prototype['notifyLowstockQty'] = undefined;
  /**
   * @member {Number} useHandlingFee
   */
  exports.prototype['useHandlingFee'] = undefined;
  /**
   * @member {Number} useRatesFallback
   */
  exports.prototype['useRatesFallback'] = undefined;
  /**
   * @member {Number} allowShippingExtraCharge
   */
  exports.prototype['allowShippingExtraCharge'] = undefined;
  /**
   * @member {String} defaultShippingExtraChargeSuffix
   */
  exports.prototype['defaultShippingExtraChargeSuffix'] = undefined;
  /**
   * @member {String} defaultShippingExtraChargeType
   */
  exports.prototype['defaultShippingExtraChargeType'] = undefined;
  /**
   * @member {String} defaultShippingExtraCharge
   */
  exports.prototype['defaultShippingExtraCharge'] = undefined;
  /**
   * @member {Number} isExtraChargeShippingDefault
   */
  exports.prototype['isExtraChargeShippingDefault'] = undefined;
  /**
   * @member {Number} defaultShippingId
   */
  exports.prototype['defaultShippingId'] = undefined;
  /**
   * @member {Number} billingUseShipping
   */
  exports.prototype['billingUseShipping'] = undefined;
  /**
   * @member {String} billingEmail
   */
  exports.prototype['billingEmail'] = undefined;
  /**
   * @member {String} billingTelephone
   */
  exports.prototype['billingTelephone'] = undefined;
  /**
   * @member {String} billingFax
   */
  exports.prototype['billingFax'] = undefined;
  /**
   * @member {String} billingVendorAttn
   */
  exports.prototype['billingVendorAttn'] = undefined;
  /**
   * @member {String} billingStreet
   */
  exports.prototype['billingStreet'] = undefined;
  /**
   * @member {String} billingCity
   */
  exports.prototype['billingCity'] = undefined;
  /**
   * @member {String} billingZip
   */
  exports.prototype['billingZip'] = undefined;
  /**
   * @member {String} billingCountryId
   */
  exports.prototype['billingCountryId'] = undefined;
  /**
   * @member {Number} billingRegionId
   */
  exports.prototype['billingRegionId'] = undefined;
  /**
   * @member {String} billingRegion
   */
  exports.prototype['billingRegion'] = undefined;
  /**
   * @member {Number} subdomainLevel
   */
  exports.prototype['subdomainLevel'] = undefined;
  /**
   * @member {Number} updateStoreBaseUrl
   */
  exports.prototype['updateStoreBaseUrl'] = undefined;
  /**
   * @member {String} confirmation
   */
  exports.prototype['confirmation'] = undefined;
  /**
   * @member {Number} confirmationSent
   */
  exports.prototype['confirmationSent'] = undefined;
  /**
   * @member {String} rejectReason
   */
  exports.prototype['rejectReason'] = undefined;
  /**
   * @member {Number} backorderByAvailability
   */
  exports.prototype['backorderByAvailability'] = undefined;
  /**
   * @member {Number} useReservedQty
   */
  exports.prototype['useReservedQty'] = undefined;
  /**
   * @member {String} tiercomRates
   */
  exports.prototype['tiercomRates'] = undefined;
  /**
   * @member {String} tiercomFixedRule
   */
  exports.prototype['tiercomFixedRule'] = undefined;
  /**
   * @member {String} tiercomFixedRates
   */
  exports.prototype['tiercomFixedRates'] = undefined;
  /**
   * @member {String} tiercomFixedCalcType
   */
  exports.prototype['tiercomFixedCalcType'] = undefined;
  /**
   * @member {String} tiershipRates
   */
  exports.prototype['tiershipRates'] = undefined;
  /**
   * @member {String} tiershipSimpleRates
   */
  exports.prototype['tiershipSimpleRates'] = undefined;
  /**
   * @member {Number} tiershipUseV2Rates
   */
  exports.prototype['tiershipUseV2Rates'] = undefined;
  /**
   * @member {Number} vacationMode
   */
  exports.prototype['vacationMode'] = undefined;
  /**
   * @member {Date} vacationEnd
   */
  exports.prototype['vacationEnd'] = undefined;
  /**
   * @member {String} vacationMessage
   */
  exports.prototype['vacationMessage'] = undefined;
  /**
   * @member {Number} udmemberLimitProducts
   */
  exports.prototype['udmemberLimitProducts'] = undefined;
  /**
   * @member {Number} udmemberProfileId
   */
  exports.prototype['udmemberProfileId'] = undefined;
  /**
   * @member {String} udmemberProfileRefid
   */
  exports.prototype['udmemberProfileRefid'] = undefined;
  /**
   * @member {String} udmemberMembershipCode
   */
  exports.prototype['udmemberMembershipCode'] = undefined;
  /**
   * @member {String} udmemberMembershipTitle
   */
  exports.prototype['udmemberMembershipTitle'] = undefined;
  /**
   * @member {String} udmemberBillingType
   */
  exports.prototype['udmemberBillingType'] = undefined;
  /**
   * @member {String} udmemberHistory
   */
  exports.prototype['udmemberHistory'] = undefined;
  /**
   * @member {Number} udmemberProfileSyncOff
   */
  exports.prototype['udmemberProfileSyncOff'] = undefined;
  /**
   * @member {Number} udmemberAllowMicrosite
   */
  exports.prototype['udmemberAllowMicrosite'] = undefined;
  /**
   * @member {String} udprodTemplateSku
   */
  exports.prototype['udprodTemplateSku'] = undefined;
  /**
   * @member {String} vendorTaxClass
   */
  exports.prototype['vendorTaxClass'] = undefined;
  /**
   * @member {Array.<Object>} catalogProducts
   */
  exports.prototype['catalogProducts'] = undefined;
  /**
   * @member {Array.<Object>} vendorLocations
   */
  exports.prototype['vendorLocations'] = undefined;
  /**
   * @member {Object} config
   */
  exports.prototype['config'] = undefined;
  /**
   * @member {Array.<Object>} partners
   */
  exports.prototype['partners'] = undefined;
  /**
   * @member {Array.<Object>} udropshipVendorProducts
   */
  exports.prototype['udropshipVendorProducts'] = undefined;
  /**
   * @member {Array.<Object>} harpoonHpublicApplicationpartners
   */
  exports.prototype['harpoonHpublicApplicationpartners'] = undefined;
  /**
   * @member {Array.<Object>} harpoonHpublicv12VendorAppCategories
   */
  exports.prototype['harpoonHpublicv12VendorAppCategories'] = undefined;



  return exports;
}));



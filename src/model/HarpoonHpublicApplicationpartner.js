/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.HarpoonHpublicApplicationpartner = factory(root.harpoonApi.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The HarpoonHpublicApplicationpartner model module.
   * @module model/HarpoonHpublicApplicationpartner
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>HarpoonHpublicApplicationpartner</code>.
   * @alias module:model/HarpoonHpublicApplicationpartner
   * @class
   * @param id {Number} 
   * @param applicationId {Number} 
   * @param brandId {Number} 
   * @param status {String} 
   */
  var exports = function(id, applicationId, brandId, status) {
    var _this = this;

    _this['id'] = id;
    _this['applicationId'] = applicationId;
    _this['brandId'] = brandId;
    _this['status'] = status;




















  };

  /**
   * Constructs a <code>HarpoonHpublicApplicationpartner</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/HarpoonHpublicApplicationpartner} obj Optional instance to populate.
   * @return {module:model/HarpoonHpublicApplicationpartner} The populated <code>HarpoonHpublicApplicationpartner</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('id')) {
        obj['id'] = ApiClient.convertToType(data['id'], 'Number');
      }
      if (data.hasOwnProperty('applicationId')) {
        obj['applicationId'] = ApiClient.convertToType(data['applicationId'], 'Number');
      }
      if (data.hasOwnProperty('brandId')) {
        obj['brandId'] = ApiClient.convertToType(data['brandId'], 'Number');
      }
      if (data.hasOwnProperty('status')) {
        obj['status'] = ApiClient.convertToType(data['status'], 'String');
      }
      if (data.hasOwnProperty('invitedEmail')) {
        obj['invitedEmail'] = ApiClient.convertToType(data['invitedEmail'], 'String');
      }
      if (data.hasOwnProperty('invitedAt')) {
        obj['invitedAt'] = ApiClient.convertToType(data['invitedAt'], 'Date');
      }
      if (data.hasOwnProperty('acceptedAt')) {
        obj['acceptedAt'] = ApiClient.convertToType(data['acceptedAt'], 'Date');
      }
      if (data.hasOwnProperty('createdAt')) {
        obj['createdAt'] = ApiClient.convertToType(data['createdAt'], 'Date');
      }
      if (data.hasOwnProperty('updatedAt')) {
        obj['updatedAt'] = ApiClient.convertToType(data['updatedAt'], 'Date');
      }
      if (data.hasOwnProperty('configList')) {
        obj['configList'] = ApiClient.convertToType(data['configList'], 'Number');
      }
      if (data.hasOwnProperty('configFeed')) {
        obj['configFeed'] = ApiClient.convertToType(data['configFeed'], 'Number');
      }
      if (data.hasOwnProperty('configNeedFollow')) {
        obj['configNeedFollow'] = ApiClient.convertToType(data['configNeedFollow'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptEvent')) {
        obj['configAcceptEvent'] = ApiClient.convertToType(data['configAcceptEvent'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptCoupon')) {
        obj['configAcceptCoupon'] = ApiClient.convertToType(data['configAcceptCoupon'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptDealsimple')) {
        obj['configAcceptDealsimple'] = ApiClient.convertToType(data['configAcceptDealsimple'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptDealgroup')) {
        obj['configAcceptDealgroup'] = ApiClient.convertToType(data['configAcceptDealgroup'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptNotificationpush')) {
        obj['configAcceptNotificationpush'] = ApiClient.convertToType(data['configAcceptNotificationpush'], 'Number');
      }
      if (data.hasOwnProperty('configAcceptNotificationbeacon')) {
        obj['configAcceptNotificationbeacon'] = ApiClient.convertToType(data['configAcceptNotificationbeacon'], 'Number');
      }
      if (data.hasOwnProperty('isOwner')) {
        obj['isOwner'] = ApiClient.convertToType(data['isOwner'], 'Number');
      }
      if (data.hasOwnProperty('configApproveEvent')) {
        obj['configApproveEvent'] = ApiClient.convertToType(data['configApproveEvent'], 'Number');
      }
      if (data.hasOwnProperty('configApproveCoupon')) {
        obj['configApproveCoupon'] = ApiClient.convertToType(data['configApproveCoupon'], 'Number');
      }
      if (data.hasOwnProperty('configApproveDealsimple')) {
        obj['configApproveDealsimple'] = ApiClient.convertToType(data['configApproveDealsimple'], 'Number');
      }
      if (data.hasOwnProperty('configApproveDealgroup')) {
        obj['configApproveDealgroup'] = ApiClient.convertToType(data['configApproveDealgroup'], 'Number');
      }
      if (data.hasOwnProperty('udropshipVendor')) {
        obj['udropshipVendor'] = ApiClient.convertToType(data['udropshipVendor'], Object);
      }
    }
    return obj;
  }

  /**
   * @member {Number} id
   */
  exports.prototype['id'] = undefined;
  /**
   * @member {Number} applicationId
   */
  exports.prototype['applicationId'] = undefined;
  /**
   * @member {Number} brandId
   */
  exports.prototype['brandId'] = undefined;
  /**
   * @member {String} status
   */
  exports.prototype['status'] = undefined;
  /**
   * @member {String} invitedEmail
   */
  exports.prototype['invitedEmail'] = undefined;
  /**
   * @member {Date} invitedAt
   */
  exports.prototype['invitedAt'] = undefined;
  /**
   * @member {Date} acceptedAt
   */
  exports.prototype['acceptedAt'] = undefined;
  /**
   * @member {Date} createdAt
   */
  exports.prototype['createdAt'] = undefined;
  /**
   * @member {Date} updatedAt
   */
  exports.prototype['updatedAt'] = undefined;
  /**
   * @member {Number} configList
   */
  exports.prototype['configList'] = undefined;
  /**
   * @member {Number} configFeed
   */
  exports.prototype['configFeed'] = undefined;
  /**
   * @member {Number} configNeedFollow
   */
  exports.prototype['configNeedFollow'] = undefined;
  /**
   * @member {Number} configAcceptEvent
   */
  exports.prototype['configAcceptEvent'] = undefined;
  /**
   * @member {Number} configAcceptCoupon
   */
  exports.prototype['configAcceptCoupon'] = undefined;
  /**
   * @member {Number} configAcceptDealsimple
   */
  exports.prototype['configAcceptDealsimple'] = undefined;
  /**
   * @member {Number} configAcceptDealgroup
   */
  exports.prototype['configAcceptDealgroup'] = undefined;
  /**
   * @member {Number} configAcceptNotificationpush
   */
  exports.prototype['configAcceptNotificationpush'] = undefined;
  /**
   * @member {Number} configAcceptNotificationbeacon
   */
  exports.prototype['configAcceptNotificationbeacon'] = undefined;
  /**
   * @member {Number} isOwner
   */
  exports.prototype['isOwner'] = undefined;
  /**
   * @member {Number} configApproveEvent
   */
  exports.prototype['configApproveEvent'] = undefined;
  /**
   * @member {Number} configApproveCoupon
   */
  exports.prototype['configApproveCoupon'] = undefined;
  /**
   * @member {Number} configApproveDealsimple
   */
  exports.prototype['configApproveDealsimple'] = undefined;
  /**
   * @member {Number} configApproveDealgroup
   */
  exports.prototype['configApproveDealgroup'] = undefined;
  /**
   * @member {Object} udropshipVendor
   */
  exports.prototype['udropshipVendor'] = undefined;



  return exports;
}));



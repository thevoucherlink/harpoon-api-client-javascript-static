/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/FormSelectorOption'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./FormSelectorOption'));
  } else {
    // Browser globals (root is window)
    if (!root.harpoonApi) {
      root.harpoonApi = {};
    }
    root.harpoonApi.FormRow = factory(root.harpoonApi.ApiClient, root.harpoonApi.FormSelectorOption);
  }
}(this, function(ApiClient, FormSelectorOption) {
  'use strict';




  /**
   * The FormRow model module.
   * @module model/FormRow
   * @version 1.2.30
   */

  /**
   * Constructs a new <code>FormRow</code>.
   * @alias module:model/FormRow
   * @class
   * @param metadataKey {String} 
   * @param type {module:model/FormRow.TypeEnum} 
   * @param title {String} 
   */
  var exports = function(metadataKey, type, title) {
    var _this = this;

    _this['metadataKey'] = metadataKey;
    _this['type'] = type;
    _this['title'] = title;







  };

  /**
   * Constructs a <code>FormRow</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/FormRow} obj Optional instance to populate.
   * @return {module:model/FormRow} The populated <code>FormRow</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('metadataKey')) {
        obj['metadataKey'] = ApiClient.convertToType(data['metadataKey'], 'String');
      }
      if (data.hasOwnProperty('type')) {
        obj['type'] = ApiClient.convertToType(data['type'], 'String');
      }
      if (data.hasOwnProperty('title')) {
        obj['title'] = ApiClient.convertToType(data['title'], 'String');
      }
      if (data.hasOwnProperty('defaultValue')) {
        obj['defaultValue'] = ApiClient.convertToType(data['defaultValue'], 'String');
      }
      if (data.hasOwnProperty('placeholder')) {
        obj['placeholder'] = ApiClient.convertToType(data['placeholder'], 'String');
      }
      if (data.hasOwnProperty('hidden')) {
        obj['hidden'] = ApiClient.convertToType(data['hidden'], 'Boolean');
      }
      if (data.hasOwnProperty('required')) {
        obj['required'] = ApiClient.convertToType(data['required'], 'Boolean');
      }
      if (data.hasOwnProperty('validationRegExp')) {
        obj['validationRegExp'] = ApiClient.convertToType(data['validationRegExp'], 'String');
      }
      if (data.hasOwnProperty('validationMessage')) {
        obj['validationMessage'] = ApiClient.convertToType(data['validationMessage'], 'String');
      }
      if (data.hasOwnProperty('selectorOptions')) {
        obj['selectorOptions'] = ApiClient.convertToType(data['selectorOptions'], [FormSelectorOption]);
      }
    }
    return obj;
  }

  /**
   * @member {String} metadataKey
   */
  exports.prototype['metadataKey'] = undefined;
  /**
   * @member {module:model/FormRow.TypeEnum} type
   * @default 'text'
   */
  exports.prototype['type'] = 'text';
  /**
   * @member {String} title
   */
  exports.prototype['title'] = undefined;
  /**
   * @member {String} defaultValue
   */
  exports.prototype['defaultValue'] = undefined;
  /**
   * @member {String} placeholder
   */
  exports.prototype['placeholder'] = undefined;
  /**
   * @member {Boolean} hidden
   * @default false
   */
  exports.prototype['hidden'] = false;
  /**
   * @member {Boolean} required
   * @default true
   */
  exports.prototype['required'] = true;
  /**
   * @member {String} validationRegExp
   */
  exports.prototype['validationRegExp'] = undefined;
  /**
   * @member {String} validationMessage
   */
  exports.prototype['validationMessage'] = undefined;
  /**
   * @member {Array.<module:model/FormSelectorOption>} selectorOptions
   */
  exports.prototype['selectorOptions'] = undefined;


  /**
   * Allowed values for the <code>type</code> property.
   * @enum {String}
   * @readonly
   */
  exports.TypeEnum = {
    /**
     * value: "text"
     * @const
     */
    "text": "text",
    /**
     * value: "name"
     * @const
     */
    "name": "name",
    /**
     * value: "url"
     * @const
     */
    "url": "url",
    /**
     * value: "email"
     * @const
     */
    "email": "email",
    /**
     * value: "password"
     * @const
     */
    "password": "password",
    /**
     * value: "number"
     * @const
     */
    "number": "number",
    /**
     * value: "phone"
     * @const
     */
    "phone": "phone",
    /**
     * value: "twitter"
     * @const
     */
    "twitter": "twitter",
    /**
     * value: "account"
     * @const
     */
    "account": "account",
    /**
     * value: "integer"
     * @const
     */
    "integer": "integer",
    /**
     * value: "decimal"
     * @const
     */
    "decimal": "decimal",
    /**
     * value: "textView"
     * @const
     */
    "textView": "textView",
    /**
     * value: "selectorPush"
     * @const
     */
    "selectorPush": "selectorPush",
    /**
     * value: "selectorPickerView"
     * @const
     */
    "selectorPickerView": "selectorPickerView",
    /**
     * value: "dateInline"
     * @const
     */
    "dateInline": "dateInline",
    /**
     * value: "datetimeInline"
     * @const
     */
    "datetimeInline": "datetimeInline",
    /**
     * value: "timeInline"
     * @const
     */
    "timeInline": "timeInline",
    /**
     * value: "countDownTimerInline"
     * @const
     */
    "countDownTimerInline": "countDownTimerInline",
    /**
     * value: "date"
     * @const
     */
    "date": "date",
    /**
     * value: "datetime"
     * @const
     */
    "datetime": "datetime",
    /**
     * value: "time"
     * @const
     */
    "time": "time",
    /**
     * value: "countDownTimer"
     * @const
     */
    "countDownTimer": "countDownTimer"  };


  return exports;
}));



/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.harpoonApi);
  }
}(this, function(expect, harpoonApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new harpoonApi.PlaylistItem();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('PlaylistItem', function() {
    it('should create an instance of PlaylistItem', function() {
      // uncomment below and update the code to test PlaylistItem
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be.a(harpoonApi.PlaylistItem);
    });

    it('should have the property title (base name: "title")', function() {
      // uncomment below and update the code to test the property title
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property shortDescription (base name: "shortDescription")', function() {
      // uncomment below and update the code to test the property shortDescription
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property image (base name: "image")', function() {
      // uncomment below and update the code to test the property image
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property file (base name: "file")', function() {
      // uncomment below and update the code to test the property file
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property type (base name: "type")', function() {
      // uncomment below and update the code to test the property type
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property mediaType (base name: "mediaType")', function() {
      // uncomment below and update the code to test the property mediaType
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property mediaId (base name: "mediaId")', function() {
      // uncomment below and update the code to test the property mediaId
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property order (base name: "order")', function() {
      // uncomment below and update the code to test the property order
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property id (base name: "id")', function() {
      // uncomment below and update the code to test the property id
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property playlistId (base name: "playlistId")', function() {
      // uncomment below and update the code to test the property playlistId
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property playlist (base name: "playlist")', function() {
      // uncomment below and update the code to test the property playlist
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property playerSources (base name: "playerSources")', function() {
      // uncomment below and update the code to test the property playerSources
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property playerTracks (base name: "playerTracks")', function() {
      // uncomment below and update the code to test the property playerTracks
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

    it('should have the property radioShows (base name: "radioShows")', function() {
      // uncomment below and update the code to test the property radioShows
      //var instane = new harpoonApi.PlaylistItem();
      //expect(instance).to.be();
    });

  });

}));

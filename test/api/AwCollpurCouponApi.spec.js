/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.harpoonApi);
  }
}(this, function(expect, harpoonApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new harpoonApi.AwCollpurCouponApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('AwCollpurCouponApi', function() {
    describe('awCollpurCouponCount', function() {
      it('should call awCollpurCouponCount successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponCount
        //instance.awCollpurCouponCount(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponCreate', function() {
      it('should call awCollpurCouponCreate successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponCreate
        //instance.awCollpurCouponCreate(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream', function() {
      it('should call awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream
        //instance.awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream', function() {
      it('should call awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream
        //instance.awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponDeleteById', function() {
      it('should call awCollpurCouponDeleteById successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponDeleteById
        //instance.awCollpurCouponDeleteById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponExistsGetAwCollpurCouponsidExists', function() {
      it('should call awCollpurCouponExistsGetAwCollpurCouponsidExists successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponExistsGetAwCollpurCouponsidExists
        //instance.awCollpurCouponExistsGetAwCollpurCouponsidExists(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponExistsHeadAwCollpurCouponsid', function() {
      it('should call awCollpurCouponExistsHeadAwCollpurCouponsid successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponExistsHeadAwCollpurCouponsid
        //instance.awCollpurCouponExistsHeadAwCollpurCouponsid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponFind', function() {
      it('should call awCollpurCouponFind successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponFind
        //instance.awCollpurCouponFind(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponFindById', function() {
      it('should call awCollpurCouponFindById successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponFindById
        //instance.awCollpurCouponFindById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponFindOne', function() {
      it('should call awCollpurCouponFindOne successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponFindOne
        //instance.awCollpurCouponFindOne(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid', function() {
      it('should call awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid
        //instance.awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid', function() {
      it('should call awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid
        //instance.awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponReplaceById', function() {
      it('should call awCollpurCouponReplaceById successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponReplaceById
        //instance.awCollpurCouponReplaceById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponReplaceOrCreate', function() {
      it('should call awCollpurCouponReplaceOrCreate successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponReplaceOrCreate
        //instance.awCollpurCouponReplaceOrCreate(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponUpdateAll', function() {
      it('should call awCollpurCouponUpdateAll successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponUpdateAll
        //instance.awCollpurCouponUpdateAll(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponUpsertPatchAwCollpurCoupons', function() {
      it('should call awCollpurCouponUpsertPatchAwCollpurCoupons successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponUpsertPatchAwCollpurCoupons
        //instance.awCollpurCouponUpsertPatchAwCollpurCoupons(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponUpsertPutAwCollpurCoupons', function() {
      it('should call awCollpurCouponUpsertPutAwCollpurCoupons successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponUpsertPutAwCollpurCoupons
        //instance.awCollpurCouponUpsertPutAwCollpurCoupons(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('awCollpurCouponUpsertWithWhere', function() {
      it('should call awCollpurCouponUpsertWithWhere successfully', function(done) {
        //uncomment below and update the code to test awCollpurCouponUpsertWithWhere
        //instance.awCollpurCouponUpsertWithWhere(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));

/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.harpoonApi);
  }
}(this, function(expect, harpoonApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new harpoonApi.RadioShowTimeApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('RadioShowTimeApi', function() {
    describe('radioShowTimeCount', function() {
      it('should call radioShowTimeCount successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeCount
        //instance.radioShowTimeCount(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeCreate', function() {
      it('should call radioShowTimeCreate successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeCreate
        //instance.radioShowTimeCreate(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream', function() {
      it('should call radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream
        //instance.radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream', function() {
      it('should call radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream
        //instance.radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeDeleteById', function() {
      it('should call radioShowTimeDeleteById successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeDeleteById
        //instance.radioShowTimeDeleteById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeExistsGetRadioShowTimesidExists', function() {
      it('should call radioShowTimeExistsGetRadioShowTimesidExists successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeExistsGetRadioShowTimesidExists
        //instance.radioShowTimeExistsGetRadioShowTimesidExists(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeExistsHeadRadioShowTimesid', function() {
      it('should call radioShowTimeExistsHeadRadioShowTimesid successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeExistsHeadRadioShowTimesid
        //instance.radioShowTimeExistsHeadRadioShowTimesid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeFind', function() {
      it('should call radioShowTimeFind successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeFind
        //instance.radioShowTimeFind(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeFindById', function() {
      it('should call radioShowTimeFindById successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeFindById
        //instance.radioShowTimeFindById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeFindOne', function() {
      it('should call radioShowTimeFindOne successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeFindOne
        //instance.radioShowTimeFindOne(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimePrototypeGetRadioShow', function() {
      it('should call radioShowTimePrototypeGetRadioShow successfully', function(done) {
        //uncomment below and update the code to test radioShowTimePrototypeGetRadioShow
        //instance.radioShowTimePrototypeGetRadioShow(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid', function() {
      it('should call radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid successfully', function(done) {
        //uncomment below and update the code to test radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid
        //instance.radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid', function() {
      it('should call radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid successfully', function(done) {
        //uncomment below and update the code to test radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid
        //instance.radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeReplaceById', function() {
      it('should call radioShowTimeReplaceById successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeReplaceById
        //instance.radioShowTimeReplaceById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeReplaceOrCreate', function() {
      it('should call radioShowTimeReplaceOrCreate successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeReplaceOrCreate
        //instance.radioShowTimeReplaceOrCreate(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeUpdateAll', function() {
      it('should call radioShowTimeUpdateAll successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeUpdateAll
        //instance.radioShowTimeUpdateAll(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeUpsertPatchRadioShowTimes', function() {
      it('should call radioShowTimeUpsertPatchRadioShowTimes successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeUpsertPatchRadioShowTimes
        //instance.radioShowTimeUpsertPatchRadioShowTimes(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeUpsertPutRadioShowTimes', function() {
      it('should call radioShowTimeUpsertPutRadioShowTimes successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeUpsertPutRadioShowTimes
        //instance.radioShowTimeUpsertPutRadioShowTimes(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('radioShowTimeUpsertWithWhere', function() {
      it('should call radioShowTimeUpsertWithWhere successfully', function(done) {
        //uncomment below and update the code to test radioShowTimeUpsertWithWhere
        //instance.radioShowTimeUpsertWithWhere(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));

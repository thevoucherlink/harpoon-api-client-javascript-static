/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.harpoonApi);
  }
}(this, function(expect, harpoonApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new harpoonApi.DealGroupApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DealGroupApi', function() {
    describe('dealGroupCheckout', function() {
      it('should call dealGroupCheckout successfully', function(done) {
        //uncomment below and update the code to test dealGroupCheckout
        //instance.dealGroupCheckout(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupFind', function() {
      it('should call dealGroupFind successfully', function(done) {
        //uncomment below and update the code to test dealGroupFind
        //instance.dealGroupFind(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupFindById', function() {
      it('should call dealGroupFindById successfully', function(done) {
        //uncomment below and update the code to test dealGroupFindById
        //instance.dealGroupFindById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupFindOne', function() {
      it('should call dealGroupFindOne successfully', function(done) {
        //uncomment below and update the code to test dealGroupFindOne
        //instance.dealGroupFindOne(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupRedeem', function() {
      it('should call dealGroupRedeem successfully', function(done) {
        //uncomment below and update the code to test dealGroupRedeem
        //instance.dealGroupRedeem(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupReplaceById', function() {
      it('should call dealGroupReplaceById successfully', function(done) {
        //uncomment below and update the code to test dealGroupReplaceById
        //instance.dealGroupReplaceById(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupReplaceOrCreate', function() {
      it('should call dealGroupReplaceOrCreate successfully', function(done) {
        //uncomment below and update the code to test dealGroupReplaceOrCreate
        //instance.dealGroupReplaceOrCreate(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupUpsertWithWhere', function() {
      it('should call dealGroupUpsertWithWhere successfully', function(done) {
        //uncomment below and update the code to test dealGroupUpsertWithWhere
        //instance.dealGroupUpsertWithWhere(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('dealGroupVenues', function() {
      it('should call dealGroupVenues successfully', function(done) {
        //uncomment below and update the code to test dealGroupVenues
        //instance.dealGroupVenues(pet, function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));

# harpoonApi.CatalogCategoryProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryId** | **Number** |  | 
**productId** | **Number** |  | 
**position** | **Number** |  | 
**catalogProduct** | **Object** |  | [optional] 
**catalogCategory** | **Object** |  | [optional] 



# harpoonApi.DealGroupCheckout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartId** | **Number** |  | [optional] 
**url** | **String** |  | [optional] 
**status** | **String** |  | [default to &#39;unknown&#39;]
**id** | **Number** |  | [optional] 



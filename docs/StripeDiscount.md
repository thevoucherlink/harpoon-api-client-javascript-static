# harpoonApi.StripeDiscount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_object** | **String** |  | [optional] 
**customer** | **String** |  | [optional] 
**end** | **Number** |  | [optional] 
**start** | **Number** |  | [optional] 
**subscription** | **String** |  | [optional] 
**coupon** | [**StripeCoupon**](StripeCoupon.md) |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.NotificationFrom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**Customer**](Customer.md) |  | [optional] 
**brand** | [**Brand**](Brand.md) |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.AwEventbookingTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**eventTicketId** | **Number** |  | 
**orderItemId** | **Number** |  | 
**code** | **String** |  | 
**redeemed** | **Number** |  | 
**paymentStatus** | **Number** |  | 
**createdAt** | **Date** |  | [optional] 
**redeemedAt** | **Date** |  | [optional] 
**redeemedByTerminal** | **String** |  | [optional] 
**fee** | **String** |  | 
**isPassOnFee** | **Number** |  | 
**competitionwinnerId** | **Number** |  | 



# harpoonApi.Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**streetAddress** | **String** | Main street line for the address (first line) | [optional] 
**streetAddressComp** | **String** | Line to complete the address (second line) | [optional] 
**city** | **String** |  | [optional] 
**region** | **String** | Region, County, Province or State | [optional] 
**countryCode** | **String** | Country ISO code (must be max of 2 capital letter) | [optional] [default to &#39;IE&#39;]
**postcode** | **String** |  | [optional] 
**attnFirstName** | **String** | Postal Attn {person first name} | [optional] 
**attnLastName** | **String** | Postal Attn {person last name} | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.AwEventbookingEventTicketAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**ticketId** | **Number** |  | 
**storeId** | **Number** |  | [optional] 
**attributeCode** | **String** |  | [optional] 
**value** | **String** |  | [optional] 



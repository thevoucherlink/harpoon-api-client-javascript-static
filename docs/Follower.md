# harpoonApi.Follower

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**dob** | **String** |  | [optional] 
**gender** | **String** |  | [optional] [default to &#39;other&#39;]
**password** | **String** |  | 
**profilePicture** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**profilePictureUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**coverUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**metadata** | **Object** |  | [optional] 
**connection** | [**CustomerConnection**](CustomerConnection.md) |  | [optional] 
**followingCount** | **Number** |  | [optional] [default to 0.0]
**followerCount** | **Number** |  | [optional] [default to 0.0]
**isFollowed** | **Boolean** |  | [optional] [default to false]
**isFollower** | **Boolean** |  | [optional] [default to false]
**notificationCount** | **Number** |  | [optional] [default to 0.0]
**authorizationCode** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



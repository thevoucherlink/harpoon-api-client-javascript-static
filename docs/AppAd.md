# harpoonApi.AppAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unitId** | **String** |  | 
**unitName** | **String** |  | [optional] 
**unitType** | **String** |  | [optional] 


<a name="UnitTypeEnum"></a>
## Enum: UnitTypeEnum


* `banner` (value: `"banner"`)

* `fullscreen` (value: `"fullscreen"`)

* `nativeAd` (value: `"nativeAd"`)

* `rewardVideo` (value: `"rewardVideo"`)

* `mRect` (value: `"mRect"`)

* `custom` (value: `"custom"`)





# harpoonApi.RssFeedGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**sortOrder** | **Number** |  | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 
**appId** | **String** |  | [optional] 
**rssFeeds** | **[Object]** |  | [optional] 



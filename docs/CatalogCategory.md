# harpoonApi.CatalogCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**parentId** | **Number** |  | 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**path** | **String** |  | 
**position** | **Number** |  | 
**level** | **Number** |  | 
**childrenCount** | **Number** |  | 
**storeId** | **Number** |  | 
**allChildren** | **String** |  | [optional] 
**availableSortBy** | **String** |  | [optional] 
**children** | **String** |  | [optional] 
**customApplyToProducts** | **Number** |  | [optional] 
**customDesign** | **String** |  | [optional] 
**customDesignFrom** | **Date** |  | [optional] 
**customDesignTo** | **Date** |  | [optional] 
**customLayoutUpdate** | **String** |  | [optional] 
**customUseParentSettings** | **Number** |  | [optional] 
**defaultSortBy** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**displayMode** | **String** |  | [optional] 
**filterPriceRange** | **String** |  | [optional] 
**image** | **String** |  | [optional] 
**includeInMenu** | **Number** |  | [optional] 
**isActive** | **Number** |  | [optional] 
**isAnchor** | **Number** |  | [optional] 
**landingPage** | **Number** |  | [optional] 
**metaDescription** | **String** |  | [optional] 
**metaKeywords** | **String** |  | [optional] 
**metaTitle** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**pageLayout** | **String** |  | [optional] 
**pathInStore** | **String** |  | [optional] 
**thumbnail** | **String** |  | [optional] 
**ummCatLabel** | **String** |  | [optional] 
**ummCatTarget** | **String** |  | [optional] 
**ummDdBlocks** | **String** |  | [optional] 
**ummDdColumns** | **Number** |  | [optional] 
**ummDdProportions** | **String** |  | [optional] 
**ummDdType** | **String** |  | [optional] 
**ummDdWidth** | **String** |  | [optional] 
**urlKey** | **String** |  | [optional] 
**urlPath** | **String** |  | [optional] 
**catalogProducts** | **[Object]** |  | [optional] 



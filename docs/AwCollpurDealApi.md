# harpoonApi.AwCollpurDealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealCount**](AwCollpurDealApi.md#awCollpurDealCount) | **GET** /AwCollpurDeals/count | Count instances of the model matched by where from the data source.
[**awCollpurDealCreate**](AwCollpurDealApi.md#awCollpurDealCreate) | **POST** /AwCollpurDeals | Create a new instance of the model and persist it into the data source.
[**awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream) | **GET** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream) | **POST** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealDeleteById**](AwCollpurDealApi.md#awCollpurDealDeleteById) | **DELETE** /AwCollpurDeals/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealExistsGetAwCollpurDealsidExists**](AwCollpurDealApi.md#awCollpurDealExistsGetAwCollpurDealsidExists) | **GET** /AwCollpurDeals/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealExistsHeadAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealExistsHeadAwCollpurDealsid) | **HEAD** /AwCollpurDeals/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealFind**](AwCollpurDealApi.md#awCollpurDealFind) | **GET** /AwCollpurDeals | Find all instances of the model matched by filter from the data source.
[**awCollpurDealFindById**](AwCollpurDealApi.md#awCollpurDealFindById) | **GET** /AwCollpurDeals/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealFindOne**](AwCollpurDealApi.md#awCollpurDealFindOne) | **GET** /AwCollpurDeals/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPrototypeCountAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCountAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/count | Counts awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeCreateAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCreateAwCollpurDealPurchases) | **POST** /AwCollpurDeals/{id}/awCollpurDealPurchases | Creates a new instance in awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDeleteAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDeleteAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases | Deletes all awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Delete a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeFindByIdAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Find a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeGetAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeGetAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases | Queries awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid) | **PATCH** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid) | **PUT** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases) | **PUT** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Update a related item by id for awCollpurDealPurchases.
[**awCollpurDealReplaceById**](AwCollpurDealApi.md#awCollpurDealReplaceById) | **POST** /AwCollpurDeals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealReplaceOrCreate**](AwCollpurDealApi.md#awCollpurDealReplaceOrCreate) | **POST** /AwCollpurDeals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealUpdateAll**](AwCollpurDealApi.md#awCollpurDealUpdateAll) | **POST** /AwCollpurDeals/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealUpsertPatchAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPatchAwCollpurDeals) | **PATCH** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertPutAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPutAwCollpurDeals) | **PUT** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertWithWhere**](AwCollpurDealApi.md#awCollpurDealUpsertWithWhere) | **POST** /AwCollpurDeals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurDealCount"></a>
# **awCollpurDealCount**
> InlineResponse2001 awCollpurDealCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awCollpurDealCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreate"></a>
# **awCollpurDealCreate**
> AwCollpurDeal awCollpurDealCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | Model instance data
};
apiInstance.awCollpurDealCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream"></a>
# **awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**
> File awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream"></a>
# **awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**
> File awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealDeleteById"></a>
# **awCollpurDealDeleteById**
> Object awCollpurDealDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealExistsGetAwCollpurDealsidExists"></a>
# **awCollpurDealExistsGetAwCollpurDealsidExists**
> InlineResponse2003 awCollpurDealExistsGetAwCollpurDealsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealExistsGetAwCollpurDealsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealExistsHeadAwCollpurDealsid"></a>
# **awCollpurDealExistsHeadAwCollpurDealsid**
> InlineResponse2003 awCollpurDealExistsHeadAwCollpurDealsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealExistsHeadAwCollpurDealsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFind"></a>
# **awCollpurDealFind**
> [AwCollpurDeal] awCollpurDealFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurDealFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwCollpurDeal]**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFindById"></a>
# **awCollpurDealFindById**
> AwCollpurDeal awCollpurDealFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awCollpurDealFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFindOne"></a>
# **awCollpurDealFindOne**
> AwCollpurDeal awCollpurDealFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurDealFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeCountAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeCountAwCollpurDealPurchases**
> InlineResponse2001 awCollpurDealPrototypeCountAwCollpurDealPurchases(id, opts)

Counts awCollpurDealPurchases of AwCollpurDeal.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awCollpurDealPrototypeCountAwCollpurDealPurchases(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeCreateAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeCreateAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeCreateAwCollpurDealPurchases(id, opts)

Creates a new instance in awCollpurDealPurchases of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | 
};
apiInstance.awCollpurDealPrototypeCreateAwCollpurDealPurchases(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)|  | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeDeleteAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeDeleteAwCollpurDealPurchases**
> awCollpurDealPrototypeDeleteAwCollpurDealPurchases(id)

Deletes all awCollpurDealPurchases of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

apiInstance.awCollpurDealPrototypeDeleteAwCollpurDealPurchases(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**
> awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases(fk, id)

Delete a related item by id for awCollpurDealPurchases.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases

var id = "id_example"; // String | AwCollpurDeal id

apiInstance.awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases | 
 **id** | **String**| AwCollpurDeal id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeFindByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeFindByIdAwCollpurDealPurchases(fk, id)

Find a related item by id for awCollpurDealPurchases.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases

var id = "id_example"; // String | AwCollpurDeal id

apiInstance.awCollpurDealPrototypeFindByIdAwCollpurDealPurchases(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases | 
 **id** | **String**| AwCollpurDeal id | 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeGetAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeGetAwCollpurDealPurchases**
> [AwCollpurDealPurchases] awCollpurDealPrototypeGetAwCollpurDealPurchases(id, opts)

Queries awCollpurDealPurchases of AwCollpurDeal.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awCollpurDealPrototypeGetAwCollpurDealPurchases(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwCollpurDealPurchases]**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid"></a>
# **awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**
> AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | An object of model property name/value pairs
};
apiInstance.awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid"></a>
# **awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**
> AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | An object of model property name/value pairs
};
apiInstance.awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id | 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases(fk, id, opts)

Update a related item by id for awCollpurDealPurchases.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases

var id = "id_example"; // String | AwCollpurDeal id

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | 
};
apiInstance.awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases | 
 **id** | **String**| AwCollpurDeal id | 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)|  | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealReplaceById"></a>
# **awCollpurDealReplaceById**
> AwCollpurDeal awCollpurDealReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | Model instance data
};
apiInstance.awCollpurDealReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealReplaceOrCreate"></a>
# **awCollpurDealReplaceOrCreate**
> AwCollpurDeal awCollpurDealReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | Model instance data
};
apiInstance.awCollpurDealReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpdateAll"></a>
# **awCollpurDealUpdateAll**
> InlineResponse2002 awCollpurDealUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | An object of model property name/value pairs
};
apiInstance.awCollpurDealUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertPatchAwCollpurDeals"></a>
# **awCollpurDealUpsertPatchAwCollpurDeals**
> AwCollpurDeal awCollpurDealUpsertPatchAwCollpurDeals(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | Model instance data
};
apiInstance.awCollpurDealUpsertPatchAwCollpurDeals(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertPutAwCollpurDeals"></a>
# **awCollpurDealUpsertPutAwCollpurDeals**
> AwCollpurDeal awCollpurDealUpsertPutAwCollpurDeals(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | Model instance data
};
apiInstance.awCollpurDealUpsertPutAwCollpurDeals(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertWithWhere"></a>
# **awCollpurDealUpsertWithWhere**
> AwCollpurDeal awCollpurDealUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | An object of model property name/value pairs
};
apiInstance.awCollpurDealUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


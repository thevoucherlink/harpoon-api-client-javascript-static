# harpoonApi.StripeInvoiceItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceItemCount**](StripeInvoiceItemApi.md#stripeInvoiceItemCount) | **GET** /StripeInvoiceItems/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceItemCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemCreate) | **POST** /StripeInvoiceItems | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream) | **GET** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream) | **POST** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemDeleteById**](StripeInvoiceItemApi.md#stripeInvoiceItemDeleteById) | **DELETE** /StripeInvoiceItems/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsGetStripeInvoiceItemsidExists) | **GET** /StripeInvoiceItems/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceItemExistsHeadStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsHeadStripeInvoiceItemsid) | **HEAD** /StripeInvoiceItems/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceItemFind**](StripeInvoiceItemApi.md#stripeInvoiceItemFind) | **GET** /StripeInvoiceItems | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceItemFindById**](StripeInvoiceItemApi.md#stripeInvoiceItemFindById) | **GET** /StripeInvoiceItems/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceItemFindOne**](StripeInvoiceItemApi.md#stripeInvoiceItemFindOne) | **GET** /StripeInvoiceItems/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid) | **PATCH** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid) | **PUT** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceById**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceById) | **POST** /StripeInvoiceItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceOrCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceOrCreate) | **POST** /StripeInvoiceItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpdateAll**](StripeInvoiceItemApi.md#stripeInvoiceItemUpdateAll) | **POST** /StripeInvoiceItems/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceItemUpsertPatchStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPatchStripeInvoiceItems) | **PATCH** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertPutStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPutStripeInvoiceItems) | **PUT** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertWithWhere**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertWithWhere) | **POST** /StripeInvoiceItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeInvoiceItemCount"></a>
# **stripeInvoiceItemCount**
> InlineResponse2001 stripeInvoiceItemCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripeInvoiceItemCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreate"></a>
# **stripeInvoiceItemCreate**
> StripeInvoiceItem stripeInvoiceItemCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | Model instance data
};
apiInstance.stripeInvoiceItemCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream"></a>
# **stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**
> File stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream"></a>
# **stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**
> File stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemDeleteById"></a>
# **stripeInvoiceItemDeleteById**
> Object stripeInvoiceItemDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceItemDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemExistsGetStripeInvoiceItemsidExists"></a>
# **stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**
> InlineResponse2003 stripeInvoiceItemExistsGetStripeInvoiceItemsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceItemExistsGetStripeInvoiceItemsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemExistsHeadStripeInvoiceItemsid"></a>
# **stripeInvoiceItemExistsHeadStripeInvoiceItemsid**
> InlineResponse2003 stripeInvoiceItemExistsHeadStripeInvoiceItemsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceItemExistsHeadStripeInvoiceItemsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFind"></a>
# **stripeInvoiceItemFind**
> [StripeInvoiceItem] stripeInvoiceItemFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeInvoiceItemFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripeInvoiceItem]**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFindById"></a>
# **stripeInvoiceItemFindById**
> StripeInvoiceItem stripeInvoiceItemFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripeInvoiceItemFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFindOne"></a>
# **stripeInvoiceItemFindOne**
> StripeInvoiceItem stripeInvoiceItemFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeInvoiceItemFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid"></a>
# **stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**
> StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | StripeInvoiceItem id

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | An object of model property name/value pairs
};
apiInstance.stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoiceItem id | 
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid"></a>
# **stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**
> StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | StripeInvoiceItem id

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | An object of model property name/value pairs
};
apiInstance.stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoiceItem id | 
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemReplaceById"></a>
# **stripeInvoiceItemReplaceById**
> StripeInvoiceItem stripeInvoiceItemReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | Model instance data
};
apiInstance.stripeInvoiceItemReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemReplaceOrCreate"></a>
# **stripeInvoiceItemReplaceOrCreate**
> StripeInvoiceItem stripeInvoiceItemReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | Model instance data
};
apiInstance.stripeInvoiceItemReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpdateAll"></a>
# **stripeInvoiceItemUpdateAll**
> InlineResponse2002 stripeInvoiceItemUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | An object of model property name/value pairs
};
apiInstance.stripeInvoiceItemUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertPatchStripeInvoiceItems"></a>
# **stripeInvoiceItemUpsertPatchStripeInvoiceItems**
> StripeInvoiceItem stripeInvoiceItemUpsertPatchStripeInvoiceItems(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | Model instance data
};
apiInstance.stripeInvoiceItemUpsertPatchStripeInvoiceItems(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertPutStripeInvoiceItems"></a>
# **stripeInvoiceItemUpsertPutStripeInvoiceItems**
> StripeInvoiceItem stripeInvoiceItemUpsertPutStripeInvoiceItems(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | Model instance data
};
apiInstance.stripeInvoiceItemUpsertPutStripeInvoiceItems(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertWithWhere"></a>
# **stripeInvoiceItemUpsertWithWhere**
> StripeInvoiceItem stripeInvoiceItemUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceItemApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeInvoiceItem() // StripeInvoiceItem | An object of model property name/value pairs
};
apiInstance.stripeInvoiceItemUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.HarpoonHpublicBrandvenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**brandId** | **Number** |  | 
**name** | **String** |  | 
**address** | **String** |  | [optional] 
**latitude** | **Number** |  | 
**longitude** | **Number** |  | 
**createdAt** | **Date** |  | 
**updatedAt** | **Date** |  | 
**email** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**country** | **String** |  | 



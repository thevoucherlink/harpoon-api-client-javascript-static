# harpoonApi.CustomerRefreshTokenCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **String** | Refresh Token can be found in a logged in and authorized Customer associated to an AuthToken under the property refreshToken | 



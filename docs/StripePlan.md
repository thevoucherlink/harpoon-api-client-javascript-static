# harpoonApi.StripePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** |  | [optional] 
**currency** | **String** |  | [optional] 
**interval** | **String** |  | [optional] 
**intervalCount** | **Number** |  | [optional] 
**metadata** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**statementDescriptor** | **String** |  | [optional] 
**trialPeriodDays** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 



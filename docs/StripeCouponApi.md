# harpoonApi.StripeCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeCouponCount**](StripeCouponApi.md#stripeCouponCount) | **GET** /StripeCoupons/count | Count instances of the model matched by where from the data source.
[**stripeCouponCreate**](StripeCouponApi.md#stripeCouponCreate) | **POST** /StripeCoupons | Create a new instance of the model and persist it into the data source.
[**stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamGetStripeCouponsChangeStream) | **GET** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamPostStripeCouponsChangeStream) | **POST** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponDeleteById**](StripeCouponApi.md#stripeCouponDeleteById) | **DELETE** /StripeCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**stripeCouponExistsGetStripeCouponsidExists**](StripeCouponApi.md#stripeCouponExistsGetStripeCouponsidExists) | **GET** /StripeCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**stripeCouponExistsHeadStripeCouponsid**](StripeCouponApi.md#stripeCouponExistsHeadStripeCouponsid) | **HEAD** /StripeCoupons/{id} | Check whether a model instance exists in the data source.
[**stripeCouponFind**](StripeCouponApi.md#stripeCouponFind) | **GET** /StripeCoupons | Find all instances of the model matched by filter from the data source.
[**stripeCouponFindById**](StripeCouponApi.md#stripeCouponFindById) | **GET** /StripeCoupons/{id} | Find a model instance by {{id}} from the data source.
[**stripeCouponFindOne**](StripeCouponApi.md#stripeCouponFindOne) | **GET** /StripeCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid) | **PATCH** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPutStripeCouponsid) | **PUT** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceById**](StripeCouponApi.md#stripeCouponReplaceById) | **POST** /StripeCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceOrCreate**](StripeCouponApi.md#stripeCouponReplaceOrCreate) | **POST** /StripeCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeCouponUpdateAll**](StripeCouponApi.md#stripeCouponUpdateAll) | **POST** /StripeCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**stripeCouponUpsertPatchStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPatchStripeCoupons) | **PATCH** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertPutStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPutStripeCoupons) | **PUT** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertWithWhere**](StripeCouponApi.md#stripeCouponUpsertWithWhere) | **POST** /StripeCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeCouponCount"></a>
# **stripeCouponCount**
> InlineResponse2001 stripeCouponCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripeCouponCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreate"></a>
# **stripeCouponCreate**
> StripeCoupon stripeCouponCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | Model instance data
};
apiInstance.stripeCouponCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreateChangeStreamGetStripeCouponsChangeStream"></a>
# **stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**
> File stripeCouponCreateChangeStreamGetStripeCouponsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeCouponCreateChangeStreamGetStripeCouponsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreateChangeStreamPostStripeCouponsChangeStream"></a>
# **stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**
> File stripeCouponCreateChangeStreamPostStripeCouponsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeCouponCreateChangeStreamPostStripeCouponsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponDeleteById"></a>
# **stripeCouponDeleteById**
> Object stripeCouponDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | Model id

apiInstance.stripeCouponDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponExistsGetStripeCouponsidExists"></a>
# **stripeCouponExistsGetStripeCouponsidExists**
> InlineResponse2003 stripeCouponExistsGetStripeCouponsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | Model id

apiInstance.stripeCouponExistsGetStripeCouponsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponExistsHeadStripeCouponsid"></a>
# **stripeCouponExistsHeadStripeCouponsid**
> InlineResponse2003 stripeCouponExistsHeadStripeCouponsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | Model id

apiInstance.stripeCouponExistsHeadStripeCouponsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFind"></a>
# **stripeCouponFind**
> [StripeCoupon] stripeCouponFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeCouponFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripeCoupon]**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFindById"></a>
# **stripeCouponFindById**
> StripeCoupon stripeCouponFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripeCouponFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFindOne"></a>
# **stripeCouponFindOne**
> StripeCoupon stripeCouponFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeCouponFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid"></a>
# **stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**
> StripeCoupon stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | StripeCoupon id

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | An object of model property name/value pairs
};
apiInstance.stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeCoupon id | 
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponPrototypeUpdateAttributesPutStripeCouponsid"></a>
# **stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**
> StripeCoupon stripeCouponPrototypeUpdateAttributesPutStripeCouponsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | StripeCoupon id

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | An object of model property name/value pairs
};
apiInstance.stripeCouponPrototypeUpdateAttributesPutStripeCouponsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeCoupon id | 
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponReplaceById"></a>
# **stripeCouponReplaceById**
> StripeCoupon stripeCouponReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | Model instance data
};
apiInstance.stripeCouponReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponReplaceOrCreate"></a>
# **stripeCouponReplaceOrCreate**
> StripeCoupon stripeCouponReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | Model instance data
};
apiInstance.stripeCouponReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpdateAll"></a>
# **stripeCouponUpdateAll**
> InlineResponse2002 stripeCouponUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | An object of model property name/value pairs
};
apiInstance.stripeCouponUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertPatchStripeCoupons"></a>
# **stripeCouponUpsertPatchStripeCoupons**
> StripeCoupon stripeCouponUpsertPatchStripeCoupons(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | Model instance data
};
apiInstance.stripeCouponUpsertPatchStripeCoupons(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertPutStripeCoupons"></a>
# **stripeCouponUpsertPutStripeCoupons**
> StripeCoupon stripeCouponUpsertPutStripeCoupons(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | Model instance data
};
apiInstance.stripeCouponUpsertPutStripeCoupons(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertWithWhere"></a>
# **stripeCouponUpsertWithWhere**
> StripeCoupon stripeCouponUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeCouponApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeCoupon() // StripeCoupon | An object of model property name/value pairs
};
apiInstance.stripeCouponUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.StripePlanApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripePlanCount**](StripePlanApi.md#stripePlanCount) | **GET** /StripePlans/count | Count instances of the model matched by where from the data source.
[**stripePlanCreate**](StripePlanApi.md#stripePlanCreate) | **POST** /StripePlans | Create a new instance of the model and persist it into the data source.
[**stripePlanCreateChangeStreamGetStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamGetStripePlansChangeStream) | **GET** /StripePlans/change-stream | Create a change stream.
[**stripePlanCreateChangeStreamPostStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamPostStripePlansChangeStream) | **POST** /StripePlans/change-stream | Create a change stream.
[**stripePlanDeleteById**](StripePlanApi.md#stripePlanDeleteById) | **DELETE** /StripePlans/{id} | Delete a model instance by {{id}} from the data source.
[**stripePlanExistsGetStripePlansidExists**](StripePlanApi.md#stripePlanExistsGetStripePlansidExists) | **GET** /StripePlans/{id}/exists | Check whether a model instance exists in the data source.
[**stripePlanExistsHeadStripePlansid**](StripePlanApi.md#stripePlanExistsHeadStripePlansid) | **HEAD** /StripePlans/{id} | Check whether a model instance exists in the data source.
[**stripePlanFind**](StripePlanApi.md#stripePlanFind) | **GET** /StripePlans | Find all instances of the model matched by filter from the data source.
[**stripePlanFindById**](StripePlanApi.md#stripePlanFindById) | **GET** /StripePlans/{id} | Find a model instance by {{id}} from the data source.
[**stripePlanFindOne**](StripePlanApi.md#stripePlanFindOne) | **GET** /StripePlans/findOne | Find first instance of the model matched by filter from the data source.
[**stripePlanPrototypeUpdateAttributesPatchStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPatchStripePlansid) | **PATCH** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanPrototypeUpdateAttributesPutStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPutStripePlansid) | **PUT** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanReplaceById**](StripePlanApi.md#stripePlanReplaceById) | **POST** /StripePlans/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripePlanReplaceOrCreate**](StripePlanApi.md#stripePlanReplaceOrCreate) | **POST** /StripePlans/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripePlanUpdateAll**](StripePlanApi.md#stripePlanUpdateAll) | **POST** /StripePlans/update | Update instances of the model matched by {{where}} from the data source.
[**stripePlanUpsertPatchStripePlans**](StripePlanApi.md#stripePlanUpsertPatchStripePlans) | **PATCH** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertPutStripePlans**](StripePlanApi.md#stripePlanUpsertPutStripePlans) | **PUT** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertWithWhere**](StripePlanApi.md#stripePlanUpsertWithWhere) | **POST** /StripePlans/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripePlanCount"></a>
# **stripePlanCount**
> InlineResponse2001 stripePlanCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripePlanCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreate"></a>
# **stripePlanCreate**
> StripePlan stripePlanCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | Model instance data
};
apiInstance.stripePlanCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreateChangeStreamGetStripePlansChangeStream"></a>
# **stripePlanCreateChangeStreamGetStripePlansChangeStream**
> File stripePlanCreateChangeStreamGetStripePlansChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripePlanCreateChangeStreamGetStripePlansChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreateChangeStreamPostStripePlansChangeStream"></a>
# **stripePlanCreateChangeStreamPostStripePlansChangeStream**
> File stripePlanCreateChangeStreamPostStripePlansChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripePlanCreateChangeStreamPostStripePlansChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanDeleteById"></a>
# **stripePlanDeleteById**
> Object stripePlanDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | Model id

apiInstance.stripePlanDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanExistsGetStripePlansidExists"></a>
# **stripePlanExistsGetStripePlansidExists**
> InlineResponse2003 stripePlanExistsGetStripePlansidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | Model id

apiInstance.stripePlanExistsGetStripePlansidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanExistsHeadStripePlansid"></a>
# **stripePlanExistsHeadStripePlansid**
> InlineResponse2003 stripePlanExistsHeadStripePlansid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | Model id

apiInstance.stripePlanExistsHeadStripePlansid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFind"></a>
# **stripePlanFind**
> [StripePlan] stripePlanFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripePlanFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripePlan]**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFindById"></a>
# **stripePlanFindById**
> StripePlan stripePlanFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripePlanFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFindOne"></a>
# **stripePlanFindOne**
> StripePlan stripePlanFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripePlanFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanPrototypeUpdateAttributesPatchStripePlansid"></a>
# **stripePlanPrototypeUpdateAttributesPatchStripePlansid**
> StripePlan stripePlanPrototypeUpdateAttributesPatchStripePlansid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | StripePlan id

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | An object of model property name/value pairs
};
apiInstance.stripePlanPrototypeUpdateAttributesPatchStripePlansid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripePlan id | 
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanPrototypeUpdateAttributesPutStripePlansid"></a>
# **stripePlanPrototypeUpdateAttributesPutStripePlansid**
> StripePlan stripePlanPrototypeUpdateAttributesPutStripePlansid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | StripePlan id

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | An object of model property name/value pairs
};
apiInstance.stripePlanPrototypeUpdateAttributesPutStripePlansid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripePlan id | 
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanReplaceById"></a>
# **stripePlanReplaceById**
> StripePlan stripePlanReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | Model instance data
};
apiInstance.stripePlanReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanReplaceOrCreate"></a>
# **stripePlanReplaceOrCreate**
> StripePlan stripePlanReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | Model instance data
};
apiInstance.stripePlanReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpdateAll"></a>
# **stripePlanUpdateAll**
> InlineResponse2002 stripePlanUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripePlan() // StripePlan | An object of model property name/value pairs
};
apiInstance.stripePlanUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertPatchStripePlans"></a>
# **stripePlanUpsertPatchStripePlans**
> StripePlan stripePlanUpsertPatchStripePlans(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | Model instance data
};
apiInstance.stripePlanUpsertPatchStripePlans(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertPutStripePlans"></a>
# **stripePlanUpsertPutStripePlans**
> StripePlan stripePlanUpsertPutStripePlans(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'data': new harpoonApi.StripePlan() // StripePlan | Model instance data
};
apiInstance.stripePlanUpsertPutStripePlans(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertWithWhere"></a>
# **stripePlanUpsertWithWhere**
> StripePlan stripePlanUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripePlanApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripePlan() // StripePlan | An object of model property name/value pairs
};
apiInstance.stripePlanUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


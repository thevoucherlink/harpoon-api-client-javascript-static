# harpoonApi.StripeDiscountApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeDiscountCount**](StripeDiscountApi.md#stripeDiscountCount) | **GET** /StripeDiscounts/count | Count instances of the model matched by where from the data source.
[**stripeDiscountCreate**](StripeDiscountApi.md#stripeDiscountCreate) | **POST** /StripeDiscounts | Create a new instance of the model and persist it into the data source.
[**stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream) | **GET** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream) | **POST** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountDeleteById**](StripeDiscountApi.md#stripeDiscountDeleteById) | **DELETE** /StripeDiscounts/{id} | Delete a model instance by {{id}} from the data source.
[**stripeDiscountExistsGetStripeDiscountsidExists**](StripeDiscountApi.md#stripeDiscountExistsGetStripeDiscountsidExists) | **GET** /StripeDiscounts/{id}/exists | Check whether a model instance exists in the data source.
[**stripeDiscountExistsHeadStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountExistsHeadStripeDiscountsid) | **HEAD** /StripeDiscounts/{id} | Check whether a model instance exists in the data source.
[**stripeDiscountFind**](StripeDiscountApi.md#stripeDiscountFind) | **GET** /StripeDiscounts | Find all instances of the model matched by filter from the data source.
[**stripeDiscountFindById**](StripeDiscountApi.md#stripeDiscountFindById) | **GET** /StripeDiscounts/{id} | Find a model instance by {{id}} from the data source.
[**stripeDiscountFindOne**](StripeDiscountApi.md#stripeDiscountFindOne) | **GET** /StripeDiscounts/findOne | Find first instance of the model matched by filter from the data source.
[**stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid) | **PATCH** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid) | **PUT** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceById**](StripeDiscountApi.md#stripeDiscountReplaceById) | **POST** /StripeDiscounts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceOrCreate**](StripeDiscountApi.md#stripeDiscountReplaceOrCreate) | **POST** /StripeDiscounts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeDiscountUpdateAll**](StripeDiscountApi.md#stripeDiscountUpdateAll) | **POST** /StripeDiscounts/update | Update instances of the model matched by {{where}} from the data source.
[**stripeDiscountUpsertPatchStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPatchStripeDiscounts) | **PATCH** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertPutStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPutStripeDiscounts) | **PUT** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertWithWhere**](StripeDiscountApi.md#stripeDiscountUpsertWithWhere) | **POST** /StripeDiscounts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeDiscountCount"></a>
# **stripeDiscountCount**
> InlineResponse2001 stripeDiscountCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripeDiscountCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreate"></a>
# **stripeDiscountCreate**
> StripeDiscount stripeDiscountCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | Model instance data
};
apiInstance.stripeDiscountCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream"></a>
# **stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**
> File stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream"></a>
# **stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**
> File stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountDeleteById"></a>
# **stripeDiscountDeleteById**
> Object stripeDiscountDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | Model id

apiInstance.stripeDiscountDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountExistsGetStripeDiscountsidExists"></a>
# **stripeDiscountExistsGetStripeDiscountsidExists**
> InlineResponse2003 stripeDiscountExistsGetStripeDiscountsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | Model id

apiInstance.stripeDiscountExistsGetStripeDiscountsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountExistsHeadStripeDiscountsid"></a>
# **stripeDiscountExistsHeadStripeDiscountsid**
> InlineResponse2003 stripeDiscountExistsHeadStripeDiscountsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | Model id

apiInstance.stripeDiscountExistsHeadStripeDiscountsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFind"></a>
# **stripeDiscountFind**
> [StripeDiscount] stripeDiscountFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeDiscountFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripeDiscount]**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFindById"></a>
# **stripeDiscountFindById**
> StripeDiscount stripeDiscountFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripeDiscountFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFindOne"></a>
# **stripeDiscountFindOne**
> StripeDiscount stripeDiscountFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeDiscountFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid"></a>
# **stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**
> StripeDiscount stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | StripeDiscount id

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | An object of model property name/value pairs
};
apiInstance.stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeDiscount id | 
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid"></a>
# **stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**
> StripeDiscount stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | StripeDiscount id

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | An object of model property name/value pairs
};
apiInstance.stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeDiscount id | 
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountReplaceById"></a>
# **stripeDiscountReplaceById**
> StripeDiscount stripeDiscountReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | Model instance data
};
apiInstance.stripeDiscountReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountReplaceOrCreate"></a>
# **stripeDiscountReplaceOrCreate**
> StripeDiscount stripeDiscountReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | Model instance data
};
apiInstance.stripeDiscountReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpdateAll"></a>
# **stripeDiscountUpdateAll**
> InlineResponse2002 stripeDiscountUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | An object of model property name/value pairs
};
apiInstance.stripeDiscountUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertPatchStripeDiscounts"></a>
# **stripeDiscountUpsertPatchStripeDiscounts**
> StripeDiscount stripeDiscountUpsertPatchStripeDiscounts(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | Model instance data
};
apiInstance.stripeDiscountUpsertPatchStripeDiscounts(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertPutStripeDiscounts"></a>
# **stripeDiscountUpsertPutStripeDiscounts**
> StripeDiscount stripeDiscountUpsertPutStripeDiscounts(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | Model instance data
};
apiInstance.stripeDiscountUpsertPutStripeDiscounts(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertWithWhere"></a>
# **stripeDiscountUpsertWithWhere**
> StripeDiscount stripeDiscountUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeDiscountApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeDiscount() // StripeDiscount | An object of model property name/value pairs
};
apiInstance.stripeDiscountUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


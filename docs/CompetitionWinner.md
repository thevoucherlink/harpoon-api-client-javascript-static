# harpoonApi.CompetitionWinner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**chosenAt** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 



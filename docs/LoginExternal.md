# harpoonApi.LoginExternal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  | 
**version** | **String** |  | [optional] 
**requestUrl** | **String** |  | 
**authorizeUrl** | **String** |  | 
**accessUrl** | **String** |  | 



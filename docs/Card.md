# harpoonApi.Card

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | 
**type** | **String** |  | [optional] 
**funding** | **String** |  | [optional] 
**lastDigits** | **String** |  | [optional] 
**exMonth** | **String** |  | 
**exYear** | **String** |  | 
**cardholderName** | **String** |  | 



# harpoonApi.RadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowCount**](RadioShowApi.md#radioShowCount) | **GET** /RadioShows/count | Count instances of the model matched by where from the data source.
[**radioShowCreate**](RadioShowApi.md#radioShowCreate) | **POST** /RadioShows | Create a new instance of the model and persist it into the data source.
[**radioShowCreateChangeStreamGetRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamGetRadioShowsChangeStream) | **GET** /RadioShows/change-stream | Create a change stream.
[**radioShowCreateChangeStreamPostRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamPostRadioShowsChangeStream) | **POST** /RadioShows/change-stream | Create a change stream.
[**radioShowDeleteById**](RadioShowApi.md#radioShowDeleteById) | **DELETE** /RadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowExistsGetRadioShowsidExists**](RadioShowApi.md#radioShowExistsGetRadioShowsidExists) | **GET** /RadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowExistsHeadRadioShowsid**](RadioShowApi.md#radioShowExistsHeadRadioShowsid) | **HEAD** /RadioShows/{id} | Check whether a model instance exists in the data source.
[**radioShowFind**](RadioShowApi.md#radioShowFind) | **GET** /RadioShows | Find all instances of the model matched by filter from the data source.
[**radioShowFindById**](RadioShowApi.md#radioShowFindById) | **GET** /RadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioShowFindOne**](RadioShowApi.md#radioShowFindOne) | **GET** /RadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowPrototypeCountRadioPresenters**](RadioShowApi.md#radioShowPrototypeCountRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/count | Counts radioPresenters of RadioShow.
[**radioShowPrototypeCountRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCountRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/count | Counts radioShowTimes of RadioShow.
[**radioShowPrototypeCreateRadioPresenters**](RadioShowApi.md#radioShowPrototypeCreateRadioPresenters) | **POST** /RadioShows/{id}/radioPresenters | Creates a new instance in radioPresenters of this model.
[**radioShowPrototypeCreateRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCreateRadioShowTimes) | **POST** /RadioShows/{id}/radioShowTimes | Creates a new instance in radioShowTimes of this model.
[**radioShowPrototypeDeleteRadioPresenters**](RadioShowApi.md#radioShowPrototypeDeleteRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters | Deletes all radioPresenters of this model.
[**radioShowPrototypeDeleteRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDeleteRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes | Deletes all radioShowTimes of this model.
[**radioShowPrototypeDestroyByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/{fk} | Delete a related item by id for radioPresenters.
[**radioShowPrototypeDestroyByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes/{fk} | Delete a related item by id for radioShowTimes.
[**radioShowPrototypeExistsRadioPresenters**](RadioShowApi.md#radioShowPrototypeExistsRadioPresenters) | **HEAD** /RadioShows/{id}/radioPresenters/rel/{fk} | Check the existence of radioPresenters relation to an item by id.
[**radioShowPrototypeFindByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeFindByIdRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/{fk} | Find a related item by id for radioPresenters.
[**radioShowPrototypeFindByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeFindByIdRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/{fk} | Find a related item by id for radioShowTimes.
[**radioShowPrototypeGetPlaylistItem**](RadioShowApi.md#radioShowPrototypeGetPlaylistItem) | **GET** /RadioShows/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**radioShowPrototypeGetRadioPresenters**](RadioShowApi.md#radioShowPrototypeGetRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters | Queries radioPresenters of RadioShow.
[**radioShowPrototypeGetRadioShowTimes**](RadioShowApi.md#radioShowPrototypeGetRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes | Queries radioShowTimes of RadioShow.
[**radioShowPrototypeGetRadioStream**](RadioShowApi.md#radioShowPrototypeGetRadioStream) | **GET** /RadioShows/{id}/radioStream | Fetches belongsTo relation radioStream.
[**radioShowPrototypeLinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeLinkRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/rel/{fk} | Add a related item by id for radioPresenters.
[**radioShowPrototypeUnlinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeUnlinkRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/rel/{fk} | Remove the radioPresenters relation to an item by id.
[**radioShowPrototypeUpdateAttributesPatchRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPatchRadioShowsid) | **PATCH** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateAttributesPutRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPutRadioShowsid) | **PUT** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/{fk} | Update a related item by id for radioPresenters.
[**radioShowPrototypeUpdateByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioShowTimes) | **PUT** /RadioShows/{id}/radioShowTimes/{fk} | Update a related item by id for radioShowTimes.
[**radioShowReplaceById**](RadioShowApi.md#radioShowReplaceById) | **POST** /RadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowReplaceOrCreate**](RadioShowApi.md#radioShowReplaceOrCreate) | **POST** /RadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowUpdateAll**](RadioShowApi.md#radioShowUpdateAll) | **POST** /RadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowUploadFile**](RadioShowApi.md#radioShowUploadFile) | **POST** /RadioShows/{id}/file | Upload File to Radio Show
[**radioShowUpsertPatchRadioShows**](RadioShowApi.md#radioShowUpsertPatchRadioShows) | **PATCH** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertPutRadioShows**](RadioShowApi.md#radioShowUpsertPutRadioShows) | **PUT** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertWithWhere**](RadioShowApi.md#radioShowUpsertWithWhere) | **POST** /RadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioShowCount"></a>
# **radioShowCount**
> InlineResponse2001 radioShowCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioShowCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreate"></a>
# **radioShowCreate**
> RadioShow radioShowCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | Model instance data
};
apiInstance.radioShowCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreateChangeStreamGetRadioShowsChangeStream"></a>
# **radioShowCreateChangeStreamGetRadioShowsChangeStream**
> File radioShowCreateChangeStreamGetRadioShowsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioShowCreateChangeStreamGetRadioShowsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreateChangeStreamPostRadioShowsChangeStream"></a>
# **radioShowCreateChangeStreamPostRadioShowsChangeStream**
> File radioShowCreateChangeStreamPostRadioShowsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioShowCreateChangeStreamPostRadioShowsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowDeleteById"></a>
# **radioShowDeleteById**
> Object radioShowDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowExistsGetRadioShowsidExists"></a>
# **radioShowExistsGetRadioShowsidExists**
> InlineResponse2003 radioShowExistsGetRadioShowsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowExistsGetRadioShowsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowExistsHeadRadioShowsid"></a>
# **radioShowExistsHeadRadioShowsid**
> InlineResponse2003 radioShowExistsHeadRadioShowsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowExistsHeadRadioShowsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFind"></a>
# **radioShowFind**
> [RadioShow] radioShowFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioShowFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioShow]**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFindById"></a>
# **radioShowFindById**
> RadioShow radioShowFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioShowFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFindOne"></a>
# **radioShowFindOne**
> RadioShow radioShowFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioShowFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCountRadioPresenters"></a>
# **radioShowPrototypeCountRadioPresenters**
> InlineResponse2001 radioShowPrototypeCountRadioPresenters(id, opts)

Counts radioPresenters of RadioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioShowPrototypeCountRadioPresenters(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCountRadioShowTimes"></a>
# **radioShowPrototypeCountRadioShowTimes**
> InlineResponse2001 radioShowPrototypeCountRadioShowTimes(id, opts)

Counts radioShowTimes of RadioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioShowPrototypeCountRadioShowTimes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCreateRadioPresenters"></a>
# **radioShowPrototypeCreateRadioPresenters**
> RadioPresenter radioShowPrototypeCreateRadioPresenters(id, opts)

Creates a new instance in radioPresenters of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | 
};
apiInstance.radioShowPrototypeCreateRadioPresenters(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **data** | [**RadioPresenter**](RadioPresenter.md)|  | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCreateRadioShowTimes"></a>
# **radioShowPrototypeCreateRadioShowTimes**
> RadioShowTime radioShowPrototypeCreateRadioShowTimes(id, opts)

Creates a new instance in radioShowTimes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | 
};
apiInstance.radioShowPrototypeCreateRadioShowTimes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **data** | [**RadioShowTime**](RadioShowTime.md)|  | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDeleteRadioPresenters"></a>
# **radioShowPrototypeDeleteRadioPresenters**
> radioShowPrototypeDeleteRadioPresenters(id)

Deletes all radioPresenters of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeDeleteRadioPresenters(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDeleteRadioShowTimes"></a>
# **radioShowPrototypeDeleteRadioShowTimes**
> radioShowPrototypeDeleteRadioShowTimes(id)

Deletes all radioShowTimes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeDeleteRadioShowTimes(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDestroyByIdRadioPresenters"></a>
# **radioShowPrototypeDestroyByIdRadioPresenters**
> radioShowPrototypeDestroyByIdRadioPresenters(fk, id)

Delete a related item by id for radioPresenters.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeDestroyByIdRadioPresenters(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDestroyByIdRadioShowTimes"></a>
# **radioShowPrototypeDestroyByIdRadioShowTimes**
> radioShowPrototypeDestroyByIdRadioShowTimes(fk, id)

Delete a related item by id for radioShowTimes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioShowTimes

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeDestroyByIdRadioShowTimes(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes | 
 **id** | **String**| RadioShow id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeExistsRadioPresenters"></a>
# **radioShowPrototypeExistsRadioPresenters**
> &#39;Boolean&#39; radioShowPrototypeExistsRadioPresenters(fk, id)

Check the existence of radioPresenters relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeExistsRadioPresenters(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeFindByIdRadioPresenters"></a>
# **radioShowPrototypeFindByIdRadioPresenters**
> RadioPresenter radioShowPrototypeFindByIdRadioPresenters(fk, id)

Find a related item by id for radioPresenters.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeFindByIdRadioPresenters(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeFindByIdRadioShowTimes"></a>
# **radioShowPrototypeFindByIdRadioShowTimes**
> RadioShowTime radioShowPrototypeFindByIdRadioShowTimes(fk, id)

Find a related item by id for radioShowTimes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioShowTimes

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeFindByIdRadioShowTimes(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes | 
 **id** | **String**| RadioShow id | 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetPlaylistItem"></a>
# **radioShowPrototypeGetPlaylistItem**
> PlaylistItem radioShowPrototypeGetPlaylistItem(id, opts)

Fetches belongsTo relation playlistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.radioShowPrototypeGetPlaylistItem(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioPresenters"></a>
# **radioShowPrototypeGetRadioPresenters**
> [RadioPresenter] radioShowPrototypeGetRadioPresenters(id, opts)

Queries radioPresenters of RadioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.radioShowPrototypeGetRadioPresenters(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioPresenter]**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioShowTimes"></a>
# **radioShowPrototypeGetRadioShowTimes**
> [RadioShowTime] radioShowPrototypeGetRadioShowTimes(id, opts)

Queries radioShowTimes of RadioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.radioShowPrototypeGetRadioShowTimes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioShowTime]**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioStream"></a>
# **radioShowPrototypeGetRadioStream**
> RadioStream radioShowPrototypeGetRadioStream(id, opts)

Fetches belongsTo relation radioStream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.radioShowPrototypeGetRadioStream(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeLinkRadioPresenters"></a>
# **radioShowPrototypeLinkRadioPresenters**
> RadioPresenterRadioShow radioShowPrototypeLinkRadioPresenters(fk, id, opts)

Add a related item by id for radioPresenters.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | 
};
apiInstance.radioShowPrototypeLinkRadioPresenters(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)|  | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUnlinkRadioPresenters"></a>
# **radioShowPrototypeUnlinkRadioPresenters**
> radioShowPrototypeUnlinkRadioPresenters(fk, id)

Remove the radioPresenters relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

apiInstance.radioShowPrototypeUnlinkRadioPresenters(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateAttributesPatchRadioShowsid"></a>
# **radioShowPrototypeUpdateAttributesPatchRadioShowsid**
> RadioShow radioShowPrototypeUpdateAttributesPatchRadioShowsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | An object of model property name/value pairs
};
apiInstance.radioShowPrototypeUpdateAttributesPatchRadioShowsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateAttributesPutRadioShowsid"></a>
# **radioShowPrototypeUpdateAttributesPutRadioShowsid**
> RadioShow radioShowPrototypeUpdateAttributesPutRadioShowsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | An object of model property name/value pairs
};
apiInstance.radioShowPrototypeUpdateAttributesPutRadioShowsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id | 
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateByIdRadioPresenters"></a>
# **radioShowPrototypeUpdateByIdRadioPresenters**
> RadioPresenter radioShowPrototypeUpdateByIdRadioPresenters(fk, id, opts)

Update a related item by id for radioPresenters.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioPresenters

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | 
};
apiInstance.radioShowPrototypeUpdateByIdRadioPresenters(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters | 
 **id** | **String**| RadioShow id | 
 **data** | [**RadioPresenter**](RadioPresenter.md)|  | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateByIdRadioShowTimes"></a>
# **radioShowPrototypeUpdateByIdRadioShowTimes**
> RadioShowTime radioShowPrototypeUpdateByIdRadioShowTimes(fk, id, opts)

Update a related item by id for radioShowTimes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var fk = "fk_example"; // String | Foreign key for radioShowTimes

var id = "id_example"; // String | RadioShow id

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | 
};
apiInstance.radioShowPrototypeUpdateByIdRadioShowTimes(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes | 
 **id** | **String**| RadioShow id | 
 **data** | [**RadioShowTime**](RadioShowTime.md)|  | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowReplaceById"></a>
# **radioShowReplaceById**
> RadioShow radioShowReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | Model instance data
};
apiInstance.radioShowReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowReplaceOrCreate"></a>
# **radioShowReplaceOrCreate**
> RadioShow radioShowReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | Model instance data
};
apiInstance.radioShowReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpdateAll"></a>
# **radioShowUpdateAll**
> InlineResponse2002 radioShowUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioShow() // RadioShow | An object of model property name/value pairs
};
apiInstance.radioShowUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUploadFile"></a>
# **radioShowUploadFile**
> MagentoFileUpload radioShowUploadFile(id, opts)

Upload File to Radio Show

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var id = "id_example"; // String | Radio Show Id

var opts = { 
  'data': new harpoonApi.MagentoFileUpload() // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object
};
apiInstance.radioShowUploadFile(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Radio Show Id | 
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object | [optional] 

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertPatchRadioShows"></a>
# **radioShowUpsertPatchRadioShows**
> RadioShow radioShowUpsertPatchRadioShows(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | Model instance data
};
apiInstance.radioShowUpsertPatchRadioShows(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertPutRadioShows"></a>
# **radioShowUpsertPutRadioShows**
> RadioShow radioShowUpsertPutRadioShows(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | Model instance data
};
apiInstance.radioShowUpsertPutRadioShows(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertWithWhere"></a>
# **radioShowUpsertWithWhere**
> RadioShow radioShowUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioShow() // RadioShow | An object of model property name/value pairs
};
apiInstance.radioShowUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.AwEventbookingOrderHistoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingOrderHistoryCount**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCount) | **GET** /AwEventbookingOrderHistories/count | Count instances of the model matched by where from the data source.
[**awEventbookingOrderHistoryCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreate) | **POST** /AwEventbookingOrderHistories | Create a new instance of the model and persist it into the data source.
[**awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream) | **GET** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream) | **POST** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryDeleteById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryDeleteById) | **DELETE** /AwEventbookingOrderHistories/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists) | **GET** /AwEventbookingOrderHistories/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid) | **HEAD** /AwEventbookingOrderHistories/{id} | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryFind**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFind) | **GET** /AwEventbookingOrderHistories | Find all instances of the model matched by filter from the data source.
[**awEventbookingOrderHistoryFindById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindById) | **GET** /AwEventbookingOrderHistories/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryFindOne**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindOne) | **GET** /AwEventbookingOrderHistories/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid) | **PATCH** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid) | **PUT** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceById) | **POST** /AwEventbookingOrderHistories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceOrCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceOrCreate) | **POST** /AwEventbookingOrderHistories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpdateAll**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpdateAll) | **POST** /AwEventbookingOrderHistories/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories) | **PATCH** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories) | **PUT** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertWithWhere**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertWithWhere) | **POST** /AwEventbookingOrderHistories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingOrderHistoryCount"></a>
# **awEventbookingOrderHistoryCount**
> InlineResponse2001 awEventbookingOrderHistoryCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingOrderHistoryCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreate"></a>
# **awEventbookingOrderHistoryCreate**
> AwEventbookingOrderHistory awEventbookingOrderHistoryCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | Model instance data
};
apiInstance.awEventbookingOrderHistoryCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream"></a>
# **awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**
> File awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream"></a>
# **awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**
> File awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryDeleteById"></a>
# **awEventbookingOrderHistoryDeleteById**
> Object awEventbookingOrderHistoryDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingOrderHistoryDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists"></a>
# **awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**
> InlineResponse2003 awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**
> InlineResponse2003 awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFind"></a>
# **awEventbookingOrderHistoryFind**
> [AwEventbookingOrderHistory] awEventbookingOrderHistoryFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingOrderHistoryFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwEventbookingOrderHistory]**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFindById"></a>
# **awEventbookingOrderHistoryFindById**
> AwEventbookingOrderHistory awEventbookingOrderHistoryFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awEventbookingOrderHistoryFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFindOne"></a>
# **awEventbookingOrderHistoryFindOne**
> AwEventbookingOrderHistory awEventbookingOrderHistoryFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingOrderHistoryFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**
> AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | AwEventbookingOrderHistory id

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | An object of model property name/value pairs
};
apiInstance.awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingOrderHistory id | 
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**
> AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | AwEventbookingOrderHistory id

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | An object of model property name/value pairs
};
apiInstance.awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingOrderHistory id | 
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryReplaceById"></a>
# **awEventbookingOrderHistoryReplaceById**
> AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | Model instance data
};
apiInstance.awEventbookingOrderHistoryReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryReplaceOrCreate"></a>
# **awEventbookingOrderHistoryReplaceOrCreate**
> AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | Model instance data
};
apiInstance.awEventbookingOrderHistoryReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpdateAll"></a>
# **awEventbookingOrderHistoryUpdateAll**
> InlineResponse2002 awEventbookingOrderHistoryUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | An object of model property name/value pairs
};
apiInstance.awEventbookingOrderHistoryUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories"></a>
# **awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | Model instance data
};
apiInstance.awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories"></a>
# **awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | Model instance data
};
apiInstance.awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertWithWhere"></a>
# **awEventbookingOrderHistoryUpsertWithWhere**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingOrderHistoryApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingOrderHistory() // AwEventbookingOrderHistory | An object of model property name/value pairs
};
apiInstance.awEventbookingOrderHistoryUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.CustomerBadge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | **Number** |  | [optional] [default to 0.0]
**event** | **Number** |  | [optional] [default to 0.0]
**dealAll** | **Number** |  | [optional] [default to 0.0]
**dealCoupon** | **Number** |  | [optional] [default to 0.0]
**dealSimple** | **Number** |  | [optional] [default to 0.0]
**dealGroup** | **Number** |  | [optional] [default to 0.0]
**competition** | **Number** |  | [optional] [default to 0.0]
**walletOfferAvailable** | **Number** |  | [optional] [default to 0.0]
**walletOfferNew** | **Number** |  | [optional] [default to 0.0]
**walletOfferExpiring** | **Number** |  | [optional] [default to 0.0]
**walletOfferExpired** | **Number** |  | [optional] [default to 0.0]
**notificationUnread** | **Number** |  | [optional] [default to 0.0]
**brandFeed** | **Number** |  | [optional] [default to 0.0]
**brand** | **Number** |  | [optional] [default to 0.0]
**brandActivity** | **Number** |  | [optional] [default to 0.0]
**all** | **Number** |  | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 



# harpoonApi.RadioPresenterApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterCount**](RadioPresenterApi.md#radioPresenterCount) | **GET** /RadioPresenters/count | Count instances of the model matched by where from the data source.
[**radioPresenterCreate**](RadioPresenterApi.md#radioPresenterCreate) | **POST** /RadioPresenters | Create a new instance of the model and persist it into the data source.
[**radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamGetRadioPresentersChangeStream) | **GET** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamPostRadioPresentersChangeStream) | **POST** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterDeleteById**](RadioPresenterApi.md#radioPresenterDeleteById) | **DELETE** /RadioPresenters/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterExistsGetRadioPresentersidExists**](RadioPresenterApi.md#radioPresenterExistsGetRadioPresentersidExists) | **GET** /RadioPresenters/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterExistsHeadRadioPresentersid**](RadioPresenterApi.md#radioPresenterExistsHeadRadioPresentersid) | **HEAD** /RadioPresenters/{id} | Check whether a model instance exists in the data source.
[**radioPresenterFind**](RadioPresenterApi.md#radioPresenterFind) | **GET** /RadioPresenters | Find all instances of the model matched by filter from the data source.
[**radioPresenterFindById**](RadioPresenterApi.md#radioPresenterFindById) | **GET** /RadioPresenters/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterFindOne**](RadioPresenterApi.md#radioPresenterFindOne) | **GET** /RadioPresenters/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterPrototypeCountRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCountRadioShows) | **GET** /RadioPresenters/{id}/radioShows/count | Counts radioShows of RadioPresenter.
[**radioPresenterPrototypeCreateRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCreateRadioShows) | **POST** /RadioPresenters/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioPresenterPrototypeDeleteRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDeleteRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows | Deletes all radioShows of this model.
[**radioPresenterPrototypeDestroyByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDestroyByIdRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioPresenterPrototypeExistsRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeExistsRadioShows) | **HEAD** /RadioPresenters/{id}/radioShows/rel/{fk} | Check the existence of radioShows relation to an item by id.
[**radioPresenterPrototypeFindByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeFindByIdRadioShows) | **GET** /RadioPresenters/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioPresenterPrototypeGetRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeGetRadioShows) | **GET** /RadioPresenters/{id}/radioShows | Queries radioShows of RadioPresenter.
[**radioPresenterPrototypeLinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeLinkRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/rel/{fk} | Add a related item by id for radioShows.
[**radioPresenterPrototypeUnlinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUnlinkRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/rel/{fk} | Remove the radioShows relation to an item by id.
[**radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid) | **PATCH** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPutRadioPresentersid) | **PUT** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUpdateByIdRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioPresenterReplaceById**](RadioPresenterApi.md#radioPresenterReplaceById) | **POST** /RadioPresenters/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterReplaceOrCreate**](RadioPresenterApi.md#radioPresenterReplaceOrCreate) | **POST** /RadioPresenters/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterUpdateAll**](RadioPresenterApi.md#radioPresenterUpdateAll) | **POST** /RadioPresenters/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterUploadImage**](RadioPresenterApi.md#radioPresenterUploadImage) | **POST** /RadioPresenters/{id}/file/image | 
[**radioPresenterUpsertPatchRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPatchRadioPresenters) | **PATCH** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertPutRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPutRadioPresenters) | **PUT** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertWithWhere**](RadioPresenterApi.md#radioPresenterUpsertWithWhere) | **POST** /RadioPresenters/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPresenterCount"></a>
# **radioPresenterCount**
> InlineResponse2001 radioPresenterCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioPresenterCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreate"></a>
# **radioPresenterCreate**
> RadioPresenter radioPresenterCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | Model instance data
};
apiInstance.radioPresenterCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreateChangeStreamGetRadioPresentersChangeStream"></a>
# **radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**
> File radioPresenterCreateChangeStreamGetRadioPresentersChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPresenterCreateChangeStreamGetRadioPresentersChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreateChangeStreamPostRadioPresentersChangeStream"></a>
# **radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**
> File radioPresenterCreateChangeStreamPostRadioPresentersChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPresenterCreateChangeStreamPostRadioPresentersChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterDeleteById"></a>
# **radioPresenterDeleteById**
> Object radioPresenterDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterExistsGetRadioPresentersidExists"></a>
# **radioPresenterExistsGetRadioPresentersidExists**
> InlineResponse2003 radioPresenterExistsGetRadioPresentersidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterExistsGetRadioPresentersidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterExistsHeadRadioPresentersid"></a>
# **radioPresenterExistsHeadRadioPresentersid**
> InlineResponse2003 radioPresenterExistsHeadRadioPresentersid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterExistsHeadRadioPresentersid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFind"></a>
# **radioPresenterFind**
> [RadioPresenter] radioPresenterFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPresenterFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioPresenter]**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFindById"></a>
# **radioPresenterFindById**
> RadioPresenter radioPresenterFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioPresenterFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFindOne"></a>
# **radioPresenterFindOne**
> RadioPresenter radioPresenterFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPresenterFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeCountRadioShows"></a>
# **radioPresenterPrototypeCountRadioShows**
> InlineResponse2001 radioPresenterPrototypeCountRadioShows(id, opts)

Counts radioShows of RadioPresenter.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioPresenterPrototypeCountRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeCreateRadioShows"></a>
# **radioPresenterPrototypeCreateRadioShows**
> RadioShow radioPresenterPrototypeCreateRadioShows(id, opts)

Creates a new instance in radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.radioPresenterPrototypeCreateRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeDeleteRadioShows"></a>
# **radioPresenterPrototypeDeleteRadioShows**
> radioPresenterPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

apiInstance.radioPresenterPrototypeDeleteRadioShows(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeDestroyByIdRadioShows"></a>
# **radioPresenterPrototypeDestroyByIdRadioShows**
> radioPresenterPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

apiInstance.radioPresenterPrototypeDestroyByIdRadioShows(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeExistsRadioShows"></a>
# **radioPresenterPrototypeExistsRadioShows**
> &#39;Boolean&#39; radioPresenterPrototypeExistsRadioShows(fk, id)

Check the existence of radioShows relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

apiInstance.radioPresenterPrototypeExistsRadioShows(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeFindByIdRadioShows"></a>
# **radioPresenterPrototypeFindByIdRadioShows**
> RadioShow radioPresenterPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

apiInstance.radioPresenterPrototypeFindByIdRadioShows(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeGetRadioShows"></a>
# **radioPresenterPrototypeGetRadioShows**
> [RadioShow] radioPresenterPrototypeGetRadioShows(id, opts)

Queries radioShows of RadioPresenter.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.radioPresenterPrototypeGetRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioShow]**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeLinkRadioShows"></a>
# **radioPresenterPrototypeLinkRadioShows**
> RadioPresenterRadioShow radioPresenterPrototypeLinkRadioShows(fk, id, opts)

Add a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | 
};
apiInstance.radioPresenterPrototypeLinkRadioShows(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)|  | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUnlinkRadioShows"></a>
# **radioPresenterPrototypeUnlinkRadioShows**
> radioPresenterPrototypeUnlinkRadioShows(fk, id)

Remove the radioShows relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

apiInstance.radioPresenterPrototypeUnlinkRadioShows(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid"></a>
# **radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**
> RadioPresenter radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | An object of model property name/value pairs
};
apiInstance.radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateAttributesPutRadioPresentersid"></a>
# **radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**
> RadioPresenter radioPresenterPrototypeUpdateAttributesPutRadioPresentersid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | An object of model property name/value pairs
};
apiInstance.radioPresenterPrototypeUpdateAttributesPutRadioPresentersid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id | 
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateByIdRadioShows"></a>
# **radioPresenterPrototypeUpdateByIdRadioShows**
> RadioShow radioPresenterPrototypeUpdateByIdRadioShows(fk, id, opts)

Update a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioPresenter id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.radioPresenterPrototypeUpdateByIdRadioShows(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioPresenter id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterReplaceById"></a>
# **radioPresenterReplaceById**
> RadioPresenter radioPresenterReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | Model instance data
};
apiInstance.radioPresenterReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterReplaceOrCreate"></a>
# **radioPresenterReplaceOrCreate**
> RadioPresenter radioPresenterReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | Model instance data
};
apiInstance.radioPresenterReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpdateAll"></a>
# **radioPresenterUpdateAll**
> InlineResponse2002 radioPresenterUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | An object of model property name/value pairs
};
apiInstance.radioPresenterUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUploadImage"></a>
# **radioPresenterUploadImage**
> MagentoFileUpload radioPresenterUploadImage(id, opts)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var id = "id_example"; // String | Radio Presenter Id

var opts = { 
  'data': new harpoonApi.MagentoFileUpload() // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.
};
apiInstance.radioPresenterUploadImage(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Radio Presenter Id | 
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional] 

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertPatchRadioPresenters"></a>
# **radioPresenterUpsertPatchRadioPresenters**
> RadioPresenter radioPresenterUpsertPatchRadioPresenters(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | Model instance data
};
apiInstance.radioPresenterUpsertPatchRadioPresenters(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertPutRadioPresenters"></a>
# **radioPresenterUpsertPutRadioPresenters**
> RadioPresenter radioPresenterUpsertPutRadioPresenters(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | Model instance data
};
apiInstance.radioPresenterUpsertPutRadioPresenters(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertWithWhere"></a>
# **radioPresenterUpsertWithWhere**
> RadioPresenter radioPresenterUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPresenter() // RadioPresenter | An object of model property name/value pairs
};
apiInstance.radioPresenterUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.EventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**price** | **Number** |  | [optional] [default to 0.0]
**qtyBought** | **Number** |  | [optional] [default to 0.0]
**qtyTotal** | **Number** |  | [optional] [default to 0.0]
**qtyLeft** | **Number** |  | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 



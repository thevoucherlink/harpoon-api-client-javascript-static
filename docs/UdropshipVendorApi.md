# harpoonApi.UdropshipVendorApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**udropshipVendorCount**](UdropshipVendorApi.md#udropshipVendorCount) | **GET** /UdropshipVendors/count | Count instances of the model matched by where from the data source.
[**udropshipVendorCreate**](UdropshipVendorApi.md#udropshipVendorCreate) | **POST** /UdropshipVendors | Create a new instance of the model and persist it into the data source.
[**udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream) | **GET** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream) | **POST** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorDeleteById**](UdropshipVendorApi.md#udropshipVendorDeleteById) | **DELETE** /UdropshipVendors/{id} | Delete a model instance by {{id}} from the data source.
[**udropshipVendorExistsGetUdropshipVendorsidExists**](UdropshipVendorApi.md#udropshipVendorExistsGetUdropshipVendorsidExists) | **GET** /UdropshipVendors/{id}/exists | Check whether a model instance exists in the data source.
[**udropshipVendorExistsHeadUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorExistsHeadUdropshipVendorsid) | **HEAD** /UdropshipVendors/{id} | Check whether a model instance exists in the data source.
[**udropshipVendorFind**](UdropshipVendorApi.md#udropshipVendorFind) | **GET** /UdropshipVendors | Find all instances of the model matched by filter from the data source.
[**udropshipVendorFindById**](UdropshipVendorApi.md#udropshipVendorFindById) | **GET** /UdropshipVendors/{id} | Find a model instance by {{id}} from the data source.
[**udropshipVendorFindOne**](UdropshipVendorApi.md#udropshipVendorFindOne) | **GET** /UdropshipVendors/findOne | Find first instance of the model matched by filter from the data source.
[**udropshipVendorPrototypeCountCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/count | Counts catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/count | Counts harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/count | Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeCountPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountPartners) | **GET** /UdropshipVendors/{id}/partners/count | Counts partners of UdropshipVendor.
[**udropshipVendorPrototypeCountUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCountVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/count | Counts vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeCreateCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateCatalogProducts) | **POST** /UdropshipVendors/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**udropshipVendorPrototypeCreateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateConfig) | **POST** /UdropshipVendors/{id}/config | Creates a new instance in config of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners) | **POST** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Creates a new instance in harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories) | **POST** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeCreatePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreatePartners) | **POST** /UdropshipVendors/{id}/partners | Creates a new instance in partners of this model.
[**udropshipVendorPrototypeCreateUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateUdropshipVendorProducts) | **POST** /UdropshipVendors/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**udropshipVendorPrototypeCreateVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateVendorLocations) | **POST** /UdropshipVendors/{id}/vendorLocations | Creates a new instance in vendorLocations of this model.
[**udropshipVendorPrototypeDeleteCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Deletes all harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Deletes all harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeDeletePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeletePartners) | **DELETE** /UdropshipVendors/{id}/partners | Deletes all partners of this model.
[**udropshipVendorPrototypeDeleteUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**udropshipVendorPrototypeDeleteVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations | Deletes all vendorLocations of this model.
[**udropshipVendorPrototypeDestroyByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Delete a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Delete a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeDestroyByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdPartners) | **DELETE** /UdropshipVendors/{id}/partners/{fk} | Delete a related item by id for partners.
[**udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeDestroyByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations/{fk} | Delete a related item by id for vendorLocations.
[**udropshipVendorPrototypeDestroyConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyConfig) | **DELETE** /UdropshipVendors/{id}/config | Deletes config of this model.
[**udropshipVendorPrototypeExistsCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsCatalogProducts) | **HEAD** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**udropshipVendorPrototypeExistsPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsPartners) | **HEAD** /UdropshipVendors/{id}/partners/rel/{fk} | Check the existence of partners relation to an item by id.
[**udropshipVendorPrototypeFindByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Find a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Find a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeFindByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdPartners) | **GET** /UdropshipVendors/{id}/partners/{fk} | Find a related item by id for partners.
[**udropshipVendorPrototypeFindByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeFindByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/{fk} | Find a related item by id for vendorLocations.
[**udropshipVendorPrototypeGetCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts | Queries catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeGetConfig) | **GET** /UdropshipVendors/{id}/config | Fetches hasOne relation config.
[**udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Queries harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeGetPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetPartners) | **GET** /UdropshipVendors/{id}/partners | Queries partners of UdropshipVendor.
[**udropshipVendorPrototypeGetUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeGetVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations | Queries vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeLinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**udropshipVendorPrototypeLinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkPartners) | **PUT** /UdropshipVendors/{id}/partners/rel/{fk} | Add a related item by id for partners.
[**udropshipVendorPrototypeUnlinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**udropshipVendorPrototypeUnlinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkPartners) | **DELETE** /UdropshipVendors/{id}/partners/rel/{fk} | Remove the partners relation to an item by id.
[**udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid) | **PATCH** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid) | **PUT** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners) | **PUT** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Update a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories) | **PUT** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Update a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeUpdateByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdPartners) | **PUT** /UdropshipVendors/{id}/partners/{fk} | Update a related item by id for partners.
[**udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeUpdateByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdVendorLocations) | **PUT** /UdropshipVendors/{id}/vendorLocations/{fk} | Update a related item by id for vendorLocations.
[**udropshipVendorPrototypeUpdateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateConfig) | **PUT** /UdropshipVendors/{id}/config | Update config of this model.
[**udropshipVendorReplaceById**](UdropshipVendorApi.md#udropshipVendorReplaceById) | **POST** /UdropshipVendors/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**udropshipVendorReplaceOrCreate**](UdropshipVendorApi.md#udropshipVendorReplaceOrCreate) | **POST** /UdropshipVendors/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**udropshipVendorUpdateAll**](UdropshipVendorApi.md#udropshipVendorUpdateAll) | **POST** /UdropshipVendors/update | Update instances of the model matched by {{where}} from the data source.
[**udropshipVendorUpsertPatchUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPatchUdropshipVendors) | **PATCH** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertPutUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPutUdropshipVendors) | **PUT** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertWithWhere**](UdropshipVendorApi.md#udropshipVendorUpsertWithWhere) | **POST** /UdropshipVendors/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="udropshipVendorCount"></a>
# **udropshipVendorCount**
> InlineResponse2001 udropshipVendorCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreate"></a>
# **udropshipVendorCreate**
> UdropshipVendor udropshipVendorCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | Model instance data
};
apiInstance.udropshipVendorCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream"></a>
# **udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**
> File udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream"></a>
# **udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**
> File udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorDeleteById"></a>
# **udropshipVendorDeleteById**
> Object udropshipVendorDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | Model id

apiInstance.udropshipVendorDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorExistsGetUdropshipVendorsidExists"></a>
# **udropshipVendorExistsGetUdropshipVendorsidExists**
> InlineResponse2003 udropshipVendorExistsGetUdropshipVendorsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | Model id

apiInstance.udropshipVendorExistsGetUdropshipVendorsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorExistsHeadUdropshipVendorsid"></a>
# **udropshipVendorExistsHeadUdropshipVendorsid**
> InlineResponse2003 udropshipVendorExistsHeadUdropshipVendorsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | Model id

apiInstance.udropshipVendorExistsHeadUdropshipVendorsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFind"></a>
# **udropshipVendorFind**
> [UdropshipVendor] udropshipVendorFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.udropshipVendorFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[UdropshipVendor]**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFindById"></a>
# **udropshipVendorFindById**
> UdropshipVendor udropshipVendorFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.udropshipVendorFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFindOne"></a>
# **udropshipVendorFindOne**
> UdropshipVendor udropshipVendorFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.udropshipVendorFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountCatalogProducts"></a>
# **udropshipVendorPrototypeCountCatalogProducts**
> InlineResponse2001 udropshipVendorPrototypeCountCatalogProducts(id, opts)

Counts catalogProducts of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**
> InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners(id, opts)

Counts harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**
> InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories(id, opts)

Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountPartners"></a>
# **udropshipVendorPrototypeCountPartners**
> InlineResponse2001 udropshipVendorPrototypeCountPartners(id, opts)

Counts partners of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountPartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeCountUdropshipVendorProducts**
> InlineResponse2001 udropshipVendorPrototypeCountUdropshipVendorProducts(id, opts)

Counts udropshipVendorProducts of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountVendorLocations"></a>
# **udropshipVendorPrototypeCountVendorLocations**
> InlineResponse2001 udropshipVendorPrototypeCountVendorLocations(id, opts)

Counts vendorLocations of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.udropshipVendorPrototypeCountVendorLocations(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateCatalogProducts"></a>
# **udropshipVendorPrototypeCreateCatalogProducts**
> CatalogProduct udropshipVendorPrototypeCreateCatalogProducts(id, opts)

Creates a new instance in catalogProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | 
};
apiInstance.udropshipVendorPrototypeCreateCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateConfig"></a>
# **udropshipVendorPrototypeCreateConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeCreateConfig(id, opts)

Creates a new instance in config of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicVendorconfig() // HarpoonHpublicVendorconfig | 
};
apiInstance.udropshipVendorPrototypeCreateConfig(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)|  | [optional] 

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners(id, opts)

Creates a new instance in harpoonHpublicApplicationpartners of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | 
};
apiInstance.udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)|  | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories(id, opts)

Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicv12VendorAppCategory() // HarpoonHpublicv12VendorAppCategory | 
};
apiInstance.udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)|  | [optional] 

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreatePartners"></a>
# **udropshipVendorPrototypeCreatePartners**
> UdropshipVendor udropshipVendorPrototypeCreatePartners(id, opts)

Creates a new instance in partners of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | 
};
apiInstance.udropshipVendorPrototypeCreatePartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeCreateUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeCreateUdropshipVendorProducts(id, opts)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProduct() // UdropshipVendorProduct | 
};
apiInstance.udropshipVendorPrototypeCreateUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional] 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateVendorLocations"></a>
# **udropshipVendorPrototypeCreateVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeCreateVendorLocations(id, opts)

Creates a new instance in vendorLocations of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicBrandvenue() // HarpoonHpublicBrandvenue | 
};
apiInstance.udropshipVendorPrototypeCreateVendorLocations(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)|  | [optional] 

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteCatalogProducts"></a>
# **udropshipVendorPrototypeDeleteCatalogProducts**
> udropshipVendorPrototypeDeleteCatalogProducts(id)

Deletes all catalogProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeleteCatalogProducts(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners(id)

Deletes all harpoonHpublicApplicationpartners of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories(id)

Deletes all harpoonHpublicv12VendorAppCategories of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeletePartners"></a>
# **udropshipVendorPrototypeDeletePartners**
> udropshipVendorPrototypeDeletePartners(id)

Deletes all partners of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeletePartners(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeDeleteUdropshipVendorProducts**
> udropshipVendorPrototypeDeleteUdropshipVendorProducts(id)

Deletes all udropshipVendorProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeleteUdropshipVendorProducts(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteVendorLocations"></a>
# **udropshipVendorPrototypeDeleteVendorLocations**
> udropshipVendorPrototypeDeleteVendorLocations(id)

Deletes all vendorLocations of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDeleteVendorLocations(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdCatalogProducts"></a>
# **udropshipVendorPrototypeDestroyByIdCatalogProducts**
> udropshipVendorPrototypeDestroyByIdCatalogProducts(fk, id)

Delete a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdCatalogProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners(fk, id)

Delete a related item by id for harpoonHpublicApplicationpartners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories(fk, id)

Delete a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdPartners"></a>
# **udropshipVendorPrototypeDestroyByIdPartners**
> udropshipVendorPrototypeDestroyByIdPartners(fk, id)

Delete a related item by id for partners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdPartners(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**
> udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts(fk, id)

Delete a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdVendorLocations"></a>
# **udropshipVendorPrototypeDestroyByIdVendorLocations**
> udropshipVendorPrototypeDestroyByIdVendorLocations(fk, id)

Delete a related item by id for vendorLocations.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for vendorLocations

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyByIdVendorLocations(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyConfig"></a>
# **udropshipVendorPrototypeDestroyConfig**
> udropshipVendorPrototypeDestroyConfig(id)

Deletes config of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeDestroyConfig(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeExistsCatalogProducts"></a>
# **udropshipVendorPrototypeExistsCatalogProducts**
> &#39;Boolean&#39; udropshipVendorPrototypeExistsCatalogProducts(fk, id)

Check the existence of catalogProducts relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeExistsCatalogProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeExistsPartners"></a>
# **udropshipVendorPrototypeExistsPartners**
> &#39;Boolean&#39; udropshipVendorPrototypeExistsPartners(fk, id)

Check the existence of partners relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeExistsPartners(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdCatalogProducts"></a>
# **udropshipVendorPrototypeFindByIdCatalogProducts**
> CatalogProduct udropshipVendorPrototypeFindByIdCatalogProducts(fk, id)

Find a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdCatalogProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners(fk, id)

Find a related item by id for harpoonHpublicApplicationpartners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories(fk, id)

Find a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdPartners"></a>
# **udropshipVendorPrototypeFindByIdPartners**
> UdropshipVendor udropshipVendorPrototypeFindByIdPartners(fk, id)

Find a related item by id for partners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdPartners(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeFindByIdUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeFindByIdUdropshipVendorProducts(fk, id)

Find a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdUdropshipVendorProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdVendorLocations"></a>
# **udropshipVendorPrototypeFindByIdVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeFindByIdVendorLocations(fk, id)

Find a related item by id for vendorLocations.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for vendorLocations

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeFindByIdVendorLocations(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations | 
 **id** | **String**| UdropshipVendor id | 

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetCatalogProducts"></a>
# **udropshipVendorPrototypeGetCatalogProducts**
> [CatalogProduct] udropshipVendorPrototypeGetCatalogProducts(id, opts)

Queries catalogProducts of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[CatalogProduct]**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetConfig"></a>
# **udropshipVendorPrototypeGetConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeGetConfig(id, opts)

Fetches hasOne relation config.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.udropshipVendorPrototypeGetConfig(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**
> [HarpoonHpublicApplicationpartner] udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners(id, opts)

Queries harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[HarpoonHpublicApplicationpartner]**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**
> [HarpoonHpublicv12VendorAppCategory] udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories(id, opts)

Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[HarpoonHpublicv12VendorAppCategory]**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetPartners"></a>
# **udropshipVendorPrototypeGetPartners**
> [UdropshipVendor] udropshipVendorPrototypeGetPartners(id, opts)

Queries partners of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetPartners(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[UdropshipVendor]**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeGetUdropshipVendorProducts**
> [UdropshipVendorProduct] udropshipVendorPrototypeGetUdropshipVendorProducts(id, opts)

Queries udropshipVendorProducts of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[UdropshipVendorProduct]**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetVendorLocations"></a>
# **udropshipVendorPrototypeGetVendorLocations**
> [HarpoonHpublicBrandvenue] udropshipVendorPrototypeGetVendorLocations(id, opts)

Queries vendorLocations of UdropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.udropshipVendorPrototypeGetVendorLocations(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[HarpoonHpublicBrandvenue]**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeLinkCatalogProducts"></a>
# **udropshipVendorPrototypeLinkCatalogProducts**
> UdropshipVendorProduct udropshipVendorPrototypeLinkCatalogProducts(fk, id, opts)

Add a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProduct() // UdropshipVendorProduct | 
};
apiInstance.udropshipVendorPrototypeLinkCatalogProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional] 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeLinkPartners"></a>
# **udropshipVendorPrototypeLinkPartners**
> UdropshipVendorPartner udropshipVendorPrototypeLinkPartners(fk, id, opts)

Add a related item by id for partners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendorPartner() // UdropshipVendorPartner | 
};
apiInstance.udropshipVendorPrototypeLinkPartners(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendorPartner**](UdropshipVendorPartner.md)|  | [optional] 

### Return type

[**UdropshipVendorPartner**](UdropshipVendorPartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUnlinkCatalogProducts"></a>
# **udropshipVendorPrototypeUnlinkCatalogProducts**
> udropshipVendorPrototypeUnlinkCatalogProducts(fk, id)

Remove the catalogProducts relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeUnlinkCatalogProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUnlinkPartners"></a>
# **udropshipVendorPrototypeUnlinkPartners**
> udropshipVendorPrototypeUnlinkPartners(fk, id)

Remove the partners relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

apiInstance.udropshipVendorPrototypeUnlinkPartners(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid"></a>
# **udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**
> UdropshipVendor udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | An object of model property name/value pairs
};
apiInstance.udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid"></a>
# **udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**
> UdropshipVendor udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | An object of model property name/value pairs
};
apiInstance.udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdCatalogProducts"></a>
# **udropshipVendorPrototypeUpdateByIdCatalogProducts**
> CatalogProduct udropshipVendorPrototypeUpdateByIdCatalogProducts(fk, id, opts)

Update a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdCatalogProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners(fk, id, opts)

Update a related item by id for harpoonHpublicApplicationpartners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)|  | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories(fk, id, opts)

Update a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicv12VendorAppCategory() // HarpoonHpublicv12VendorAppCategory | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)|  | [optional] 

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdPartners"></a>
# **udropshipVendorPrototypeUpdateByIdPartners**
> UdropshipVendor udropshipVendorPrototypeUpdateByIdPartners(fk, id, opts)

Update a related item by id for partners.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for partners

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdPartners(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts(fk, id, opts)

Update a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProduct() // UdropshipVendorProduct | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional] 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdVendorLocations"></a>
# **udropshipVendorPrototypeUpdateByIdVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeUpdateByIdVendorLocations(fk, id, opts)

Update a related item by id for vendorLocations.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var fk = "fk_example"; // String | Foreign key for vendorLocations

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicBrandvenue() // HarpoonHpublicBrandvenue | 
};
apiInstance.udropshipVendorPrototypeUpdateByIdVendorLocations(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations | 
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)|  | [optional] 

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateConfig"></a>
# **udropshipVendorPrototypeUpdateConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeUpdateConfig(id, opts)

Update config of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | UdropshipVendor id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicVendorconfig() // HarpoonHpublicVendorconfig | 
};
apiInstance.udropshipVendorPrototypeUpdateConfig(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id | 
 **data** | [**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)|  | [optional] 

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorReplaceById"></a>
# **udropshipVendorReplaceById**
> UdropshipVendor udropshipVendorReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | Model instance data
};
apiInstance.udropshipVendorReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorReplaceOrCreate"></a>
# **udropshipVendorReplaceOrCreate**
> UdropshipVendor udropshipVendorReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | Model instance data
};
apiInstance.udropshipVendorReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpdateAll"></a>
# **udropshipVendorUpdateAll**
> InlineResponse2002 udropshipVendorUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | An object of model property name/value pairs
};
apiInstance.udropshipVendorUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertPatchUdropshipVendors"></a>
# **udropshipVendorUpsertPatchUdropshipVendors**
> UdropshipVendor udropshipVendorUpsertPatchUdropshipVendors(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | Model instance data
};
apiInstance.udropshipVendorUpsertPatchUdropshipVendors(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertPutUdropshipVendors"></a>
# **udropshipVendorUpsertPutUdropshipVendors**
> UdropshipVendor udropshipVendorUpsertPutUdropshipVendors(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | Model instance data
};
apiInstance.udropshipVendorUpsertPutUdropshipVendors(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertWithWhere"></a>
# **udropshipVendorUpsertWithWhere**
> UdropshipVendor udropshipVendorUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.UdropshipVendorApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | An object of model property name/value pairs
};
apiInstance.udropshipVendorUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


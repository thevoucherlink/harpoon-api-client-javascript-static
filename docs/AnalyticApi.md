# harpoonApi.AnalyticApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**analyticReplaceById**](AnalyticApi.md#analyticReplaceById) | **POST** /Analytics/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**analyticReplaceOrCreate**](AnalyticApi.md#analyticReplaceOrCreate) | **POST** /Analytics/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**analyticTrack**](AnalyticApi.md#analyticTrack) | **POST** /Analytics/track | 
[**analyticUpsertWithWhere**](AnalyticApi.md#analyticUpsertWithWhere) | **POST** /Analytics/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="analyticReplaceById"></a>
# **analyticReplaceById**
> Analytic analyticReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AnalyticApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.Analytic() // Analytic | Model instance data
};
apiInstance.analyticReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**Analytic**](Analytic.md)| Model instance data | [optional] 

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticReplaceOrCreate"></a>
# **analyticReplaceOrCreate**
> Analytic analyticReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AnalyticApi();

var opts = { 
  'data': new harpoonApi.Analytic() // Analytic | Model instance data
};
apiInstance.analyticReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Analytic**](Analytic.md)| Model instance data | [optional] 

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticTrack"></a>
# **analyticTrack**
> InlineResponse200 analyticTrack(opts)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AnalyticApi();

var opts = { 
  'data': new harpoonApi.AnalyticRequest() // AnalyticRequest | 
};
apiInstance.analyticTrack(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AnalyticRequest**](AnalyticRequest.md)|  | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticUpsertWithWhere"></a>
# **analyticUpsertWithWhere**
> Analytic analyticUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AnalyticApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.Analytic() // Analytic | An object of model property name/value pairs
};
apiInstance.analyticUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**Analytic**](Analytic.md)| An object of model property name/value pairs | [optional] 

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


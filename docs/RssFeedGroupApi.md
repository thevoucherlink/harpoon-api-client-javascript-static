# harpoonApi.RssFeedGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedGroupCount**](RssFeedGroupApi.md#rssFeedGroupCount) | **GET** /RssFeedGroups/count | Count instances of the model matched by where from the data source.
[**rssFeedGroupCreate**](RssFeedGroupApi.md#rssFeedGroupCreate) | **POST** /RssFeedGroups | Create a new instance of the model and persist it into the data source.
[**rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream) | **GET** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream) | **POST** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupDeleteById**](RssFeedGroupApi.md#rssFeedGroupDeleteById) | **DELETE** /RssFeedGroups/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedGroupExistsGetRssFeedGroupsidExists**](RssFeedGroupApi.md#rssFeedGroupExistsGetRssFeedGroupsidExists) | **GET** /RssFeedGroups/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedGroupExistsHeadRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupExistsHeadRssFeedGroupsid) | **HEAD** /RssFeedGroups/{id} | Check whether a model instance exists in the data source.
[**rssFeedGroupFind**](RssFeedGroupApi.md#rssFeedGroupFind) | **GET** /RssFeedGroups | Find all instances of the model matched by filter from the data source.
[**rssFeedGroupFindById**](RssFeedGroupApi.md#rssFeedGroupFindById) | **GET** /RssFeedGroups/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedGroupFindOne**](RssFeedGroupApi.md#rssFeedGroupFindOne) | **GET** /RssFeedGroups/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedGroupPrototypeCountRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCountRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/count | Counts rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeCreateRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCreateRssFeeds) | **POST** /RssFeedGroups/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**rssFeedGroupPrototypeDeleteRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDeleteRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**rssFeedGroupPrototypeDestroyByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDestroyByIdRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**rssFeedGroupPrototypeFindByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeFindByIdRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**rssFeedGroupPrototypeGetRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeGetRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds | Queries rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid) | **PATCH** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid) | **PUT** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateByIdRssFeeds) | **PUT** /RssFeedGroups/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**rssFeedGroupReplaceById**](RssFeedGroupApi.md#rssFeedGroupReplaceById) | **POST** /RssFeedGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedGroupReplaceOrCreate**](RssFeedGroupApi.md#rssFeedGroupReplaceOrCreate) | **POST** /RssFeedGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpdateAll**](RssFeedGroupApi.md#rssFeedGroupUpdateAll) | **POST** /RssFeedGroups/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedGroupUpsertPatchRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPatchRssFeedGroups) | **PATCH** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertPutRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPutRssFeedGroups) | **PUT** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertWithWhere**](RssFeedGroupApi.md#rssFeedGroupUpsertWithWhere) | **POST** /RssFeedGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="rssFeedGroupCount"></a>
# **rssFeedGroupCount**
> InlineResponse2001 rssFeedGroupCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.rssFeedGroupCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreate"></a>
# **rssFeedGroupCreate**
> RssFeedGroup rssFeedGroupCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | Model instance data
};
apiInstance.rssFeedGroupCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream"></a>
# **rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**
> File rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream"></a>
# **rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**
> File rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupDeleteById"></a>
# **rssFeedGroupDeleteById**
> Object rssFeedGroupDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedGroupDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupExistsGetRssFeedGroupsidExists"></a>
# **rssFeedGroupExistsGetRssFeedGroupsidExists**
> InlineResponse2003 rssFeedGroupExistsGetRssFeedGroupsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedGroupExistsGetRssFeedGroupsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupExistsHeadRssFeedGroupsid"></a>
# **rssFeedGroupExistsHeadRssFeedGroupsid**
> InlineResponse2003 rssFeedGroupExistsHeadRssFeedGroupsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedGroupExistsHeadRssFeedGroupsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFind"></a>
# **rssFeedGroupFind**
> [RssFeedGroup] rssFeedGroupFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.rssFeedGroupFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RssFeedGroup]**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFindById"></a>
# **rssFeedGroupFindById**
> RssFeedGroup rssFeedGroupFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.rssFeedGroupFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFindOne"></a>
# **rssFeedGroupFindOne**
> RssFeedGroup rssFeedGroupFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.rssFeedGroupFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeCountRssFeeds"></a>
# **rssFeedGroupPrototypeCountRssFeeds**
> InlineResponse2001 rssFeedGroupPrototypeCountRssFeeds(id, opts)

Counts rssFeeds of RssFeedGroup.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.rssFeedGroupPrototypeCountRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeCreateRssFeeds"></a>
# **rssFeedGroupPrototypeCreateRssFeeds**
> RssFeed rssFeedGroupPrototypeCreateRssFeeds(id, opts)

Creates a new instance in rssFeeds of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | 
};
apiInstance.rssFeedGroupPrototypeCreateRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 
 **data** | [**RssFeed**](RssFeed.md)|  | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeDeleteRssFeeds"></a>
# **rssFeedGroupPrototypeDeleteRssFeeds**
> rssFeedGroupPrototypeDeleteRssFeeds(id)

Deletes all rssFeeds of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

apiInstance.rssFeedGroupPrototypeDeleteRssFeeds(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeDestroyByIdRssFeeds"></a>
# **rssFeedGroupPrototypeDestroyByIdRssFeeds**
> rssFeedGroupPrototypeDestroyByIdRssFeeds(fk, id)

Delete a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | RssFeedGroup id

apiInstance.rssFeedGroupPrototypeDestroyByIdRssFeeds(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| RssFeedGroup id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeFindByIdRssFeeds"></a>
# **rssFeedGroupPrototypeFindByIdRssFeeds**
> RssFeed rssFeedGroupPrototypeFindByIdRssFeeds(fk, id)

Find a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | RssFeedGroup id

apiInstance.rssFeedGroupPrototypeFindByIdRssFeeds(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| RssFeedGroup id | 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeGetRssFeeds"></a>
# **rssFeedGroupPrototypeGetRssFeeds**
> [RssFeed] rssFeedGroupPrototypeGetRssFeeds(id, opts)

Queries rssFeeds of RssFeedGroup.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.rssFeedGroupPrototypeGetRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RssFeed]**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid"></a>
# **rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**
> RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | An object of model property name/value pairs
};
apiInstance.rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid"></a>
# **rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**
> RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | An object of model property name/value pairs
};
apiInstance.rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id | 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateByIdRssFeeds"></a>
# **rssFeedGroupPrototypeUpdateByIdRssFeeds**
> RssFeed rssFeedGroupPrototypeUpdateByIdRssFeeds(fk, id, opts)

Update a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | RssFeedGroup id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | 
};
apiInstance.rssFeedGroupPrototypeUpdateByIdRssFeeds(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| RssFeedGroup id | 
 **data** | [**RssFeed**](RssFeed.md)|  | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupReplaceById"></a>
# **rssFeedGroupReplaceById**
> RssFeedGroup rssFeedGroupReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | Model instance data
};
apiInstance.rssFeedGroupReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupReplaceOrCreate"></a>
# **rssFeedGroupReplaceOrCreate**
> RssFeedGroup rssFeedGroupReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | Model instance data
};
apiInstance.rssFeedGroupReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpdateAll"></a>
# **rssFeedGroupUpdateAll**
> InlineResponse2002 rssFeedGroupUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | An object of model property name/value pairs
};
apiInstance.rssFeedGroupUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertPatchRssFeedGroups"></a>
# **rssFeedGroupUpsertPatchRssFeedGroups**
> RssFeedGroup rssFeedGroupUpsertPatchRssFeedGroups(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | Model instance data
};
apiInstance.rssFeedGroupUpsertPatchRssFeedGroups(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertPutRssFeedGroups"></a>
# **rssFeedGroupUpsertPutRssFeedGroups**
> RssFeedGroup rssFeedGroupUpsertPutRssFeedGroups(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | Model instance data
};
apiInstance.rssFeedGroupUpsertPutRssFeedGroups(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertWithWhere"></a>
# **rssFeedGroupUpsertWithWhere**
> RssFeedGroup rssFeedGroupUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedGroupApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | An object of model property name/value pairs
};
apiInstance.rssFeedGroupUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


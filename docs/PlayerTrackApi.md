# harpoonApi.PlayerTrackApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerTrackCount**](PlayerTrackApi.md#playerTrackCount) | **GET** /PlayerTracks/count | Count instances of the model matched by where from the data source.
[**playerTrackCreate**](PlayerTrackApi.md#playerTrackCreate) | **POST** /PlayerTracks | Create a new instance of the model and persist it into the data source.
[**playerTrackCreateChangeStreamGetPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamGetPlayerTracksChangeStream) | **GET** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackCreateChangeStreamPostPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamPostPlayerTracksChangeStream) | **POST** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackDeleteById**](PlayerTrackApi.md#playerTrackDeleteById) | **DELETE** /PlayerTracks/{id} | Delete a model instance by {{id}} from the data source.
[**playerTrackExistsGetPlayerTracksidExists**](PlayerTrackApi.md#playerTrackExistsGetPlayerTracksidExists) | **GET** /PlayerTracks/{id}/exists | Check whether a model instance exists in the data source.
[**playerTrackExistsHeadPlayerTracksid**](PlayerTrackApi.md#playerTrackExistsHeadPlayerTracksid) | **HEAD** /PlayerTracks/{id} | Check whether a model instance exists in the data source.
[**playerTrackFind**](PlayerTrackApi.md#playerTrackFind) | **GET** /PlayerTracks | Find all instances of the model matched by filter from the data source.
[**playerTrackFindById**](PlayerTrackApi.md#playerTrackFindById) | **GET** /PlayerTracks/{id} | Find a model instance by {{id}} from the data source.
[**playerTrackFindOne**](PlayerTrackApi.md#playerTrackFindOne) | **GET** /PlayerTracks/findOne | Find first instance of the model matched by filter from the data source.
[**playerTrackPrototypeGetPlaylistItem**](PlayerTrackApi.md#playerTrackPrototypeGetPlaylistItem) | **GET** /PlayerTracks/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPatchPlayerTracksid) | **PATCH** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackPrototypeUpdateAttributesPutPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPutPlayerTracksid) | **PUT** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackReplaceById**](PlayerTrackApi.md#playerTrackReplaceById) | **POST** /PlayerTracks/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerTrackReplaceOrCreate**](PlayerTrackApi.md#playerTrackReplaceOrCreate) | **POST** /PlayerTracks/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerTrackUpdateAll**](PlayerTrackApi.md#playerTrackUpdateAll) | **POST** /PlayerTracks/update | Update instances of the model matched by {{where}} from the data source.
[**playerTrackUpsertPatchPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPatchPlayerTracks) | **PATCH** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertPutPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPutPlayerTracks) | **PUT** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertWithWhere**](PlayerTrackApi.md#playerTrackUpsertWithWhere) | **POST** /PlayerTracks/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playerTrackCount"></a>
# **playerTrackCount**
> InlineResponse2001 playerTrackCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playerTrackCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreate"></a>
# **playerTrackCreate**
> PlayerTrack playerTrackCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | Model instance data
};
apiInstance.playerTrackCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreateChangeStreamGetPlayerTracksChangeStream"></a>
# **playerTrackCreateChangeStreamGetPlayerTracksChangeStream**
> File playerTrackCreateChangeStreamGetPlayerTracksChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playerTrackCreateChangeStreamGetPlayerTracksChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreateChangeStreamPostPlayerTracksChangeStream"></a>
# **playerTrackCreateChangeStreamPostPlayerTracksChangeStream**
> File playerTrackCreateChangeStreamPostPlayerTracksChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playerTrackCreateChangeStreamPostPlayerTracksChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackDeleteById"></a>
# **playerTrackDeleteById**
> Object playerTrackDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | Model id

apiInstance.playerTrackDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackExistsGetPlayerTracksidExists"></a>
# **playerTrackExistsGetPlayerTracksidExists**
> InlineResponse2003 playerTrackExistsGetPlayerTracksidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | Model id

apiInstance.playerTrackExistsGetPlayerTracksidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackExistsHeadPlayerTracksid"></a>
# **playerTrackExistsHeadPlayerTracksid**
> InlineResponse2003 playerTrackExistsHeadPlayerTracksid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | Model id

apiInstance.playerTrackExistsHeadPlayerTracksid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFind"></a>
# **playerTrackFind**
> [PlayerTrack] playerTrackFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playerTrackFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[PlayerTrack]**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFindById"></a>
# **playerTrackFindById**
> PlayerTrack playerTrackFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.playerTrackFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFindOne"></a>
# **playerTrackFindOne**
> PlayerTrack playerTrackFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playerTrackFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeGetPlaylistItem"></a>
# **playerTrackPrototypeGetPlaylistItem**
> PlaylistItem playerTrackPrototypeGetPlaylistItem(id, opts)

Fetches belongsTo relation playlistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | PlayerTrack id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.playerTrackPrototypeGetPlaylistItem(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeUpdateAttributesPatchPlayerTracksid"></a>
# **playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**
> PlayerTrack playerTrackPrototypeUpdateAttributesPatchPlayerTracksid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | PlayerTrack id

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | An object of model property name/value pairs
};
apiInstance.playerTrackPrototypeUpdateAttributesPatchPlayerTracksid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id | 
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeUpdateAttributesPutPlayerTracksid"></a>
# **playerTrackPrototypeUpdateAttributesPutPlayerTracksid**
> PlayerTrack playerTrackPrototypeUpdateAttributesPutPlayerTracksid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | PlayerTrack id

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | An object of model property name/value pairs
};
apiInstance.playerTrackPrototypeUpdateAttributesPutPlayerTracksid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id | 
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackReplaceById"></a>
# **playerTrackReplaceById**
> PlayerTrack playerTrackReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | Model instance data
};
apiInstance.playerTrackReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackReplaceOrCreate"></a>
# **playerTrackReplaceOrCreate**
> PlayerTrack playerTrackReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | Model instance data
};
apiInstance.playerTrackReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpdateAll"></a>
# **playerTrackUpdateAll**
> InlineResponse2002 playerTrackUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | An object of model property name/value pairs
};
apiInstance.playerTrackUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertPatchPlayerTracks"></a>
# **playerTrackUpsertPatchPlayerTracks**
> PlayerTrack playerTrackUpsertPatchPlayerTracks(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | Model instance data
};
apiInstance.playerTrackUpsertPatchPlayerTracks(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertPutPlayerTracks"></a>
# **playerTrackUpsertPutPlayerTracks**
> PlayerTrack playerTrackUpsertPutPlayerTracks(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | Model instance data
};
apiInstance.playerTrackUpsertPutPlayerTracks(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertWithWhere"></a>
# **playerTrackUpsertWithWhere**
> PlayerTrack playerTrackUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerTrackApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | An object of model property name/value pairs
};
apiInstance.playerTrackUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.FormSelectorOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**value** | **String** |  | 
**format** | **String** |  | [optional] [default to &#39;text&#39;]
**id** | **Number** |  | [optional] 


<a name="FormatEnum"></a>
## Enum: FormatEnum


* `text` (value: `"text"`)

* `number` (value: `"number"`)





# harpoonApi.PushWooshConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appName** | **String** |  | [optional] 
**appId** | **String** |  | 
**appToken** | **String** |  | [optional] 



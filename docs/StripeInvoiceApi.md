# harpoonApi.StripeInvoiceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceCount**](StripeInvoiceApi.md#stripeInvoiceCount) | **GET** /StripeInvoices/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceCreate**](StripeInvoiceApi.md#stripeInvoiceCreate) | **POST** /StripeInvoices | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream) | **GET** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream) | **POST** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceDeleteById**](StripeInvoiceApi.md#stripeInvoiceDeleteById) | **DELETE** /StripeInvoices/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceExistsGetStripeInvoicesidExists**](StripeInvoiceApi.md#stripeInvoiceExistsGetStripeInvoicesidExists) | **GET** /StripeInvoices/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceExistsHeadStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoiceExistsHeadStripeInvoicesid) | **HEAD** /StripeInvoices/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceFind**](StripeInvoiceApi.md#stripeInvoiceFind) | **GET** /StripeInvoices | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceFindById**](StripeInvoiceApi.md#stripeInvoiceFindById) | **GET** /StripeInvoices/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceFindOne**](StripeInvoiceApi.md#stripeInvoiceFindOne) | **GET** /StripeInvoices/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid) | **PATCH** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid) | **PUT** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceById**](StripeInvoiceApi.md#stripeInvoiceReplaceById) | **POST** /StripeInvoices/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceOrCreate**](StripeInvoiceApi.md#stripeInvoiceReplaceOrCreate) | **POST** /StripeInvoices/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpdateAll**](StripeInvoiceApi.md#stripeInvoiceUpdateAll) | **POST** /StripeInvoices/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceUpsertPatchStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPatchStripeInvoices) | **PATCH** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertPutStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPutStripeInvoices) | **PUT** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertWithWhere**](StripeInvoiceApi.md#stripeInvoiceUpsertWithWhere) | **POST** /StripeInvoices/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeInvoiceCount"></a>
# **stripeInvoiceCount**
> InlineResponse2001 stripeInvoiceCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripeInvoiceCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreate"></a>
# **stripeInvoiceCreate**
> StripeInvoice stripeInvoiceCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | Model instance data
};
apiInstance.stripeInvoiceCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream"></a>
# **stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**
> File stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream"></a>
# **stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**
> File stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceDeleteById"></a>
# **stripeInvoiceDeleteById**
> Object stripeInvoiceDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceExistsGetStripeInvoicesidExists"></a>
# **stripeInvoiceExistsGetStripeInvoicesidExists**
> InlineResponse2003 stripeInvoiceExistsGetStripeInvoicesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceExistsGetStripeInvoicesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceExistsHeadStripeInvoicesid"></a>
# **stripeInvoiceExistsHeadStripeInvoicesid**
> InlineResponse2003 stripeInvoiceExistsHeadStripeInvoicesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | Model id

apiInstance.stripeInvoiceExistsHeadStripeInvoicesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFind"></a>
# **stripeInvoiceFind**
> [StripeInvoice] stripeInvoiceFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeInvoiceFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripeInvoice]**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFindById"></a>
# **stripeInvoiceFindById**
> StripeInvoice stripeInvoiceFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripeInvoiceFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFindOne"></a>
# **stripeInvoiceFindOne**
> StripeInvoice stripeInvoiceFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeInvoiceFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid"></a>
# **stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**
> StripeInvoice stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | StripeInvoice id

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | An object of model property name/value pairs
};
apiInstance.stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoice id | 
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid"></a>
# **stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**
> StripeInvoice stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | StripeInvoice id

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | An object of model property name/value pairs
};
apiInstance.stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoice id | 
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceReplaceById"></a>
# **stripeInvoiceReplaceById**
> StripeInvoice stripeInvoiceReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | Model instance data
};
apiInstance.stripeInvoiceReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceReplaceOrCreate"></a>
# **stripeInvoiceReplaceOrCreate**
> StripeInvoice stripeInvoiceReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | Model instance data
};
apiInstance.stripeInvoiceReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpdateAll"></a>
# **stripeInvoiceUpdateAll**
> InlineResponse2002 stripeInvoiceUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | An object of model property name/value pairs
};
apiInstance.stripeInvoiceUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertPatchStripeInvoices"></a>
# **stripeInvoiceUpsertPatchStripeInvoices**
> StripeInvoice stripeInvoiceUpsertPatchStripeInvoices(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | Model instance data
};
apiInstance.stripeInvoiceUpsertPatchStripeInvoices(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertPutStripeInvoices"></a>
# **stripeInvoiceUpsertPutStripeInvoices**
> StripeInvoice stripeInvoiceUpsertPutStripeInvoices(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | Model instance data
};
apiInstance.stripeInvoiceUpsertPutStripeInvoices(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertWithWhere"></a>
# **stripeInvoiceUpsertWithWhere**
> StripeInvoice stripeInvoiceUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeInvoiceApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeInvoice() // StripeInvoice | An object of model property name/value pairs
};
apiInstance.stripeInvoiceUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


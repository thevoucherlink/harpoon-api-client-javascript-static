# harpoonApi.CompetitionCheckoutChanceData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Chance to checkout | 
**qty** | **Number** | Quantity to checkout | [optional] [default to 0.0]



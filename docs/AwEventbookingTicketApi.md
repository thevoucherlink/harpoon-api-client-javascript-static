# harpoonApi.AwEventbookingTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingTicketCount**](AwEventbookingTicketApi.md#awEventbookingTicketCount) | **GET** /AwEventbookingTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingTicketCreate**](AwEventbookingTicketApi.md#awEventbookingTicketCreate) | **POST** /AwEventbookingTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream) | **GET** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream) | **POST** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketDeleteById**](AwEventbookingTicketApi.md#awEventbookingTicketDeleteById) | **DELETE** /AwEventbookingTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingTicketExistsGetAwEventbookingTicketsidExists**](AwEventbookingTicketApi.md#awEventbookingTicketExistsGetAwEventbookingTicketsidExists) | **GET** /AwEventbookingTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingTicketExistsHeadAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketExistsHeadAwEventbookingTicketsid) | **HEAD** /AwEventbookingTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingTicketFind**](AwEventbookingTicketApi.md#awEventbookingTicketFind) | **GET** /AwEventbookingTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingTicketFindById**](AwEventbookingTicketApi.md#awEventbookingTicketFindById) | **GET** /AwEventbookingTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingTicketFindOne**](AwEventbookingTicketApi.md#awEventbookingTicketFindOne) | **GET** /AwEventbookingTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid) | **PATCH** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid) | **PUT** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceById**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceById) | **POST** /AwEventbookingTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceOrCreate**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceOrCreate) | **POST** /AwEventbookingTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpdateAll**](AwEventbookingTicketApi.md#awEventbookingTicketUpdateAll) | **POST** /AwEventbookingTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingTicketUpsertPatchAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPatchAwEventbookingTickets) | **PATCH** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertPutAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPutAwEventbookingTickets) | **PUT** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertWithWhere**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertWithWhere) | **POST** /AwEventbookingTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingTicketCount"></a>
# **awEventbookingTicketCount**
> InlineResponse2001 awEventbookingTicketCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingTicketCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreate"></a>
# **awEventbookingTicketCreate**
> AwEventbookingTicket awEventbookingTicketCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | Model instance data
};
apiInstance.awEventbookingTicketCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream"></a>
# **awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**
> File awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream"></a>
# **awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**
> File awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketDeleteById"></a>
# **awEventbookingTicketDeleteById**
> Object awEventbookingTicketDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingTicketDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketExistsGetAwEventbookingTicketsidExists"></a>
# **awEventbookingTicketExistsGetAwEventbookingTicketsidExists**
> InlineResponse2003 awEventbookingTicketExistsGetAwEventbookingTicketsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingTicketExistsGetAwEventbookingTicketsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketExistsHeadAwEventbookingTicketsid"></a>
# **awEventbookingTicketExistsHeadAwEventbookingTicketsid**
> InlineResponse2003 awEventbookingTicketExistsHeadAwEventbookingTicketsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingTicketExistsHeadAwEventbookingTicketsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFind"></a>
# **awEventbookingTicketFind**
> [AwEventbookingTicket] awEventbookingTicketFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingTicketFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwEventbookingTicket]**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFindById"></a>
# **awEventbookingTicketFindById**
> AwEventbookingTicket awEventbookingTicketFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awEventbookingTicketFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFindOne"></a>
# **awEventbookingTicketFindOne**
> AwEventbookingTicket awEventbookingTicketFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingTicketFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid"></a>
# **awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**
> AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | AwEventbookingTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid"></a>
# **awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**
> AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | AwEventbookingTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketReplaceById"></a>
# **awEventbookingTicketReplaceById**
> AwEventbookingTicket awEventbookingTicketReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | Model instance data
};
apiInstance.awEventbookingTicketReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketReplaceOrCreate"></a>
# **awEventbookingTicketReplaceOrCreate**
> AwEventbookingTicket awEventbookingTicketReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | Model instance data
};
apiInstance.awEventbookingTicketReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpdateAll"></a>
# **awEventbookingTicketUpdateAll**
> InlineResponse2002 awEventbookingTicketUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingTicketUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertPatchAwEventbookingTickets"></a>
# **awEventbookingTicketUpsertPatchAwEventbookingTickets**
> AwEventbookingTicket awEventbookingTicketUpsertPatchAwEventbookingTickets(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | Model instance data
};
apiInstance.awEventbookingTicketUpsertPatchAwEventbookingTickets(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertPutAwEventbookingTickets"></a>
# **awEventbookingTicketUpsertPutAwEventbookingTickets**
> AwEventbookingTicket awEventbookingTicketUpsertPutAwEventbookingTickets(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | Model instance data
};
apiInstance.awEventbookingTicketUpsertPutAwEventbookingTickets(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertWithWhere"></a>
# **awEventbookingTicketUpsertWithWhere**
> AwEventbookingTicket awEventbookingTicketUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingTicketApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingTicketUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


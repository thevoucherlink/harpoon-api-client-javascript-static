# harpoonApi.Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basePrice** | **Number** |  | [optional] [default to 0.0]
**attendees** | [**[EventAttendee]**](EventAttendee.md) |  | [optional] 
**attendeeCount** | **Number** |  | [optional] [default to 0.0]
**isGoing** | **Boolean** |  | [optional] [default to false]
**tickets** | [**[EventTicket]**](EventTicket.md) |  | [optional] 
**termsConditions** | **String** |  | [optional] 
**facebook** | [**FacebookEvent**](FacebookEvent.md) |  | [optional] 
**connectFacebookId** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**campaignType** | [**Category**](Category.md) |  | [optional] 
**category** | [**Category**](Category.md) |  | [optional] 
**topic** | [**Category**](Category.md) |  | [optional] 
**alias** | **String** |  | [optional] 
**from** | **Date** |  | [optional] 
**to** | **Date** |  | [optional] 
**baseCurrency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**priceText** | **String** |  | [optional] 
**bannerText** | **String** |  | [optional] 
**checkoutLink** | **String** |  | [optional] 
**nearestVenue** | [**Venue**](Venue.md) |  | [optional] 
**actionText** | **String** |  | [optional] [default to &#39;Sold Out&#39;]
**status** | **String** |  | [optional] [default to &#39;soldOut&#39;]
**collectionNotes** | **String** |  | [optional] 
**locationLink** | **String** |  | [optional] 
**altLink** | **String** |  | [optional] 
**redemptionType** | **String** |  | [optional] 
**brand** | [**Brand**](Brand.md) |  | [optional] 
**closestPurchase** | [**OfferClosestPurchase**](OfferClosestPurchase.md) |  | [optional] 
**isFeatured** | **Boolean** |  | [optional] [default to false]
**qtyPerOrder** | **Number** |  | [optional] [default to 1.0]
**shareLink** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.PlayerSource

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** | Source file | [optional] 
**type** | **String** | Source type (used only in JS SDK) | [optional] 
**label** | **String** | Source label | [optional] 
**_default** | **Boolean** | If Source is the default Source | [optional] [default to false]
**order** | **Number** | Sort order index | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 
**playlistItemId** | **Number** |  | [optional] 
**playlistItem** | **Object** |  | [optional] 



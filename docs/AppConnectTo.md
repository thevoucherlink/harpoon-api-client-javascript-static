# harpoonApi.AppConnectTo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  | 
**heading** | **String** |  | 
**description** | **String** |  | 
**link** | **String** |  | 



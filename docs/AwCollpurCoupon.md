# harpoonApi.AwCollpurCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**dealId** | **Number** |  | 
**purchaseId** | **Number** |  | 
**couponCode** | **String** |  | 
**status** | **String** |  | 
**couponDeliveryDatetime** | **Date** |  | [optional] 
**couponDateUpdated** | **Date** |  | [optional] 
**redeemedAt** | **Date** |  | [optional] 
**redeemedByTerminal** | **String** |  | [optional] 



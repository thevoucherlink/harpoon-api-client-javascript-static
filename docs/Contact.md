# harpoonApi.Contact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | Email address like hello@harpoonconnect.com | [optional] 
**website** | **String** | URL like http://harpoonconnect.com or https://www.harpoonconnect.com | [optional] 
**phone** | **String** | Telephone number like 08123456789 | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**twitter** | **String** | URL starting with https://www.twitter.com/ followed by your username or id | [optional] 
**facebook** | **String** | URL starting with https://www.facebook.com/ followed by your username or id | [optional] 
**whatsapp** | **String** | Telephone number including country code like: +35381234567890 | [optional] 
**snapchat** | **String** | SnapChat username | [optional] 
**googlePlus** | **String** | URL starting with https://plus.google.com/+ followed by your username or id | [optional] 
**linkedIn** | **String** | LinkedIn profile URL | [optional] 
**hashtag** | **String** | A string starting with # and followed by alphanumeric characters (both lower and upper case) | [optional] 
**text** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.RssFeedApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedCount**](RssFeedApi.md#rssFeedCount) | **GET** /RssFeeds/count | Count instances of the model matched by where from the data source.
[**rssFeedCreate**](RssFeedApi.md#rssFeedCreate) | **POST** /RssFeeds | Create a new instance of the model and persist it into the data source.
[**rssFeedCreateChangeStreamGetRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamGetRssFeedsChangeStream) | **GET** /RssFeeds/change-stream | Create a change stream.
[**rssFeedCreateChangeStreamPostRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamPostRssFeedsChangeStream) | **POST** /RssFeeds/change-stream | Create a change stream.
[**rssFeedDeleteById**](RssFeedApi.md#rssFeedDeleteById) | **DELETE** /RssFeeds/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedExistsGetRssFeedsidExists**](RssFeedApi.md#rssFeedExistsGetRssFeedsidExists) | **GET** /RssFeeds/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedExistsHeadRssFeedsid**](RssFeedApi.md#rssFeedExistsHeadRssFeedsid) | **HEAD** /RssFeeds/{id} | Check whether a model instance exists in the data source.
[**rssFeedFind**](RssFeedApi.md#rssFeedFind) | **GET** /RssFeeds | Find all instances of the model matched by filter from the data source.
[**rssFeedFindById**](RssFeedApi.md#rssFeedFindById) | **GET** /RssFeeds/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedFindOne**](RssFeedApi.md#rssFeedFindOne) | **GET** /RssFeeds/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedPrototypeGetRssFeedGroup**](RssFeedApi.md#rssFeedPrototypeGetRssFeedGroup) | **GET** /RssFeeds/{id}/rssFeedGroup | Fetches belongsTo relation rssFeedGroup.
[**rssFeedPrototypeUpdateAttributesPatchRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPatchRssFeedsid) | **PATCH** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedPrototypeUpdateAttributesPutRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPutRssFeedsid) | **PUT** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedReplaceById**](RssFeedApi.md#rssFeedReplaceById) | **POST** /RssFeeds/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedReplaceOrCreate**](RssFeedApi.md#rssFeedReplaceOrCreate) | **POST** /RssFeeds/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedUpdateAll**](RssFeedApi.md#rssFeedUpdateAll) | **POST** /RssFeeds/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedUpsertPatchRssFeeds**](RssFeedApi.md#rssFeedUpsertPatchRssFeeds) | **PATCH** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertPutRssFeeds**](RssFeedApi.md#rssFeedUpsertPutRssFeeds) | **PUT** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertWithWhere**](RssFeedApi.md#rssFeedUpsertWithWhere) | **POST** /RssFeeds/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="rssFeedCount"></a>
# **rssFeedCount**
> InlineResponse2001 rssFeedCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.rssFeedCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreate"></a>
# **rssFeedCreate**
> RssFeed rssFeedCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | Model instance data
};
apiInstance.rssFeedCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreateChangeStreamGetRssFeedsChangeStream"></a>
# **rssFeedCreateChangeStreamGetRssFeedsChangeStream**
> File rssFeedCreateChangeStreamGetRssFeedsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.rssFeedCreateChangeStreamGetRssFeedsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreateChangeStreamPostRssFeedsChangeStream"></a>
# **rssFeedCreateChangeStreamPostRssFeedsChangeStream**
> File rssFeedCreateChangeStreamPostRssFeedsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.rssFeedCreateChangeStreamPostRssFeedsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedDeleteById"></a>
# **rssFeedDeleteById**
> Object rssFeedDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedExistsGetRssFeedsidExists"></a>
# **rssFeedExistsGetRssFeedsidExists**
> InlineResponse2003 rssFeedExistsGetRssFeedsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedExistsGetRssFeedsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedExistsHeadRssFeedsid"></a>
# **rssFeedExistsHeadRssFeedsid**
> InlineResponse2003 rssFeedExistsHeadRssFeedsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | Model id

apiInstance.rssFeedExistsHeadRssFeedsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFind"></a>
# **rssFeedFind**
> [RssFeed] rssFeedFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.rssFeedFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RssFeed]**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFindById"></a>
# **rssFeedFindById**
> RssFeed rssFeedFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.rssFeedFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFindOne"></a>
# **rssFeedFindOne**
> RssFeed rssFeedFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.rssFeedFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeGetRssFeedGroup"></a>
# **rssFeedPrototypeGetRssFeedGroup**
> RssFeedGroup rssFeedPrototypeGetRssFeedGroup(id, opts)

Fetches belongsTo relation rssFeedGroup.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | RssFeed id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.rssFeedPrototypeGetRssFeedGroup(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeUpdateAttributesPatchRssFeedsid"></a>
# **rssFeedPrototypeUpdateAttributesPatchRssFeedsid**
> RssFeed rssFeedPrototypeUpdateAttributesPatchRssFeedsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | RssFeed id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | An object of model property name/value pairs
};
apiInstance.rssFeedPrototypeUpdateAttributesPatchRssFeedsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id | 
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeUpdateAttributesPutRssFeedsid"></a>
# **rssFeedPrototypeUpdateAttributesPutRssFeedsid**
> RssFeed rssFeedPrototypeUpdateAttributesPutRssFeedsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | RssFeed id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | An object of model property name/value pairs
};
apiInstance.rssFeedPrototypeUpdateAttributesPutRssFeedsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id | 
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedReplaceById"></a>
# **rssFeedReplaceById**
> RssFeed rssFeedReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | Model instance data
};
apiInstance.rssFeedReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedReplaceOrCreate"></a>
# **rssFeedReplaceOrCreate**
> RssFeed rssFeedReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | Model instance data
};
apiInstance.rssFeedReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpdateAll"></a>
# **rssFeedUpdateAll**
> InlineResponse2002 rssFeedUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RssFeed() // RssFeed | An object of model property name/value pairs
};
apiInstance.rssFeedUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertPatchRssFeeds"></a>
# **rssFeedUpsertPatchRssFeeds**
> RssFeed rssFeedUpsertPatchRssFeeds(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | Model instance data
};
apiInstance.rssFeedUpsertPatchRssFeeds(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertPutRssFeeds"></a>
# **rssFeedUpsertPutRssFeeds**
> RssFeed rssFeedUpsertPutRssFeeds(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | Model instance data
};
apiInstance.rssFeedUpsertPutRssFeeds(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertWithWhere"></a>
# **rssFeedUpsertWithWhere**
> RssFeed rssFeedUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RssFeedApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RssFeed() // RssFeed | An object of model property name/value pairs
};
apiInstance.rssFeedUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.PlaylistItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistItemCount**](PlaylistItemApi.md#playlistItemCount) | **GET** /PlaylistItems/count | Count instances of the model matched by where from the data source.
[**playlistItemCreate**](PlaylistItemApi.md#playlistItemCreate) | **POST** /PlaylistItems | Create a new instance of the model and persist it into the data source.
[**playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamGetPlaylistItemsChangeStream) | **GET** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamPostPlaylistItemsChangeStream) | **POST** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemDeleteById**](PlaylistItemApi.md#playlistItemDeleteById) | **DELETE** /PlaylistItems/{id} | Delete a model instance by {{id}} from the data source.
[**playlistItemExistsGetPlaylistItemsidExists**](PlaylistItemApi.md#playlistItemExistsGetPlaylistItemsidExists) | **GET** /PlaylistItems/{id}/exists | Check whether a model instance exists in the data source.
[**playlistItemExistsHeadPlaylistItemsid**](PlaylistItemApi.md#playlistItemExistsHeadPlaylistItemsid) | **HEAD** /PlaylistItems/{id} | Check whether a model instance exists in the data source.
[**playlistItemFind**](PlaylistItemApi.md#playlistItemFind) | **GET** /PlaylistItems | Find all instances of the model matched by filter from the data source.
[**playlistItemFindById**](PlaylistItemApi.md#playlistItemFindById) | **GET** /PlaylistItems/{id} | Find a model instance by {{id}} from the data source.
[**playlistItemFindOne**](PlaylistItemApi.md#playlistItemFindOne) | **GET** /PlaylistItems/findOne | Find first instance of the model matched by filter from the data source.
[**playlistItemPrototypeCountPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/count | Counts playerSources of PlaylistItem.
[**playlistItemPrototypeCountPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/count | Counts playerTracks of PlaylistItem.
[**playlistItemPrototypeCountRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCountRadioShows) | **GET** /PlaylistItems/{id}/radioShows/count | Counts radioShows of PlaylistItem.
[**playlistItemPrototypeCreatePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerSources) | **POST** /PlaylistItems/{id}/playerSources | Creates a new instance in playerSources of this model.
[**playlistItemPrototypeCreatePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerTracks) | **POST** /PlaylistItems/{id}/playerTracks | Creates a new instance in playerTracks of this model.
[**playlistItemPrototypeCreateRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCreateRadioShows) | **POST** /PlaylistItems/{id}/radioShows | Creates a new instance in radioShows of this model.
[**playlistItemPrototypeDeletePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources | Deletes all playerSources of this model.
[**playlistItemPrototypeDeletePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks | Deletes all playerTracks of this model.
[**playlistItemPrototypeDeleteRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDeleteRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows | Deletes all radioShows of this model.
[**playlistItemPrototypeDestroyByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources/{fk} | Delete a related item by id for playerSources.
[**playlistItemPrototypeDestroyByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks/{fk} | Delete a related item by id for playerTracks.
[**playlistItemPrototypeDestroyByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**playlistItemPrototypeFindByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/{fk} | Find a related item by id for playerSources.
[**playlistItemPrototypeFindByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/{fk} | Find a related item by id for playerTracks.
[**playlistItemPrototypeFindByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeFindByIdRadioShows) | **GET** /PlaylistItems/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**playlistItemPrototypeGetPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerSources) | **GET** /PlaylistItems/{id}/playerSources | Queries playerSources of PlaylistItem.
[**playlistItemPrototypeGetPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks | Queries playerTracks of PlaylistItem.
[**playlistItemPrototypeGetPlaylist**](PlaylistItemApi.md#playlistItemPrototypeGetPlaylist) | **GET** /PlaylistItems/{id}/playlist | Fetches belongsTo relation playlist.
[**playlistItemPrototypeGetRadioShows**](PlaylistItemApi.md#playlistItemPrototypeGetRadioShows) | **GET** /PlaylistItems/{id}/radioShows | Queries radioShows of PlaylistItem.
[**playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid) | **PATCH** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPutPlaylistItemsid) | **PUT** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerSources) | **PUT** /PlaylistItems/{id}/playerSources/{fk} | Update a related item by id for playerSources.
[**playlistItemPrototypeUpdateByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerTracks) | **PUT** /PlaylistItems/{id}/playerTracks/{fk} | Update a related item by id for playerTracks.
[**playlistItemPrototypeUpdateByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdRadioShows) | **PUT** /PlaylistItems/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**playlistItemReplaceById**](PlaylistItemApi.md#playlistItemReplaceById) | **POST** /PlaylistItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistItemReplaceOrCreate**](PlaylistItemApi.md#playlistItemReplaceOrCreate) | **POST** /PlaylistItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistItemUpdateAll**](PlaylistItemApi.md#playlistItemUpdateAll) | **POST** /PlaylistItems/update | Update instances of the model matched by {{where}} from the data source.
[**playlistItemUpsertPatchPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPatchPlaylistItems) | **PATCH** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertPutPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPutPlaylistItems) | **PUT** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertWithWhere**](PlaylistItemApi.md#playlistItemUpsertWithWhere) | **POST** /PlaylistItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playlistItemCount"></a>
# **playlistItemCount**
> InlineResponse2001 playlistItemCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistItemCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreate"></a>
# **playlistItemCreate**
> PlaylistItem playlistItemCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | Model instance data
};
apiInstance.playlistItemCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreateChangeStreamGetPlaylistItemsChangeStream"></a>
# **playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**
> File playlistItemCreateChangeStreamGetPlaylistItemsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playlistItemCreateChangeStreamGetPlaylistItemsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreateChangeStreamPostPlaylistItemsChangeStream"></a>
# **playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**
> File playlistItemCreateChangeStreamPostPlaylistItemsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playlistItemCreateChangeStreamPostPlaylistItemsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemDeleteById"></a>
# **playlistItemDeleteById**
> Object playlistItemDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | Model id

apiInstance.playlistItemDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemExistsGetPlaylistItemsidExists"></a>
# **playlistItemExistsGetPlaylistItemsidExists**
> InlineResponse2003 playlistItemExistsGetPlaylistItemsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | Model id

apiInstance.playlistItemExistsGetPlaylistItemsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemExistsHeadPlaylistItemsid"></a>
# **playlistItemExistsHeadPlaylistItemsid**
> InlineResponse2003 playlistItemExistsHeadPlaylistItemsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | Model id

apiInstance.playlistItemExistsHeadPlaylistItemsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFind"></a>
# **playlistItemFind**
> [PlaylistItem] playlistItemFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playlistItemFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[PlaylistItem]**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFindById"></a>
# **playlistItemFindById**
> PlaylistItem playlistItemFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.playlistItemFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFindOne"></a>
# **playlistItemFindOne**
> PlaylistItem playlistItemFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playlistItemFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountPlayerSources"></a>
# **playlistItemPrototypeCountPlayerSources**
> InlineResponse2001 playlistItemPrototypeCountPlayerSources(id, opts)

Counts playerSources of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistItemPrototypeCountPlayerSources(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountPlayerTracks"></a>
# **playlistItemPrototypeCountPlayerTracks**
> InlineResponse2001 playlistItemPrototypeCountPlayerTracks(id, opts)

Counts playerTracks of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistItemPrototypeCountPlayerTracks(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountRadioShows"></a>
# **playlistItemPrototypeCountRadioShows**
> InlineResponse2001 playlistItemPrototypeCountRadioShows(id, opts)

Counts radioShows of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistItemPrototypeCountRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreatePlayerSources"></a>
# **playlistItemPrototypeCreatePlayerSources**
> PlayerSource playlistItemPrototypeCreatePlayerSources(id, opts)

Creates a new instance in playerSources of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | 
};
apiInstance.playlistItemPrototypeCreatePlayerSources(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlayerSource**](PlayerSource.md)|  | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreatePlayerTracks"></a>
# **playlistItemPrototypeCreatePlayerTracks**
> PlayerTrack playlistItemPrototypeCreatePlayerTracks(id, opts)

Creates a new instance in playerTracks of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | 
};
apiInstance.playlistItemPrototypeCreatePlayerTracks(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlayerTrack**](PlayerTrack.md)|  | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreateRadioShows"></a>
# **playlistItemPrototypeCreateRadioShows**
> RadioShow playlistItemPrototypeCreateRadioShows(id, opts)

Creates a new instance in radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.playlistItemPrototypeCreateRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeletePlayerSources"></a>
# **playlistItemPrototypeDeletePlayerSources**
> playlistItemPrototypeDeletePlayerSources(id)

Deletes all playerSources of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDeletePlayerSources(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeletePlayerTracks"></a>
# **playlistItemPrototypeDeletePlayerTracks**
> playlistItemPrototypeDeletePlayerTracks(id)

Deletes all playerTracks of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDeletePlayerTracks(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeleteRadioShows"></a>
# **playlistItemPrototypeDeleteRadioShows**
> playlistItemPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDeleteRadioShows(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdPlayerSources"></a>
# **playlistItemPrototypeDestroyByIdPlayerSources**
> playlistItemPrototypeDestroyByIdPlayerSources(fk, id)

Delete a related item by id for playerSources.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerSources

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDestroyByIdPlayerSources(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources | 
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdPlayerTracks"></a>
# **playlistItemPrototypeDestroyByIdPlayerTracks**
> playlistItemPrototypeDestroyByIdPlayerTracks(fk, id)

Delete a related item by id for playerTracks.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerTracks

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDestroyByIdPlayerTracks(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks | 
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdRadioShows"></a>
# **playlistItemPrototypeDestroyByIdRadioShows**
> playlistItemPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeDestroyByIdRadioShows(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| PlaylistItem id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdPlayerSources"></a>
# **playlistItemPrototypeFindByIdPlayerSources**
> PlayerSource playlistItemPrototypeFindByIdPlayerSources(fk, id)

Find a related item by id for playerSources.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerSources

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeFindByIdPlayerSources(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources | 
 **id** | **String**| PlaylistItem id | 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdPlayerTracks"></a>
# **playlistItemPrototypeFindByIdPlayerTracks**
> PlayerTrack playlistItemPrototypeFindByIdPlayerTracks(fk, id)

Find a related item by id for playerTracks.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerTracks

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeFindByIdPlayerTracks(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks | 
 **id** | **String**| PlaylistItem id | 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdRadioShows"></a>
# **playlistItemPrototypeFindByIdRadioShows**
> RadioShow playlistItemPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | PlaylistItem id

apiInstance.playlistItemPrototypeFindByIdRadioShows(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| PlaylistItem id | 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlayerSources"></a>
# **playlistItemPrototypeGetPlayerSources**
> [PlayerSource] playlistItemPrototypeGetPlayerSources(id, opts)

Queries playerSources of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.playlistItemPrototypeGetPlayerSources(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[PlayerSource]**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlayerTracks"></a>
# **playlistItemPrototypeGetPlayerTracks**
> [PlayerTrack] playlistItemPrototypeGetPlayerTracks(id, opts)

Queries playerTracks of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.playlistItemPrototypeGetPlayerTracks(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[PlayerTrack]**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlaylist"></a>
# **playlistItemPrototypeGetPlaylist**
> Playlist playlistItemPrototypeGetPlaylist(id, opts)

Fetches belongsTo relation playlist.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.playlistItemPrototypeGetPlaylist(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetRadioShows"></a>
# **playlistItemPrototypeGetRadioShows**
> [RadioShow] playlistItemPrototypeGetRadioShows(id, opts)

Queries radioShows of PlaylistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.playlistItemPrototypeGetRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioShow]**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid"></a>
# **playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**
> PlaylistItem playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | An object of model property name/value pairs
};
apiInstance.playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateAttributesPutPlaylistItemsid"></a>
# **playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**
> PlaylistItem playlistItemPrototypeUpdateAttributesPutPlaylistItemsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | An object of model property name/value pairs
};
apiInstance.playlistItemPrototypeUpdateAttributesPutPlaylistItemsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdPlayerSources"></a>
# **playlistItemPrototypeUpdateByIdPlayerSources**
> PlayerSource playlistItemPrototypeUpdateByIdPlayerSources(fk, id, opts)

Update a related item by id for playerSources.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerSources

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | 
};
apiInstance.playlistItemPrototypeUpdateByIdPlayerSources(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources | 
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlayerSource**](PlayerSource.md)|  | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdPlayerTracks"></a>
# **playlistItemPrototypeUpdateByIdPlayerTracks**
> PlayerTrack playlistItemPrototypeUpdateByIdPlayerTracks(fk, id, opts)

Update a related item by id for playerTracks.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for playerTracks

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.PlayerTrack() // PlayerTrack | 
};
apiInstance.playlistItemPrototypeUpdateByIdPlayerTracks(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks | 
 **id** | **String**| PlaylistItem id | 
 **data** | [**PlayerTrack**](PlayerTrack.md)|  | [optional] 

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdRadioShows"></a>
# **playlistItemPrototypeUpdateByIdRadioShows**
> RadioShow playlistItemPrototypeUpdateByIdRadioShows(fk, id, opts)

Update a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | PlaylistItem id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.playlistItemPrototypeUpdateByIdRadioShows(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| PlaylistItem id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemReplaceById"></a>
# **playlistItemReplaceById**
> PlaylistItem playlistItemReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | Model instance data
};
apiInstance.playlistItemReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemReplaceOrCreate"></a>
# **playlistItemReplaceOrCreate**
> PlaylistItem playlistItemReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | Model instance data
};
apiInstance.playlistItemReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpdateAll"></a>
# **playlistItemUpdateAll**
> InlineResponse2002 playlistItemUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | An object of model property name/value pairs
};
apiInstance.playlistItemUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertPatchPlaylistItems"></a>
# **playlistItemUpsertPatchPlaylistItems**
> PlaylistItem playlistItemUpsertPatchPlaylistItems(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | Model instance data
};
apiInstance.playlistItemUpsertPatchPlaylistItems(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertPutPlaylistItems"></a>
# **playlistItemUpsertPutPlaylistItems**
> PlaylistItem playlistItemUpsertPutPlaylistItems(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | Model instance data
};
apiInstance.playlistItemUpsertPutPlaylistItems(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertWithWhere"></a>
# **playlistItemUpsertWithWhere**
> PlaylistItem playlistItemUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistItemApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | An object of model property name/value pairs
};
apiInstance.playlistItemUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


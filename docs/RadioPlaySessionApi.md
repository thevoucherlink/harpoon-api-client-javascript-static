# harpoonApi.RadioPlaySessionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPlaySessionCount**](RadioPlaySessionApi.md#radioPlaySessionCount) | **GET** /RadioPlaySessions/count | Count instances of the model matched by where from the data source.
[**radioPlaySessionCreate**](RadioPlaySessionApi.md#radioPlaySessionCreate) | **POST** /RadioPlaySessions | Create a new instance of the model and persist it into the data source.
[**radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream) | **GET** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream) | **POST** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionDeleteById**](RadioPlaySessionApi.md#radioPlaySessionDeleteById) | **DELETE** /RadioPlaySessions/{id} | Delete a model instance by {{id}} from the data source.
[**radioPlaySessionExistsGetRadioPlaySessionsidExists**](RadioPlaySessionApi.md#radioPlaySessionExistsGetRadioPlaySessionsidExists) | **GET** /RadioPlaySessions/{id}/exists | Check whether a model instance exists in the data source.
[**radioPlaySessionExistsHeadRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionExistsHeadRadioPlaySessionsid) | **HEAD** /RadioPlaySessions/{id} | Check whether a model instance exists in the data source.
[**radioPlaySessionFind**](RadioPlaySessionApi.md#radioPlaySessionFind) | **GET** /RadioPlaySessions | Find all instances of the model matched by filter from the data source.
[**radioPlaySessionFindById**](RadioPlaySessionApi.md#radioPlaySessionFindById) | **GET** /RadioPlaySessions/{id} | Find a model instance by {{id}} from the data source.
[**radioPlaySessionFindOne**](RadioPlaySessionApi.md#radioPlaySessionFindOne) | **GET** /RadioPlaySessions/findOne | Find first instance of the model matched by filter from the data source.
[**radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid) | **PATCH** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid) | **PUT** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceById**](RadioPlaySessionApi.md#radioPlaySessionReplaceById) | **POST** /RadioPlaySessions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceOrCreate**](RadioPlaySessionApi.md#radioPlaySessionReplaceOrCreate) | **POST** /RadioPlaySessions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpdateAll**](RadioPlaySessionApi.md#radioPlaySessionUpdateAll) | **POST** /RadioPlaySessions/update | Update instances of the model matched by {{where}} from the data source.
[**radioPlaySessionUpsertPatchRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPatchRadioPlaySessions) | **PATCH** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertPutRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPutRadioPlaySessions) | **PUT** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertWithWhere**](RadioPlaySessionApi.md#radioPlaySessionUpsertWithWhere) | **POST** /RadioPlaySessions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPlaySessionCount"></a>
# **radioPlaySessionCount**
> InlineResponse2001 radioPlaySessionCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioPlaySessionCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreate"></a>
# **radioPlaySessionCreate**
> RadioPlaySession radioPlaySessionCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | Model instance data
};
apiInstance.radioPlaySessionCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream"></a>
# **radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**
> File radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream"></a>
# **radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**
> File radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionDeleteById"></a>
# **radioPlaySessionDeleteById**
> Object radioPlaySessionDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | Model id

apiInstance.radioPlaySessionDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionExistsGetRadioPlaySessionsidExists"></a>
# **radioPlaySessionExistsGetRadioPlaySessionsidExists**
> InlineResponse2003 radioPlaySessionExistsGetRadioPlaySessionsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | Model id

apiInstance.radioPlaySessionExistsGetRadioPlaySessionsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionExistsHeadRadioPlaySessionsid"></a>
# **radioPlaySessionExistsHeadRadioPlaySessionsid**
> InlineResponse2003 radioPlaySessionExistsHeadRadioPlaySessionsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | Model id

apiInstance.radioPlaySessionExistsHeadRadioPlaySessionsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFind"></a>
# **radioPlaySessionFind**
> [RadioPlaySession] radioPlaySessionFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPlaySessionFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioPlaySession]**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFindById"></a>
# **radioPlaySessionFindById**
> RadioPlaySession radioPlaySessionFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioPlaySessionFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFindOne"></a>
# **radioPlaySessionFindOne**
> RadioPlaySession radioPlaySessionFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPlaySessionFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid"></a>
# **radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**
> RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | RadioPlaySession id

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | An object of model property name/value pairs
};
apiInstance.radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPlaySession id | 
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid"></a>
# **radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**
> RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | RadioPlaySession id

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | An object of model property name/value pairs
};
apiInstance.radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPlaySession id | 
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionReplaceById"></a>
# **radioPlaySessionReplaceById**
> RadioPlaySession radioPlaySessionReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | Model instance data
};
apiInstance.radioPlaySessionReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionReplaceOrCreate"></a>
# **radioPlaySessionReplaceOrCreate**
> RadioPlaySession radioPlaySessionReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | Model instance data
};
apiInstance.radioPlaySessionReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpdateAll"></a>
# **radioPlaySessionUpdateAll**
> InlineResponse2002 radioPlaySessionUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | An object of model property name/value pairs
};
apiInstance.radioPlaySessionUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertPatchRadioPlaySessions"></a>
# **radioPlaySessionUpsertPatchRadioPlaySessions**
> RadioPlaySession radioPlaySessionUpsertPatchRadioPlaySessions(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | Model instance data
};
apiInstance.radioPlaySessionUpsertPatchRadioPlaySessions(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertPutRadioPlaySessions"></a>
# **radioPlaySessionUpsertPutRadioPlaySessions**
> RadioPlaySession radioPlaySessionUpsertPutRadioPlaySessions(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | Model instance data
};
apiInstance.radioPlaySessionUpsertPutRadioPlaySessions(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertWithWhere"></a>
# **radioPlaySessionUpsertWithWhere**
> RadioPlaySession radioPlaySessionUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPlaySessionApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPlaySession() // RadioPlaySession | An object of model property name/value pairs
};
apiInstance.radioPlaySessionUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


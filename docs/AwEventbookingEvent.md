# harpoonApi.AwEventbookingEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**productId** | **Number** |  | [optional] 
**isEnabled** | **Number** |  | [optional] 
**eventStartDate** | **Date** |  | [optional] 
**eventEndDate** | **Date** |  | [optional] 
**dayCountBeforeSendReminderLetter** | **Number** |  | [optional] 
**isReminderSend** | **Number** |  | 
**isTermsEnabled** | **Number** |  | [optional] 
**generatePdfTickets** | **Number** |  | 
**redeemRoles** | **String** |  | [optional] 
**location** | **String** |  | [optional] 
**tickets** | **[Object]** |  | [optional] 
**attributes** | **[Object]** |  | [optional] 
**purchasedTickets** | **[Object]** |  | [optional] 



# harpoonApi.RadioStreamApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioStreamCount**](RadioStreamApi.md#radioStreamCount) | **GET** /RadioStreams/count | Count instances of the model matched by where from the data source.
[**radioStreamCreate**](RadioStreamApi.md#radioStreamCreate) | **POST** /RadioStreams | Create a new instance of the model and persist it into the data source.
[**radioStreamCreateChangeStreamGetRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamGetRadioStreamsChangeStream) | **GET** /RadioStreams/change-stream | Create a change stream.
[**radioStreamCreateChangeStreamPostRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamPostRadioStreamsChangeStream) | **POST** /RadioStreams/change-stream | Create a change stream.
[**radioStreamDeleteById**](RadioStreamApi.md#radioStreamDeleteById) | **DELETE** /RadioStreams/{id} | Delete a model instance by {{id}} from the data source.
[**radioStreamExistsGetRadioStreamsidExists**](RadioStreamApi.md#radioStreamExistsGetRadioStreamsidExists) | **GET** /RadioStreams/{id}/exists | Check whether a model instance exists in the data source.
[**radioStreamExistsHeadRadioStreamsid**](RadioStreamApi.md#radioStreamExistsHeadRadioStreamsid) | **HEAD** /RadioStreams/{id} | Check whether a model instance exists in the data source.
[**radioStreamFind**](RadioStreamApi.md#radioStreamFind) | **GET** /RadioStreams | Find all instances of the model matched by filter from the data source.
[**radioStreamFindById**](RadioStreamApi.md#radioStreamFindById) | **GET** /RadioStreams/{id} | Find a model instance by {{id}} from the data source.
[**radioStreamFindOne**](RadioStreamApi.md#radioStreamFindOne) | **GET** /RadioStreams/findOne | Find first instance of the model matched by filter from the data source.
[**radioStreamPrototypeCountRadioShows**](RadioStreamApi.md#radioStreamPrototypeCountRadioShows) | **GET** /RadioStreams/{id}/radioShows/count | Counts radioShows of RadioStream.
[**radioStreamPrototypeCreateRadioShows**](RadioStreamApi.md#radioStreamPrototypeCreateRadioShows) | **POST** /RadioStreams/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioStreamPrototypeDeleteRadioShows**](RadioStreamApi.md#radioStreamPrototypeDeleteRadioShows) | **DELETE** /RadioStreams/{id}/radioShows | Deletes all radioShows of this model.
[**radioStreamPrototypeDestroyByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeDestroyByIdRadioShows) | **DELETE** /RadioStreams/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioStreamPrototypeFindByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeFindByIdRadioShows) | **GET** /RadioStreams/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioStreamPrototypeGetRadioShows**](RadioStreamApi.md#radioStreamPrototypeGetRadioShows) | **GET** /RadioStreams/{id}/radioShows | Queries radioShows of RadioStream.
[**radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPatchRadioStreamsid) | **PATCH** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateAttributesPutRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPutRadioStreamsid) | **PUT** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeUpdateByIdRadioShows) | **PUT** /RadioStreams/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioStreamRadioShowCurrent**](RadioStreamApi.md#radioStreamRadioShowCurrent) | **GET** /RadioStreams/{id}/radioShows/current | 
[**radioStreamRadioShowSchedule**](RadioStreamApi.md#radioStreamRadioShowSchedule) | **GET** /RadioStreams/{id}/radioShows/schedule | 
[**radioStreamReplaceById**](RadioStreamApi.md#radioStreamReplaceById) | **POST** /RadioStreams/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioStreamReplaceOrCreate**](RadioStreamApi.md#radioStreamReplaceOrCreate) | **POST** /RadioStreams/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioStreamUpdateAll**](RadioStreamApi.md#radioStreamUpdateAll) | **POST** /RadioStreams/update | Update instances of the model matched by {{where}} from the data source.
[**radioStreamUpsertPatchRadioStreams**](RadioStreamApi.md#radioStreamUpsertPatchRadioStreams) | **PATCH** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertPutRadioStreams**](RadioStreamApi.md#radioStreamUpsertPutRadioStreams) | **PUT** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertWithWhere**](RadioStreamApi.md#radioStreamUpsertWithWhere) | **POST** /RadioStreams/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioStreamCount"></a>
# **radioStreamCount**
> InlineResponse2001 radioStreamCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioStreamCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreate"></a>
# **radioStreamCreate**
> RadioStream radioStreamCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | Model instance data
};
apiInstance.radioStreamCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreateChangeStreamGetRadioStreamsChangeStream"></a>
# **radioStreamCreateChangeStreamGetRadioStreamsChangeStream**
> File radioStreamCreateChangeStreamGetRadioStreamsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioStreamCreateChangeStreamGetRadioStreamsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreateChangeStreamPostRadioStreamsChangeStream"></a>
# **radioStreamCreateChangeStreamPostRadioStreamsChangeStream**
> File radioStreamCreateChangeStreamPostRadioStreamsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioStreamCreateChangeStreamPostRadioStreamsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamDeleteById"></a>
# **radioStreamDeleteById**
> Object radioStreamDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model id

apiInstance.radioStreamDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamExistsGetRadioStreamsidExists"></a>
# **radioStreamExistsGetRadioStreamsidExists**
> InlineResponse2003 radioStreamExistsGetRadioStreamsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model id

apiInstance.radioStreamExistsGetRadioStreamsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamExistsHeadRadioStreamsid"></a>
# **radioStreamExistsHeadRadioStreamsid**
> InlineResponse2003 radioStreamExistsHeadRadioStreamsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model id

apiInstance.radioStreamExistsHeadRadioStreamsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFind"></a>
# **radioStreamFind**
> [RadioStream] radioStreamFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioStreamFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioStream]**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFindById"></a>
# **radioStreamFindById**
> RadioStream radioStreamFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioStreamFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFindOne"></a>
# **radioStreamFindOne**
> RadioStream radioStreamFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioStreamFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeCountRadioShows"></a>
# **radioStreamPrototypeCountRadioShows**
> InlineResponse2001 radioStreamPrototypeCountRadioShows(id, opts)

Counts radioShows of RadioStream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioStreamPrototypeCountRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeCreateRadioShows"></a>
# **radioStreamPrototypeCreateRadioShows**
> RadioShow radioStreamPrototypeCreateRadioShows(id, opts)

Creates a new instance in radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.radioStreamPrototypeCreateRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeDeleteRadioShows"></a>
# **radioStreamPrototypeDeleteRadioShows**
> radioStreamPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

apiInstance.radioStreamPrototypeDeleteRadioShows(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeDestroyByIdRadioShows"></a>
# **radioStreamPrototypeDestroyByIdRadioShows**
> radioStreamPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioStream id

apiInstance.radioStreamPrototypeDestroyByIdRadioShows(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioStream id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeFindByIdRadioShows"></a>
# **radioStreamPrototypeFindByIdRadioShows**
> RadioShow radioStreamPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioStream id

apiInstance.radioStreamPrototypeFindByIdRadioShows(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioStream id | 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeGetRadioShows"></a>
# **radioStreamPrototypeGetRadioShows**
> [RadioShow] radioStreamPrototypeGetRadioShows(id, opts)

Queries radioShows of RadioStream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.radioStreamPrototypeGetRadioShows(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioShow]**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateAttributesPatchRadioStreamsid"></a>
# **radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**
> RadioStream radioStreamPrototypeUpdateAttributesPatchRadioStreamsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | An object of model property name/value pairs
};
apiInstance.radioStreamPrototypeUpdateAttributesPatchRadioStreamsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateAttributesPutRadioStreamsid"></a>
# **radioStreamPrototypeUpdateAttributesPutRadioStreamsid**
> RadioStream radioStreamPrototypeUpdateAttributesPutRadioStreamsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | RadioStream id

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | An object of model property name/value pairs
};
apiInstance.radioStreamPrototypeUpdateAttributesPutRadioStreamsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id | 
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateByIdRadioShows"></a>
# **radioStreamPrototypeUpdateByIdRadioShows**
> RadioShow radioStreamPrototypeUpdateByIdRadioShows(fk, id, opts)

Update a related item by id for radioShows.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var fk = "fk_example"; // String | Foreign key for radioShows

var id = "id_example"; // String | RadioStream id

var opts = { 
  'data': new harpoonApi.RadioShow() // RadioShow | 
};
apiInstance.radioStreamPrototypeUpdateByIdRadioShows(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows | 
 **id** | **String**| RadioStream id | 
 **data** | [**RadioShow**](RadioShow.md)|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamRadioShowCurrent"></a>
# **radioStreamRadioShowCurrent**
> RadioShow radioStreamRadioShowCurrent(id)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model Id

apiInstance.radioStreamRadioShowCurrent(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model Id | 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamRadioShowSchedule"></a>
# **radioStreamRadioShowSchedule**
> [RadioShow] radioStreamRadioShowSchedule(id)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model Id

apiInstance.radioStreamRadioShowSchedule(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model Id | 

### Return type

[**[RadioShow]**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamReplaceById"></a>
# **radioStreamReplaceById**
> RadioStream radioStreamReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | Model instance data
};
apiInstance.radioStreamReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamReplaceOrCreate"></a>
# **radioStreamReplaceOrCreate**
> RadioStream radioStreamReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | Model instance data
};
apiInstance.radioStreamReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpdateAll"></a>
# **radioStreamUpdateAll**
> InlineResponse2002 radioStreamUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioStream() // RadioStream | An object of model property name/value pairs
};
apiInstance.radioStreamUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertPatchRadioStreams"></a>
# **radioStreamUpsertPatchRadioStreams**
> RadioStream radioStreamUpsertPatchRadioStreams(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | Model instance data
};
apiInstance.radioStreamUpsertPatchRadioStreams(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertPutRadioStreams"></a>
# **radioStreamUpsertPutRadioStreams**
> RadioStream radioStreamUpsertPutRadioStreams(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | Model instance data
};
apiInstance.radioStreamUpsertPutRadioStreams(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertWithWhere"></a>
# **radioStreamUpsertWithWhere**
> RadioStream radioStreamUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioStreamApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioStream() // RadioStream | An object of model property name/value pairs
};
apiInstance.radioStreamUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


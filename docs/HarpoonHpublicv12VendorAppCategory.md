# harpoonApi.HarpoonHpublicv12VendorAppCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**appId** | **Number** |  | 
**vendorId** | **Number** |  | 
**categoryId** | **Number** |  | 
**isPrimary** | **Number** |  | [optional] [default to 0.0]
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 



# harpoonApi.AppApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appAnalyticTrack**](AppApi.md#appAnalyticTrack) | **POST** /Apps/{id}/analytics/track | 
[**appCount**](AppApi.md#appCount) | **GET** /Apps/count | Count instances of the model matched by where from the data source.
[**appCreateChangeStreamGetAppsChangeStream**](AppApi.md#appCreateChangeStreamGetAppsChangeStream) | **GET** /Apps/change-stream | Create a change stream.
[**appCreateChangeStreamPostAppsChangeStream**](AppApi.md#appCreateChangeStreamPostAppsChangeStream) | **POST** /Apps/change-stream | Create a change stream.
[**appCreateWithAuthentication**](AppApi.md#appCreateWithAuthentication) | **POST** /Apps | Create a new instance of the model and persist it into the data source.
[**appDeleteById**](AppApi.md#appDeleteById) | **DELETE** /Apps/{id} | Delete a model instance by {{id}} from the data source.
[**appExistsGetAppsidExists**](AppApi.md#appExistsGetAppsidExists) | **GET** /Apps/{id}/exists | Check whether a model instance exists in the data source.
[**appExistsHeadAppsid**](AppApi.md#appExistsHeadAppsid) | **HEAD** /Apps/{id} | Check whether a model instance exists in the data source.
[**appFind**](AppApi.md#appFind) | **GET** /Apps | Find all instances of the model matched by filter from the data source.
[**appFindById**](AppApi.md#appFindById) | **GET** /Apps/{id} | Find a model instance by {{id}} from the data source.
[**appFindByIdForVersion**](AppApi.md#appFindByIdForVersion) | **GET** /Apps/{id}/{appOs}/{appVersion} | 
[**appFindOne**](AppApi.md#appFindOne) | **GET** /Apps/findOne | Find first instance of the model matched by filter from the data source.
[**appPrototypeCountAppConfigs**](AppApi.md#appPrototypeCountAppConfigs) | **GET** /Apps/{id}/appConfigs/count | Counts appConfigs of App.
[**appPrototypeCountApps**](AppApi.md#appPrototypeCountApps) | **GET** /Apps/{id}/apps/count | Counts apps of App.
[**appPrototypeCountPlaylists**](AppApi.md#appPrototypeCountPlaylists) | **GET** /Apps/{id}/playlists/count | Counts playlists of App.
[**appPrototypeCountRadioStreams**](AppApi.md#appPrototypeCountRadioStreams) | **GET** /Apps/{id}/radioStreams/count | Counts radioStreams of App.
[**appPrototypeCountRssFeedGroups**](AppApi.md#appPrototypeCountRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/count | Counts rssFeedGroups of App.
[**appPrototypeCountRssFeeds**](AppApi.md#appPrototypeCountRssFeeds) | **GET** /Apps/{id}/rssFeeds/count | Counts rssFeeds of App.
[**appPrototypeCreateAppConfigs**](AppApi.md#appPrototypeCreateAppConfigs) | **POST** /Apps/{id}/appConfigs | Creates a new instance in appConfigs of this model.
[**appPrototypeCreateApps**](AppApi.md#appPrototypeCreateApps) | **POST** /Apps/{id}/apps | Creates a new instance in apps of this model.
[**appPrototypeCreateCustomers**](AppApi.md#appPrototypeCreateCustomers) | **POST** /Apps/{id}/customers | Creates a new instance in customers of this model.
[**appPrototypeCreatePlaylists**](AppApi.md#appPrototypeCreatePlaylists) | **POST** /Apps/{id}/playlists | Creates a new instance in playlists of this model.
[**appPrototypeCreateRadioStreams**](AppApi.md#appPrototypeCreateRadioStreams) | **POST** /Apps/{id}/radioStreams | Creates a new instance in radioStreams of this model.
[**appPrototypeCreateRssFeedGroups**](AppApi.md#appPrototypeCreateRssFeedGroups) | **POST** /Apps/{id}/rssFeedGroups | Creates a new instance in rssFeedGroups of this model.
[**appPrototypeCreateRssFeeds**](AppApi.md#appPrototypeCreateRssFeeds) | **POST** /Apps/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**appPrototypeDeleteAppConfigs**](AppApi.md#appPrototypeDeleteAppConfigs) | **DELETE** /Apps/{id}/appConfigs | Deletes all appConfigs of this model.
[**appPrototypeDeleteApps**](AppApi.md#appPrototypeDeleteApps) | **DELETE** /Apps/{id}/apps | Deletes all apps of this model.
[**appPrototypeDeletePlaylists**](AppApi.md#appPrototypeDeletePlaylists) | **DELETE** /Apps/{id}/playlists | Deletes all playlists of this model.
[**appPrototypeDeleteRadioStreams**](AppApi.md#appPrototypeDeleteRadioStreams) | **DELETE** /Apps/{id}/radioStreams | Deletes all radioStreams of this model.
[**appPrototypeDeleteRssFeedGroups**](AppApi.md#appPrototypeDeleteRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups | Deletes all rssFeedGroups of this model.
[**appPrototypeDeleteRssFeeds**](AppApi.md#appPrototypeDeleteRssFeeds) | **DELETE** /Apps/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**appPrototypeDestroyByIdAppConfigs**](AppApi.md#appPrototypeDestroyByIdAppConfigs) | **DELETE** /Apps/{id}/appConfigs/{fk} | Delete a related item by id for appConfigs.
[**appPrototypeDestroyByIdApps**](AppApi.md#appPrototypeDestroyByIdApps) | **DELETE** /Apps/{id}/apps/{fk} | Delete a related item by id for apps.
[**appPrototypeDestroyByIdPlaylists**](AppApi.md#appPrototypeDestroyByIdPlaylists) | **DELETE** /Apps/{id}/playlists/{fk} | Delete a related item by id for playlists.
[**appPrototypeDestroyByIdRadioStreams**](AppApi.md#appPrototypeDestroyByIdRadioStreams) | **DELETE** /Apps/{id}/radioStreams/{fk} | Delete a related item by id for radioStreams.
[**appPrototypeDestroyByIdRssFeedGroups**](AppApi.md#appPrototypeDestroyByIdRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups/{fk} | Delete a related item by id for rssFeedGroups.
[**appPrototypeDestroyByIdRssFeeds**](AppApi.md#appPrototypeDestroyByIdRssFeeds) | **DELETE** /Apps/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**appPrototypeFindByIdAppConfigs**](AppApi.md#appPrototypeFindByIdAppConfigs) | **GET** /Apps/{id}/appConfigs/{fk} | Find a related item by id for appConfigs.
[**appPrototypeFindByIdApps**](AppApi.md#appPrototypeFindByIdApps) | **GET** /Apps/{id}/apps/{fk} | Find a related item by id for apps.
[**appPrototypeFindByIdCustomers**](AppApi.md#appPrototypeFindByIdCustomers) | **GET** /Apps/{id}/customers/{fk} | Find a related item by id for customers.
[**appPrototypeFindByIdPlaylists**](AppApi.md#appPrototypeFindByIdPlaylists) | **GET** /Apps/{id}/playlists/{fk} | Find a related item by id for playlists.
[**appPrototypeFindByIdRadioStreams**](AppApi.md#appPrototypeFindByIdRadioStreams) | **GET** /Apps/{id}/radioStreams/{fk} | Find a related item by id for radioStreams.
[**appPrototypeFindByIdRssFeedGroups**](AppApi.md#appPrototypeFindByIdRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/{fk} | Find a related item by id for rssFeedGroups.
[**appPrototypeFindByIdRssFeeds**](AppApi.md#appPrototypeFindByIdRssFeeds) | **GET** /Apps/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**appPrototypeGetAppConfigs**](AppApi.md#appPrototypeGetAppConfigs) | **GET** /Apps/{id}/appConfigs | Queries appConfigs of App.
[**appPrototypeGetApps**](AppApi.md#appPrototypeGetApps) | **GET** /Apps/{id}/apps | Queries apps of App.
[**appPrototypeGetCustomers**](AppApi.md#appPrototypeGetCustomers) | **GET** /Apps/{id}/customers | Queries customers of App.
[**appPrototypeGetParent**](AppApi.md#appPrototypeGetParent) | **GET** /Apps/{id}/parent | Fetches belongsTo relation parent.
[**appPrototypeGetPlaylists**](AppApi.md#appPrototypeGetPlaylists) | **GET** /Apps/{id}/playlists | Queries playlists of App.
[**appPrototypeGetRadioStreams**](AppApi.md#appPrototypeGetRadioStreams) | **GET** /Apps/{id}/radioStreams | Queries radioStreams of App.
[**appPrototypeGetRssFeedGroups**](AppApi.md#appPrototypeGetRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups | Queries rssFeedGroups of App.
[**appPrototypeGetRssFeeds**](AppApi.md#appPrototypeGetRssFeeds) | **GET** /Apps/{id}/rssFeeds | Queries rssFeeds of App.
[**appPrototypeUpdateAttributesPatchAppsid**](AppApi.md#appPrototypeUpdateAttributesPatchAppsid) | **PATCH** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateAttributesPutAppsid**](AppApi.md#appPrototypeUpdateAttributesPutAppsid) | **PUT** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateByIdAppConfigs**](AppApi.md#appPrototypeUpdateByIdAppConfigs) | **PUT** /Apps/{id}/appConfigs/{fk} | Update a related item by id for appConfigs.
[**appPrototypeUpdateByIdApps**](AppApi.md#appPrototypeUpdateByIdApps) | **PUT** /Apps/{id}/apps/{fk} | Update a related item by id for apps.
[**appPrototypeUpdateByIdCustomers**](AppApi.md#appPrototypeUpdateByIdCustomers) | **PUT** /Apps/{id}/customers/{fk} | Update a related item by id for customers.
[**appPrototypeUpdateByIdPlaylists**](AppApi.md#appPrototypeUpdateByIdPlaylists) | **PUT** /Apps/{id}/playlists/{fk} | Update a related item by id for playlists.
[**appPrototypeUpdateByIdRadioStreams**](AppApi.md#appPrototypeUpdateByIdRadioStreams) | **PUT** /Apps/{id}/radioStreams/{fk} | Update a related item by id for radioStreams.
[**appPrototypeUpdateByIdRssFeedGroups**](AppApi.md#appPrototypeUpdateByIdRssFeedGroups) | **PUT** /Apps/{id}/rssFeedGroups/{fk} | Update a related item by id for rssFeedGroups.
[**appPrototypeUpdateByIdRssFeeds**](AppApi.md#appPrototypeUpdateByIdRssFeeds) | **PUT** /Apps/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**appReplaceById**](AppApi.md#appReplaceById) | **POST** /Apps/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**appReplaceOrCreate**](AppApi.md#appReplaceOrCreate) | **POST** /Apps/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**appUpdateAll**](AppApi.md#appUpdateAll) | **POST** /Apps/update | Update instances of the model matched by {{where}} from the data source.
[**appUploadFile**](AppApi.md#appUploadFile) | **POST** /Apps/{id}/file/{type} | 
[**appUpsertPatchApps**](AppApi.md#appUpsertPatchApps) | **PATCH** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertPutApps**](AppApi.md#appUpsertPutApps) | **PUT** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertWithWhere**](AppApi.md#appUpsertWithWhere) | **POST** /Apps/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**appVerify**](AppApi.md#appVerify) | **GET** /Apps/verify | 


<a name="appAnalyticTrack"></a>
# **appAnalyticTrack**
> Object appAnalyticTrack(id, opts)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | 

var opts = { 
  'data': new harpoonApi.AnalyticRequest() // AnalyticRequest | 
};
apiInstance.appAnalyticTrack(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **data** | [**AnalyticRequest**](AnalyticRequest.md)|  | [optional] 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCount"></a>
# **appCount**
> InlineResponse2001 appCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateChangeStreamGetAppsChangeStream"></a>
# **appCreateChangeStreamGetAppsChangeStream**
> File appCreateChangeStreamGetAppsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.appCreateChangeStreamGetAppsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateChangeStreamPostAppsChangeStream"></a>
# **appCreateChangeStreamPostAppsChangeStream**
> File appCreateChangeStreamPostAppsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.appCreateChangeStreamPostAppsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateWithAuthentication"></a>
# **appCreateWithAuthentication**
> App appCreateWithAuthentication(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'data': new harpoonApi.App() // App | Model instance data
};
apiInstance.appCreateWithAuthentication(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appDeleteById"></a>
# **appDeleteById**
> Object appDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Model id

apiInstance.appDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appExistsGetAppsidExists"></a>
# **appExistsGetAppsidExists**
> InlineResponse2003 appExistsGetAppsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Model id

apiInstance.appExistsGetAppsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appExistsHeadAppsid"></a>
# **appExistsHeadAppsid**
> InlineResponse2003 appExistsHeadAppsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Model id

apiInstance.appExistsHeadAppsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFind"></a>
# **appFind**
> [App] appFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.appFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[App]**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindById"></a>
# **appFindById**
> App appFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.appFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindByIdForVersion"></a>
# **appFindByIdForVersion**
> App appFindByIdForVersion(id, appOs, appVersion)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | 

var appOs = "appOs_example"; // String | Either \"ios\" or \"android\"

var appVersion = "appVersion_example"; // String | e.g. 1.2.3

apiInstance.appFindByIdForVersion(id, appOs, appVersion).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  | 
 **appOs** | **String**| Either \&quot;ios\&quot; or \&quot;android\&quot; | 
 **appVersion** | **String**| e.g. 1.2.3 | 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindOne"></a>
# **appFindOne**
> App appFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.appFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountAppConfigs"></a>
# **appPrototypeCountAppConfigs**
> InlineResponse2001 appPrototypeCountAppConfigs(id, opts)

Counts appConfigs of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountAppConfigs(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountApps"></a>
# **appPrototypeCountApps**
> InlineResponse2001 appPrototypeCountApps(id, opts)

Counts apps of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountApps(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountPlaylists"></a>
# **appPrototypeCountPlaylists**
> InlineResponse2001 appPrototypeCountPlaylists(id, opts)

Counts playlists of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountPlaylists(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRadioStreams"></a>
# **appPrototypeCountRadioStreams**
> InlineResponse2001 appPrototypeCountRadioStreams(id, opts)

Counts radioStreams of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountRadioStreams(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRssFeedGroups"></a>
# **appPrototypeCountRssFeedGroups**
> InlineResponse2001 appPrototypeCountRssFeedGroups(id, opts)

Counts rssFeedGroups of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountRssFeedGroups(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRssFeeds"></a>
# **appPrototypeCountRssFeeds**
> InlineResponse2001 appPrototypeCountRssFeeds(id, opts)

Counts rssFeeds of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.appPrototypeCountRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateAppConfigs"></a>
# **appPrototypeCreateAppConfigs**
> AppConfig appPrototypeCreateAppConfigs(id, opts)

Creates a new instance in appConfigs of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.AppConfig() // AppConfig | 
};
apiInstance.appPrototypeCreateAppConfigs(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**AppConfig**](AppConfig.md)|  | [optional] 

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateApps"></a>
# **appPrototypeCreateApps**
> App appPrototypeCreateApps(id, opts)

Creates a new instance in apps of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.App() // App | 
};
apiInstance.appPrototypeCreateApps(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**App**](App.md)|  | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateCustomers"></a>
# **appPrototypeCreateCustomers**
> Customer appPrototypeCreateCustomers(id, opts)

Creates a new instance in customers of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.Customer() // Customer | 
};
apiInstance.appPrototypeCreateCustomers(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**Customer**](Customer.md)|  | [optional] 

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreatePlaylists"></a>
# **appPrototypeCreatePlaylists**
> Playlist appPrototypeCreatePlaylists(id, opts)

Creates a new instance in playlists of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | 
};
apiInstance.appPrototypeCreatePlaylists(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**Playlist**](Playlist.md)|  | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRadioStreams"></a>
# **appPrototypeCreateRadioStreams**
> RadioStream appPrototypeCreateRadioStreams(id, opts)

Creates a new instance in radioStreams of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | 
};
apiInstance.appPrototypeCreateRadioStreams(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**RadioStream**](RadioStream.md)|  | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRssFeedGroups"></a>
# **appPrototypeCreateRssFeedGroups**
> RssFeedGroup appPrototypeCreateRssFeedGroups(id, opts)

Creates a new instance in rssFeedGroups of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | 
};
apiInstance.appPrototypeCreateRssFeedGroups(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)|  | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRssFeeds"></a>
# **appPrototypeCreateRssFeeds**
> RssFeed appPrototypeCreateRssFeeds(id, opts)

Creates a new instance in rssFeeds of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | 
};
apiInstance.appPrototypeCreateRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**RssFeed**](RssFeed.md)|  | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteAppConfigs"></a>
# **appPrototypeDeleteAppConfigs**
> appPrototypeDeleteAppConfigs(id)

Deletes all appConfigs of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeleteAppConfigs(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteApps"></a>
# **appPrototypeDeleteApps**
> appPrototypeDeleteApps(id)

Deletes all apps of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeleteApps(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeletePlaylists"></a>
# **appPrototypeDeletePlaylists**
> appPrototypeDeletePlaylists(id)

Deletes all playlists of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeletePlaylists(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRadioStreams"></a>
# **appPrototypeDeleteRadioStreams**
> appPrototypeDeleteRadioStreams(id)

Deletes all radioStreams of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeleteRadioStreams(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRssFeedGroups"></a>
# **appPrototypeDeleteRssFeedGroups**
> appPrototypeDeleteRssFeedGroups(id)

Deletes all rssFeedGroups of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeleteRssFeedGroups(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRssFeeds"></a>
# **appPrototypeDeleteRssFeeds**
> appPrototypeDeleteRssFeeds(id)

Deletes all rssFeeds of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

apiInstance.appPrototypeDeleteRssFeeds(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdAppConfigs"></a>
# **appPrototypeDestroyByIdAppConfigs**
> appPrototypeDestroyByIdAppConfigs(fk, id)

Delete a related item by id for appConfigs.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for appConfigs

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdAppConfigs(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdApps"></a>
# **appPrototypeDestroyByIdApps**
> appPrototypeDestroyByIdApps(fk, id)

Delete a related item by id for apps.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for apps

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdApps(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdPlaylists"></a>
# **appPrototypeDestroyByIdPlaylists**
> appPrototypeDestroyByIdPlaylists(fk, id)

Delete a related item by id for playlists.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for playlists

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdPlaylists(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRadioStreams"></a>
# **appPrototypeDestroyByIdRadioStreams**
> appPrototypeDestroyByIdRadioStreams(fk, id)

Delete a related item by id for radioStreams.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for radioStreams

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdRadioStreams(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRssFeedGroups"></a>
# **appPrototypeDestroyByIdRssFeedGroups**
> appPrototypeDestroyByIdRssFeedGroups(fk, id)

Delete a related item by id for rssFeedGroups.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeedGroups

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdRssFeedGroups(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRssFeeds"></a>
# **appPrototypeDestroyByIdRssFeeds**
> appPrototypeDestroyByIdRssFeeds(fk, id)

Delete a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | App id

apiInstance.appPrototypeDestroyByIdRssFeeds(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| App id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdAppConfigs"></a>
# **appPrototypeFindByIdAppConfigs**
> AppConfig appPrototypeFindByIdAppConfigs(fk, id)

Find a related item by id for appConfigs.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for appConfigs

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdAppConfigs(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs | 
 **id** | **String**| App id | 

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdApps"></a>
# **appPrototypeFindByIdApps**
> App appPrototypeFindByIdApps(fk, id)

Find a related item by id for apps.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for apps

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdApps(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps | 
 **id** | **String**| App id | 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdCustomers"></a>
# **appPrototypeFindByIdCustomers**
> Customer appPrototypeFindByIdCustomers(fk, id)

Find a related item by id for customers.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for customers

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdCustomers(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for customers | 
 **id** | **String**| App id | 

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdPlaylists"></a>
# **appPrototypeFindByIdPlaylists**
> Playlist appPrototypeFindByIdPlaylists(fk, id)

Find a related item by id for playlists.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for playlists

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdPlaylists(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists | 
 **id** | **String**| App id | 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRadioStreams"></a>
# **appPrototypeFindByIdRadioStreams**
> RadioStream appPrototypeFindByIdRadioStreams(fk, id)

Find a related item by id for radioStreams.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for radioStreams

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdRadioStreams(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams | 
 **id** | **String**| App id | 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRssFeedGroups"></a>
# **appPrototypeFindByIdRssFeedGroups**
> RssFeedGroup appPrototypeFindByIdRssFeedGroups(fk, id)

Find a related item by id for rssFeedGroups.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeedGroups

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdRssFeedGroups(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups | 
 **id** | **String**| App id | 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRssFeeds"></a>
# **appPrototypeFindByIdRssFeeds**
> RssFeed appPrototypeFindByIdRssFeeds(fk, id)

Find a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | App id

apiInstance.appPrototypeFindByIdRssFeeds(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| App id | 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetAppConfigs"></a>
# **appPrototypeGetAppConfigs**
> [AppConfig] appPrototypeGetAppConfigs(id, opts)

Queries appConfigs of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetAppConfigs(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AppConfig]**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetApps"></a>
# **appPrototypeGetApps**
> [App] appPrototypeGetApps(id, opts)

Queries apps of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetApps(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[App]**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetCustomers"></a>
# **appPrototypeGetCustomers**
> [Customer] appPrototypeGetCustomers(id, opts)

Queries customers of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetCustomers(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[Customer]**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetParent"></a>
# **appPrototypeGetParent**
> App appPrototypeGetParent(id, opts)

Fetches belongsTo relation parent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.appPrototypeGetParent(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetPlaylists"></a>
# **appPrototypeGetPlaylists**
> [Playlist] appPrototypeGetPlaylists(id, opts)

Queries playlists of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetPlaylists(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[Playlist]**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRadioStreams"></a>
# **appPrototypeGetRadioStreams**
> [RadioStream] appPrototypeGetRadioStreams(id, opts)

Queries radioStreams of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetRadioStreams(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RadioStream]**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRssFeedGroups"></a>
# **appPrototypeGetRssFeedGroups**
> [RssFeedGroup] appPrototypeGetRssFeedGroups(id, opts)

Queries rssFeedGroups of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetRssFeedGroups(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RssFeedGroup]**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRssFeeds"></a>
# **appPrototypeGetRssFeeds**
> [RssFeed] appPrototypeGetRssFeeds(id, opts)

Queries rssFeeds of App.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.appPrototypeGetRssFeeds(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[RssFeed]**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateAttributesPatchAppsid"></a>
# **appPrototypeUpdateAttributesPatchAppsid**
> App appPrototypeUpdateAttributesPatchAppsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.App() // App | An object of model property name/value pairs
};
apiInstance.appPrototypeUpdateAttributesPatchAppsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateAttributesPutAppsid"></a>
# **appPrototypeUpdateAttributesPutAppsid**
> App appPrototypeUpdateAttributesPutAppsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.App() // App | An object of model property name/value pairs
};
apiInstance.appPrototypeUpdateAttributesPutAppsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id | 
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdAppConfigs"></a>
# **appPrototypeUpdateByIdAppConfigs**
> AppConfig appPrototypeUpdateByIdAppConfigs(fk, id, opts)

Update a related item by id for appConfigs.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for appConfigs

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.AppConfig() // AppConfig | 
};
apiInstance.appPrototypeUpdateByIdAppConfigs(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs | 
 **id** | **String**| App id | 
 **data** | [**AppConfig**](AppConfig.md)|  | [optional] 

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdApps"></a>
# **appPrototypeUpdateByIdApps**
> App appPrototypeUpdateByIdApps(fk, id, opts)

Update a related item by id for apps.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for apps

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.App() // App | 
};
apiInstance.appPrototypeUpdateByIdApps(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps | 
 **id** | **String**| App id | 
 **data** | [**App**](App.md)|  | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdCustomers"></a>
# **appPrototypeUpdateByIdCustomers**
> Customer appPrototypeUpdateByIdCustomers(fk, id, opts)

Update a related item by id for customers.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for customers

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.Customer() // Customer | 
};
apiInstance.appPrototypeUpdateByIdCustomers(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for customers | 
 **id** | **String**| App id | 
 **data** | [**Customer**](Customer.md)|  | [optional] 

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdPlaylists"></a>
# **appPrototypeUpdateByIdPlaylists**
> Playlist appPrototypeUpdateByIdPlaylists(fk, id, opts)

Update a related item by id for playlists.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for playlists

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | 
};
apiInstance.appPrototypeUpdateByIdPlaylists(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists | 
 **id** | **String**| App id | 
 **data** | [**Playlist**](Playlist.md)|  | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRadioStreams"></a>
# **appPrototypeUpdateByIdRadioStreams**
> RadioStream appPrototypeUpdateByIdRadioStreams(fk, id, opts)

Update a related item by id for radioStreams.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for radioStreams

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RadioStream() // RadioStream | 
};
apiInstance.appPrototypeUpdateByIdRadioStreams(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams | 
 **id** | **String**| App id | 
 **data** | [**RadioStream**](RadioStream.md)|  | [optional] 

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRssFeedGroups"></a>
# **appPrototypeUpdateByIdRssFeedGroups**
> RssFeedGroup appPrototypeUpdateByIdRssFeedGroups(fk, id, opts)

Update a related item by id for rssFeedGroups.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeedGroups

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RssFeedGroup() // RssFeedGroup | 
};
apiInstance.appPrototypeUpdateByIdRssFeedGroups(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups | 
 **id** | **String**| App id | 
 **data** | [**RssFeedGroup**](RssFeedGroup.md)|  | [optional] 

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRssFeeds"></a>
# **appPrototypeUpdateByIdRssFeeds**
> RssFeed appPrototypeUpdateByIdRssFeeds(fk, id, opts)

Update a related item by id for rssFeeds.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var fk = "fk_example"; // String | Foreign key for rssFeeds

var id = "id_example"; // String | App id

var opts = { 
  'data': new harpoonApi.RssFeed() // RssFeed | 
};
apiInstance.appPrototypeUpdateByIdRssFeeds(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds | 
 **id** | **String**| App id | 
 **data** | [**RssFeed**](RssFeed.md)|  | [optional] 

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appReplaceById"></a>
# **appReplaceById**
> App appReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.App() // App | Model instance data
};
apiInstance.appReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**App**](App.md)| Model instance data | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appReplaceOrCreate"></a>
# **appReplaceOrCreate**
> App appReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'data': new harpoonApi.App() // App | Model instance data
};
apiInstance.appReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpdateAll"></a>
# **appUpdateAll**
> InlineResponse2002 appUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.App() // App | An object of model property name/value pairs
};
apiInstance.appUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUploadFile"></a>
# **appUploadFile**
> MagentoFileUpload appUploadFile(id, type, opts)



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var id = "id_example"; // String | Application Id

var type = "type_example"; // String | Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter).

var opts = { 
  'data': new harpoonApi.MagentoFileUpload() // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.
};
apiInstance.appUploadFile(id, type, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Application Id | 
 **type** | **String**| Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter). | 
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional] 

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertPatchApps"></a>
# **appUpsertPatchApps**
> App appUpsertPatchApps(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'data': new harpoonApi.App() // App | Model instance data
};
apiInstance.appUpsertPatchApps(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertPutApps"></a>
# **appUpsertPutApps**
> App appUpsertPutApps(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'data': new harpoonApi.App() // App | Model instance data
};
apiInstance.appUpsertPutApps(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertWithWhere"></a>
# **appUpsertWithWhere**
> App appUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.App() // App | An object of model property name/value pairs
};
apiInstance.appUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional] 

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appVerify"></a>
# **appVerify**
> Object appVerify()



### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AppApi();
apiInstance.appVerify().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


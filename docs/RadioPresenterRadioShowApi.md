# harpoonApi.RadioPresenterRadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterRadioShowCount**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCount) | **GET** /RadioPresenterRadioShows/count | Count instances of the model matched by where from the data source.
[**radioPresenterRadioShowCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreate) | **POST** /RadioPresenterRadioShows | Create a new instance of the model and persist it into the data source.
[**radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream) | **GET** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream) | **POST** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowDeleteById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowDeleteById) | **DELETE** /RadioPresenterRadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists) | **GET** /RadioPresenterRadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid) | **HEAD** /RadioPresenterRadioShows/{id} | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowFind**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFind) | **GET** /RadioPresenterRadioShows | Find all instances of the model matched by filter from the data source.
[**radioPresenterRadioShowFindById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindById) | **GET** /RadioPresenterRadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterRadioShowFindOne**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindOne) | **GET** /RadioPresenterRadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterRadioShowPrototypeGetRadioPresenter**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioPresenter) | **GET** /RadioPresenterRadioShows/{id}/radioPresenter | Fetches belongsTo relation radioPresenter.
[**radioPresenterRadioShowPrototypeGetRadioShow**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioShow) | **GET** /RadioPresenterRadioShows/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid) | **PATCH** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid) | **PUT** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceById) | **POST** /RadioPresenterRadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceOrCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceOrCreate) | **POST** /RadioPresenterRadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpdateAll**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpdateAll) | **POST** /RadioPresenterRadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows) | **PATCH** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPutRadioPresenterRadioShows) | **PUT** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertWithWhere**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertWithWhere) | **POST** /RadioPresenterRadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPresenterRadioShowCount"></a>
# **radioPresenterRadioShowCount**
> InlineResponse2001 radioPresenterRadioShowCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioPresenterRadioShowCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreate"></a>
# **radioPresenterRadioShowCreate**
> RadioPresenterRadioShow radioPresenterRadioShowCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | Model instance data
};
apiInstance.radioPresenterRadioShowCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream"></a>
# **radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**
> File radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream"></a>
# **radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**
> File radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowDeleteById"></a>
# **radioPresenterRadioShowDeleteById**
> Object radioPresenterRadioShowDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterRadioShowDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists"></a>
# **radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**
> InlineResponse2003 radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**
> InlineResponse2003 radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | Model id

apiInstance.radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFind"></a>
# **radioPresenterRadioShowFind**
> [RadioPresenterRadioShow] radioPresenterRadioShowFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPresenterRadioShowFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioPresenterRadioShow]**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFindById"></a>
# **radioPresenterRadioShowFindById**
> RadioPresenterRadioShow radioPresenterRadioShowFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioPresenterRadioShowFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFindOne"></a>
# **radioPresenterRadioShowFindOne**
> RadioPresenterRadioShow radioPresenterRadioShowFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioPresenterRadioShowFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeGetRadioPresenter"></a>
# **radioPresenterRadioShowPrototypeGetRadioPresenter**
> RadioPresenter radioPresenterRadioShowPrototypeGetRadioPresenter(id, opts)

Fetches belongsTo relation radioPresenter.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | RadioPresenterRadioShow id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.radioPresenterRadioShowPrototypeGetRadioPresenter(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeGetRadioShow"></a>
# **radioPresenterRadioShowPrototypeGetRadioShow**
> RadioShow radioPresenterRadioShowPrototypeGetRadioShow(id, opts)

Fetches belongsTo relation radioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | RadioPresenterRadioShow id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.radioPresenterRadioShowPrototypeGetRadioShow(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**
> RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | RadioPresenterRadioShow id

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | An object of model property name/value pairs
};
apiInstance.radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id | 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**
> RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | RadioPresenterRadioShow id

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | An object of model property name/value pairs
};
apiInstance.radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id | 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowReplaceById"></a>
# **radioPresenterRadioShowReplaceById**
> RadioPresenterRadioShow radioPresenterRadioShowReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | Model instance data
};
apiInstance.radioPresenterRadioShowReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowReplaceOrCreate"></a>
# **radioPresenterRadioShowReplaceOrCreate**
> RadioPresenterRadioShow radioPresenterRadioShowReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | Model instance data
};
apiInstance.radioPresenterRadioShowReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpdateAll"></a>
# **radioPresenterRadioShowUpdateAll**
> InlineResponse2002 radioPresenterRadioShowUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | An object of model property name/value pairs
};
apiInstance.radioPresenterRadioShowUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows"></a>
# **radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | Model instance data
};
apiInstance.radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertPutRadioPresenterRadioShows"></a>
# **radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertPutRadioPresenterRadioShows(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | Model instance data
};
apiInstance.radioPresenterRadioShowUpsertPutRadioPresenterRadioShows(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertWithWhere"></a>
# **radioPresenterRadioShowUpsertWithWhere**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioPresenterRadioShowApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioPresenterRadioShow() // RadioPresenterRadioShow | An object of model property name/value pairs
};
apiInstance.radioPresenterRadioShowUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.RadioPlaySession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionTime** | **Number** |  | [optional] 
**playUpdateTime** | **Date** |  | [optional] 
**radioStreamId** | **Number** |  | [optional] 
**radioShowId** | **Number** |  | [optional] 
**playEndTime** | **Date** |  | [optional] 
**playStartTime** | **Date** |  | [optional] 
**customerId** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.AppConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgAppLogo** | **String** | App logo | [optional] 
**imgSplashScreen** | **String** | App splash-screen | [optional] 
**imgBrandLogo** | **String** |  | [optional] 
**imgNavBarLogo** | **String** |  | [optional] 
**imgBrandPlaceholder** | **String** |  | [optional] 
**imgMenuBackground** | **String** |  | [optional] 
**featureRadio** | **Boolean** | Enable or disable the Radio feature within the app | [optional] [default to false]
**featureRadioAutoPlay** | **Boolean** | Enable or disable the Radio autoplay feature within the app | [optional] [default to false]
**featureNews** | **Boolean** |  | [optional] [default to true]
**featureEvents** | **Boolean** |  | [optional] [default to true]
**featureEventsCategorized** | **Boolean** | If true the Events list is categorised | [optional] [default to true]
**featureEventsAttendeeList** | **Boolean** | If true the user will have the possibility to see Events&#39; attendees | [optional] [default to true]
**featureCommunity** | **Boolean** |  | [optional] [default to true]
**featureCommunityCategorized** | **Boolean** | If true the Brand list is categories | [optional] [default to true]
**featureCompetitions** | **Boolean** |  | [optional] [default to true]
**featureCompetitionsAttendeeList** | **Boolean** | If true the user will have the possibility to see Competitions&#39; attendees | [optional] [default to true]
**featureOffers** | **Boolean** |  | [optional] [default to true]
**featureOffersCategorized** | **Boolean** | If true the Offer list is categories | [optional] [default to true]
**featureBrandFollow** | **Boolean** | If true the user will have the possibility to follow or unfollow Brands | [optional] [default to true]
**featureBrandFollowerList** | **Boolean** | If true the user will have the possibility to see Brands&#39; followers | [optional] [default to true]
**featureWordpressConnect** | **Boolean** |  | [optional] [default to false]
**featureLogin** | **Boolean** | If true the user will be asked to login before getting access to the app | [optional] [default to true]
**featureHarpoonLogin** | **Boolean** |  | [optional] 
**featureFacebookLogin** | **Boolean** | If true the user will have the possibility to login with Facebook | [optional] [default to true]
**featurePhoneVerification** | **Boolean** | If true the user will need to verify its account using a phone number | [optional] [default to true]
**featurePlayer** | **Boolean** | Show / Hide media player | [optional] [default to false]
**featurePlayerMainButton** | **Boolean** | Show / Hide main button | [optional] [default to false]
**featureGoogleAnalytics** | **Boolean** | If true the user actions will be tracked while using the app | [optional] [default to false]
**playerMainButtonTitle** | **String** | Media Player Main Button title | [optional] [default to &#39;Main&#39;]
**playerConfig** | [**PlayerConfig**](PlayerConfig.md) | Media Player config | [optional] 
**menu** | [**[MenuItem]**](MenuItem.md) |  | [optional] 
**color** | **String** |  | [optional] [default to &#39;black&#39;]
**colors** | [**AppColor**](AppColor.md) |  | [optional] 
**googleAppId** | **String** |  | [optional] 
**iOSAppId** | **String** |  | [optional] 
**googleAnalyticsTracking** | [**GoogleAnalyticsTracking**](GoogleAnalyticsTracking.md) |  | [optional] 
**loginExternal** | [**LoginExternal**](LoginExternal.md) |  | [optional] 
**rssWebViewCss** | **String** |  | [optional] 
**ads** | [**[AppAd]**](AppAd.md) |  | [optional] 
**iOSAds** | [**[AppAd]**](AppAd.md) |  | [optional] 
**androidAds** | [**[AppAd]**](AppAd.md) |  | [optional] 
**pushNotificationProvider** | **String** |  | [optional] [default to &#39;none&#39;]
**batchConfig** | [**BatchConfig**](BatchConfig.md) |  | [optional] 
**urbanAirshipConfig** | [**UrbanAirshipConfig**](UrbanAirshipConfig.md) |  | [optional] 
**pushWooshConfig** | [**PushWooshConfig**](PushWooshConfig.md) |  | [optional] 
**facebookConfig** | [**FacebookConfig**](FacebookConfig.md) |  | [optional] 
**twitterConfig** | [**TwitterConfig**](TwitterConfig.md) |  | [optional] 
**rssTags** | [**RssTags**](RssTags.md) |  | [optional] 
**connectTo** | [**AppConnectTo**](AppConnectTo.md) |  | [optional] 
**iOSVersion** | **String** |  | [optional] [default to &#39;1.0.0&#39;]
**iOSBuild** | **String** |  | [optional] [default to &#39;1&#39;]
**iOSGoogleServices** | **String** |  | [optional] 
**iOSTeamId** | **String** |  | [optional] [default to &#39;8LXTLH6AD9&#39;]
**iOSAppleId** | **String** |  | [optional] [default to &#39;dev@harpoonconnect.com&#39;]
**iOSPushNotificationPrivateKey** | **String** |  | [optional] 
**iOSPushNotificationCertificate** | **String** |  | [optional] 
**iOSPushNotificationPem** | **String** |  | [optional] 
**iOSPushNotificationPassword** | **String** |  | [optional] 
**iOSReleaseNotes** | **String** |  | [optional] 
**androidBuild** | **String** |  | [optional] [default to &#39;1&#39;]
**androidVersion** | **String** |  | [optional] [default to &#39;1.0.0&#39;]
**androidGoogleServices** | **String** |  | [optional] 
**androidGoogleApiKey** | **String** |  | [optional] 
**androidGoogleCloudMessagingSenderId** | **String** |  | [optional] 
**androidKey** | **String** |  | [optional] 
**androidKeystore** | **String** |  | [optional] 
**androidKeystoreConfig** | [**AndroidKeystoreConfig**](AndroidKeystoreConfig.md) |  | [optional] 
**androidApk** | **String** |  | [optional] 
**customerDetailsForm** | [**[FormSection]**](FormSection.md) |  | [optional] 
**contact** | [**Contact**](Contact.md) |  | [optional] 
**imgSponsorMenu** | **String** |  | [optional] 
**imgSponsorSplash** | **String** |  | [optional] 
**audioSponsorPreRollAd** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**isChildConfig** | **Boolean** |  | [optional] 
**typeNewsFocusSource** | **String** |  | [optional] [default to &#39;rssContent&#39;]
**sponsorSplashImgDisplayTime** | **Number** |  | [optional] [default to 0.0]
**radioSessionInterval** | **Number** |  | [optional] 
**featureRadioSession** | **Boolean** |  | [optional] 
**wordpressShareUrlStub** | **String** |  | [optional] 
**iosUriScheme** | **String** |  | [optional] 
**androidUriScheme** | **String** |  | [optional] 
**branchConfig** | [**BranchConfig**](BranchConfig.md) |  | [optional] 
**defaultRadioStreamId** | **Number** |  | [optional] 
**featureTritonPlayer** | **Boolean** | Enable or disable the Triton Player in App | [optional] [default to false]
**id** | **Number** |  | [optional] 
**appId** | **String** |  | [optional] 
**app** | **Object** |  | [optional] 


<a name="PushNotificationProviderEnum"></a>
## Enum: PushNotificationProviderEnum


* `none` (value: `"none"`)

* `batch` (value: `"batch"`)

* `urbanairship` (value: `"urbanairship"`)

* `pushwoosh` (value: `"pushwoosh"`)




<a name="TypeNewsFocusSourceEnum"></a>
## Enum: TypeNewsFocusSourceEnum


* `linkTag` (value: `"linkTag"`)

* `rssContent` (value: `"rssContent"`)

* `rssContentWithFeatureImage` (value: `"rssContentWithFeatureImage"`)





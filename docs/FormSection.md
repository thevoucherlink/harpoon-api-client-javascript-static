# harpoonApi.FormSection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [optional] 
**rows** | [**[FormRow]**](FormRow.md) |  | 
**id** | **Number** |  | [optional] 



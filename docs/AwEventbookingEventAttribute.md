# harpoonApi.AwEventbookingEventAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**eventId** | **Number** |  | 
**storeId** | **Number** |  | [optional] 
**attributeCode** | **String** |  | [optional] 
**value** | **String** |  | [optional] 



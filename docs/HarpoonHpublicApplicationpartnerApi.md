# harpoonApi.HarpoonHpublicApplicationpartnerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**harpoonHpublicApplicationpartnerCount**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCount) | **GET** /HarpoonHpublicApplicationpartners/count | Count instances of the model matched by where from the data source.
[**harpoonHpublicApplicationpartnerCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreate) | **POST** /HarpoonHpublicApplicationpartners | Create a new instance of the model and persist it into the data source.
[**harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream) | **GET** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream) | **POST** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerDeleteById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerDeleteById) | **DELETE** /HarpoonHpublicApplicationpartners/{id} | Delete a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists) | **GET** /HarpoonHpublicApplicationpartners/{id}/exists | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid) | **HEAD** /HarpoonHpublicApplicationpartners/{id} | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerFind**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFind) | **GET** /HarpoonHpublicApplicationpartners | Find all instances of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerFindById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindById) | **GET** /HarpoonHpublicApplicationpartners/{id} | Find a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerFindOne**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindOne) | **GET** /HarpoonHpublicApplicationpartners/findOne | Find first instance of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor) | **GET** /HarpoonHpublicApplicationpartners/{id}/udropshipVendor | Fetches belongsTo relation udropshipVendor.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid) | **PATCH** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid) | **PUT** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceById) | **POST** /HarpoonHpublicApplicationpartners/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceOrCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceOrCreate) | **POST** /HarpoonHpublicApplicationpartners/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpdateAll**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpdateAll) | **POST** /HarpoonHpublicApplicationpartners/update | Update instances of the model matched by {{where}} from the data source.
[**harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners) | **PATCH** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners) | **PUT** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertWithWhere**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertWithWhere) | **POST** /HarpoonHpublicApplicationpartners/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="harpoonHpublicApplicationpartnerCount"></a>
# **harpoonHpublicApplicationpartnerCount**
> InlineResponse2001 harpoonHpublicApplicationpartnerCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.harpoonHpublicApplicationpartnerCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreate"></a>
# **harpoonHpublicApplicationpartnerCreate**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | Model instance data
};
apiInstance.harpoonHpublicApplicationpartnerCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream"></a>
# **harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**
> File harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream"></a>
# **harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**
> File harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerDeleteById"></a>
# **harpoonHpublicApplicationpartnerDeleteById**
> Object harpoonHpublicApplicationpartnerDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | Model id

apiInstance.harpoonHpublicApplicationpartnerDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists"></a>
# **harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**
> InlineResponse2003 harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | Model id

apiInstance.harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**
> InlineResponse2003 harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | Model id

apiInstance.harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFind"></a>
# **harpoonHpublicApplicationpartnerFind**
> [HarpoonHpublicApplicationpartner] harpoonHpublicApplicationpartnerFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.harpoonHpublicApplicationpartnerFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[HarpoonHpublicApplicationpartner]**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFindById"></a>
# **harpoonHpublicApplicationpartnerFindById**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.harpoonHpublicApplicationpartnerFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFindOne"></a>
# **harpoonHpublicApplicationpartnerFindOne**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.harpoonHpublicApplicationpartnerFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor"></a>
# **harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**
> UdropshipVendor harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor(id, opts)

Fetches belongsTo relation udropshipVendor.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | HarpoonHpublicApplicationpartner id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | HarpoonHpublicApplicationpartner id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
};
apiInstance.harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id | 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | HarpoonHpublicApplicationpartner id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
};
apiInstance.harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id | 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerReplaceById"></a>
# **harpoonHpublicApplicationpartnerReplaceById**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | Model instance data
};
apiInstance.harpoonHpublicApplicationpartnerReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerReplaceOrCreate"></a>
# **harpoonHpublicApplicationpartnerReplaceOrCreate**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | Model instance data
};
apiInstance.harpoonHpublicApplicationpartnerReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpdateAll"></a>
# **harpoonHpublicApplicationpartnerUpdateAll**
> InlineResponse2002 harpoonHpublicApplicationpartnerUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
};
apiInstance.harpoonHpublicApplicationpartnerUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners"></a>
# **harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | Model instance data
};
apiInstance.harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners"></a>
# **harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | Model instance data
};
apiInstance.harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertWithWhere"></a>
# **harpoonHpublicApplicationpartnerUpsertWithWhere**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.HarpoonHpublicApplicationpartnerApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.HarpoonHpublicApplicationpartner() // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
};
apiInstance.harpoonHpublicApplicationpartnerUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


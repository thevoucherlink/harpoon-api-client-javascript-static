# harpoonApi.CompetitionPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**competition** | [**Competition**](Competition.md) |  | [optional] 
**redemptionType** | **String** |  | [optional] 
**unlockTime** | **Number** |  | [optional] [default to 15.0]
**code** | **String** |  | [optional] 
**qrcode** | **String** |  | [optional] 
**isAvailable** | **Boolean** |  | [optional] [default to false]
**status** | **String** |  | [optional] [default to &#39;unknown&#39;]
**createdAt** | **Date** |  | [optional] 
**expiredAt** | **Date** |  | [optional] 
**redeemedAt** | **Date** |  | [optional] 
**redeemedByTerminal** | **String** |  | [optional] 
**basePrice** | **Number** |  | [optional] [default to 0.0]
**ticketPrice** | **Number** |  | [optional] [default to 9.0]
**currency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**attendee** | [**Customer**](Customer.md) |  | [optional] 
**chanceCount** | **Number** |  | [optional] [default to 9.0]
**winner** | [**CompetitionWinner**](CompetitionWinner.md) |  | [optional] 
**id** | **Number** |  | [optional] 



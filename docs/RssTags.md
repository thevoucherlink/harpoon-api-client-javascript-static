# harpoonApi.RssTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgTumbnail** | **String** |  | [optional] [default to &#39;&lt;media&gt;&#39;]
**imgMain** | **String** |  | [optional] [default to &#39;media&#39;]
**link** | **String** |  | [optional] [default to &#39;link&#39;]
**postDateTime** | **String** |  | [optional] [default to &#39;pubDate&#39;]
**description** | **String** |  | [optional] [default to &#39;content:encoded&#39;]
**category** | **String** |  | [optional] [default to &#39;category&#39;]
**title** | **String** |  | [optional] [default to &#39;title&#39;]
**id** | **Number** |  | [optional] 



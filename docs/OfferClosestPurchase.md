# harpoonApi.OfferClosestPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiredAt** | **Date** |  | [optional] 
**purchasedAt** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 



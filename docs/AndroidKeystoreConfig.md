# harpoonApi.AndroidKeystoreConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**storePassword** | **String** |  | 
**keyAlias** | **String** |  | 
**keyPassword** | **String** |  | 
**hashKey** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



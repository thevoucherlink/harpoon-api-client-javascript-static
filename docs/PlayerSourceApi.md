# harpoonApi.PlayerSourceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerSourceCount**](PlayerSourceApi.md#playerSourceCount) | **GET** /PlayerSources/count | Count instances of the model matched by where from the data source.
[**playerSourceCreate**](PlayerSourceApi.md#playerSourceCreate) | **POST** /PlayerSources | Create a new instance of the model and persist it into the data source.
[**playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamGetPlayerSourcesChangeStream) | **GET** /PlayerSources/change-stream | Create a change stream.
[**playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamPostPlayerSourcesChangeStream) | **POST** /PlayerSources/change-stream | Create a change stream.
[**playerSourceDeleteById**](PlayerSourceApi.md#playerSourceDeleteById) | **DELETE** /PlayerSources/{id} | Delete a model instance by {{id}} from the data source.
[**playerSourceExistsGetPlayerSourcesidExists**](PlayerSourceApi.md#playerSourceExistsGetPlayerSourcesidExists) | **GET** /PlayerSources/{id}/exists | Check whether a model instance exists in the data source.
[**playerSourceExistsHeadPlayerSourcesid**](PlayerSourceApi.md#playerSourceExistsHeadPlayerSourcesid) | **HEAD** /PlayerSources/{id} | Check whether a model instance exists in the data source.
[**playerSourceFind**](PlayerSourceApi.md#playerSourceFind) | **GET** /PlayerSources | Find all instances of the model matched by filter from the data source.
[**playerSourceFindById**](PlayerSourceApi.md#playerSourceFindById) | **GET** /PlayerSources/{id} | Find a model instance by {{id}} from the data source.
[**playerSourceFindOne**](PlayerSourceApi.md#playerSourceFindOne) | **GET** /PlayerSources/findOne | Find first instance of the model matched by filter from the data source.
[**playerSourcePrototypeGetPlaylistItem**](PlayerSourceApi.md#playerSourcePrototypeGetPlaylistItem) | **GET** /PlayerSources/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid) | **PATCH** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPutPlayerSourcesid) | **PUT** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourceReplaceById**](PlayerSourceApi.md#playerSourceReplaceById) | **POST** /PlayerSources/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerSourceReplaceOrCreate**](PlayerSourceApi.md#playerSourceReplaceOrCreate) | **POST** /PlayerSources/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerSourceUpdateAll**](PlayerSourceApi.md#playerSourceUpdateAll) | **POST** /PlayerSources/update | Update instances of the model matched by {{where}} from the data source.
[**playerSourceUpsertPatchPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPatchPlayerSources) | **PATCH** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertPutPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPutPlayerSources) | **PUT** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertWithWhere**](PlayerSourceApi.md#playerSourceUpsertWithWhere) | **POST** /PlayerSources/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playerSourceCount"></a>
# **playerSourceCount**
> InlineResponse2001 playerSourceCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playerSourceCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreate"></a>
# **playerSourceCreate**
> PlayerSource playerSourceCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | Model instance data
};
apiInstance.playerSourceCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreateChangeStreamGetPlayerSourcesChangeStream"></a>
# **playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**
> File playerSourceCreateChangeStreamGetPlayerSourcesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playerSourceCreateChangeStreamGetPlayerSourcesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreateChangeStreamPostPlayerSourcesChangeStream"></a>
# **playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**
> File playerSourceCreateChangeStreamPostPlayerSourcesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playerSourceCreateChangeStreamPostPlayerSourcesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceDeleteById"></a>
# **playerSourceDeleteById**
> Object playerSourceDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | Model id

apiInstance.playerSourceDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceExistsGetPlayerSourcesidExists"></a>
# **playerSourceExistsGetPlayerSourcesidExists**
> InlineResponse2003 playerSourceExistsGetPlayerSourcesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | Model id

apiInstance.playerSourceExistsGetPlayerSourcesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceExistsHeadPlayerSourcesid"></a>
# **playerSourceExistsHeadPlayerSourcesid**
> InlineResponse2003 playerSourceExistsHeadPlayerSourcesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | Model id

apiInstance.playerSourceExistsHeadPlayerSourcesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFind"></a>
# **playerSourceFind**
> [PlayerSource] playerSourceFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playerSourceFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[PlayerSource]**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFindById"></a>
# **playerSourceFindById**
> PlayerSource playerSourceFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.playerSourceFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFindOne"></a>
# **playerSourceFindOne**
> PlayerSource playerSourceFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playerSourceFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeGetPlaylistItem"></a>
# **playerSourcePrototypeGetPlaylistItem**
> PlaylistItem playerSourcePrototypeGetPlaylistItem(id, opts)

Fetches belongsTo relation playlistItem.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | PlayerSource id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.playerSourcePrototypeGetPlaylistItem(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid"></a>
# **playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**
> PlayerSource playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | PlayerSource id

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | An object of model property name/value pairs
};
apiInstance.playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id | 
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeUpdateAttributesPutPlayerSourcesid"></a>
# **playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**
> PlayerSource playerSourcePrototypeUpdateAttributesPutPlayerSourcesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | PlayerSource id

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | An object of model property name/value pairs
};
apiInstance.playerSourcePrototypeUpdateAttributesPutPlayerSourcesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id | 
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceReplaceById"></a>
# **playerSourceReplaceById**
> PlayerSource playerSourceReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | Model instance data
};
apiInstance.playerSourceReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceReplaceOrCreate"></a>
# **playerSourceReplaceOrCreate**
> PlayerSource playerSourceReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | Model instance data
};
apiInstance.playerSourceReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpdateAll"></a>
# **playerSourceUpdateAll**
> InlineResponse2002 playerSourceUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlayerSource() // PlayerSource | An object of model property name/value pairs
};
apiInstance.playerSourceUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertPatchPlayerSources"></a>
# **playerSourceUpsertPatchPlayerSources**
> PlayerSource playerSourceUpsertPatchPlayerSources(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | Model instance data
};
apiInstance.playerSourceUpsertPatchPlayerSources(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertPutPlayerSources"></a>
# **playerSourceUpsertPutPlayerSources**
> PlayerSource playerSourceUpsertPutPlayerSources(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'data': new harpoonApi.PlayerSource() // PlayerSource | Model instance data
};
apiInstance.playerSourceUpsertPutPlayerSources(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertWithWhere"></a>
# **playerSourceUpsertWithWhere**
> PlayerSource playerSourceUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlayerSourceApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.PlayerSource() // PlayerSource | An object of model property name/value pairs
};
apiInstance.playerSourceUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional] 

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


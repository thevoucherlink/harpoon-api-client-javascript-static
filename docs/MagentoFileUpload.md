# harpoonApi.MagentoFileUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** | File contents encoded as base64 | 
**contentType** | **String** | The file&#39;s encode type e.g application/json | [optional] 
**name** | **String** | the file name including the extension | [optional] 



# harpoonApi.NotificationRelated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Number** |  | [optional] [default to 0.0]
**brandId** | **Number** |  | [optional] [default to 0.0]
**venueId** | **Number** |  | [optional] [default to 0.0]
**competitionId** | **Number** |  | [optional] [default to 0.0]
**couponId** | **Number** |  | [optional] [default to 0.0]
**eventId** | **Number** |  | [optional] [default to 0.0]
**dealPaidId** | **Number** |  | [optional] [default to 0.0]
**dealGroupId** | **Number** |  | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 



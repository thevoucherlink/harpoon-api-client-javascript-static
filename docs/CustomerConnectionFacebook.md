# harpoonApi.CustomerConnectionFacebook

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.UdropshipVendorPartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**invitedEmail** | **String** |  | 
**invitedAt** | **Date** |  | 
**acceptedAt** | **Date** |  | 
**createdAt** | **Date** |  | 
**updatedAt** | **Date** |  | 
**configList** | **Number** |  | 
**configFeed** | **Number** |  | 
**configNeedFollow** | **Number** |  | 
**configAcceptEvent** | **Number** |  | 
**configAcceptCoupon** | **Number** |  | 
**configAcceptDealsimple** | **Number** |  | 
**configAcceptDealgroup** | **Number** |  | 
**configAcceptNotificationpush** | **Number** |  | 
**configAcceptNotificationbeacon** | **Number** |  | 
**isOwner** | **Number** |  | 
**vendorId** | **Number** |  | 
**partnerId** | **Number** |  | 
**id** | **Number** |  | 
**udropshipVendor** | **Object** |  | [optional] 



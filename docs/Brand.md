# harpoonApi.Brand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**managerName** | **String** |  | [optional] 
**followerCount** | **Number** |  | [optional] [default to 0.0]
**isFollowed** | **Boolean** |  | [optional] [default to false]
**categories** | [**[Category]**](Category.md) |  | [optional] 
**website** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



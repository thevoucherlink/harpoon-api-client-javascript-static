# harpoonApi.AwCollpurDealPurchasesApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealPurchasesCount**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCount) | **GET** /AwCollpurDealPurchases/count | Count instances of the model matched by where from the data source.
[**awCollpurDealPurchasesCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreate) | **POST** /AwCollpurDealPurchases | Create a new instance of the model and persist it into the data source.
[**awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream) | **GET** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream) | **POST** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesDeleteById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesDeleteById) | **DELETE** /AwCollpurDealPurchases/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists) | **GET** /AwCollpurDealPurchases/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid) | **HEAD** /AwCollpurDealPurchases/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesFind**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFind) | **GET** /AwCollpurDealPurchases | Find all instances of the model matched by filter from the data source.
[**awCollpurDealPurchasesFindById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindById) | **GET** /AwCollpurDealPurchases/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesFindOne**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindOne) | **GET** /AwCollpurDealPurchases/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon) | **POST** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Creates a new instance in awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon) | **DELETE** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Deletes awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeGetAwCollpurCoupon) | **GET** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Fetches hasOne relation awCollpurCoupon.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid) | **PATCH** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid) | **PUT** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon) | **PUT** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Update awCollpurCoupon of this model.
[**awCollpurDealPurchasesReplaceById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceById) | **POST** /AwCollpurDealPurchases/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesReplaceOrCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceOrCreate) | **POST** /AwCollpurDealPurchases/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpdateAll**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpdateAll) | **POST** /AwCollpurDealPurchases/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases) | **PATCH** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases) | **PUT** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertWithWhere**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertWithWhere) | **POST** /AwCollpurDealPurchases/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurDealPurchasesCount"></a>
# **awCollpurDealPurchasesCount**
> InlineResponse2001 awCollpurDealPurchasesCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awCollpurDealPurchasesCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreate"></a>
# **awCollpurDealPurchasesCreate**
> AwCollpurDealPurchases awCollpurDealPurchasesCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | Model instance data
};
apiInstance.awCollpurDealPurchasesCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream"></a>
# **awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**
> File awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream"></a>
# **awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**
> File awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesDeleteById"></a>
# **awCollpurDealPurchasesDeleteById**
> Object awCollpurDealPurchasesDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealPurchasesDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists"></a>
# **awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**
> InlineResponse2003 awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**
> InlineResponse2003 awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFind"></a>
# **awCollpurDealPurchasesFind**
> [AwCollpurDealPurchases] awCollpurDealPurchasesFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurDealPurchasesFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwCollpurDealPurchases]**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFindById"></a>
# **awCollpurDealPurchasesFindById**
> AwCollpurDealPurchases awCollpurDealPurchasesFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awCollpurDealPurchasesFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFindOne"></a>
# **awCollpurDealPurchasesFindOne**
> AwCollpurDealPurchases awCollpurDealPurchasesFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurDealPurchasesFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon(id, opts)

Creates a new instance in awCollpurCoupon of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | 
};
apiInstance.awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)|  | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**
> awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon(id)

Deletes awCollpurCoupon of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

apiInstance.awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeGetAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeGetAwCollpurCoupon(id, opts)

Fetches hasOne relation awCollpurCoupon.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.awCollpurDealPurchasesPrototypeGetAwCollpurCoupon(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**
> AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | An object of model property name/value pairs
};
apiInstance.awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**
> AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | An object of model property name/value pairs
};
apiInstance.awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon(id, opts)

Update awCollpurCoupon of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | AwCollpurDealPurchases id

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | 
};
apiInstance.awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id | 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)|  | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesReplaceById"></a>
# **awCollpurDealPurchasesReplaceById**
> AwCollpurDealPurchases awCollpurDealPurchasesReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | Model instance data
};
apiInstance.awCollpurDealPurchasesReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesReplaceOrCreate"></a>
# **awCollpurDealPurchasesReplaceOrCreate**
> AwCollpurDealPurchases awCollpurDealPurchasesReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | Model instance data
};
apiInstance.awCollpurDealPurchasesReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpdateAll"></a>
# **awCollpurDealPurchasesUpdateAll**
> InlineResponse2002 awCollpurDealPurchasesUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | An object of model property name/value pairs
};
apiInstance.awCollpurDealPurchasesUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases"></a>
# **awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | Model instance data
};
apiInstance.awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases"></a>
# **awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | Model instance data
};
apiInstance.awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertWithWhere"></a>
# **awCollpurDealPurchasesUpsertWithWhere**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurDealPurchasesApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurDealPurchases() // AwCollpurDealPurchases | An object of model property name/value pairs
};
apiInstance.awCollpurDealPurchasesUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.AwCollpurDeal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**productId** | **Number** |  | 
**productName** | **String** |  | 
**storeIds** | **String** |  | 
**isActive** | **Number** |  | 
**isSuccess** | **Number** |  | 
**closeState** | **Number** |  | 
**qtyToReachDeal** | **Number** |  | 
**purchasesLeft** | **Number** |  | 
**maximumAllowedPurchases** | **Number** |  | 
**availableFrom** | **Date** |  | [optional] 
**availableTo** | **Date** |  | [optional] 
**price** | **String** |  | 
**autoClose** | **Number** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**fullDescription** | **String** |  | [optional] 
**dealImage** | **String** |  | 
**isFeatured** | **Number** |  | [optional] 
**enableCoupons** | **Number** |  | 
**couponPrefix** | **String** |  | 
**couponExpireAfterDays** | **Number** |  | [optional] 
**expiredFlag** | **Number** |  | [optional] 
**sentBeforeFlag** | **Number** |  | [optional] 
**isSuccessedFlag** | **Number** |  | [optional] 
**awCollpurDealPurchases** | **[Object]** |  | [optional] 



# harpoonApi.RadioShowTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **String** | Time when the show starts, format HHmm | [optional] [default to &#39;0000&#39;]
**endTime** | **String** | Time when the show ends, format HHmm | [optional] [default to &#39;2359&#39;]
**weekday** | **Number** | Day of the week when the show is live, 1 &#x3D; Monday / 7 &#x3D; Sunday | [optional] [default to 1.0]
**id** | **Number** |  | [optional] 
**radioShowId** | **Number** |  | [optional] 
**radioShow** | **Object** |  | [optional] 



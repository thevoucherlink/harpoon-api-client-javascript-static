# harpoonApi.CustomerLoginCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | Customer Email | 
**password** | **String** | Customer Password | 



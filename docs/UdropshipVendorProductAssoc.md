# harpoonApi.UdropshipVendorProductAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isUdmulti** | **Number** |  | [optional] 
**id** | **Number** |  | 
**isAttribute** | **Number** |  | 
**productId** | **Number** |  | 
**vendorId** | **Number** |  | 
**udropshipVendor** | **Object** |  | [optional] 
**catalogProduct** | **Object** |  | [optional] 



# harpoonApi.HarpoonHpublicVendorconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**type** | **Number** |  | 
**category** | **String** |  | 
**logo** | **String** |  | 
**cover** | **String** |  | 
**facebookPageId** | **String** |  | 
**facebookPageToken** | **String** |  | 
**facebookEventEnabled** | **Number** |  | 
**facebookEventUpdatedAt** | **Date** |  | 
**facebookEventCount** | **Number** |  | 
**facebookFeedEnabled** | **Number** |  | 
**facebookFeedUpdatedAt** | **Date** |  | 
**facebookFeedCount** | **Number** |  | 
**stripePublishableKey** | **String** |  | 
**stripeRefreshToken** | **String** |  | 
**stripeAccessToken** | **String** |  | 
**feesEventTicket** | **String** |  | 
**stripeUserId** | **String** |  | 
**vendorconfigId** | **Number** |  | 
**createdAt** | **Date** |  | 
**updatedAt** | **Date** |  | 



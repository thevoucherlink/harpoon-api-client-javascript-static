# harpoonApi.AwCollpurCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurCouponCount**](AwCollpurCouponApi.md#awCollpurCouponCount) | **GET** /AwCollpurCoupons/count | Count instances of the model matched by where from the data source.
[**awCollpurCouponCreate**](AwCollpurCouponApi.md#awCollpurCouponCreate) | **POST** /AwCollpurCoupons | Create a new instance of the model and persist it into the data source.
[**awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream) | **GET** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream) | **POST** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponDeleteById**](AwCollpurCouponApi.md#awCollpurCouponDeleteById) | **DELETE** /AwCollpurCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurCouponExistsGetAwCollpurCouponsidExists**](AwCollpurCouponApi.md#awCollpurCouponExistsGetAwCollpurCouponsidExists) | **GET** /AwCollpurCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurCouponExistsHeadAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponExistsHeadAwCollpurCouponsid) | **HEAD** /AwCollpurCoupons/{id} | Check whether a model instance exists in the data source.
[**awCollpurCouponFind**](AwCollpurCouponApi.md#awCollpurCouponFind) | **GET** /AwCollpurCoupons | Find all instances of the model matched by filter from the data source.
[**awCollpurCouponFindById**](AwCollpurCouponApi.md#awCollpurCouponFindById) | **GET** /AwCollpurCoupons/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurCouponFindOne**](AwCollpurCouponApi.md#awCollpurCouponFindOne) | **GET** /AwCollpurCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid) | **PATCH** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid) | **PUT** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceById**](AwCollpurCouponApi.md#awCollpurCouponReplaceById) | **POST** /AwCollpurCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceOrCreate**](AwCollpurCouponApi.md#awCollpurCouponReplaceOrCreate) | **POST** /AwCollpurCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpdateAll**](AwCollpurCouponApi.md#awCollpurCouponUpdateAll) | **POST** /AwCollpurCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurCouponUpsertPatchAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPatchAwCollpurCoupons) | **PATCH** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertPutAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPutAwCollpurCoupons) | **PUT** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertWithWhere**](AwCollpurCouponApi.md#awCollpurCouponUpsertWithWhere) | **POST** /AwCollpurCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurCouponCount"></a>
# **awCollpurCouponCount**
> InlineResponse2001 awCollpurCouponCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awCollpurCouponCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreate"></a>
# **awCollpurCouponCreate**
> AwCollpurCoupon awCollpurCouponCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | Model instance data
};
apiInstance.awCollpurCouponCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream"></a>
# **awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**
> File awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream"></a>
# **awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**
> File awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponDeleteById"></a>
# **awCollpurCouponDeleteById**
> Object awCollpurCouponDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurCouponDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponExistsGetAwCollpurCouponsidExists"></a>
# **awCollpurCouponExistsGetAwCollpurCouponsidExists**
> InlineResponse2003 awCollpurCouponExistsGetAwCollpurCouponsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurCouponExistsGetAwCollpurCouponsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponExistsHeadAwCollpurCouponsid"></a>
# **awCollpurCouponExistsHeadAwCollpurCouponsid**
> InlineResponse2003 awCollpurCouponExistsHeadAwCollpurCouponsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | Model id

apiInstance.awCollpurCouponExistsHeadAwCollpurCouponsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFind"></a>
# **awCollpurCouponFind**
> [AwCollpurCoupon] awCollpurCouponFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurCouponFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwCollpurCoupon]**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFindById"></a>
# **awCollpurCouponFindById**
> AwCollpurCoupon awCollpurCouponFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awCollpurCouponFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFindOne"></a>
# **awCollpurCouponFindOne**
> AwCollpurCoupon awCollpurCouponFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awCollpurCouponFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid"></a>
# **awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**
> AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | AwCollpurCoupon id

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | An object of model property name/value pairs
};
apiInstance.awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurCoupon id | 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid"></a>
# **awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**
> AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | AwCollpurCoupon id

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | An object of model property name/value pairs
};
apiInstance.awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurCoupon id | 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponReplaceById"></a>
# **awCollpurCouponReplaceById**
> AwCollpurCoupon awCollpurCouponReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | Model instance data
};
apiInstance.awCollpurCouponReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponReplaceOrCreate"></a>
# **awCollpurCouponReplaceOrCreate**
> AwCollpurCoupon awCollpurCouponReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | Model instance data
};
apiInstance.awCollpurCouponReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpdateAll"></a>
# **awCollpurCouponUpdateAll**
> InlineResponse2002 awCollpurCouponUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | An object of model property name/value pairs
};
apiInstance.awCollpurCouponUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertPatchAwCollpurCoupons"></a>
# **awCollpurCouponUpsertPatchAwCollpurCoupons**
> AwCollpurCoupon awCollpurCouponUpsertPatchAwCollpurCoupons(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | Model instance data
};
apiInstance.awCollpurCouponUpsertPatchAwCollpurCoupons(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertPutAwCollpurCoupons"></a>
# **awCollpurCouponUpsertPutAwCollpurCoupons**
> AwCollpurCoupon awCollpurCouponUpsertPutAwCollpurCoupons(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | Model instance data
};
apiInstance.awCollpurCouponUpsertPutAwCollpurCoupons(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertWithWhere"></a>
# **awCollpurCouponUpsertWithWhere**
> AwCollpurCoupon awCollpurCouponUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwCollpurCouponApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwCollpurCoupon() // AwCollpurCoupon | An object of model property name/value pairs
};
apiInstance.awCollpurCouponUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.CouponCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**qty** | **Number** | Quantity to checkout | [default to 1.0]



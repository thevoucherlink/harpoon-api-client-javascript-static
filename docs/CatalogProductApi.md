# harpoonApi.CatalogProductApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogProductCount**](CatalogProductApi.md#catalogProductCount) | **GET** /CatalogProducts/count | Count instances of the model matched by where from the data source.
[**catalogProductCreate**](CatalogProductApi.md#catalogProductCreate) | **POST** /CatalogProducts | Create a new instance of the model and persist it into the data source.
[**catalogProductCreateChangeStreamGetCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamGetCatalogProductsChangeStream) | **GET** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductCreateChangeStreamPostCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamPostCatalogProductsChangeStream) | **POST** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductDeleteById**](CatalogProductApi.md#catalogProductDeleteById) | **DELETE** /CatalogProducts/{id} | Delete a model instance by {{id}} from the data source.
[**catalogProductExistsGetCatalogProductsidExists**](CatalogProductApi.md#catalogProductExistsGetCatalogProductsidExists) | **GET** /CatalogProducts/{id}/exists | Check whether a model instance exists in the data source.
[**catalogProductExistsHeadCatalogProductsid**](CatalogProductApi.md#catalogProductExistsHeadCatalogProductsid) | **HEAD** /CatalogProducts/{id} | Check whether a model instance exists in the data source.
[**catalogProductFind**](CatalogProductApi.md#catalogProductFind) | **GET** /CatalogProducts | Find all instances of the model matched by filter from the data source.
[**catalogProductFindById**](CatalogProductApi.md#catalogProductFindById) | **GET** /CatalogProducts/{id} | Find a model instance by {{id}} from the data source.
[**catalogProductFindOne**](CatalogProductApi.md#catalogProductFindOne) | **GET** /CatalogProducts/findOne | Find first instance of the model matched by filter from the data source.
[**catalogProductPrototypeCountCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCountCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/count | Counts catalogCategories of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/count | Counts udropshipVendors of CatalogProduct.
[**catalogProductPrototypeCreateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeCreateAwCollpurDeal) | **POST** /CatalogProducts/{id}/awCollpurDeal | Creates a new instance in awCollpurDeal of this model.
[**catalogProductPrototypeCreateCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCreateCatalogCategories) | **POST** /CatalogProducts/{id}/catalogCategories | Creates a new instance in catalogCategories of this model.
[**catalogProductPrototypeCreateInventory**](CatalogProductApi.md#catalogProductPrototypeCreateInventory) | **POST** /CatalogProducts/{id}/inventory | Creates a new instance in inventory of this model.
[**catalogProductPrototypeCreateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeCreateProductEventCompetition) | **POST** /CatalogProducts/{id}/productEventCompetition | Creates a new instance in productEventCompetition of this model.
[**catalogProductPrototypeCreateUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendorProducts) | **POST** /CatalogProducts/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**catalogProductPrototypeCreateUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendors) | **POST** /CatalogProducts/{id}/udropshipVendors | Creates a new instance in udropshipVendors of this model.
[**catalogProductPrototypeDeleteCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDeleteCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories | Deletes all catalogCategories of this model.
[**catalogProductPrototypeDeleteUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**catalogProductPrototypeDeleteUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors | Deletes all udropshipVendors of this model.
[**catalogProductPrototypeDestroyAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeDestroyAwCollpurDeal) | **DELETE** /CatalogProducts/{id}/awCollpurDeal | Deletes awCollpurDeal of this model.
[**catalogProductPrototypeDestroyByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/{fk} | Delete a related item by id for catalogCategories.
[**catalogProductPrototypeDestroyByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeDestroyByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/{fk} | Delete a related item by id for udropshipVendors.
[**catalogProductPrototypeDestroyInventory**](CatalogProductApi.md#catalogProductPrototypeDestroyInventory) | **DELETE** /CatalogProducts/{id}/inventory | Deletes inventory of this model.
[**catalogProductPrototypeDestroyProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeDestroyProductEventCompetition) | **DELETE** /CatalogProducts/{id}/productEventCompetition | Deletes productEventCompetition of this model.
[**catalogProductPrototypeExistsCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeExistsCatalogCategories) | **HEAD** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Check the existence of catalogCategories relation to an item by id.
[**catalogProductPrototypeExistsUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeExistsUdropshipVendors) | **HEAD** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Check the existence of udropshipVendors relation to an item by id.
[**catalogProductPrototypeFindByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeFindByIdCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/{fk} | Find a related item by id for catalogCategories.
[**catalogProductPrototypeFindByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeFindByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/{fk} | Find a related item by id for udropshipVendors.
[**catalogProductPrototypeGetAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeGetAwCollpurDeal) | **GET** /CatalogProducts/{id}/awCollpurDeal | Fetches hasOne relation awCollpurDeal.
[**catalogProductPrototypeGetCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeGetCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories | Queries catalogCategories of CatalogProduct.
[**catalogProductPrototypeGetCheckoutAgreement**](CatalogProductApi.md#catalogProductPrototypeGetCheckoutAgreement) | **GET** /CatalogProducts/{id}/checkoutAgreement | Fetches belongsTo relation checkoutAgreement.
[**catalogProductPrototypeGetCreator**](CatalogProductApi.md#catalogProductPrototypeGetCreator) | **GET** /CatalogProducts/{id}/creator | Fetches belongsTo relation creator.
[**catalogProductPrototypeGetInventory**](CatalogProductApi.md#catalogProductPrototypeGetInventory) | **GET** /CatalogProducts/{id}/inventory | Fetches hasOne relation inventory.
[**catalogProductPrototypeGetProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeGetProductEventCompetition) | **GET** /CatalogProducts/{id}/productEventCompetition | Fetches hasOne relation productEventCompetition.
[**catalogProductPrototypeGetUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeGetUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors | Queries udropshipVendors of CatalogProduct.
[**catalogProductPrototypeLinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeLinkCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Add a related item by id for catalogCategories.
[**catalogProductPrototypeLinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeLinkUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Add a related item by id for udropshipVendors.
[**catalogProductPrototypeUnlinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUnlinkCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Remove the catalogCategories relation to an item by id.
[**catalogProductPrototypeUnlinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUnlinkUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Remove the udropshipVendors relation to an item by id.
[**catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPatchCatalogProductsid) | **PATCH** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAttributesPutCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPutCatalogProductsid) | **PUT** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeUpdateAwCollpurDeal) | **PUT** /CatalogProducts/{id}/awCollpurDeal | Update awCollpurDeal of this model.
[**catalogProductPrototypeUpdateByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/{fk} | Update a related item by id for catalogCategories.
[**catalogProductPrototypeUpdateByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeUpdateByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/{fk} | Update a related item by id for udropshipVendors.
[**catalogProductPrototypeUpdateInventory**](CatalogProductApi.md#catalogProductPrototypeUpdateInventory) | **PUT** /CatalogProducts/{id}/inventory | Update inventory of this model.
[**catalogProductPrototypeUpdateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeUpdateProductEventCompetition) | **PUT** /CatalogProducts/{id}/productEventCompetition | Update productEventCompetition of this model.
[**catalogProductReplaceById**](CatalogProductApi.md#catalogProductReplaceById) | **POST** /CatalogProducts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogProductReplaceOrCreate**](CatalogProductApi.md#catalogProductReplaceOrCreate) | **POST** /CatalogProducts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogProductUpdateAll**](CatalogProductApi.md#catalogProductUpdateAll) | **POST** /CatalogProducts/update | Update instances of the model matched by {{where}} from the data source.
[**catalogProductUpsertPatchCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPatchCatalogProducts) | **PATCH** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertPutCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPutCatalogProducts) | **PUT** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertWithWhere**](CatalogProductApi.md#catalogProductUpsertWithWhere) | **POST** /CatalogProducts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="catalogProductCount"></a>
# **catalogProductCount**
> InlineResponse2001 catalogProductCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogProductCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreate"></a>
# **catalogProductCreate**
> CatalogProduct catalogProductCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | Model instance data
};
apiInstance.catalogProductCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreateChangeStreamGetCatalogProductsChangeStream"></a>
# **catalogProductCreateChangeStreamGetCatalogProductsChangeStream**
> File catalogProductCreateChangeStreamGetCatalogProductsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.catalogProductCreateChangeStreamGetCatalogProductsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreateChangeStreamPostCatalogProductsChangeStream"></a>
# **catalogProductCreateChangeStreamPostCatalogProductsChangeStream**
> File catalogProductCreateChangeStreamPostCatalogProductsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.catalogProductCreateChangeStreamPostCatalogProductsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductDeleteById"></a>
# **catalogProductDeleteById**
> Object catalogProductDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | Model id

apiInstance.catalogProductDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductExistsGetCatalogProductsidExists"></a>
# **catalogProductExistsGetCatalogProductsidExists**
> InlineResponse2003 catalogProductExistsGetCatalogProductsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | Model id

apiInstance.catalogProductExistsGetCatalogProductsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductExistsHeadCatalogProductsid"></a>
# **catalogProductExistsHeadCatalogProductsid**
> InlineResponse2003 catalogProductExistsHeadCatalogProductsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | Model id

apiInstance.catalogProductExistsHeadCatalogProductsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFind"></a>
# **catalogProductFind**
> [CatalogProduct] catalogProductFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.catalogProductFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[CatalogProduct]**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFindById"></a>
# **catalogProductFindById**
> CatalogProduct catalogProductFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.catalogProductFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFindOne"></a>
# **catalogProductFindOne**
> CatalogProduct catalogProductFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.catalogProductFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountCatalogCategories"></a>
# **catalogProductPrototypeCountCatalogCategories**
> InlineResponse2001 catalogProductPrototypeCountCatalogCategories(id, opts)

Counts catalogCategories of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogProductPrototypeCountCatalogCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountUdropshipVendorProducts"></a>
# **catalogProductPrototypeCountUdropshipVendorProducts**
> InlineResponse2001 catalogProductPrototypeCountUdropshipVendorProducts(id, opts)

Counts udropshipVendorProducts of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogProductPrototypeCountUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountUdropshipVendors"></a>
# **catalogProductPrototypeCountUdropshipVendors**
> InlineResponse2001 catalogProductPrototypeCountUdropshipVendors(id, opts)

Counts udropshipVendors of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogProductPrototypeCountUdropshipVendors(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateAwCollpurDeal"></a>
# **catalogProductPrototypeCreateAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeCreateAwCollpurDeal(id, opts)

Creates a new instance in awCollpurDeal of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | 
};
apiInstance.catalogProductPrototypeCreateAwCollpurDeal(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)|  | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateCatalogCategories"></a>
# **catalogProductPrototypeCreateCatalogCategories**
> CatalogCategory catalogProductPrototypeCreateCatalogCategories(id, opts)

Creates a new instance in catalogCategories of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | 
};
apiInstance.catalogProductPrototypeCreateCatalogCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogCategory**](CatalogCategory.md)|  | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateInventory"></a>
# **catalogProductPrototypeCreateInventory**
> CatalogInventoryStockItem catalogProductPrototypeCreateInventory(id, opts)

Creates a new instance in inventory of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogInventoryStockItem() // CatalogInventoryStockItem | 
};
apiInstance.catalogProductPrototypeCreateInventory(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)|  | [optional] 

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateProductEventCompetition"></a>
# **catalogProductPrototypeCreateProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeCreateProductEventCompetition(id, opts)

Creates a new instance in productEventCompetition of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | 
};
apiInstance.catalogProductPrototypeCreateProductEventCompetition(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)|  | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateUdropshipVendorProducts"></a>
# **catalogProductPrototypeCreateUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeCreateUdropshipVendorProducts(id, opts)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProduct() // UdropshipVendorProduct | 
};
apiInstance.catalogProductPrototypeCreateUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional] 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateUdropshipVendors"></a>
# **catalogProductPrototypeCreateUdropshipVendors**
> UdropshipVendor catalogProductPrototypeCreateUdropshipVendors(id, opts)

Creates a new instance in udropshipVendors of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | 
};
apiInstance.catalogProductPrototypeCreateUdropshipVendors(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteCatalogCategories"></a>
# **catalogProductPrototypeDeleteCatalogCategories**
> catalogProductPrototypeDeleteCatalogCategories(id)

Deletes all catalogCategories of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDeleteCatalogCategories(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteUdropshipVendorProducts"></a>
# **catalogProductPrototypeDeleteUdropshipVendorProducts**
> catalogProductPrototypeDeleteUdropshipVendorProducts(id)

Deletes all udropshipVendorProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDeleteUdropshipVendorProducts(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteUdropshipVendors"></a>
# **catalogProductPrototypeDeleteUdropshipVendors**
> catalogProductPrototypeDeleteUdropshipVendors(id)

Deletes all udropshipVendors of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDeleteUdropshipVendors(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyAwCollpurDeal"></a>
# **catalogProductPrototypeDestroyAwCollpurDeal**
> catalogProductPrototypeDestroyAwCollpurDeal(id)

Deletes awCollpurDeal of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyAwCollpurDeal(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdCatalogCategories"></a>
# **catalogProductPrototypeDestroyByIdCatalogCategories**
> catalogProductPrototypeDestroyByIdCatalogCategories(fk, id)

Delete a related item by id for catalogCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyByIdCatalogCategories(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeDestroyByIdUdropshipVendorProducts**
> catalogProductPrototypeDestroyByIdUdropshipVendorProducts(fk, id)

Delete a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyByIdUdropshipVendorProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdUdropshipVendors"></a>
# **catalogProductPrototypeDestroyByIdUdropshipVendors**
> catalogProductPrototypeDestroyByIdUdropshipVendors(fk, id)

Delete a related item by id for udropshipVendors.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyByIdUdropshipVendors(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyInventory"></a>
# **catalogProductPrototypeDestroyInventory**
> catalogProductPrototypeDestroyInventory(id)

Deletes inventory of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyInventory(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyProductEventCompetition"></a>
# **catalogProductPrototypeDestroyProductEventCompetition**
> catalogProductPrototypeDestroyProductEventCompetition(id)

Deletes productEventCompetition of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeDestroyProductEventCompetition(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeExistsCatalogCategories"></a>
# **catalogProductPrototypeExistsCatalogCategories**
> &#39;Boolean&#39; catalogProductPrototypeExistsCatalogCategories(fk, id)

Check the existence of catalogCategories relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeExistsCatalogCategories(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeExistsUdropshipVendors"></a>
# **catalogProductPrototypeExistsUdropshipVendors**
> &#39;Boolean&#39; catalogProductPrototypeExistsUdropshipVendors(fk, id)

Check the existence of udropshipVendors relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeExistsUdropshipVendors(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdCatalogCategories"></a>
# **catalogProductPrototypeFindByIdCatalogCategories**
> CatalogCategory catalogProductPrototypeFindByIdCatalogCategories(fk, id)

Find a related item by id for catalogCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeFindByIdCatalogCategories(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeFindByIdUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeFindByIdUdropshipVendorProducts(fk, id)

Find a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeFindByIdUdropshipVendorProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| CatalogProduct id | 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdUdropshipVendors"></a>
# **catalogProductPrototypeFindByIdUdropshipVendors**
> UdropshipVendor catalogProductPrototypeFindByIdUdropshipVendors(fk, id)

Find a related item by id for udropshipVendors.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeFindByIdUdropshipVendors(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetAwCollpurDeal"></a>
# **catalogProductPrototypeGetAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeGetAwCollpurDeal(id, opts)

Fetches hasOne relation awCollpurDeal.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.catalogProductPrototypeGetAwCollpurDeal(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCatalogCategories"></a>
# **catalogProductPrototypeGetCatalogCategories**
> [CatalogCategory] catalogProductPrototypeGetCatalogCategories(id, opts)

Queries catalogCategories of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.catalogProductPrototypeGetCatalogCategories(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[CatalogCategory]**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCheckoutAgreement"></a>
# **catalogProductPrototypeGetCheckoutAgreement**
> CheckoutAgreement catalogProductPrototypeGetCheckoutAgreement(id, opts)

Fetches belongsTo relation checkoutAgreement.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.catalogProductPrototypeGetCheckoutAgreement(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**CheckoutAgreement**](CheckoutAgreement.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCreator"></a>
# **catalogProductPrototypeGetCreator**
> UdropshipVendor catalogProductPrototypeGetCreator(id, opts)

Fetches belongsTo relation creator.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.catalogProductPrototypeGetCreator(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetInventory"></a>
# **catalogProductPrototypeGetInventory**
> CatalogInventoryStockItem catalogProductPrototypeGetInventory(id, opts)

Fetches hasOne relation inventory.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.catalogProductPrototypeGetInventory(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetProductEventCompetition"></a>
# **catalogProductPrototypeGetProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeGetProductEventCompetition(id, opts)

Fetches hasOne relation productEventCompetition.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.catalogProductPrototypeGetProductEventCompetition(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetUdropshipVendorProducts"></a>
# **catalogProductPrototypeGetUdropshipVendorProducts**
> [UdropshipVendorProduct] catalogProductPrototypeGetUdropshipVendorProducts(id, opts)

Queries udropshipVendorProducts of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.catalogProductPrototypeGetUdropshipVendorProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[UdropshipVendorProduct]**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetUdropshipVendors"></a>
# **catalogProductPrototypeGetUdropshipVendors**
> [UdropshipVendor] catalogProductPrototypeGetUdropshipVendors(id, opts)

Queries udropshipVendors of CatalogProduct.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.catalogProductPrototypeGetUdropshipVendors(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[UdropshipVendor]**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeLinkCatalogCategories"></a>
# **catalogProductPrototypeLinkCatalogCategories**
> CatalogCategoryProduct catalogProductPrototypeLinkCatalogCategories(fk, id, opts)

Add a related item by id for catalogCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogCategoryProduct() // CatalogCategoryProduct | 
};
apiInstance.catalogProductPrototypeLinkCatalogCategories(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogCategoryProduct**](CatalogCategoryProduct.md)|  | [optional] 

### Return type

[**CatalogCategoryProduct**](CatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeLinkUdropshipVendors"></a>
# **catalogProductPrototypeLinkUdropshipVendors**
> UdropshipVendorProductAssoc catalogProductPrototypeLinkUdropshipVendors(fk, id, opts)

Add a related item by id for udropshipVendors.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProductAssoc() // UdropshipVendorProductAssoc | 
};
apiInstance.catalogProductPrototypeLinkUdropshipVendors(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 
 **data** | [**UdropshipVendorProductAssoc**](UdropshipVendorProductAssoc.md)|  | [optional] 

### Return type

[**UdropshipVendorProductAssoc**](UdropshipVendorProductAssoc.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUnlinkCatalogCategories"></a>
# **catalogProductPrototypeUnlinkCatalogCategories**
> catalogProductPrototypeUnlinkCatalogCategories(fk, id)

Remove the catalogCategories relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeUnlinkCatalogCategories(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUnlinkUdropshipVendors"></a>
# **catalogProductPrototypeUnlinkUdropshipVendors**
> catalogProductPrototypeUnlinkUdropshipVendors(fk, id)

Remove the udropshipVendors relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

apiInstance.catalogProductPrototypeUnlinkUdropshipVendors(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAttributesPatchCatalogProductsid"></a>
# **catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**
> CatalogProduct catalogProductPrototypeUpdateAttributesPatchCatalogProductsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | An object of model property name/value pairs
};
apiInstance.catalogProductPrototypeUpdateAttributesPatchCatalogProductsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAttributesPutCatalogProductsid"></a>
# **catalogProductPrototypeUpdateAttributesPutCatalogProductsid**
> CatalogProduct catalogProductPrototypeUpdateAttributesPutCatalogProductsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | An object of model property name/value pairs
};
apiInstance.catalogProductPrototypeUpdateAttributesPutCatalogProductsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAwCollpurDeal"></a>
# **catalogProductPrototypeUpdateAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeUpdateAwCollpurDeal(id, opts)

Update awCollpurDeal of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.AwCollpurDeal() // AwCollpurDeal | 
};
apiInstance.catalogProductPrototypeUpdateAwCollpurDeal(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)|  | [optional] 

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdCatalogCategories"></a>
# **catalogProductPrototypeUpdateByIdCatalogCategories**
> CatalogCategory catalogProductPrototypeUpdateByIdCatalogCategories(fk, id, opts)

Update a related item by id for catalogCategories.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for catalogCategories

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | 
};
apiInstance.catalogProductPrototypeUpdateByIdCatalogCategories(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories | 
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogCategory**](CatalogCategory.md)|  | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeUpdateByIdUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeUpdateByIdUdropshipVendorProducts(fk, id, opts)

Update a related item by id for udropshipVendorProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendorProducts

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.UdropshipVendorProduct() // UdropshipVendorProduct | 
};
apiInstance.catalogProductPrototypeUpdateByIdUdropshipVendorProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts | 
 **id** | **String**| CatalogProduct id | 
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional] 

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdUdropshipVendors"></a>
# **catalogProductPrototypeUpdateByIdUdropshipVendors**
> UdropshipVendor catalogProductPrototypeUpdateByIdUdropshipVendors(fk, id, opts)

Update a related item by id for udropshipVendors.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var fk = "fk_example"; // String | Foreign key for udropshipVendors

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.UdropshipVendor() // UdropshipVendor | 
};
apiInstance.catalogProductPrototypeUpdateByIdUdropshipVendors(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors | 
 **id** | **String**| CatalogProduct id | 
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional] 

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateInventory"></a>
# **catalogProductPrototypeUpdateInventory**
> CatalogInventoryStockItem catalogProductPrototypeUpdateInventory(id, opts)

Update inventory of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.CatalogInventoryStockItem() // CatalogInventoryStockItem | 
};
apiInstance.catalogProductPrototypeUpdateInventory(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)|  | [optional] 

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateProductEventCompetition"></a>
# **catalogProductPrototypeUpdateProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeUpdateProductEventCompetition(id, opts)

Update productEventCompetition of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | CatalogProduct id

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | 
};
apiInstance.catalogProductPrototypeUpdateProductEventCompetition(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id | 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)|  | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductReplaceById"></a>
# **catalogProductReplaceById**
> CatalogProduct catalogProductReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | Model instance data
};
apiInstance.catalogProductReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductReplaceOrCreate"></a>
# **catalogProductReplaceOrCreate**
> CatalogProduct catalogProductReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | Model instance data
};
apiInstance.catalogProductReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpdateAll"></a>
# **catalogProductUpdateAll**
> InlineResponse2002 catalogProductUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | An object of model property name/value pairs
};
apiInstance.catalogProductUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertPatchCatalogProducts"></a>
# **catalogProductUpsertPatchCatalogProducts**
> CatalogProduct catalogProductUpsertPatchCatalogProducts(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | Model instance data
};
apiInstance.catalogProductUpsertPatchCatalogProducts(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertPutCatalogProducts"></a>
# **catalogProductUpsertPutCatalogProducts**
> CatalogProduct catalogProductUpsertPutCatalogProducts(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | Model instance data
};
apiInstance.catalogProductUpsertPutCatalogProducts(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertWithWhere"></a>
# **catalogProductUpsertWithWhere**
> CatalogProduct catalogProductUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogProductApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | An object of model property name/value pairs
};
apiInstance.catalogProductUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


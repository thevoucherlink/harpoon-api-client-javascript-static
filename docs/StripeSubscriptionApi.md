# harpoonApi.StripeSubscriptionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeSubscriptionCount**](StripeSubscriptionApi.md#stripeSubscriptionCount) | **GET** /StripeSubscriptions/count | Count instances of the model matched by where from the data source.
[**stripeSubscriptionCreate**](StripeSubscriptionApi.md#stripeSubscriptionCreate) | **POST** /StripeSubscriptions | Create a new instance of the model and persist it into the data source.
[**stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream) | **GET** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream) | **POST** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionDeleteById**](StripeSubscriptionApi.md#stripeSubscriptionDeleteById) | **DELETE** /StripeSubscriptions/{id} | Delete a model instance by {{id}} from the data source.
[**stripeSubscriptionExistsGetStripeSubscriptionsidExists**](StripeSubscriptionApi.md#stripeSubscriptionExistsGetStripeSubscriptionsidExists) | **GET** /StripeSubscriptions/{id}/exists | Check whether a model instance exists in the data source.
[**stripeSubscriptionExistsHeadStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionExistsHeadStripeSubscriptionsid) | **HEAD** /StripeSubscriptions/{id} | Check whether a model instance exists in the data source.
[**stripeSubscriptionFind**](StripeSubscriptionApi.md#stripeSubscriptionFind) | **GET** /StripeSubscriptions | Find all instances of the model matched by filter from the data source.
[**stripeSubscriptionFindById**](StripeSubscriptionApi.md#stripeSubscriptionFindById) | **GET** /StripeSubscriptions/{id} | Find a model instance by {{id}} from the data source.
[**stripeSubscriptionFindOne**](StripeSubscriptionApi.md#stripeSubscriptionFindOne) | **GET** /StripeSubscriptions/findOne | Find first instance of the model matched by filter from the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid) | **PATCH** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid) | **PUT** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceById**](StripeSubscriptionApi.md#stripeSubscriptionReplaceById) | **POST** /StripeSubscriptions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceOrCreate**](StripeSubscriptionApi.md#stripeSubscriptionReplaceOrCreate) | **POST** /StripeSubscriptions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpdateAll**](StripeSubscriptionApi.md#stripeSubscriptionUpdateAll) | **POST** /StripeSubscriptions/update | Update instances of the model matched by {{where}} from the data source.
[**stripeSubscriptionUpsertPatchStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPatchStripeSubscriptions) | **PATCH** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertPutStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPutStripeSubscriptions) | **PUT** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertWithWhere**](StripeSubscriptionApi.md#stripeSubscriptionUpsertWithWhere) | **POST** /StripeSubscriptions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeSubscriptionCount"></a>
# **stripeSubscriptionCount**
> InlineResponse2001 stripeSubscriptionCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.stripeSubscriptionCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreate"></a>
# **stripeSubscriptionCreate**
> StripeSubscription stripeSubscriptionCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | Model instance data
};
apiInstance.stripeSubscriptionCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream"></a>
# **stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**
> File stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream"></a>
# **stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**
> File stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionDeleteById"></a>
# **stripeSubscriptionDeleteById**
> Object stripeSubscriptionDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | Model id

apiInstance.stripeSubscriptionDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionExistsGetStripeSubscriptionsidExists"></a>
# **stripeSubscriptionExistsGetStripeSubscriptionsidExists**
> InlineResponse2003 stripeSubscriptionExistsGetStripeSubscriptionsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | Model id

apiInstance.stripeSubscriptionExistsGetStripeSubscriptionsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionExistsHeadStripeSubscriptionsid"></a>
# **stripeSubscriptionExistsHeadStripeSubscriptionsid**
> InlineResponse2003 stripeSubscriptionExistsHeadStripeSubscriptionsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | Model id

apiInstance.stripeSubscriptionExistsHeadStripeSubscriptionsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFind"></a>
# **stripeSubscriptionFind**
> [StripeSubscription] stripeSubscriptionFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeSubscriptionFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[StripeSubscription]**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFindById"></a>
# **stripeSubscriptionFindById**
> StripeSubscription stripeSubscriptionFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.stripeSubscriptionFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFindOne"></a>
# **stripeSubscriptionFindOne**
> StripeSubscription stripeSubscriptionFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.stripeSubscriptionFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid"></a>
# **stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**
> StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | StripeSubscription id

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | An object of model property name/value pairs
};
apiInstance.stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeSubscription id | 
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid"></a>
# **stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**
> StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | StripeSubscription id

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | An object of model property name/value pairs
};
apiInstance.stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeSubscription id | 
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionReplaceById"></a>
# **stripeSubscriptionReplaceById**
> StripeSubscription stripeSubscriptionReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | Model instance data
};
apiInstance.stripeSubscriptionReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionReplaceOrCreate"></a>
# **stripeSubscriptionReplaceOrCreate**
> StripeSubscription stripeSubscriptionReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | Model instance data
};
apiInstance.stripeSubscriptionReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpdateAll"></a>
# **stripeSubscriptionUpdateAll**
> InlineResponse2002 stripeSubscriptionUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | An object of model property name/value pairs
};
apiInstance.stripeSubscriptionUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertPatchStripeSubscriptions"></a>
# **stripeSubscriptionUpsertPatchStripeSubscriptions**
> StripeSubscription stripeSubscriptionUpsertPatchStripeSubscriptions(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | Model instance data
};
apiInstance.stripeSubscriptionUpsertPatchStripeSubscriptions(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertPutStripeSubscriptions"></a>
# **stripeSubscriptionUpsertPutStripeSubscriptions**
> StripeSubscription stripeSubscriptionUpsertPutStripeSubscriptions(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | Model instance data
};
apiInstance.stripeSubscriptionUpsertPutStripeSubscriptions(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertWithWhere"></a>
# **stripeSubscriptionUpsertWithWhere**
> StripeSubscription stripeSubscriptionUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.StripeSubscriptionApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.StripeSubscription() // StripeSubscription | An object of model property name/value pairs
};
apiInstance.stripeSubscriptionUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional] 

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


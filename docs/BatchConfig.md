# harpoonApi.BatchConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  | [optional] 
**iOSApiKey** | **String** |  | [optional] 
**androidApiKey** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



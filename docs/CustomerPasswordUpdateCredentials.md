# harpoonApi.CustomerPasswordUpdateCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPassword** | **String** | Current Customer Passwrod | 
**newPassword** | **String** | New Customer Password | 



# harpoonApi.CustomerConnectionTwitter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.AwEventbookingEventTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventTicketCount**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCount) | **GET** /AwEventbookingEventTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventTicketCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreate) | **POST** /AwEventbookingEventTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream) | **GET** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream) | **POST** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketDeleteById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketDeleteById) | **DELETE** /AwEventbookingEventTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists) | **GET** /AwEventbookingEventTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid) | **HEAD** /AwEventbookingEventTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketFind**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFind) | **GET** /AwEventbookingEventTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventTicketFindById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindById) | **GET** /AwEventbookingEventTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventTicketFindOne**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindOne) | **GET** /AwEventbookingEventTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventTicketPrototypeCountAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/count | Counts attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/count | Counts awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/count | Counts tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCreateAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAttributes) | **POST** /AwEventbookingEventTickets/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAwEventbookingTicket) | **POST** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Creates a new instance in awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeCreateTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateTickets) | **POST** /AwEventbookingEventTickets/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventTicketPrototypeDeleteAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Deletes all awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeDeleteTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventTicketPrototypeDestroyByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Delete a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeDestroyByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventTicketPrototypeFindByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Find a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeFindByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventTicketPrototypeGetAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes | Queries attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Queries awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets | Queries tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid) | **PATCH** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid) | **PUT** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEventTickets/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket) | **PUT** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Update a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeUpdateByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEventTickets/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventTicketReplaceById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceById) | **POST** /AwEventbookingEventTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketReplaceOrCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceOrCreate) | **POST** /AwEventbookingEventTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpdateAll**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpdateAll) | **POST** /AwEventbookingEventTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets) | **PATCH** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPutAwEventbookingEventTickets) | **PUT** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertWithWhere**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertWithWhere) | **POST** /AwEventbookingEventTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingEventTicketCount"></a>
# **awEventbookingEventTicketCount**
> InlineResponse2001 awEventbookingEventTicketCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventTicketCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreate"></a>
# **awEventbookingEventTicketCreate**
> AwEventbookingEventTicket awEventbookingEventTicketCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | Model instance data
};
apiInstance.awEventbookingEventTicketCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream"></a>
# **awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**
> File awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream"></a>
# **awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**
> File awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketDeleteById"></a>
# **awEventbookingEventTicketDeleteById**
> Object awEventbookingEventTicketDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventTicketDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists"></a>
# **awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**
> InlineResponse2003 awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**
> InlineResponse2003 awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFind"></a>
# **awEventbookingEventTicketFind**
> [AwEventbookingEventTicket] awEventbookingEventTicketFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingEventTicketFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwEventbookingEventTicket]**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFindById"></a>
# **awEventbookingEventTicketFindById**
> AwEventbookingEventTicket awEventbookingEventTicketFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awEventbookingEventTicketFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFindOne"></a>
# **awEventbookingEventTicketFindOne**
> AwEventbookingEventTicket awEventbookingEventTicketFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingEventTicketFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountAttributes"></a>
# **awEventbookingEventTicketPrototypeCountAttributes**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountAttributes(id, opts)

Counts attributes of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventTicketPrototypeCountAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeCountAwEventbookingTicket**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountAwEventbookingTicket(id, opts)

Counts awEventbookingTicket of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventTicketPrototypeCountAwEventbookingTicket(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountTickets"></a>
# **awEventbookingEventTicketPrototypeCountTickets**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountTickets(id, opts)

Counts tickets of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventTicketPrototypeCountTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateAttributes"></a>
# **awEventbookingEventTicketPrototypeCreateAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeCreateAttributes(id, opts)

Creates a new instance in attributes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicketAttribute() // AwEventbookingEventTicketAttribute | 
};
apiInstance.awEventbookingEventTicketPrototypeCreateAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)|  | [optional] 

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeCreateAwEventbookingTicket(id, opts)

Creates a new instance in awEventbookingTicket of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventTicketPrototypeCreateAwEventbookingTicket(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateTickets"></a>
# **awEventbookingEventTicketPrototypeCreateTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeCreateTickets(id, opts)

Creates a new instance in tickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventTicketPrototypeCreateTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteAttributes"></a>
# **awEventbookingEventTicketPrototypeDeleteAttributes**
> awEventbookingEventTicketPrototypeDeleteAttributes(id)

Deletes all attributes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDeleteAttributes(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket(id)

Deletes all awEventbookingTicket of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteTickets"></a>
# **awEventbookingEventTicketPrototypeDeleteTickets**
> awEventbookingEventTicketPrototypeDeleteTickets(id)

Deletes all tickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDeleteTickets(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdAttributes**
> awEventbookingEventTicketPrototypeDestroyByIdAttributes(fk, id)

Delete a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDestroyByIdAttributes(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket(fk, id)

Delete a related item by id for awEventbookingTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for awEventbookingTicket

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdTickets"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdTickets**
> awEventbookingEventTicketPrototypeDestroyByIdTickets(fk, id)

Delete a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeDestroyByIdTickets(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeFindByIdAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeFindByIdAttributes(fk, id)

Find a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeFindByIdAttributes(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket(fk, id)

Find a related item by id for awEventbookingTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for awEventbookingTicket

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdTickets"></a>
# **awEventbookingEventTicketPrototypeFindByIdTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdTickets(fk, id)

Find a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEventTicket id

apiInstance.awEventbookingEventTicketPrototypeFindByIdTickets(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEventTicket id | 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetAttributes"></a>
# **awEventbookingEventTicketPrototypeGetAttributes**
> [AwEventbookingEventTicketAttribute] awEventbookingEventTicketPrototypeGetAttributes(id, opts)

Queries attributes of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventTicketPrototypeGetAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingEventTicketAttribute]**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeGetAwEventbookingTicket**
> [AwEventbookingTicket] awEventbookingEventTicketPrototypeGetAwEventbookingTicket(id, opts)

Queries awEventbookingTicket of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventTicketPrototypeGetAwEventbookingTicket(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingTicket]**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetTickets"></a>
# **awEventbookingEventTicketPrototypeGetTickets**
> [AwEventbookingTicket] awEventbookingEventTicketPrototypeGetTickets(id, opts)

Queries tickets of AwEventbookingEventTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventTicketPrototypeGetTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingTicket]**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**
> AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**
> AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeUpdateByIdAttributes(fk, id, opts)

Update a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicketAttribute() // AwEventbookingEventTicketAttribute | 
};
apiInstance.awEventbookingEventTicketPrototypeUpdateByIdAttributes(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)|  | [optional] 

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket(fk, id, opts)

Update a related item by id for awEventbookingTicket.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for awEventbookingTicket

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket | 
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdTickets"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdTickets(fk, id, opts)

Update a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEventTicket id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventTicketPrototypeUpdateByIdTickets(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEventTicket id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketReplaceById"></a>
# **awEventbookingEventTicketReplaceById**
> AwEventbookingEventTicket awEventbookingEventTicketReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | Model instance data
};
apiInstance.awEventbookingEventTicketReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketReplaceOrCreate"></a>
# **awEventbookingEventTicketReplaceOrCreate**
> AwEventbookingEventTicket awEventbookingEventTicketReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | Model instance data
};
apiInstance.awEventbookingEventTicketReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpdateAll"></a>
# **awEventbookingEventTicketUpdateAll**
> InlineResponse2002 awEventbookingEventTicketUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingEventTicketUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets"></a>
# **awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | Model instance data
};
apiInstance.awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertPutAwEventbookingEventTickets"></a>
# **awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertPutAwEventbookingEventTickets(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | Model instance data
};
apiInstance.awEventbookingEventTicketUpsertPutAwEventbookingEventTickets(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertWithWhere"></a>
# **awEventbookingEventTicketUpsertWithWhere**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventTicketApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | An object of model property name/value pairs
};
apiInstance.awEventbookingEventTicketUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


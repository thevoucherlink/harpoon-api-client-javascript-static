# harpoonApi.PlayerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mute** | **Boolean** | Available on the following SDKs: JS, iOS | [optional] [default to false]
**autostart** | **Boolean** | Available on the following SDKs: JS, iOS, Android | [optional] [default to false]
**repeat** | **Boolean** | Available on the following SDKs: JS, iOS, Android | [optional] [default to false]
**controls** | **Boolean** | Available on the following SDKs: JS, iOS, Android | [optional] [default to true]
**visualPlaylist** | **Boolean** | Available on the following SDKs: JS (as visualplaylist) | [optional] [default to true]
**displayTitle** | **Boolean** | Available on the following SDKs: JS (as displaytitle) | [optional] [default to true]
**displayDescription** | **Boolean** | Available on the following SDKs: JS (as displaydescription) | [optional] [default to true]
**stretching** | **String** | Available on the following SDKs: JS, iOS, Android (as stretch) | [optional] [default to &#39;uniform&#39;]
**hlshtml** | **Boolean** | Available on the following SDKs: JS | [optional] [default to false]
**primary** | **String** | Available on the following SDKs: JS | [optional] [default to &#39;html5&#39;]
**flashPlayer** | **String** | Available on the following SDKs: JS (as flashplayer) | [optional] [default to &#39;/&#39;]
**baseSkinPath** | **String** | Available on the following SDKs: JS (as base) | [optional] [default to &#39;/&#39;]
**preload** | **String** | Available on the following SDKs: JS | [optional] 
**playerAdConfig** | **Object** | Available on the following SDKs: JS, iOS, Android | [optional] 
**playerCaptionConfig** | **Object** | Available on the following SDKs: JS, iOS, Android | [optional] 
**skinName** | **String** | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin) | [optional] 
**skinCss** | **String** | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin) | [optional] 
**skinActive** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS | [optional] 
**skinInactive** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS | [optional] 
**skinBackground** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS | [optional] 
**id** | **Number** |  | [optional] 



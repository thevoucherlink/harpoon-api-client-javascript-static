# harpoonApi.RadioPresenterRadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**radioPresenterId** | **Number** |  | [optional] 
**radioShowId** | **Number** |  | [optional] 
**radioPresenter** | **Object** |  | [optional] 
**radioShow** | **Object** |  | [optional] 



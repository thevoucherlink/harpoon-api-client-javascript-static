# harpoonApi.FacebookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **String** |  | [optional] 
**category** | **String** |  | [optional] 
**updatedTime** | **String** |  | [optional] 
**maybeCount** | **Number** |  | [optional] [default to 0.0]
**noreplyCount** | **Number** |  | [optional] [default to 0.0]
**attendingCount** | **Number** |  | [optional] [default to 0.0]
**place** | **Object** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.PlaylistApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistCount**](PlaylistApi.md#playlistCount) | **GET** /Playlists/count | Count instances of the model matched by where from the data source.
[**playlistCreate**](PlaylistApi.md#playlistCreate) | **POST** /Playlists | Create a new instance of the model and persist it into the data source.
[**playlistCreateChangeStreamGetPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamGetPlaylistsChangeStream) | **GET** /Playlists/change-stream | Create a change stream.
[**playlistCreateChangeStreamPostPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamPostPlaylistsChangeStream) | **POST** /Playlists/change-stream | Create a change stream.
[**playlistDeleteById**](PlaylistApi.md#playlistDeleteById) | **DELETE** /Playlists/{id} | Delete a model instance by {{id}} from the data source.
[**playlistExistsGetPlaylistsidExists**](PlaylistApi.md#playlistExistsGetPlaylistsidExists) | **GET** /Playlists/{id}/exists | Check whether a model instance exists in the data source.
[**playlistExistsHeadPlaylistsid**](PlaylistApi.md#playlistExistsHeadPlaylistsid) | **HEAD** /Playlists/{id} | Check whether a model instance exists in the data source.
[**playlistFind**](PlaylistApi.md#playlistFind) | **GET** /Playlists | Find all instances of the model matched by filter from the data source.
[**playlistFindById**](PlaylistApi.md#playlistFindById) | **GET** /Playlists/{id} | Find a model instance by {{id}} from the data source.
[**playlistFindOne**](PlaylistApi.md#playlistFindOne) | **GET** /Playlists/findOne | Find first instance of the model matched by filter from the data source.
[**playlistPrototypeCountPlaylistItems**](PlaylistApi.md#playlistPrototypeCountPlaylistItems) | **GET** /Playlists/{id}/playlistItems/count | Counts playlistItems of Playlist.
[**playlistPrototypeCreatePlaylistItems**](PlaylistApi.md#playlistPrototypeCreatePlaylistItems) | **POST** /Playlists/{id}/playlistItems | Creates a new instance in playlistItems of this model.
[**playlistPrototypeDeletePlaylistItems**](PlaylistApi.md#playlistPrototypeDeletePlaylistItems) | **DELETE** /Playlists/{id}/playlistItems | Deletes all playlistItems of this model.
[**playlistPrototypeDestroyByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeDestroyByIdPlaylistItems) | **DELETE** /Playlists/{id}/playlistItems/{fk} | Delete a related item by id for playlistItems.
[**playlistPrototypeFindByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeFindByIdPlaylistItems) | **GET** /Playlists/{id}/playlistItems/{fk} | Find a related item by id for playlistItems.
[**playlistPrototypeGetPlaylistItems**](PlaylistApi.md#playlistPrototypeGetPlaylistItems) | **GET** /Playlists/{id}/playlistItems | Queries playlistItems of Playlist.
[**playlistPrototypeUpdateAttributesPatchPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPatchPlaylistsid) | **PATCH** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateAttributesPutPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPutPlaylistsid) | **PUT** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeUpdateByIdPlaylistItems) | **PUT** /Playlists/{id}/playlistItems/{fk} | Update a related item by id for playlistItems.
[**playlistReplaceById**](PlaylistApi.md#playlistReplaceById) | **POST** /Playlists/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistReplaceOrCreate**](PlaylistApi.md#playlistReplaceOrCreate) | **POST** /Playlists/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistUpdateAll**](PlaylistApi.md#playlistUpdateAll) | **POST** /Playlists/update | Update instances of the model matched by {{where}} from the data source.
[**playlistUpsertPatchPlaylists**](PlaylistApi.md#playlistUpsertPatchPlaylists) | **PATCH** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertPutPlaylists**](PlaylistApi.md#playlistUpsertPutPlaylists) | **PUT** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertWithWhere**](PlaylistApi.md#playlistUpsertWithWhere) | **POST** /Playlists/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playlistCount"></a>
# **playlistCount**
> InlineResponse2001 playlistCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreate"></a>
# **playlistCreate**
> Playlist playlistCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | Model instance data
};
apiInstance.playlistCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreateChangeStreamGetPlaylistsChangeStream"></a>
# **playlistCreateChangeStreamGetPlaylistsChangeStream**
> File playlistCreateChangeStreamGetPlaylistsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playlistCreateChangeStreamGetPlaylistsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreateChangeStreamPostPlaylistsChangeStream"></a>
# **playlistCreateChangeStreamPostPlaylistsChangeStream**
> File playlistCreateChangeStreamPostPlaylistsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.playlistCreateChangeStreamPostPlaylistsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistDeleteById"></a>
# **playlistDeleteById**
> Object playlistDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Model id

apiInstance.playlistDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistExistsGetPlaylistsidExists"></a>
# **playlistExistsGetPlaylistsidExists**
> InlineResponse2003 playlistExistsGetPlaylistsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Model id

apiInstance.playlistExistsGetPlaylistsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistExistsHeadPlaylistsid"></a>
# **playlistExistsHeadPlaylistsid**
> InlineResponse2003 playlistExistsHeadPlaylistsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Model id

apiInstance.playlistExistsHeadPlaylistsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFind"></a>
# **playlistFind**
> [Playlist] playlistFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playlistFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[Playlist]**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFindById"></a>
# **playlistFindById**
> Playlist playlistFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.playlistFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFindOne"></a>
# **playlistFindOne**
> Playlist playlistFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.playlistFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeCountPlaylistItems"></a>
# **playlistPrototypeCountPlaylistItems**
> InlineResponse2001 playlistPrototypeCountPlaylistItems(id, opts)

Counts playlistItems of Playlist.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.playlistPrototypeCountPlaylistItems(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeCreatePlaylistItems"></a>
# **playlistPrototypeCreatePlaylistItems**
> PlaylistItem playlistPrototypeCreatePlaylistItems(id, opts)

Creates a new instance in playlistItems of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | 
};
apiInstance.playlistPrototypeCreatePlaylistItems(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 
 **data** | [**PlaylistItem**](PlaylistItem.md)|  | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeDeletePlaylistItems"></a>
# **playlistPrototypeDeletePlaylistItems**
> playlistPrototypeDeletePlaylistItems(id)

Deletes all playlistItems of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

apiInstance.playlistPrototypeDeletePlaylistItems(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeDestroyByIdPlaylistItems"></a>
# **playlistPrototypeDestroyByIdPlaylistItems**
> playlistPrototypeDestroyByIdPlaylistItems(fk, id)

Delete a related item by id for playlistItems.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var fk = "fk_example"; // String | Foreign key for playlistItems

var id = "id_example"; // String | Playlist id

apiInstance.playlistPrototypeDestroyByIdPlaylistItems(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems | 
 **id** | **String**| Playlist id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeFindByIdPlaylistItems"></a>
# **playlistPrototypeFindByIdPlaylistItems**
> PlaylistItem playlistPrototypeFindByIdPlaylistItems(fk, id)

Find a related item by id for playlistItems.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var fk = "fk_example"; // String | Foreign key for playlistItems

var id = "id_example"; // String | Playlist id

apiInstance.playlistPrototypeFindByIdPlaylistItems(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems | 
 **id** | **String**| Playlist id | 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeGetPlaylistItems"></a>
# **playlistPrototypeGetPlaylistItems**
> [PlaylistItem] playlistPrototypeGetPlaylistItems(id, opts)

Queries playlistItems of Playlist.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.playlistPrototypeGetPlaylistItems(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[PlaylistItem]**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateAttributesPatchPlaylistsid"></a>
# **playlistPrototypeUpdateAttributesPatchPlaylistsid**
> Playlist playlistPrototypeUpdateAttributesPatchPlaylistsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | An object of model property name/value pairs
};
apiInstance.playlistPrototypeUpdateAttributesPatchPlaylistsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateAttributesPutPlaylistsid"></a>
# **playlistPrototypeUpdateAttributesPutPlaylistsid**
> Playlist playlistPrototypeUpdateAttributesPutPlaylistsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Playlist id

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | An object of model property name/value pairs
};
apiInstance.playlistPrototypeUpdateAttributesPutPlaylistsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id | 
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateByIdPlaylistItems"></a>
# **playlistPrototypeUpdateByIdPlaylistItems**
> PlaylistItem playlistPrototypeUpdateByIdPlaylistItems(fk, id, opts)

Update a related item by id for playlistItems.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var fk = "fk_example"; // String | Foreign key for playlistItems

var id = "id_example"; // String | Playlist id

var opts = { 
  'data': new harpoonApi.PlaylistItem() // PlaylistItem | 
};
apiInstance.playlistPrototypeUpdateByIdPlaylistItems(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems | 
 **id** | **String**| Playlist id | 
 **data** | [**PlaylistItem**](PlaylistItem.md)|  | [optional] 

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistReplaceById"></a>
# **playlistReplaceById**
> Playlist playlistReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | Model instance data
};
apiInstance.playlistReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistReplaceOrCreate"></a>
# **playlistReplaceOrCreate**
> Playlist playlistReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | Model instance data
};
apiInstance.playlistReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpdateAll"></a>
# **playlistUpdateAll**
> InlineResponse2002 playlistUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.Playlist() // Playlist | An object of model property name/value pairs
};
apiInstance.playlistUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertPatchPlaylists"></a>
# **playlistUpsertPatchPlaylists**
> Playlist playlistUpsertPatchPlaylists(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | Model instance data
};
apiInstance.playlistUpsertPatchPlaylists(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertPutPlaylists"></a>
# **playlistUpsertPutPlaylists**
> Playlist playlistUpsertPutPlaylists(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'data': new harpoonApi.Playlist() // Playlist | Model instance data
};
apiInstance.playlistUpsertPutPlaylists(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertWithWhere"></a>
# **playlistUpsertWithWhere**
> Playlist playlistUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.PlaylistApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.Playlist() // Playlist | An object of model property name/value pairs
};
apiInstance.playlistUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional] 

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


# harpoonApi.CustomerPasswordResetCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** | Customer Email | 



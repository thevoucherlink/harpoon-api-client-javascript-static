# harpoonApi.HarpoonHpublicApplicationpartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**applicationId** | **Number** |  | 
**brandId** | **Number** |  | 
**status** | **String** |  | 
**invitedEmail** | **String** |  | [optional] 
**invitedAt** | **Date** |  | [optional] 
**acceptedAt** | **Date** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**configList** | **Number** |  | [optional] 
**configFeed** | **Number** |  | [optional] 
**configNeedFollow** | **Number** |  | [optional] 
**configAcceptEvent** | **Number** |  | [optional] 
**configAcceptCoupon** | **Number** |  | [optional] 
**configAcceptDealsimple** | **Number** |  | [optional] 
**configAcceptDealgroup** | **Number** |  | [optional] 
**configAcceptNotificationpush** | **Number** |  | [optional] 
**configAcceptNotificationbeacon** | **Number** |  | [optional] 
**isOwner** | **Number** |  | [optional] 
**configApproveEvent** | **Number** |  | [optional] 
**configApproveCoupon** | **Number** |  | [optional] 
**configApproveDealsimple** | **Number** |  | [optional] 
**configApproveDealgroup** | **Number** |  | [optional] 
**udropshipVendor** | **Object** |  | [optional] 



# harpoonApi.EventCheckoutTicketData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Ticket to checkout | 
**qty** | **Number** | Quantity to checkout | [optional] [default to 0.0]



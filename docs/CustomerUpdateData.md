# harpoonApi.CustomerUpdateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** | New first name for the Customer | 
**lastName** | **String** | New last name for the Customer | [optional] 
**email** | **String** | New email for the Customer | [optional] 
**gender** | **String** | New gender for the Customer | [optional] 
**dob** | **String** | New date of birth for the Customer | [optional] 
**metadata** | **Object** | New metadata collection for the Customer | [optional] 
**privacy** | [**CustomerPrivacy**](CustomerPrivacy.md) | New privacy settings for the Customer | [optional] 
**profilePictureUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) | New profile picture for the Customer | [optional] 
**coverUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) | New cover photo for the customer | [optional] 



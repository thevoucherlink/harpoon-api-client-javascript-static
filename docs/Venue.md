# harpoonApi.Venue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**coordinates** | [**GeoLocation**](GeoLocation.md) |  | [optional] 
**phone** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**brand** | [**Brand**](Brand.md) |  | [optional] 
**id** | **Number** |  | [optional] 



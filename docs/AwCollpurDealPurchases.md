# harpoonApi.AwCollpurDealPurchases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**dealId** | **Number** |  | 
**orderId** | **Number** |  | 
**orderItemId** | **Number** |  | 
**qtyPurchased** | **Number** |  | 
**qtyWithCoupons** | **Number** |  | 
**customerName** | **String** |  | 
**customerId** | **Number** |  | 
**purchaseDateTime** | **Date** |  | [optional] 
**shippingAmount** | **String** |  | 
**qtyOrdered** | **String** |  | 
**refundState** | **Number** |  | [optional] 
**isSuccessedFlag** | **Number** |  | [optional] 
**awCollpurCoupon** | **Object** |  | [optional] 



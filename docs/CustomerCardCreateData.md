# harpoonApi.CustomerCardCreateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**_number** | **String** |  | 
**cvc** | **String** |  | 
**exMonth** | **String** |  | 
**exYear** | **String** |  | 
**cardholderName** | **String** |  | 



# harpoonApi.PlaylistItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Stream title | [optional] 
**shortDescription** | **String** | Stream short description (on SDKs is under desc) | [optional] 
**image** | **String** | Playlist image | [optional] 
**file** | **String** | Stream file | [optional] 
**type** | **String** | Stream type (used only in JS SDK) | [optional] 
**mediaType** | **String** | Stream media type (e.g. video , audio , radioStream) | [optional] 
**mediaId** | **Number** | Ad media id (on SDKs is under mediaid) | [optional] 
**order** | **Number** | Sort order index | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 
**playlistId** | **Number** |  | [optional] 
**playlist** | **Object** |  | [optional] 
**playerSources** | **[Object]** |  | [optional] 
**playerTracks** | **[Object]** |  | [optional] 
**radioShows** | **[Object]** |  | [optional] 



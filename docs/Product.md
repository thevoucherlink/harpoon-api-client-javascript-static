# harpoonApi.Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**price** | **Number** |  | [optional] [default to 0.0]
**baseCurrency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**id** | **Number** |  | [optional] 



# harpoonApi.CatalogCategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogCategoryCount**](CatalogCategoryApi.md#catalogCategoryCount) | **GET** /CatalogCategories/count | Count instances of the model matched by where from the data source.
[**catalogCategoryCreate**](CatalogCategoryApi.md#catalogCategoryCreate) | **POST** /CatalogCategories | Create a new instance of the model and persist it into the data source.
[**catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream) | **GET** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream) | **POST** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryDeleteById**](CatalogCategoryApi.md#catalogCategoryDeleteById) | **DELETE** /CatalogCategories/{id} | Delete a model instance by {{id}} from the data source.
[**catalogCategoryExistsGetCatalogCategoriesidExists**](CatalogCategoryApi.md#catalogCategoryExistsGetCatalogCategoriesidExists) | **GET** /CatalogCategories/{id}/exists | Check whether a model instance exists in the data source.
[**catalogCategoryExistsHeadCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryExistsHeadCatalogCategoriesid) | **HEAD** /CatalogCategories/{id} | Check whether a model instance exists in the data source.
[**catalogCategoryFind**](CatalogCategoryApi.md#catalogCategoryFind) | **GET** /CatalogCategories | Find all instances of the model matched by filter from the data source.
[**catalogCategoryFindById**](CatalogCategoryApi.md#catalogCategoryFindById) | **GET** /CatalogCategories/{id} | Find a model instance by {{id}} from the data source.
[**catalogCategoryFindOne**](CatalogCategoryApi.md#catalogCategoryFindOne) | **GET** /CatalogCategories/findOne | Find first instance of the model matched by filter from the data source.
[**catalogCategoryPrototypeCountCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCountCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/count | Counts catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeCreateCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCreateCatalogProducts) | **POST** /CatalogCategories/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**catalogCategoryPrototypeDeleteCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDeleteCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**catalogCategoryPrototypeDestroyByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDestroyByIdCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**catalogCategoryPrototypeExistsCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeExistsCatalogProducts) | **HEAD** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**catalogCategoryPrototypeFindByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeFindByIdCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**catalogCategoryPrototypeGetCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeGetCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts | Queries catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeLinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeLinkCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**catalogCategoryPrototypeUnlinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUnlinkCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid) | **PATCH** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid) | **PUT** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateByIdCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**catalogCategoryReplaceById**](CatalogCategoryApi.md#catalogCategoryReplaceById) | **POST** /CatalogCategories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogCategoryReplaceOrCreate**](CatalogCategoryApi.md#catalogCategoryReplaceOrCreate) | **POST** /CatalogCategories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogCategoryUpdateAll**](CatalogCategoryApi.md#catalogCategoryUpdateAll) | **POST** /CatalogCategories/update | Update instances of the model matched by {{where}} from the data source.
[**catalogCategoryUpsertPatchCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPatchCatalogCategories) | **PATCH** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertPutCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPutCatalogCategories) | **PUT** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertWithWhere**](CatalogCategoryApi.md#catalogCategoryUpsertWithWhere) | **POST** /CatalogCategories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="catalogCategoryCount"></a>
# **catalogCategoryCount**
> InlineResponse2001 catalogCategoryCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogCategoryCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreate"></a>
# **catalogCategoryCreate**
> CatalogCategory catalogCategoryCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | Model instance data
};
apiInstance.catalogCategoryCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream"></a>
# **catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**
> File catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream"></a>
# **catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**
> File catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryDeleteById"></a>
# **catalogCategoryDeleteById**
> Object catalogCategoryDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | Model id

apiInstance.catalogCategoryDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryExistsGetCatalogCategoriesidExists"></a>
# **catalogCategoryExistsGetCatalogCategoriesidExists**
> InlineResponse2003 catalogCategoryExistsGetCatalogCategoriesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | Model id

apiInstance.catalogCategoryExistsGetCatalogCategoriesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryExistsHeadCatalogCategoriesid"></a>
# **catalogCategoryExistsHeadCatalogCategoriesid**
> InlineResponse2003 catalogCategoryExistsHeadCatalogCategoriesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | Model id

apiInstance.catalogCategoryExistsHeadCatalogCategoriesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFind"></a>
# **catalogCategoryFind**
> [CatalogCategory] catalogCategoryFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.catalogCategoryFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[CatalogCategory]**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFindById"></a>
# **catalogCategoryFindById**
> CatalogCategory catalogCategoryFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.catalogCategoryFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFindOne"></a>
# **catalogCategoryFindOne**
> CatalogCategory catalogCategoryFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.catalogCategoryFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeCountCatalogProducts"></a>
# **catalogCategoryPrototypeCountCatalogProducts**
> InlineResponse2001 catalogCategoryPrototypeCountCatalogProducts(id, opts)

Counts catalogProducts of CatalogCategory.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.catalogCategoryPrototypeCountCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeCreateCatalogProducts"></a>
# **catalogCategoryPrototypeCreateCatalogProducts**
> CatalogProduct catalogCategoryPrototypeCreateCatalogProducts(id, opts)

Creates a new instance in catalogProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | 
};
apiInstance.catalogCategoryPrototypeCreateCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeDeleteCatalogProducts"></a>
# **catalogCategoryPrototypeDeleteCatalogProducts**
> catalogCategoryPrototypeDeleteCatalogProducts(id)

Deletes all catalogProducts of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

apiInstance.catalogCategoryPrototypeDeleteCatalogProducts(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeDestroyByIdCatalogProducts"></a>
# **catalogCategoryPrototypeDestroyByIdCatalogProducts**
> catalogCategoryPrototypeDestroyByIdCatalogProducts(fk, id)

Delete a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

apiInstance.catalogCategoryPrototypeDestroyByIdCatalogProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeExistsCatalogProducts"></a>
# **catalogCategoryPrototypeExistsCatalogProducts**
> &#39;Boolean&#39; catalogCategoryPrototypeExistsCatalogProducts(fk, id)

Check the existence of catalogProducts relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

apiInstance.catalogCategoryPrototypeExistsCatalogProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeFindByIdCatalogProducts"></a>
# **catalogCategoryPrototypeFindByIdCatalogProducts**
> CatalogProduct catalogCategoryPrototypeFindByIdCatalogProducts(fk, id)

Find a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

apiInstance.catalogCategoryPrototypeFindByIdCatalogProducts(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeGetCatalogProducts"></a>
# **catalogCategoryPrototypeGetCatalogProducts**
> [CatalogProduct] catalogCategoryPrototypeGetCatalogProducts(id, opts)

Queries catalogProducts of CatalogCategory.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.catalogCategoryPrototypeGetCatalogProducts(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[CatalogProduct]**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeLinkCatalogProducts"></a>
# **catalogCategoryPrototypeLinkCatalogProducts**
> CatalogCategoryProduct catalogCategoryPrototypeLinkCatalogProducts(fk, id, opts)

Add a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'data': new harpoonApi.CatalogCategoryProduct() // CatalogCategoryProduct | 
};
apiInstance.catalogCategoryPrototypeLinkCatalogProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 
 **data** | [**CatalogCategoryProduct**](CatalogCategoryProduct.md)|  | [optional] 

### Return type

[**CatalogCategoryProduct**](CatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUnlinkCatalogProducts"></a>
# **catalogCategoryPrototypeUnlinkCatalogProducts**
> catalogCategoryPrototypeUnlinkCatalogProducts(fk, id)

Remove the catalogProducts relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

apiInstance.catalogCategoryPrototypeUnlinkCatalogProducts(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid"></a>
# **catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**
> CatalogCategory catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | An object of model property name/value pairs
};
apiInstance.catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid"></a>
# **catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**
> CatalogCategory catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | An object of model property name/value pairs
};
apiInstance.catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id | 
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateByIdCatalogProducts"></a>
# **catalogCategoryPrototypeUpdateByIdCatalogProducts**
> CatalogProduct catalogCategoryPrototypeUpdateByIdCatalogProducts(fk, id, opts)

Update a related item by id for catalogProducts.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var fk = "fk_example"; // String | Foreign key for catalogProducts

var id = "id_example"; // String | CatalogCategory id

var opts = { 
  'data': new harpoonApi.CatalogProduct() // CatalogProduct | 
};
apiInstance.catalogCategoryPrototypeUpdateByIdCatalogProducts(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts | 
 **id** | **String**| CatalogCategory id | 
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional] 

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryReplaceById"></a>
# **catalogCategoryReplaceById**
> CatalogCategory catalogCategoryReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | Model instance data
};
apiInstance.catalogCategoryReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryReplaceOrCreate"></a>
# **catalogCategoryReplaceOrCreate**
> CatalogCategory catalogCategoryReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | Model instance data
};
apiInstance.catalogCategoryReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpdateAll"></a>
# **catalogCategoryUpdateAll**
> InlineResponse2002 catalogCategoryUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | An object of model property name/value pairs
};
apiInstance.catalogCategoryUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertPatchCatalogCategories"></a>
# **catalogCategoryUpsertPatchCatalogCategories**
> CatalogCategory catalogCategoryUpsertPatchCatalogCategories(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | Model instance data
};
apiInstance.catalogCategoryUpsertPatchCatalogCategories(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertPutCatalogCategories"></a>
# **catalogCategoryUpsertPutCatalogCategories**
> CatalogCategory catalogCategoryUpsertPutCatalogCategories(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | Model instance data
};
apiInstance.catalogCategoryUpsertPutCatalogCategories(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertWithWhere"></a>
# **catalogCategoryUpsertWithWhere**
> CatalogCategory catalogCategoryUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CatalogCategoryApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CatalogCategory() // CatalogCategory | An object of model property name/value pairs
};
apiInstance.catalogCategoryUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


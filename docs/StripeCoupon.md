# harpoonApi.StripeCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_object** | **String** |  | [optional] 
**amountOff** | **Number** |  | [optional] 
**created** | **Number** |  | [optional] 
**currency** | **String** |  | [optional] 
**durationInMonths** | **Number** |  | [optional] 
**livemode** | **Boolean** |  | [optional] 
**maxRedemptions** | **Number** |  | [optional] 
**metadata** | **String** |  | [optional] 
**percentOff** | **Number** |  | [optional] 
**redeemBy** | **Number** |  | [optional] 
**timesRedeemed** | **Number** |  | [optional] 
**valid** | **Boolean** |  | [optional] 
**duration** | **String** |  | 
**id** | **Number** |  | [optional] 



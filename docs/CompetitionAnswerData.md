# harpoonApi.CompetitionAnswerData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**competitionAnswer** | **String** | Competition Answer | 



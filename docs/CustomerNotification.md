# harpoonApi.CustomerNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**coverUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  | [optional] 
**from** | [**NotificationFrom**](NotificationFrom.md) |  | [optional] 
**related** | [**NotificationRelated**](NotificationRelated.md) |  | [optional] 
**link** | **String** |  | [optional] 
**actionCode** | **String** |  | [optional] [default to &#39;post&#39;]
**status** | **String** |  | [optional] [default to &#39;unread&#39;]
**sentAt** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.BranchConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branchLiveKey** | **String** |  | [optional] 
**branchTestKey** | **String** |  | [optional] 
**branchLiveUrl** | **String** |  | [optional] 
**branchTestUrl** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



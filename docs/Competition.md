# harpoonApi.Competition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basePrice** | **Number** |  | [optional] [default to 0.0]
**attendees** | [**[CompetitionAttendee]**](CompetitionAttendee.md) |  | [optional] 
**attendeeCount** | **Number** |  | [optional] [default to 0.0]
**hasJoined** | **Boolean** |  | [optional] [default to false]
**chanceCount** | **Number** |  | [optional] [default to 0.0]
**isWinner** | **Boolean** |  | [optional] [default to false]
**facebook** | [**FacebookEvent**](FacebookEvent.md) |  | [optional] 
**chances** | [**[CompetitionChance]**](CompetitionChance.md) |  | [optional] 
**earnMoreChancesURL** | **String** |  | [optional] 
**type** | [**Category**](Category.md) |  | [optional] 
**earnMoreChances** | **Boolean** |  | [optional] 
**competitionQuestion** | **String** |  | [optional] [default to &#39;&#39;]
**competitionAnswer** | **String** |  | [optional] [default to &#39;&#39;]
**name** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**campaignType** | [**Category**](Category.md) |  | [optional] 
**category** | [**Category**](Category.md) |  | [optional] 
**topic** | [**Category**](Category.md) |  | [optional] 
**alias** | **String** |  | [optional] 
**from** | **Date** |  | [optional] 
**to** | **Date** |  | [optional] 
**baseCurrency** | **String** |  | [optional] [default to &#39;EUR&#39;]
**priceText** | **String** |  | [optional] 
**bannerText** | **String** |  | [optional] 
**checkoutLink** | **String** |  | [optional] 
**nearestVenue** | [**Venue**](Venue.md) |  | [optional] 
**actionText** | **String** |  | [optional] [default to &#39;Sold Out&#39;]
**status** | **String** |  | [optional] [default to &#39;soldOut&#39;]
**collectionNotes** | **String** |  | [optional] 
**termsConditions** | **String** |  | [optional] 
**locationLink** | **String** |  | [optional] 
**altLink** | **String** |  | [optional] 
**redemptionType** | **String** |  | [optional] 
**brand** | [**Brand**](Brand.md) |  | [optional] 
**closestPurchase** | [**OfferClosestPurchase**](OfferClosestPurchase.md) |  | [optional] 
**isFeatured** | **Boolean** |  | [optional] [default to false]
**qtyPerOrder** | **Number** |  | [optional] [default to 1.0]
**shareLink** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



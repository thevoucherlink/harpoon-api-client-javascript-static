# harpoonApi.AwEventbookingEventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventCount**](AwEventbookingEventApi.md#awEventbookingEventCount) | **GET** /AwEventbookingEvents/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventCreate**](AwEventbookingEventApi.md#awEventbookingEventCreate) | **POST** /AwEventbookingEvents | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream) | **GET** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream) | **POST** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventDeleteById**](AwEventbookingEventApi.md#awEventbookingEventDeleteById) | **DELETE** /AwEventbookingEvents/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventExistsGetAwEventbookingEventsidExists**](AwEventbookingEventApi.md#awEventbookingEventExistsGetAwEventbookingEventsidExists) | **GET** /AwEventbookingEvents/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventExistsHeadAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventExistsHeadAwEventbookingEventsid) | **HEAD** /AwEventbookingEvents/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventFind**](AwEventbookingEventApi.md#awEventbookingEventFind) | **GET** /AwEventbookingEvents | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventFindById**](AwEventbookingEventApi.md#awEventbookingEventFindById) | **GET** /AwEventbookingEvents/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventFindOne**](AwEventbookingEventApi.md#awEventbookingEventFindOne) | **GET** /AwEventbookingEvents/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventPrototypeCountAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/count | Counts attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/count | Counts purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountTickets) | **GET** /AwEventbookingEvents/{id}/tickets/count | Counts tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCreateAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateAttributes) | **POST** /AwEventbookingEvents/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventPrototypeCreatePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreatePurchasedTickets) | **POST** /AwEventbookingEvents/{id}/purchasedTickets | Creates a new instance in purchasedTickets of this model.
[**awEventbookingEventPrototypeCreateTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateTickets) | **POST** /AwEventbookingEvents/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventPrototypeDeleteAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventPrototypeDeletePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeletePurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets | Deletes all purchasedTickets of this model.
[**awEventbookingEventPrototypeDeleteTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventPrototypeDestroyByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventPrototypeDestroyByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Delete a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeDestroyByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventPrototypeExistsPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeExistsPurchasedTickets) | **HEAD** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Check the existence of purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeFindByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventPrototypeFindByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Find a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeFindByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdTickets) | **GET** /AwEventbookingEvents/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventPrototypeGetAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetAttributes) | **GET** /AwEventbookingEvents/{id}/attributes | Queries attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets | Queries purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetTickets) | **GET** /AwEventbookingEvents/{id}/tickets | Queries tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeLinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeLinkPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Add a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUnlinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUnlinkPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Remove the purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid) | **PATCH** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid) | **PUT** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEvents/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventPrototypeUpdateByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Update a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUpdateByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEvents/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventReplaceById**](AwEventbookingEventApi.md#awEventbookingEventReplaceById) | **POST** /AwEventbookingEvents/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventReplaceOrCreate**](AwEventbookingEventApi.md#awEventbookingEventReplaceOrCreate) | **POST** /AwEventbookingEvents/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpdateAll**](AwEventbookingEventApi.md#awEventbookingEventUpdateAll) | **POST** /AwEventbookingEvents/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventUpsertPatchAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPatchAwEventbookingEvents) | **PATCH** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertPutAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPutAwEventbookingEvents) | **PUT** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertWithWhere**](AwEventbookingEventApi.md#awEventbookingEventUpsertWithWhere) | **POST** /AwEventbookingEvents/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingEventCount"></a>
# **awEventbookingEventCount**
> InlineResponse2001 awEventbookingEventCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreate"></a>
# **awEventbookingEventCreate**
> AwEventbookingEvent awEventbookingEventCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | Model instance data
};
apiInstance.awEventbookingEventCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream"></a>
# **awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**
> File awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream"></a>
# **awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**
> File awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventDeleteById"></a>
# **awEventbookingEventDeleteById**
> Object awEventbookingEventDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventExistsGetAwEventbookingEventsidExists"></a>
# **awEventbookingEventExistsGetAwEventbookingEventsidExists**
> InlineResponse2003 awEventbookingEventExistsGetAwEventbookingEventsidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventExistsGetAwEventbookingEventsidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventExistsHeadAwEventbookingEventsid"></a>
# **awEventbookingEventExistsHeadAwEventbookingEventsid**
> InlineResponse2003 awEventbookingEventExistsHeadAwEventbookingEventsid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | Model id

apiInstance.awEventbookingEventExistsHeadAwEventbookingEventsid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFind"></a>
# **awEventbookingEventFind**
> [AwEventbookingEvent] awEventbookingEventFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingEventFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[AwEventbookingEvent]**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFindById"></a>
# **awEventbookingEventFindById**
> AwEventbookingEvent awEventbookingEventFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.awEventbookingEventFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFindOne"></a>
# **awEventbookingEventFindOne**
> AwEventbookingEvent awEventbookingEventFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.awEventbookingEventFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountAttributes"></a>
# **awEventbookingEventPrototypeCountAttributes**
> InlineResponse2001 awEventbookingEventPrototypeCountAttributes(id, opts)

Counts attributes of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventPrototypeCountAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountPurchasedTickets"></a>
# **awEventbookingEventPrototypeCountPurchasedTickets**
> InlineResponse2001 awEventbookingEventPrototypeCountPurchasedTickets(id, opts)

Counts purchasedTickets of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventPrototypeCountPurchasedTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountTickets"></a>
# **awEventbookingEventPrototypeCountTickets**
> InlineResponse2001 awEventbookingEventPrototypeCountTickets(id, opts)

Counts tickets of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.awEventbookingEventPrototypeCountTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreateAttributes"></a>
# **awEventbookingEventPrototypeCreateAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeCreateAttributes(id, opts)

Creates a new instance in attributes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventAttribute() // AwEventbookingEventAttribute | 
};
apiInstance.awEventbookingEventPrototypeCreateAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)|  | [optional] 

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreatePurchasedTickets"></a>
# **awEventbookingEventPrototypeCreatePurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeCreatePurchasedTickets(id, opts)

Creates a new instance in purchasedTickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventPrototypeCreatePurchasedTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreateTickets"></a>
# **awEventbookingEventPrototypeCreateTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeCreateTickets(id, opts)

Creates a new instance in tickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | 
};
apiInstance.awEventbookingEventPrototypeCreateTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeleteAttributes"></a>
# **awEventbookingEventPrototypeDeleteAttributes**
> awEventbookingEventPrototypeDeleteAttributes(id)

Deletes all attributes of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDeleteAttributes(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeletePurchasedTickets"></a>
# **awEventbookingEventPrototypeDeletePurchasedTickets**
> awEventbookingEventPrototypeDeletePurchasedTickets(id)

Deletes all purchasedTickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDeletePurchasedTickets(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeleteTickets"></a>
# **awEventbookingEventPrototypeDeleteTickets**
> awEventbookingEventPrototypeDeleteTickets(id)

Deletes all tickets of this model.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDeleteTickets(id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdAttributes"></a>
# **awEventbookingEventPrototypeDestroyByIdAttributes**
> awEventbookingEventPrototypeDestroyByIdAttributes(fk, id)

Delete a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDestroyByIdAttributes(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeDestroyByIdPurchasedTickets**
> awEventbookingEventPrototypeDestroyByIdPurchasedTickets(fk, id)

Delete a related item by id for purchasedTickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDestroyByIdPurchasedTickets(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdTickets"></a>
# **awEventbookingEventPrototypeDestroyByIdTickets**
> awEventbookingEventPrototypeDestroyByIdTickets(fk, id)

Delete a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeDestroyByIdTickets(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeExistsPurchasedTickets"></a>
# **awEventbookingEventPrototypeExistsPurchasedTickets**
> &#39;Boolean&#39; awEventbookingEventPrototypeExistsPurchasedTickets(fk, id)

Check the existence of purchasedTickets relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeExistsPurchasedTickets(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

**&#39;Boolean&#39;**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdAttributes"></a>
# **awEventbookingEventPrototypeFindByIdAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeFindByIdAttributes(fk, id)

Find a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeFindByIdAttributes(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeFindByIdPurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeFindByIdPurchasedTickets(fk, id)

Find a related item by id for purchasedTickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeFindByIdPurchasedTickets(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdTickets"></a>
# **awEventbookingEventPrototypeFindByIdTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeFindByIdTickets(fk, id)

Find a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeFindByIdTickets(fk, id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetAttributes"></a>
# **awEventbookingEventPrototypeGetAttributes**
> [AwEventbookingEventAttribute] awEventbookingEventPrototypeGetAttributes(id, opts)

Queries attributes of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventPrototypeGetAttributes(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingEventAttribute]**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetPurchasedTickets"></a>
# **awEventbookingEventPrototypeGetPurchasedTickets**
> [AwEventbookingTicket] awEventbookingEventPrototypeGetPurchasedTickets(id, opts)

Queries purchasedTickets of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventPrototypeGetPurchasedTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingTicket]**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetTickets"></a>
# **awEventbookingEventPrototypeGetTickets**
> [AwEventbookingEventTicket] awEventbookingEventPrototypeGetTickets(id, opts)

Queries tickets of AwEventbookingEvent.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'filter': "filter_example" // String | 
};
apiInstance.awEventbookingEventPrototypeGetTickets(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **filter** | **String**|  | [optional] 

### Return type

[**[AwEventbookingEventTicket]**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeLinkPurchasedTickets"></a>
# **awEventbookingEventPrototypeLinkPurchasedTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeLinkPurchasedTickets(fk, id, opts)

Add a related item by id for purchasedTickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | 
};
apiInstance.awEventbookingEventPrototypeLinkPurchasedTickets(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUnlinkPurchasedTickets"></a>
# **awEventbookingEventPrototypeUnlinkPurchasedTickets**
> awEventbookingEventPrototypeUnlinkPurchasedTickets(fk, id)

Remove the purchasedTickets relation to an item by id.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

apiInstance.awEventbookingEventPrototypeUnlinkPurchasedTickets(fk, id).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid"></a>
# **awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**
> AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | An object of model property name/value pairs
};
apiInstance.awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid"></a>
# **awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**
> AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | An object of model property name/value pairs
};
apiInstance.awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdAttributes"></a>
# **awEventbookingEventPrototypeUpdateByIdAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeUpdateByIdAttributes(fk, id, opts)

Update a related item by id for attributes.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for attributes

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventAttribute() // AwEventbookingEventAttribute | 
};
apiInstance.awEventbookingEventPrototypeUpdateByIdAttributes(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes | 
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)|  | [optional] 

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeUpdateByIdPurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeUpdateByIdPurchasedTickets(fk, id, opts)

Update a related item by id for purchasedTickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for purchasedTickets

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingTicket() // AwEventbookingTicket | 
};
apiInstance.awEventbookingEventPrototypeUpdateByIdPurchasedTickets(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets | 
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional] 

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdTickets"></a>
# **awEventbookingEventPrototypeUpdateByIdTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeUpdateByIdTickets(fk, id, opts)

Update a related item by id for tickets.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var fk = "fk_example"; // String | Foreign key for tickets

var id = "id_example"; // String | AwEventbookingEvent id

var opts = { 
  'data': new harpoonApi.AwEventbookingEventTicket() // AwEventbookingEventTicket | 
};
apiInstance.awEventbookingEventPrototypeUpdateByIdTickets(fk, id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets | 
 **id** | **String**| AwEventbookingEvent id | 
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional] 

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventReplaceById"></a>
# **awEventbookingEventReplaceById**
> AwEventbookingEvent awEventbookingEventReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | Model instance data
};
apiInstance.awEventbookingEventReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventReplaceOrCreate"></a>
# **awEventbookingEventReplaceOrCreate**
> AwEventbookingEvent awEventbookingEventReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | Model instance data
};
apiInstance.awEventbookingEventReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpdateAll"></a>
# **awEventbookingEventUpdateAll**
> InlineResponse2002 awEventbookingEventUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | An object of model property name/value pairs
};
apiInstance.awEventbookingEventUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertPatchAwEventbookingEvents"></a>
# **awEventbookingEventUpsertPatchAwEventbookingEvents**
> AwEventbookingEvent awEventbookingEventUpsertPatchAwEventbookingEvents(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | Model instance data
};
apiInstance.awEventbookingEventUpsertPatchAwEventbookingEvents(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertPutAwEventbookingEvents"></a>
# **awEventbookingEventUpsertPutAwEventbookingEvents**
> AwEventbookingEvent awEventbookingEventUpsertPutAwEventbookingEvents(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | Model instance data
};
apiInstance.awEventbookingEventUpsertPutAwEventbookingEvents(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertWithWhere"></a>
# **awEventbookingEventUpsertWithWhere**
> AwEventbookingEvent awEventbookingEventUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.AwEventbookingEventApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.AwEventbookingEvent() // AwEventbookingEvent | An object of model property name/value pairs
};
apiInstance.awEventbookingEventUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional] 

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


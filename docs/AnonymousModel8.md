# harpoonApi.AnonymousModel8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandId** | **Number** |  | [optional] 
**competitionId** | **Number** |  | [optional] 
**couponId** | **Number** |  | [optional] 
**customerId** | **Number** |  | [optional] 
**eventId** | **Number** |  | [optional] 
**dealPaid** | **Number** |  | [optional] 
**dealGroup** | **Number** |  | [optional] 



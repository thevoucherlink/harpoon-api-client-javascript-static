# harpoonApi.UrbanAirshipConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**developmentAppKey** | **String** |  | 
**developmentAppSecret** | **String** |  | [optional] 
**developmentLogLevel** | **String** |  | [optional] [default to &#39;debug&#39;]
**productionAppKey** | **String** |  | [optional] 
**productionAppSecret** | **String** |  | [optional] 
**productionLogLevel** | **String** |  | [optional] [default to &#39;none&#39;]
**inProduction** | **Boolean** |  | [optional] [default to false]


<a name="DevelopmentLogLevelEnum"></a>
## Enum: DevelopmentLogLevelEnum


* `undefined` (value: `"undefined"`)

* `none` (value: `"none"`)

* `error` (value: `"error"`)

* `warn` (value: `"warn"`)

* `info` (value: `"info"`)

* `debug` (value: `"debug"`)

* `trace` (value: `"trace"`)




<a name="ProductionLogLevelEnum"></a>
## Enum: ProductionLogLevelEnum


* `undefined` (value: `"undefined"`)

* `none` (value: `"none"`)

* `error` (value: `"error"`)

* `warn` (value: `"warn"`)

* `info` (value: `"info"`)

* `debug` (value: `"debug"`)

* `trace` (value: `"trace"`)





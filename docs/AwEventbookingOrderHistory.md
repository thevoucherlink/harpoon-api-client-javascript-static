# harpoonApi.AwEventbookingOrderHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**eventTicketId** | **Number** |  | 
**orderItemId** | **Number** |  | 



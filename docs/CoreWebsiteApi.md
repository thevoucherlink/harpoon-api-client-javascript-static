# harpoonApi.CoreWebsiteApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coreWebsiteCount**](CoreWebsiteApi.md#coreWebsiteCount) | **GET** /CoreWebsites/count | Count instances of the model matched by where from the data source.
[**coreWebsiteCreate**](CoreWebsiteApi.md#coreWebsiteCreate) | **POST** /CoreWebsites | Create a new instance of the model and persist it into the data source.
[**coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream) | **GET** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream) | **POST** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteDeleteById**](CoreWebsiteApi.md#coreWebsiteDeleteById) | **DELETE** /CoreWebsites/{id} | Delete a model instance by {{id}} from the data source.
[**coreWebsiteExistsGetCoreWebsitesidExists**](CoreWebsiteApi.md#coreWebsiteExistsGetCoreWebsitesidExists) | **GET** /CoreWebsites/{id}/exists | Check whether a model instance exists in the data source.
[**coreWebsiteExistsHeadCoreWebsitesid**](CoreWebsiteApi.md#coreWebsiteExistsHeadCoreWebsitesid) | **HEAD** /CoreWebsites/{id} | Check whether a model instance exists in the data source.
[**coreWebsiteFind**](CoreWebsiteApi.md#coreWebsiteFind) | **GET** /CoreWebsites | Find all instances of the model matched by filter from the data source.
[**coreWebsiteFindById**](CoreWebsiteApi.md#coreWebsiteFindById) | **GET** /CoreWebsites/{id} | Find a model instance by {{id}} from the data source.
[**coreWebsiteFindOne**](CoreWebsiteApi.md#coreWebsiteFindOne) | **GET** /CoreWebsites/findOne | Find first instance of the model matched by filter from the data source.
[**coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid) | **PATCH** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid) | **PUT** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceById**](CoreWebsiteApi.md#coreWebsiteReplaceById) | **POST** /CoreWebsites/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceOrCreate**](CoreWebsiteApi.md#coreWebsiteReplaceOrCreate) | **POST** /CoreWebsites/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**coreWebsiteUpdateAll**](CoreWebsiteApi.md#coreWebsiteUpdateAll) | **POST** /CoreWebsites/update | Update instances of the model matched by {{where}} from the data source.
[**coreWebsiteUpsertPatchCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPatchCoreWebsites) | **PATCH** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertPutCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPutCoreWebsites) | **PUT** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertWithWhere**](CoreWebsiteApi.md#coreWebsiteUpsertWithWhere) | **POST** /CoreWebsites/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="coreWebsiteCount"></a>
# **coreWebsiteCount**
> InlineResponse2001 coreWebsiteCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.coreWebsiteCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreate"></a>
# **coreWebsiteCreate**
> CoreWebsite coreWebsiteCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | Model instance data
};
apiInstance.coreWebsiteCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream"></a>
# **coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**
> File coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream"></a>
# **coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**
> File coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteDeleteById"></a>
# **coreWebsiteDeleteById**
> Object coreWebsiteDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | Model id

apiInstance.coreWebsiteDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteExistsGetCoreWebsitesidExists"></a>
# **coreWebsiteExistsGetCoreWebsitesidExists**
> InlineResponse2003 coreWebsiteExistsGetCoreWebsitesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | Model id

apiInstance.coreWebsiteExistsGetCoreWebsitesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteExistsHeadCoreWebsitesid"></a>
# **coreWebsiteExistsHeadCoreWebsitesid**
> InlineResponse2003 coreWebsiteExistsHeadCoreWebsitesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | Model id

apiInstance.coreWebsiteExistsHeadCoreWebsitesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFind"></a>
# **coreWebsiteFind**
> [CoreWebsite] coreWebsiteFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.coreWebsiteFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[CoreWebsite]**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFindById"></a>
# **coreWebsiteFindById**
> CoreWebsite coreWebsiteFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.coreWebsiteFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFindOne"></a>
# **coreWebsiteFindOne**
> CoreWebsite coreWebsiteFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.coreWebsiteFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid"></a>
# **coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**
> CoreWebsite coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | CoreWebsite id

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | An object of model property name/value pairs
};
apiInstance.coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CoreWebsite id | 
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid"></a>
# **coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**
> CoreWebsite coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | CoreWebsite id

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | An object of model property name/value pairs
};
apiInstance.coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CoreWebsite id | 
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteReplaceById"></a>
# **coreWebsiteReplaceById**
> CoreWebsite coreWebsiteReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | Model instance data
};
apiInstance.coreWebsiteReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteReplaceOrCreate"></a>
# **coreWebsiteReplaceOrCreate**
> CoreWebsite coreWebsiteReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | Model instance data
};
apiInstance.coreWebsiteReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpdateAll"></a>
# **coreWebsiteUpdateAll**
> InlineResponse2002 coreWebsiteUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | An object of model property name/value pairs
};
apiInstance.coreWebsiteUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertPatchCoreWebsites"></a>
# **coreWebsiteUpsertPatchCoreWebsites**
> CoreWebsite coreWebsiteUpsertPatchCoreWebsites(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | Model instance data
};
apiInstance.coreWebsiteUpsertPatchCoreWebsites(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertPutCoreWebsites"></a>
# **coreWebsiteUpsertPutCoreWebsites**
> CoreWebsite coreWebsiteUpsertPutCoreWebsites(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | Model instance data
};
apiInstance.coreWebsiteUpsertPutCoreWebsites(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertWithWhere"></a>
# **coreWebsiteUpsertWithWhere**
> CoreWebsite coreWebsiteUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.CoreWebsiteApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.CoreWebsite() // CoreWebsite | An object of model property name/value pairs
};
apiInstance.coreWebsiteUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional] 

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


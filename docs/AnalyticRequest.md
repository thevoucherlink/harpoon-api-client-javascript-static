# harpoonApi.AnalyticRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests** | **[Object]** |  | 
**scope** | **String** |  | [optional] [default to &#39;inapp&#39;]
**id** | **Number** |  | [optional] 


<a name="ScopeEnum"></a>
## Enum: ScopeEnum


* `inapp` (value: `"inapp"`)

* `beacon` (value: `"beacon"`)





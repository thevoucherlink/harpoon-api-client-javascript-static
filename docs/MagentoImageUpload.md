# harpoonApi.MagentoImageUpload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** |  | 
**contentType** | **String** |  | 



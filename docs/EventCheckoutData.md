# harpoonApi.EventCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**[EventCheckoutTicketData]**](EventCheckoutTicketData.md) | Tickets to checkout | 



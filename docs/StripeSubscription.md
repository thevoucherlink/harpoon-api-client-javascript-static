# harpoonApi.StripeSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationFeePercent** | **Number** |  | [optional] 
**cancelAtPeriodEnd** | **Boolean** |  | [optional] 
**canceledAt** | **Number** |  | [optional] 
**currentPeriodEnd** | **Number** |  | [optional] 
**currentPeriodStart** | **Number** |  | [optional] 
**metadata** | **String** |  | [optional] 
**quantity** | **Number** |  | [optional] 
**start** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**taxPercent** | **Number** |  | [optional] 
**trialEnd** | **Number** |  | [optional] 
**trialStart** | **Number** |  | [optional] 
**customer** | **String** |  | 
**discount** | [**StripeDiscount**](StripeDiscount.md) |  | [optional] 
**plan** | [**StripePlan**](StripePlan.md) |  | 
**_object** | **String** |  | [optional] 
**endedAt** | **Number** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.CoreWebsite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**code** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**sortOrder** | **Number** |  | 
**defaultGroupId** | **Number** |  | 
**isDefault** | **Number** |  | [optional] 



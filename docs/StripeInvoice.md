# harpoonApi.StripeInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_object** | **String** |  | [optional] 
**amountDue** | **Number** |  | [optional] 
**applicationFee** | **Number** |  | [optional] 
**attemptCount** | **Number** |  | [optional] 
**attempted** | **Boolean** |  | [optional] 
**charge** | **String** |  | [optional] 
**closed** | **Boolean** |  | [optional] 
**currency** | **String** |  | [optional] 
**_date** | **Number** |  | [optional] 
**description** | **String** |  | [optional] 
**endingBalance** | **Number** |  | [optional] 
**forgiven** | **Boolean** |  | [optional] 
**livemode** | **Boolean** |  | [optional] 
**metadata** | **String** |  | [optional] 
**nextPaymentAttempt** | **Number** |  | [optional] 
**paid** | **Boolean** |  | [optional] 
**periodEnd** | **Number** |  | [optional] 
**periodStart** | **Number** |  | [optional] 
**receiptNumber** | **String** |  | [optional] 
**subscriptionProrationDate** | **Number** |  | [optional] 
**subtotal** | **Number** |  | [optional] 
**tax** | **Number** |  | [optional] 
**taxPercent** | **Number** |  | [optional] 
**total** | **Number** |  | [optional] 
**webhooksDeliveredAt** | **Number** |  | [optional] 
**customer** | **String** |  | 
**discount** | [**StripeDiscount**](StripeDiscount.md) |  | [optional] 
**statmentDescriptor** | **String** |  | [optional] 
**subscription** | [**StripeSubscription**](StripeSubscription.md) |  | [optional] 
**lines** | [**[StripeInvoiceItem]**](StripeInvoiceItem.md) |  | [optional] 
**id** | **Number** |  | [optional] 



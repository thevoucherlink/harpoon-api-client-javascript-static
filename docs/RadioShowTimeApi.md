# harpoonApi.RadioShowTimeApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowTimeCount**](RadioShowTimeApi.md#radioShowTimeCount) | **GET** /RadioShowTimes/count | Count instances of the model matched by where from the data source.
[**radioShowTimeCreate**](RadioShowTimeApi.md#radioShowTimeCreate) | **POST** /RadioShowTimes | Create a new instance of the model and persist it into the data source.
[**radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream) | **GET** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream) | **POST** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeDeleteById**](RadioShowTimeApi.md#radioShowTimeDeleteById) | **DELETE** /RadioShowTimes/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowTimeExistsGetRadioShowTimesidExists**](RadioShowTimeApi.md#radioShowTimeExistsGetRadioShowTimesidExists) | **GET** /RadioShowTimes/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowTimeExistsHeadRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimeExistsHeadRadioShowTimesid) | **HEAD** /RadioShowTimes/{id} | Check whether a model instance exists in the data source.
[**radioShowTimeFind**](RadioShowTimeApi.md#radioShowTimeFind) | **GET** /RadioShowTimes | Find all instances of the model matched by filter from the data source.
[**radioShowTimeFindById**](RadioShowTimeApi.md#radioShowTimeFindById) | **GET** /RadioShowTimes/{id} | Find a model instance by {{id}} from the data source.
[**radioShowTimeFindOne**](RadioShowTimeApi.md#radioShowTimeFindOne) | **GET** /RadioShowTimes/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowTimePrototypeGetRadioShow**](RadioShowTimeApi.md#radioShowTimePrototypeGetRadioShow) | **GET** /RadioShowTimes/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid) | **PATCH** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid) | **PUT** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceById**](RadioShowTimeApi.md#radioShowTimeReplaceById) | **POST** /RadioShowTimes/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceOrCreate**](RadioShowTimeApi.md#radioShowTimeReplaceOrCreate) | **POST** /RadioShowTimes/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowTimeUpdateAll**](RadioShowTimeApi.md#radioShowTimeUpdateAll) | **POST** /RadioShowTimes/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowTimeUpsertPatchRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPatchRadioShowTimes) | **PATCH** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertPutRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPutRadioShowTimes) | **PUT** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertWithWhere**](RadioShowTimeApi.md#radioShowTimeUpsertWithWhere) | **POST** /RadioShowTimes/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioShowTimeCount"></a>
# **radioShowTimeCount**
> InlineResponse2001 radioShowTimeCount(opts)

Count instances of the model matched by where from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'where': "where_example" // String | Criteria to match model instances
};
apiInstance.radioShowTimeCount(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreate"></a>
# **radioShowTimeCreate**
> RadioShowTime radioShowTimeCreate(opts)

Create a new instance of the model and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | Model instance data
};
apiInstance.radioShowTimeCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream"></a>
# **radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**
> File radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream"></a>
# **radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**
> File radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream(opts)

Create a change stream.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'options': "options_example" // String | 
};
apiInstance.radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional] 

### Return type

**File**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeDeleteById"></a>
# **radioShowTimeDeleteById**
> Object radioShowTimeDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowTimeDeleteById(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeExistsGetRadioShowTimesidExists"></a>
# **radioShowTimeExistsGetRadioShowTimesidExists**
> InlineResponse2003 radioShowTimeExistsGetRadioShowTimesidExists(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowTimeExistsGetRadioShowTimesidExists(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeExistsHeadRadioShowTimesid"></a>
# **radioShowTimeExistsHeadRadioShowTimesid**
> InlineResponse2003 radioShowTimeExistsHeadRadioShowTimesid(id)

Check whether a model instance exists in the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | Model id

apiInstance.radioShowTimeExistsHeadRadioShowTimesid(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFind"></a>
# **radioShowTimeFind**
> [RadioShowTime] radioShowTimeFind(opts)

Find all instances of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioShowTimeFind(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**[RadioShowTime]**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFindById"></a>
# **radioShowTimeFindById**
> RadioShowTime radioShowTimeFindById(id, opts)

Find a model instance by {{id}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | Model id

var opts = { 
  'filter': "filter_example" // String | Filter defining fields and include
};
apiInstance.radioShowTimeFindById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **filter** | **String**| Filter defining fields and include | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFindOne"></a>
# **radioShowTimeFindOne**
> RadioShowTime radioShowTimeFindOne(opts)

Find first instance of the model matched by filter from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'filter': "filter_example" // String | Filter defining fields, where, include, order, offset, and limit
};
apiInstance.radioShowTimeFindOne(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeGetRadioShow"></a>
# **radioShowTimePrototypeGetRadioShow**
> RadioShow radioShowTimePrototypeGetRadioShow(id, opts)

Fetches belongsTo relation radioShow.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | RadioShowTime id

var opts = { 
  'refresh': true // Boolean | 
};
apiInstance.radioShowTimePrototypeGetRadioShow(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id | 
 **refresh** | **Boolean**|  | [optional] 

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid"></a>
# **radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**
> RadioShowTime radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | RadioShowTime id

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | An object of model property name/value pairs
};
apiInstance.radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id | 
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid"></a>
# **radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**
> RadioShowTime radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid(id, opts)

Patch attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | RadioShowTime id

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | An object of model property name/value pairs
};
apiInstance.radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id | 
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeReplaceById"></a>
# **radioShowTimeReplaceById**
> RadioShowTime radioShowTimeReplaceById(id, opts)

Replace attributes for a model instance and persist it into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var id = "id_example"; // String | Model id

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | Model instance data
};
apiInstance.radioShowTimeReplaceById(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id | 
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeReplaceOrCreate"></a>
# **radioShowTimeReplaceOrCreate**
> RadioShowTime radioShowTimeReplaceOrCreate(opts)

Replace an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | Model instance data
};
apiInstance.radioShowTimeReplaceOrCreate(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpdateAll"></a>
# **radioShowTimeUpdateAll**
> InlineResponse2002 radioShowTimeUpdateAll(opts)

Update instances of the model matched by {{where}} from the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | An object of model property name/value pairs
};
apiInstance.radioShowTimeUpdateAll(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertPatchRadioShowTimes"></a>
# **radioShowTimeUpsertPatchRadioShowTimes**
> RadioShowTime radioShowTimeUpsertPatchRadioShowTimes(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | Model instance data
};
apiInstance.radioShowTimeUpsertPatchRadioShowTimes(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertPutRadioShowTimes"></a>
# **radioShowTimeUpsertPutRadioShowTimes**
> RadioShowTime radioShowTimeUpsertPutRadioShowTimes(opts)

Patch an existing model instance or insert a new one into the data source.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | Model instance data
};
apiInstance.radioShowTimeUpsertPutRadioShowTimes(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertWithWhere"></a>
# **radioShowTimeUpsertWithWhere**
> RadioShowTime radioShowTimeUpsertWithWhere(opts)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```javascript
var harpoonApi = require('harpoon-api');
var defaultClient = harpoonApi.ApiClient.default;

// Configure API key authorization: access_token
var access_token = defaultClient.authentications['access_token'];
access_token.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//access_token.apiKeyPrefix = 'Token';

var apiInstance = new harpoonApi.RadioShowTimeApi();

var opts = { 
  'where': "where_example", // String | Criteria to match model instances
  'data': new harpoonApi.RadioShowTime() // RadioShowTime | An object of model property name/value pairs
};
apiInstance.radioShowTimeUpsertWithWhere(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional] 
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional] 

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript


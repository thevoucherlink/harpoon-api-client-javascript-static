# harpoonApi.UdropshipVendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**vendorName** | **String** |  | 
**vendorAttn** | **String** |  | 
**email** | **String** |  | 
**street** | **String** |  | 
**city** | **String** |  | 
**zip** | **String** |  | [optional] 
**countryId** | **String** |  | 
**regionId** | **Number** |  | [optional] 
**region** | **String** |  | [optional] 
**telephone** | **String** |  | [optional] 
**fax** | **String** |  | [optional] 
**status** | **Boolean** |  | 
**password** | **String** |  | [optional] 
**passwordHash** | **String** |  | [optional] 
**passwordEnc** | **String** |  | [optional] 
**carrierCode** | **String** |  | [optional] 
**notifyNewOrder** | **Number** |  | 
**labelType** | **String** |  | 
**testMode** | **Number** |  | 
**handlingFee** | **String** |  | 
**upsShipperNumber** | **String** |  | [optional] 
**customDataCombined** | **String** |  | [optional] 
**customVarsCombined** | **String** |  | [optional] 
**emailTemplate** | **Number** |  | [optional] 
**urlKey** | **String** |  | [optional] 
**randomHash** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**notifyLowstock** | **Number** |  | [optional] 
**notifyLowstockQty** | **String** |  | [optional] 
**useHandlingFee** | **Number** |  | [optional] 
**useRatesFallback** | **Number** |  | [optional] 
**allowShippingExtraCharge** | **Number** |  | [optional] 
**defaultShippingExtraChargeSuffix** | **String** |  | [optional] 
**defaultShippingExtraChargeType** | **String** |  | [optional] 
**defaultShippingExtraCharge** | **String** |  | [optional] 
**isExtraChargeShippingDefault** | **Number** |  | [optional] 
**defaultShippingId** | **Number** |  | [optional] 
**billingUseShipping** | **Number** |  | 
**billingEmail** | **String** |  | 
**billingTelephone** | **String** |  | [optional] 
**billingFax** | **String** |  | [optional] 
**billingVendorAttn** | **String** |  | 
**billingStreet** | **String** |  | 
**billingCity** | **String** |  | 
**billingZip** | **String** |  | [optional] 
**billingCountryId** | **String** |  | 
**billingRegionId** | **Number** |  | [optional] 
**billingRegion** | **String** |  | [optional] 
**subdomainLevel** | **Number** |  | 
**updateStoreBaseUrl** | **Number** |  | 
**confirmation** | **String** |  | [optional] 
**confirmationSent** | **Number** |  | [optional] 
**rejectReason** | **String** |  | [optional] 
**backorderByAvailability** | **Number** |  | [optional] 
**useReservedQty** | **Number** |  | [optional] 
**tiercomRates** | **String** |  | [optional] 
**tiercomFixedRule** | **String** |  | [optional] 
**tiercomFixedRates** | **String** |  | [optional] 
**tiercomFixedCalcType** | **String** |  | [optional] 
**tiershipRates** | **String** |  | [optional] 
**tiershipSimpleRates** | **String** |  | [optional] 
**tiershipUseV2Rates** | **Number** |  | [optional] 
**vacationMode** | **Number** |  | [optional] 
**vacationEnd** | **Date** |  | [optional] 
**vacationMessage** | **String** |  | [optional] 
**udmemberLimitProducts** | **Number** |  | [optional] 
**udmemberProfileId** | **Number** |  | [optional] 
**udmemberProfileRefid** | **String** |  | [optional] 
**udmemberMembershipCode** | **String** |  | [optional] 
**udmemberMembershipTitle** | **String** |  | [optional] 
**udmemberBillingType** | **String** |  | [optional] 
**udmemberHistory** | **String** |  | [optional] 
**udmemberProfileSyncOff** | **Number** |  | [optional] 
**udmemberAllowMicrosite** | **Number** |  | [optional] 
**udprodTemplateSku** | **String** |  | [optional] 
**vendorTaxClass** | **String** |  | [optional] 
**catalogProducts** | **[Object]** |  | [optional] 
**vendorLocations** | **[Object]** |  | [optional] 
**config** | **Object** |  | [optional] 
**partners** | **[Object]** |  | [optional] 
**udropshipVendorProducts** | **[Object]** |  | [optional] 
**harpoonHpublicApplicationpartners** | **[Object]** |  | [optional] 
**harpoonHpublicv12VendorAppCategories** | **[Object]** |  | [optional] 



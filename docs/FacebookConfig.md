# harpoonApi.FacebookConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  | 
**appToken** | **String** |  | 
**scopes** | **String** |  | [optional] [default to &#39;first_name, last_name, email, id, gender, cover{source}&#39;]



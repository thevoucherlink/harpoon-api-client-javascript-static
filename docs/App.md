# harpoonApi.App

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**internalId** | **Number** | Id used internally in Harpoon | [optional] 
**vendorId** | **Number** | Vendor App owner | [optional] 
**name** | **String** | Name of the application | 
**category** | **String** |  | [optional] [default to &#39;Lifestyle&#39;]
**description** | **String** |  | [optional] 
**keywords** | **String** |  | [optional] [default to &#39;buy,claim,offer,events,competitions&#39;]
**copyright** | **String** |  | [optional] [default to &#39;© 2016 The Voucher Link Ltd.&#39;]
**realm** | **String** |  | [optional] 
**bundle** | **String** | Bundle Identifier of the application, com.{company name}.{project name} | [optional] [default to &#39;&#39;]
**urlScheme** | **String** | URL Scheme used for deeplinking | [optional] [default to &#39;&#39;]
**brandFollowOffer** | **Boolean** | Customer need to follow the Brand to display the Offers outside of the Brand profile page | [optional] [default to false]
**privacyUrl** | **String** | URL linking to the Privacy page for the app | [optional] [default to &#39;http://harpoonconnect.com/privacy/&#39;]
**termsOfServiceUrl** | **String** | URL linking to the Terms of Service page for the app | [optional] [default to &#39;http://harpoonconnect.com/terms/&#39;]
**restrictionAge** | **Number** | Restrict the use of the app to just the customer with at least the value specified, 0 &#x3D; no limit | [optional] [default to 0.0]
**storeAndroidUrl** | **String** | URL linking to the Google Play page for the app | [optional] [default to &#39;&#39;]
**storeIosUrl** | **String** | URL linking to the iTunes App Store page for the app | [optional] [default to &#39;&#39;]
**typeNewsFeed** | **String** | News Feed source, nativeFeed/rss | [optional] [default to &#39;nativeFeed&#39;]
**clientSecret** | **String** |  | [optional] 
**grantTypes** | **[String]** |  | [optional] 
**scopes** | **[String]** |  | [optional] 
**status** | **String** | Status of the application, production/sandbox/disabled | [optional] [default to &#39;sandbox&#39;]
**created** | **Date** |  | [optional] 
**modified** | **Date** |  | [optional] 
**styleOfferList** | **String** | Size of the Offer List Item, big/small | [optional] [default to &#39;big&#39;]
**appConfig** | [**AppConfig**](AppConfig.md) |  | [optional] 
**clientToken** | **String** |  | [optional] 
**multiConfigTitle** | **String** |  | [optional] [default to &#39;Choose your App:&#39;]
**multiConfig** | [**[AppConfig]**](AppConfig.md) |  | [optional] 
**appId** | **String** |  | [optional] 
**parentId** | **String** |  | [optional] 
**rssFeeds** | **[Object]** |  | [optional] 
**rssFeedGroups** | **[Object]** |  | [optional] 
**radioStreams** | **[Object]** |  | [optional] 
**customers** | **[Object]** |  | [optional] 
**appConfigs** | **[Object]** |  | [optional] 
**playlists** | **[Object]** |  | [optional] 
**apps** | **[Object]** |  | [optional] 
**parent** | **Object** |  | [optional] 


<a name="TypeNewsFeedEnum"></a>
## Enum: TypeNewsFeedEnum


* `nativeFeed` (value: `"nativeFeed"`)

* `rss` (value: `"rss"`)




<a name="StatusEnum"></a>
## Enum: StatusEnum


* `production` (value: `"production"`)

* `sandbox` (value: `"sandbox"`)

* `disabled` (value: `"disabled"`)




<a name="StyleOfferListEnum"></a>
## Enum: StyleOfferListEnum


* `big` (value: `"big"`)

* `small` (value: `"small"`)





# harpoonApi.TwitterConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  | 
**apiSecret** | **String** |  | 
**id** | **Number** |  | [optional] 



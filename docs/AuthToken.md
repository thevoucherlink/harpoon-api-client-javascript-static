# harpoonApi.AuthToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**accessToken** | **String** |  | 
**expiresIn** | **Number** |  | [optional] 
**scope** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 
**tokenType** | **String** |  | [optional] 
**kid** | **String** |  | [optional] 
**macAlgorithm** | **String** |  | [optional] 
**macKey** | **String** |  | [optional] 



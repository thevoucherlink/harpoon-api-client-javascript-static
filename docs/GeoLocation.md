# harpoonApi.GeoLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **Number** |  | [default to 0.0]
**longitude** | **Number** |  | [default to 0.0]
**id** | **Number** |  | [optional] 



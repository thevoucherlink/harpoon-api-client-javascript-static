# harpoonApi.Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**productCount** | **Number** |  | [optional] [default to 0.0]
**hasChildren** | **Boolean** |  | [optional] [default to false]
**parent** | [**Category**](Category.md) |  | [optional] 
**isPrimary** | **Boolean** |  | [optional] [default to false]
**children** | [**[Category]**](Category.md) |  | [optional] 
**id** | **Number** |  | [optional] 



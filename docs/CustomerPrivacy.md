# harpoonApi.CustomerPrivacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activity** | **String** |  | [default to &#39;public&#39;]
**notificationPush** | **Boolean** |  | [optional] [default to true]
**notificationBeacon** | **Boolean** |  | [optional] [default to true]
**notificationLocation** | **Boolean** |  | [optional] [default to true]



# harpoonApi.FormRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadataKey** | **String** |  | 
**type** | **String** |  | [default to &#39;text&#39;]
**title** | **String** |  | 
**defaultValue** | **String** |  | [optional] 
**placeholder** | **String** |  | [optional] 
**hidden** | **Boolean** |  | [optional] [default to false]
**required** | **Boolean** |  | [optional] [default to true]
**validationRegExp** | **String** |  | [optional] 
**validationMessage** | **String** |  | [optional] 
**selectorOptions** | [**[FormSelectorOption]**](FormSelectorOption.md) |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `text` (value: `"text"`)

* `name` (value: `"name"`)

* `url` (value: `"url"`)

* `email` (value: `"email"`)

* `password` (value: `"password"`)

* `number` (value: `"number"`)

* `phone` (value: `"phone"`)

* `twitter` (value: `"twitter"`)

* `account` (value: `"account"`)

* `integer` (value: `"integer"`)

* `decimal` (value: `"decimal"`)

* `textView` (value: `"textView"`)

* `selectorPush` (value: `"selectorPush"`)

* `selectorPickerView` (value: `"selectorPickerView"`)

* `dateInline` (value: `"dateInline"`)

* `datetimeInline` (value: `"datetimeInline"`)

* `timeInline` (value: `"timeInline"`)

* `countDownTimerInline` (value: `"countDownTimerInline"`)

* `date` (value: `"date"`)

* `datetime` (value: `"datetime"`)

* `time` (value: `"time"`)

* `countDownTimer` (value: `"countDownTimer"`)





# harpoonApi.CatalogProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**attributeSetId** | **Number** |  | 
**typeId** | **String** |  | 
**actionText** | **String** |  | [optional] 
**agreementId** | **Number** |  | [optional] 
**alias** | **String** |  | [optional] 
**altLink** | **String** |  | [optional] 
**campaignId** | **String** |  | [optional] 
**campaignLiveFrom** | **Date** |  | [optional] 
**campaignLiveTo** | **Date** |  | [optional] 
**campaignShared** | **String** |  | [optional] 
**campaignStatus** | **String** |  | [optional] 
**campaignType** | **String** |  | [optional] 
**checkoutLink** | **String** |  | [optional] 
**collectionNotes** | **String** |  | [optional] 
**cost** | **String** |  | [optional] 
**couponCodeText** | **String** |  | [optional] 
**couponCodeType** | **String** |  | [optional] 
**couponType** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**creatorId** | **Number** |  | [optional] 
**description** | **String** |  | [optional] 
**externalId** | **String** |  | [optional] 
**facebookAttendingCount** | **String** |  | [optional] 
**facebookCategory** | **String** |  | [optional] 
**facebookId** | **String** |  | [optional] 
**facebookMaybeCount** | **String** |  | [optional] 
**facebookNode** | **String** |  | [optional] 
**facebookNoreplyCount** | **String** |  | [optional] 
**facebookPlace** | **String** |  | [optional] 
**facebookUpdatedTime** | **Date** |  | [optional] 
**giftMessageAvailable** | **Number** |  | [optional] 
**hasOptions** | **Number** |  | 
**image** | **String** |  | [optional] 
**imageLabel** | **String** |  | [optional] 
**isRecurring** | **Number** |  | [optional] 
**linksExist** | **Number** |  | [optional] 
**linksPurchasedSeparately** | **Number** |  | [optional] 
**linksTitle** | **String** |  | [optional] 
**locationLink** | **String** |  | [optional] 
**msrp** | **String** |  | [optional] 
**msrpDisplayActualPriceType** | **String** |  | [optional] 
**msrpEnabled** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**newsFromDate** | **Date** |  | [optional] 
**newsToDate** | **Date** |  | [optional] 
**notificationBadge** | **String** |  | [optional] 
**notificationInvisible** | **Number** |  | [optional] 
**notificationSilent** | **Number** |  | [optional] 
**partnerId** | **String** |  | [optional] 
**passedValidation** | **Number** |  | [optional] 
**price** | **String** |  | [optional] 
**priceText** | **String** |  | [optional] 
**priceType** | **Number** |  | [optional] 
**priceView** | **Number** |  | [optional] 
**pushwooshIds** | **String** |  | [optional] 
**pushwooshTokens** | **String** |  | [optional] 
**recurringProfile** | **String** |  | [optional] 
**redemptionType** | **String** |  | [optional] 
**requiredOptions** | **Number** |  | 
**reviewHtml** | **String** |  | [optional] 
**shipmentType** | **Number** |  | [optional] 
**shortDescription** | **String** |  | [optional] 
**sku** | **String** |  | [optional] 
**skuType** | **Number** |  | [optional] 
**smallImage** | **String** |  | [optional] 
**smallImageLabel** | **String** |  | [optional] 
**specialFromDate** | **Date** |  | [optional] 
**specialPrice** | **String** |  | [optional] 
**specialToDate** | **Date** |  | [optional] 
**taxClassId** | **Number** |  | [optional] 
**thumbnail** | **String** |  | [optional] 
**thumbnailLabel** | **String** |  | [optional] 
**udropshipCalculateRates** | **Number** |  | [optional] 
**udropshipVendor** | **Number** |  | [optional] 
**udropshipVendorValue** | **String** |  | [optional] 
**udtiershipRates** | **String** |  | [optional] 
**udtiershipUseCustom** | **Number** |  | [optional] 
**unlockTime** | **Number** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**urlKey** | **String** |  | [optional] 
**urlPath** | **String** |  | [optional] 
**validationReport** | **String** |  | [optional] 
**venueList** | **String** |  | [optional] 
**visibility** | **Number** |  | [optional] 
**visibleFrom** | **Date** |  | [optional] 
**visibleTo** | **Date** |  | [optional] 
**weight** | **String** |  | [optional] 
**weightType** | **Number** |  | [optional] 
**termsConditions** | **String** |  | [optional] 
**competitionQuestion** | **String** |  | [optional] 
**competitionAnswer** | **String** |  | [optional] 
**competitionWinnerType** | **String** |  | [optional] 
**competitionMultijoinStatus** | **Boolean** |  | [optional] 
**competitionMultijoinReset** | **Date** |  | [optional] 
**competitionMultijoinCooldown** | **String** |  | [optional] 
**competitionWinnerEmail** | **Boolean** |  | [optional] 
**competitionQuestionValidate** | **Boolean** |  | [optional] 
**connectFacebookId** | **String** |  | [optional] 
**udropshipVendors** | **[Object]** |  | [optional] 
**productEventCompetition** | **Object** |  | [optional] 
**inventory** | **Object** |  | [optional] 
**catalogCategories** | **[Object]** |  | [optional] 
**checkoutAgreement** | **Object** |  | [optional] 
**awCollpurDeal** | **Object** |  | [optional] 
**creator** | **Object** |  | [optional] 
**udropshipVendorProducts** | **[Object]** |  | [optional] 



# harpoonApi.TritonConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stationId** | **String** |  | 
**stationName** | **String** |  | [optional] 
**mp3Mount** | **String** |  | [optional] 
**aacMount** | **String** |  | [optional] 



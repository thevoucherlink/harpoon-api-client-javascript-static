# harpoonApi.RadioStream

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**description** | **String** |  | [optional] 
**urlHQ** | **String** |  | 
**urlLQ** | **String** |  | 
**starts** | **Date** | When the Stream starts of being public | [optional] 
**ends** | **Date** | When the Stream ceases of being public | [optional] 
**metaDataUrl** | **String** |  | [optional] 
**imgStream** | **String** |  | [optional] 
**sponsorTrack** | **String** | Url of the sponsor MP3 to be played | [optional] 
**tritonConfig** | [**TritonConfig**](TritonConfig.md) |  | [optional] 
**id** | **Number** |  | [optional] 
**appId** | **String** |  | [optional] 
**radioShows** | **[Object]** |  | [optional] 



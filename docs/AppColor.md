# harpoonApi.AppColor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appPrimary** | **String** |  | [default to &#39;#027fd3&#39;]
**appSecondary** | **String** |  | [optional] [default to &#39;#26ff00&#39;]
**textPrimary** | **String** |  | [optional] [default to &#39;#049975&#39;]
**textSecondary** | **String** |  | [optional] [default to &#39;#4e5eea&#39;]
**radioNavBar** | **String** |  | [optional] [default to &#39;#87878c&#39;]
**radioPlayer** | **String** |  | [optional] [default to &#39;#af2768&#39;]
**radioProgressBar** | **String** |  | [optional] [default to &#39;#ff00c9&#39;]
**menuBackground** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioTextSecondary** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioTextPrimary** | **String** |  | [optional] [default to &#39;#000000&#39;]
**menuItem** | **String** |  | [optional] [default to &#39;#000000&#39;]
**appGradientPrimary** | **String** |  | [optional] [default to &#39;#000000&#39;]
**feedSegmentIndicator** | **String** |  | [optional] [default to &#39;#ff0bc6&#39;]
**radioPlayButton** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioPauseButton** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioPlayButtonBackground** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioPauseButtonBackground** | **String** |  | [optional] [default to &#39;#000000&#39;]
**radioPlayerItem** | **String** |  | [optional] [default to &#39;#000000&#39;]



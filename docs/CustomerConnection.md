# harpoonApi.CustomerConnection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**facebook** | [**CustomerConnectionFacebook**](CustomerConnectionFacebook.md) |  | [optional] 
**twitter** | [**CustomerConnectionTwitter**](CustomerConnectionTwitter.md) |  | [optional] 
**id** | **Number** |  | [optional] 



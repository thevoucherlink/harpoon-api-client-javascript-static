# harpoonApi.GoogleAnalyticsTracking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mobileTrackingId** | **String** |  | 
**webTrackingId** | **String** |  | [optional] 



# harpoonApi.CheckoutAgreement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**contentHeight** | **String** |  | [optional] 
**checkboxText** | **String** |  | [optional] 
**isActive** | **Number** |  | 
**isHtml** | **Number** |  | 
**id** | **Number** |  | 
**content** | **String** |  | [optional] 



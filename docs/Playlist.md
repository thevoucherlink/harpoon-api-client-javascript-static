# harpoonApi.Playlist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Playlist name | [optional] 
**shortDescription** | **String** | Playlist short description | [optional] 
**image** | **String** | Playlist image | [optional] 
**isMain** | **Boolean** | If Playlist is included in Main | [optional] [default to false]
**id** | **Number** |  | [optional] 
**appId** | **String** |  | [optional] 
**playlistItems** | **[Object]** |  | [optional] 



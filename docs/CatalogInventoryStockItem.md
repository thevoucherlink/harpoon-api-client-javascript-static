# harpoonApi.CatalogInventoryStockItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**backorders** | **Number** |  | 
**enableQtyIncrements** | **Number** |  | 
**isDecimalDivided** | **Number** |  | 
**isQtyDecimal** | **Number** |  | 
**isInStock** | **Number** |  | 
**id** | **Number** |  | 
**lowStockDate** | **Date** |  | [optional] 
**manageStock** | **Number** |  | 
**minQty** | **String** |  | 
**maxSaleQty** | **String** |  | 
**minSaleQty** | **String** |  | 
**notifyStockQty** | **String** |  | [optional] 
**productId** | **Number** |  | 
**qty** | **String** |  | 
**stockId** | **Number** |  | 
**useConfigBackorders** | **Number** |  | 
**qtyIncrements** | **String** |  | 
**stockStatusChangedAuto** | **Number** |  | 
**useConfigEnableQtyInc** | **Number** |  | 
**useConfigMaxSaleQty** | **Number** |  | 
**useConfigManageStock** | **Number** |  | 
**useConfigMinQty** | **Number** |  | 
**useConfigNotifyStockQty** | **Number** |  | 
**useConfigMinSaleQty** | **Number** |  | 
**useConfigQtyIncrements** | **Number** |  | 



# harpoonApi.BrandFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**Brand**](Brand.md) |  | [optional] 
**message** | **String** |  | [optional] 
**cover** | **String** |  | [optional] 
**related** | [**AnonymousModel8**](AnonymousModel8.md) |  | [optional] 
**link** | **String** |  | [optional] 
**actionCode** | **String** |  | [optional] 
**privacyCode** | **String** |  | [optional] 
**postedAt** | **Date** |  | [optional] 
**id** | **Number** |  | [optional] 



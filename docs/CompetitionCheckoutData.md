# harpoonApi.CompetitionCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chances** | [**[CompetitionCheckoutChanceData]**](CompetitionCheckoutChanceData.md) | Chances to checkout | 



# harpoonApi.RadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**description** | **String** |  | [optional] 
**content** | **String** |  | [optional] 
**contentType** | **String** |  | [optional] [default to &#39;image&#39;]
**contact** | [**Contact**](Contact.md) | Contacts for this show | [optional] 
**starts** | **Date** | When the Show starts of being public | [optional] 
**ends** | **Date** | When the Show ceases of being public | [optional] 
**type** | **String** |  | [optional] 
**radioShowTimeCurrent** | [**RadioShowTime**](RadioShowTime.md) |  | [optional] 
**imgShow** | **String** |  | [optional] 
**sponsorTrack** | **String** | Url of the sponsor MP3 to be played | [optional] 
**id** | **Number** |  | [optional] 
**radioStreamId** | **Number** |  | [optional] 
**playlistItemId** | **Number** |  | [optional] 
**radioPresenters** | **[Object]** |  | [optional] 
**radioStream** | **Object** |  | [optional] 
**radioShowTimes** | **[Object]** |  | [optional] 
**playlistItem** | **Object** |  | [optional] 


<a name="ContentTypeEnum"></a>
## Enum: ContentTypeEnum


* `image` (value: `"image"`)

* `video` (value: `"video"`)





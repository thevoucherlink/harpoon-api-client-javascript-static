# harpoonApi.AwEventbookingEventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**eventId** | **Number** |  | 
**price** | **String** |  | [optional] 
**priceType** | **String** |  | [optional] 
**sku** | **String** |  | [optional] 
**qty** | **Number** |  | [optional] 
**codeprefix** | **String** |  | [optional] 
**fee** | **String** |  | 
**isPassOnFee** | **Number** |  | 
**chance** | **Number** |  | 
**attributes** | **[Object]** |  | [optional] 
**tickets** | **[Object]** |  | [optional] 
**awEventbookingTicket** | **[Object]** |  | [optional] 



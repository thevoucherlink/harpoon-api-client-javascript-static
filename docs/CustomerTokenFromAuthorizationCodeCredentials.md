# harpoonApi.CustomerTokenFromAuthorizationCodeCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | Code can be found in a logged in Customer under the key authorizationCode | 



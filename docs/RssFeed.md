# harpoonApi.RssFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | 
**subCategory** | **String** |  | [optional] 
**url** | **String** |  | 
**name** | **String** |  | [optional] 
**sortOrder** | **Number** |  | [optional] [default to 0.0]
**styleCss** | **String** |  | [optional] 
**noAds** | **Boolean** |  | [optional] [default to false]
**adMRectVisible** | **Boolean** |  | [optional] [default to false]
**adMRectPosition** | **Number** |  | [optional] [default to 0.0]
**id** | **Number** |  | [optional] 
**appId** | **String** |  | [optional] 
**rssFeedGroupId** | **Number** |  | [optional] 
**rssFeedGroup** | **Object** |  | [optional] 



# harpoonApi.StripeInvoiceItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_object** | **String** |  | [optional] 
**id** | **Number** |  | [optional] 



# harpoonApi.UdropshipVendorProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | 
**vendorId** | **Number** |  | 
**productId** | **Number** |  | 
**priority** | **Number** |  | 
**carrierCode** | **String** |  | [optional] 
**vendorSku** | **String** |  | [optional] 
**vendorCost** | **String** |  | [optional] 
**stockQty** | **String** |  | [optional] 
**backorders** | **Number** |  | 
**shippingPrice** | **String** |  | [optional] 
**status** | **Number** |  | 
**reservedQty** | **String** |  | [optional] 
**availState** | **String** |  | [optional] 
**availDate** | **Date** |  | [optional] 
**relationshipStatus** | **String** |  | [optional] 
**udropshipVendor** | **Object** |  | [optional] 
**catalogProduct** | **Object** |  | [optional] 



# harpoonApi.DealGroupPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**code** | [**CouponPurchaseCode**](CouponPurchaseCode.md) |  | [optional] 
**isAvailable** | **Boolean** |  | [optional] [default to false]
**createdAt** | **Date** |  | [optional] 
**expiredAt** | **Date** |  | [optional] 
**redeemedAt** | **Date** |  | [optional] 
**redeemedByTerminal** | **String** |  | [optional] 
**redemptionType** | **String** |  | [optional] 
**status** | **String** |  | [optional] [default to &#39;unknown&#39;]
**unlockTime** | **Number** |  | [optional] [default to 15.0]
**id** | **Number** |  | [optional] 


